/*
 * test.java
 *
 * Created on November 26, 2003, 12:45 PM
 */

package za.co.ucs.lwt.deploymentbuilder;
import java.io.*;

/**
 *
 * @author  liam
 */
public class test {
    
    /** Creates a new instance of test */
    public test() {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Testing Archives
        /*
         //1. Creating an archive and backup of original system
         Archiver archiver = new Archiver(new File("c:\\downloads3.arc"));
         Archiver backup = new Archiver(new File("c:\\downloads3.bak"));
         
         archiver.prepareArchive();
         archiver.addToArchive(new QAFile(new File("C:\\inst\\Scripts\\4.0.0\\Fix010\\fn_P_MATH_CalcSimple2.txt"), InstallGroup.SCRIPT));
         archiver.buildArchive();
         
         archiver.installArchive(new File("c:\\Program Files\\Accsys\\Archives3"), backup);
         
         //2. Creating an archive of a backup, thus simply restoring
        Archiver archiver = new Archiver(new File("C:\\TEMP\\Demo Installation"));
        archiver.installArchive(new File("c:\\Program Files\\Accsys\\Archives4"), null);
         */
        
        // Testing a Test Installation
        FileContainer fileContainer = FileContainer.getInstance(FileContainer.getContainerFile());
        fileContainer.getCRsContaining("Data Capture");
    }
}
