# Create a key store:
"C:\Program Files\Java\jre7\bin\keytool" -genkey -keystore "C:\Program Files\Apache Software Foundation\Tomcat 6.0\webapps\DeploymentBuilder\jnlp\liam.keys" -alias liam

Enter Keystore Password: gigabyte

"C:\Program Files\Java\jre7\bin\keytool" -selfcert -keystore "C:\Program Files\Apache Software Foundation\Tomcat 6.0\webapps\DeploymentBuilder\jnlp\liam.keys" -alias liam



# Now, you need to make sure that all jar files in your application that need privileges are signed. 
# Here is how you sign a single file:
"C:\Program Files\Java\jdk1.6.0_13\bin\jarsigner" -keystore "C:\Program Files\Apache Software Foundation\Tomcat 6.0\webapps\DeploymentBuilder\jnlp\liam.keys" -storepass gigabyte "C:\Program Files\Apache Software Foundation\Tomcat 6.0\webapps\DeploymentBuilder\jnlp\lwt.jar" liam

"C:\Program Files\Java\jdk1.6.0_13\bin\jarsigner" -keystore "C:\Program Files\Apache Software Foundation\Tomcat 6.0\webapps\DeploymentBuilder\jnlp\liam.keys" -storepass gigabyte "C:\Program Files\Apache Software Foundation\Tomcat 6.0\webapps\DeploymentBuilder\jnlp\sybase.jar" liam
