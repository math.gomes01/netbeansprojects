<%@page contentType="text/html"%>

<%@include file="html5_top.jsp"%>

<%@include file="html_messages.jsp"%> 
<!-- This HAS to be included if messages were generated
and added to the session attributes for this person.
Calling this script will simply display those messages -->

<!-- Now we need to verify that there is a LoginSessionObject -->

<%  // Can I connect to the database?
    if (loginSessionObject == null) {
        response.sendRedirect(response.encodeURL("Login.jsp"));
    } else {
        PageSectionGenerator gen = PageSectionGenerator.getInstance();
        String className = request.getParameter("webProcessClassName"); // the HashValue of the Reporting Structure in use
        String procAction = request.getParameter("processAction");

        if (className != null) {
            WebProcessDefinition webProcessDefinition = za.co.ucs.accsys.webmodule.FileContainer.getInstance().getWebProcessDefinition(className);
            if (procAction == null) {
                procAction = "";
            }
            if (procAction.length() == 0) {
                out.write(gen.buildHTMLPageHeader("All Processes", loginSessionObject.getUser().toString()));
            }
            if (procAction.compareTo(za.co.ucs.accsys.webmodule.WebProcessStage.sa_ACCEPT) == 0) {
                out.write(gen.buildHTMLPageHeader("Processes where the last stage was accepted", loginSessionObject.getUser().toString()));
            }
            if (procAction.compareTo(za.co.ucs.accsys.webmodule.WebProcessStage.sa_REJECT) == 0) {
                out.write(gen.buildHTMLPageHeader("Processes where the last stage was rejected", loginSessionObject.getUser().toString()));
            }
            if (procAction.compareTo(za.co.ucs.accsys.webmodule.WebProcessStage.sa_TIMEOUT) == 0) {
                out.write(gen.buildHTMLPageHeader("Processes where the last stage timed out", loginSessionObject.getUser().toString()));
            }
            if (procAction.compareTo(za.co.ucs.accsys.webmodule.WebProcessStage.sa_CANCEL) == 0) {
                out.write(gen.buildHTMLPageHeader("Processes where the last stage was cancelled", loginSessionObject.getUser().toString()));
            }


            // Page Detail
            out.write("<div id=\"container\"><center>");
            out.write("<div class=\"item\">");
            PageProducer_Processes_TopLevel producer = new PageProducer_Processes_TopLevel(webProcessDefinition, procAction, false);
            out.write(producer.generateInformationContent());
            out.write("</div>");
            out.write("</center></div>"); 
        }
    }
%>

<%@include file="html_bottom.jsp"%>