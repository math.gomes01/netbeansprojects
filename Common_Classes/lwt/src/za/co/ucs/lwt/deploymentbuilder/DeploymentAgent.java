/*
 * DeploymentAgent.java
 *
 * Created on December 5, 2003, 1:05 PM
 */

package za.co.ucs.lwt.deploymentbuilder;
import javax.swing.*;
import java.awt.*;
import za.co.ucs.lwt.db.*;
import java.io.*;
import java.sql.Connection;

/**
 *
 * @author  liam
 */
public final class DeploymentAgent extends javax.swing.JFrame {
    
    /** Creates new form DeploymentAgent */
    public DeploymentAgent() {
        
        // No use running the Deployment Agent if the database is not running
        if (!canConnectToDB()){
            JOptionPane.showMessageDialog(null, "Database is not running (localhost:2638).");
            System.exit(0);
        } else {
            
            initComponents();
            initCustomComponents();
            // Resize Frame
            this.setSize(350,250);
            
            // Configure Actions
            initActionListeners();
            setApplicationMode(NONE);
            // Check to see if there is currently a CR installed
            if (getCurrentInstalledArchive() != null){
                setApplicationMode(UNINSTALLCR);
                setCRArchiveFile(getCurrentInstalledArchive());
            }
        }
    }
    
    // No use running the Deployment Agent if the Database is not running
    private boolean canConnectToDB(){
        
        boolean rslt = false;
        DatabaseObject.setConnectInfo_PeopleWareLocalHost();
        Connection con = DatabaseObject.getNewConnection();
        
        
        rslt = DatabaseObject.canConnect("select count(*) from c_setup", con,true);
        return (DatabaseObject.canConnect("select count(*) from c_setup", DatabaseObject.getNewConnection(),true));
        
    }
    
    private void initCustomComponents() {
        // Test Installation Menu Item
        selectTestInstallation = new javax.swing.JMenuItem();
        fileMenu.add(selectTestInstallation);
        // Build Installation Menu Item
        selectBuildInstallation = new javax.swing.JMenuItem();
        fileMenu.add(selectBuildInstallation);
        // Exit Menu Item
        exitMenuItem = new javax.swing.JMenuItem();
        exitMenuItem.setText("Exit");
        exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitMenuItemActionPerformed(evt);
            }
        });
        aboutMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JOptionPane.showMessageDialog(null, "Deployment Agent\n"+version, "About", JOptionPane.INFORMATION_MESSAGE);
            }
        });
        
        fileMenu.add(exitMenuItem);
        
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panOverview = new javax.swing.JPanel();
        panInstall = new javax.swing.JPanel();
        panInstall_1 = new javax.swing.JPanel();
        panInstall_2 = new javax.swing.JPanel();
        lblCRNumber = new javax.swing.JLabel();
        panInstall_Spacer = new javax.swing.JPanel();
        chkSkipScripts = new javax.swing.JCheckBox();
        btnInstall = new javax.swing.JButton();
        panInstall_3 = new javax.swing.JPanel();
        lblCRDescription = new javax.swing.JTextArea();
        panInstall_4 = new javax.swing.JPanel();
        lblCurrentVersion = new javax.swing.JLabel();
        lblRequiredVersion = new javax.swing.JLabel();
        lblUpdatedVersion = new javax.swing.JLabel();
        panProgress = new javax.swing.JPanel();
        pbProgress = new javax.swing.JProgressBar();
        menuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        helpMenu = new javax.swing.JMenu();
        aboutMenuItem = new javax.swing.JMenuItem();

        setTitle("CR Deployment Agent");
        setBackground(new java.awt.Color(255, 255, 255));
        setBounds(new java.awt.Rectangle(20, 20, 600, 400));
        setMaximizedBounds(new java.awt.Rectangle(0, 0, 600, 600));
        setMinimumSize(new java.awt.Dimension(400, 250));
        setPreferredSize(new java.awt.Dimension(450, 188));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                exitForm(evt);
            }
        });

        panOverview.setLayout(new java.awt.BorderLayout());

        panInstall.setBackground(new java.awt.Color(255, 255, 255));
        panInstall.setBorder(javax.swing.BorderFactory.createTitledBorder("New CR"));
        panInstall.setLayout(new java.awt.GridLayout(1, 0));

        panInstall_1.setBackground(new java.awt.Color(255, 255, 255));
        panInstall_1.setMinimumSize(new java.awt.Dimension(22, 22));
        panInstall_1.setLayout(new java.awt.BorderLayout());

        panInstall_2.setBackground(new java.awt.Color(255, 255, 255));
        panInstall_2.setLayout(new java.awt.GridLayout(1, 0));
        panInstall_2.add(lblCRNumber);

        panInstall_Spacer.setBackground(new java.awt.Color(255, 255, 255));
        panInstall_Spacer.setMinimumSize(new java.awt.Dimension(22, 22));
        panInstall_2.add(panInstall_Spacer);

        chkSkipScripts.setBackground(new java.awt.Color(255, 255, 255));
        chkSkipScripts.setText("Skip Scripts");
        chkSkipScripts.setToolTipText("Do not execute database scripts that are part of this installation.");
        panInstall_2.add(chkSkipScripts);

        btnInstall.setText("Install");
        btnInstall.setEnabled(false);
        btnInstall.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInstallActionPerformed(evt);
            }
        });
        panInstall_2.add(btnInstall);

        panInstall_1.add(panInstall_2, java.awt.BorderLayout.NORTH);

        panInstall_3.setLayout(new java.awt.GridLayout(1, 0, 0, 5));

        lblCRDescription.setEditable(false);
        lblCRDescription.setLineWrap(true);
        lblCRDescription.setWrapStyleWord(true);
        panInstall_3.add(lblCRDescription);

        panInstall_1.add(panInstall_3, java.awt.BorderLayout.CENTER);

        panInstall_4.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        panInstall_4.setLayout(new java.awt.BorderLayout());
        panInstall_4.add(lblCurrentVersion, java.awt.BorderLayout.NORTH);
        panInstall_4.add(lblRequiredVersion, java.awt.BorderLayout.CENTER);
        panInstall_4.add(lblUpdatedVersion, java.awt.BorderLayout.SOUTH);

        panInstall_1.add(panInstall_4, java.awt.BorderLayout.SOUTH);

        panInstall.add(panInstall_1);

        panOverview.add(panInstall, java.awt.BorderLayout.CENTER);

        panProgress.setLayout(new java.awt.BorderLayout());
        panProgress.add(pbProgress, java.awt.BorderLayout.CENTER);

        panOverview.add(panProgress, java.awt.BorderLayout.SOUTH);

        getContentPane().add(panOverview, java.awt.BorderLayout.CENTER);

        menuBar.setBackground(new java.awt.Color(255, 255, 255));
        menuBar.setBorder(null);
        menuBar.setBorderPainted(false);

        fileMenu.setText("File");
        menuBar.add(fileMenu);

        helpMenu.setText("Help");
        helpMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                helpMenuActionPerformed(evt);
            }
        });

        aboutMenuItem.setText("About");
        aboutMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aboutMenuItemActionPerformed(evt);
            }
        });
        helpMenu.add(aboutMenuItem);

        menuBar.add(helpMenu);

        setJMenuBar(menuBar);

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void helpMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_helpMenuActionPerformed
// TODO add your handling code here:
    }//GEN-LAST:event_helpMenuActionPerformed
    
    private void aboutMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aboutMenuItemActionPerformed
        // Add your handling code here:
    }//GEN-LAST:event_aboutMenuItemActionPerformed
    
    private void btnInstallActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInstallActionPerformed
        // Add your handling code here:
    }//GEN-LAST:event_btnInstallActionPerformed
    
    /** Exit the Application */
    private void exitForm(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_exitForm
        System.exit(0);
    }//GEN-LAST:event_exitForm
    
    private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        System.exit(0);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        new DeploymentAgent().setVisible(true);
    }
    
    private void initActionListeners(){
        // MENU:Open Folder
        OpenFolderAction openCRFolderAction = new OpenFolderAction("Select a Change Request Deployment File");
        openCRFolderAction.setEnabled(true);
        selectTestInstallation.setAction(openCRFolderAction);
        
        OpenFolderAction openBuildFolderAction = new OpenFolderAction("Select a Build Deployment File");
        openBuildFolderAction.setEnabled(true);
        selectBuildInstallation.setAction(openBuildFolderAction);
        
        // BUTTON: Install
        installCRAction = new InstallAction("Install CR");
        installCRAction.setEnabled(false);
        installBuildAction = new InstallAction("Install Build");
        installBuildAction.setEnabled(false);
    }
    
    /** Private Inner Class to load the Deployment File
     */
    class OpenFolderAction extends AbstractAction{
        public OpenFolderAction(String name) {
            super(name);
        }
        public void actionPerformed(java.awt.event.ActionEvent e) {
            Frame dialogFrame = new JFrame();
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setApproveButtonText("Select");
            fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            // Defaults to c:\Downloads
            if (new File("c:\\Downloads").exists()){
                fileChooser.setCurrentDirectory(new File("c:\\Downloads"));
            }
            
            // Test Installations
            if (e.getSource() == selectTestInstallation){
                fileChooser.setDialogTitle("Select a Change Request Deployment File");
                fileChooser.setFileFilter(new MyDeploymentFilter(FileContainer.TestInstallationExtension, "Change Request Deployment File"));
                int rslt = fileChooser.showOpenDialog(dialogFrame);
                if (rslt == JFileChooser.APPROVE_OPTION){
                    if (fileChooser.getSelectedFile()!=null){
                        setApplicationMode(INSTALLCR);
                        setCRArchiveFile(fileChooser.getSelectedFile());
                    }
                }
            } else
                // Build Installations
            {
                fileChooser.setDialogTitle("Select a Build Deployment File");
                fileChooser.setFileFilter(new MyDeploymentFilter(FileContainer.BuildInstallationExtension, "Build Deployment File"));
                int rslt = fileChooser.showOpenDialog(dialogFrame);
                if (rslt == JFileChooser.APPROVE_OPTION){
                    if (fileChooser.getSelectedFile()!=null){
                        setApplicationMode(INSTALLBUILD);
                        setBuildArchiveFile(fileChooser.getSelectedFile());
                    }
                }
            }
        }
    }
    
    /** Private Inner Class to install the archive
     */
    class InstallAction extends AbstractAction{
        public InstallAction(String name) {
            super(name);
        }
        public void actionPerformed(java.awt.event.ActionEvent e) {
            if (archiveFile == null){
                System.out.println(" No archive to install!");
                return;
            }
            // Should we execute the scripts?
            if (chkSkipScripts.isSelected()){
                FileContainer.setConfigProperty("SkipScripts", "YES");
            } else {
                FileContainer.setConfigProperty("SkipScripts", "NO");
            }
            
            // Get the installation path
            Frame dialogFrame = new JFrame();
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setApproveButtonText("Select");
            fileChooser.setDialogTitle("Select the Accsys Peopleware Folder");
            fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            // Defaults to the normal Access Peopleware Installation Folder
            String progFileFolder = System.getenv("ProgramFiles(X86)");
            if (progFileFolder == null){
                progFileFolder = System.getenv("ProgramFiles");
            }
            if (progFileFolder == null){
                progFileFolder = "c:\\Program Files\\Accsys\\PeopleWare";
            }
            if (new File(progFileFolder).exists()){
                fileChooser.setCurrentDirectory(new File(progFileFolder));
            }
            
            // Reset the installFolder
            installFolder = null;
            copyInstallFolder = null;
            
            int rslt = fileChooser.showOpenDialog(dialogFrame);
            if (rslt == JFileChooser.APPROVE_OPTION){
                setInstallFolder(fileChooser.getSelectedFile());
                String path = "c:\\QAFolders\\"+lblCRNumber.getText();
                copyInstallFolder = new File(path);
                if (copyInstallFolder.exists()){
                    deleteDirectory(copyInstallFolder);
                }
                copyInstallFolder.mkdir();
            }
            
            
            
            // Test Installations
            if ((applicationMode == INSTALLCR) || (applicationMode == UNINSTALLCR)){
                // Get Installation Folder
                if (installFolder != null){
                    installCRArchive(archiveFile);
                }
            }
            // Build Installations
            if (applicationMode == INSTALLBUILD){
                // Get Installation Folder
                if (installFolder != null){
                    installBuildArchive(archiveFile);
                }
            }
        }
    }
    
    private boolean deleteDirectory(File path) {
        if( path.exists() ) {
            File[] files = path.listFiles();
            for(int i=0; i<files.length; i++) {
                if(files[i].isDirectory()) {
                    deleteDirectory(files[i]);
                } else {
                    files[i].delete();
                }
            }
        }
        return( path.delete() );
    }
    
    public void setCRArchiveFile(File archiveFile){
        if (!archiveFile.exists()){
            System.out.println("** Archive File:"+archiveFile.getAbsoluteFile()+" not found.");
            btnInstall.getAction().setEnabled(false);
            return;
        }
        this.archiveFile = archiveFile;
        TestInstallationArchiver archiver = new TestInstallationArchiver(archiveFile);
        ChangeRequest changeRequest = archiver.getChangeRequest();
        if (changeRequest != null){
            lblCRNumber.setText(changeRequest.getCRNumber());
            lblCRDescription.setText(changeRequest.getReadMe());
            lblCurrentVersion.setText("Current DB Version:"+archiver.getCurrentDBVersion());
            lblRequiredVersion.setText("Required DB Version:"+changeRequest.getRequiredDBVersion());
            lblUpdatedVersion.setText("");
        }
        btnInstall.getAction().setEnabled(archiver.getCurrentDBVersion().compareToIgnoreCase(changeRequest.getRequiredDBVersion())>=0);
    }
    
    public void setBuildArchiveFile(File archiveFile){
        if (!archiveFile.exists()){
            System.out.println("** Archive File:"+archiveFile.getAbsoluteFile()+" not found.");
            btnInstall.getAction().setEnabled(false);
            return;
        }
        this.archiveFile = archiveFile;
        BuildInstallationArchiver archiver = new BuildInstallationArchiver(archiveFile);
        lblCurrentVersion.setText("Current DB Version:"+archiver.getCurrentDBVersion());
        lblRequiredVersion.setText("Required DB Version:"+archiver.getBuildInstallation().getRequiredDBVersion());
        lblUpdatedVersion.setText("Updated DB Version:"+archiver.getBuildInstallation().getNewDBVersion());
        lblCRNumber.setText(archiver.getBuildInstallation().getBuildName());
        lblCRDescription.setText(archiver.getBuildInstallation().getReadMe());
        btnInstall.getAction().setEnabled(archiver.getCurrentDBVersion().compareToIgnoreCase(archiver.getBuildInstallation().getRequiredDBVersion())>=0);
    }
    
    public void setInstallFolder(File installFolder){
        
        if ( (!installFolder.exists()) || (!installFolder.isDirectory())) {
            System.out.println("** Install Folder:"+archiveFile.getAbsoluteFile()+" not found or not a directory.");
            btnInstall.getAction().setEnabled(false);
            installFolder = null;
            return;
        }
        this.installFolder = installFolder;
    }
    
    private void setApplicationMode(int mode){
        applicationMode = mode;
        if (applicationMode == INSTALLCR){
            ((javax.swing.border.TitledBorder)panInstall.getBorder()).setTitle("Install New CR...");
            selectTestInstallation.setEnabled(true);
            btnInstall.setText("Install");
            btnInstall.setAction( installCRAction );
            btnInstall.getAction().setEnabled(true);
            pbProgress.setValue(0);
        }
        if (applicationMode == INSTALLBUILD){
            ((javax.swing.border.TitledBorder)panInstall.getBorder()).setTitle("Install New CR...");
            selectBuildInstallation.setEnabled(true);
            btnInstall.setText("Install");
            btnInstall.setAction( installBuildAction );
            btnInstall.getAction().setEnabled(true);
            pbProgress.setValue(0);
        }
        if (applicationMode == BUSY){
            ((javax.swing.border.TitledBorder)panInstall.getBorder()).setTitle("Installing...");
            this.repaint();
            selectTestInstallation.setEnabled(false);
            btnInstall.setText("");
            btnInstall.getAction().setEnabled(false);
            this.validate();
        }
        if (applicationMode == UNINSTALLCR){
            ((javax.swing.border.TitledBorder)panInstall.getBorder()).setTitle("Unistall existing CR...");
            selectTestInstallation.setEnabled(false);
            selectBuildInstallation.setEnabled(false);
            
            btnInstall.setAction( installCRAction );
            btnInstall.setText("Uninstall");
            btnInstall.getAction().setEnabled(true);
            pbProgress.setValue(0);
        }
        if (applicationMode == NONE){
            ((javax.swing.border.TitledBorder)panInstall.getBorder()).setTitle("Please select a CR to install");
            selectTestInstallation.setEnabled(true);
            selectBuildInstallation.setEnabled(true);
            btnInstall.setText("...");
            if (btnInstall.getAction() != null){
                btnInstall.getAction().setEnabled(false);
            }
            lblCRNumber.setText("");
            lblCRDescription.setText("");
        }
        this.validate();
    }
    
    private static class TestInstallationArchiveLauncher implements Runnable{
        final TestInstallationArchiver archiver;
        final File installFolder;
        final TestInstallationArchiver backup;
        final JProgressBar pbProgress;
        
        TestInstallationArchiveLauncher(TestInstallationArchiver archiver,
                File installFolder,TestInstallationArchiver backup,
                JProgressBar pbProgress){
            this.archiver = archiver;
            this.installFolder = installFolder;
            this.backup = backup;
            this.pbProgress = pbProgress;
        }
        
        // Runnable method of this threadin interface.
        public void run() {
            archiver.installArchive(this.installFolder, backup, pbProgress);
        }
        
    }
    
    private void installCRArchive(File archiveFile){
        if (!archiveFile.exists()){
            System.out.println("** Archive File:"+archiveFile.getAbsoluteFile()+" not found.");
            return;
        }
        
        setCRArchiveFile(archiveFile);
        TestInstallationArchiver archiver = null;
        
        // When installing an archive, we create a backup archive that can be used to 'uninstall' this
        // installation later.
        if (applicationMode == INSTALLCR){
            setApplicationMode(BUSY);
            archiver = new TestInstallationArchiver(archiveFile);
            File backupArchiveFile = new File(getDefaultConfigFolder()+FileContainer.getOSFileSeperator()+defaultRestoreArchive);
            TestInstallationArchiver backup = new TestInstallationArchiver(backupArchiveFile);
            
            // Install the actual CR
            TestInstallationArchiveLauncher launcher = new TestInstallationArchiveLauncher(archiver, installFolder, backup, pbProgress);
            EventQueue.invokeLater(launcher);
            
            waitForInstallation();
            //setApplicationMode(UNINSTALLCR);
            JOptionPane.showMessageDialog(this, "Main Installation Successful.");
            this.repaint();
            
            
            // Now, make a backup into the QA Folder
            // We do not wish to run the scripts again
            String oldVal = FileContainer.getConfigProperty("SkipScripts");
            FileContainer.setConfigProperty("SkipScripts", "YES");
            TestInstallationArchiveLauncher launcherCopy = new TestInstallationArchiveLauncher(archiver, copyInstallFolder, null, pbProgress);
            EventQueue.invokeLater(launcherCopy);
            FileContainer.setConfigProperty("SkipScripts", oldVal);
            
            waitForInstallation();
            setApplicationMode(UNINSTALLCR);
            JOptionPane.showMessageDialog(this, "QA Copy Successful.");
            this.repaint();
            
            return;
        }
        
        if (applicationMode == UNINSTALLCR){
            setApplicationMode(BUSY);
            File restoreFile = getCurrentInstalledArchive();
            archiver = new TestInstallationArchiver(restoreFile);
            archiver.installArchive(this.installFolder, null, pbProgress);
            // Delete the restored Archive File
            (new File(getDefaultConfigFolder()+FileContainer.getOSFileSeperator()+defaultRestoreArchive)).delete();
            waitForInstallation();
            setApplicationMode(NONE);
            JOptionPane.showMessageDialog(this, "Uninstall Successful.");
            this.repaint();
            return;
        }
    }
    
    private void installBuildArchive(File archiveFile){
        if (!archiveFile.exists()){
            System.out.println("** Archive File:"+archiveFile.getAbsoluteFile()+" not found.");
            return;
        }
        
        setBuildArchiveFile(archiveFile);
        BuildInstallationArchiver archiver = null;
        
        if (applicationMode == INSTALLBUILD){
            setApplicationMode(BUSY);
            archiver = new  BuildInstallationArchiver(archiveFile);
            archiver.installArchive(this.installFolder, null, pbProgress);
            waitForInstallation();
            setApplicationMode(NONE);
            JOptionPane.showMessageDialog(this, "Installation Successful.");
            lblCurrentVersion.setText("Current DB Version:"+archiver.getCurrentDBVersion());
            this.repaint();
            return;
        }
    }
    
    private void waitForInstallation(){
        Thread thr = new Thread();
        try{
            while (pbProgress.isIndeterminate()){
                System.out.println("sleeping...");
                this.repaint();
                Thread.sleep(10);
            }
        } catch( InterruptedException e ){
            e.printStackTrace();
        }
    }
    
    private File getDefaultConfigFolder(){
        File defaultFolder = new File(defaultConfigFolder);
        defaultFolder.mkdirs();
        return defaultFolder;
    }
    
    /** Whenever we install a CR, an archive is generated for
     * the files/scripts that are being replaced.  This allows
     * us to UNINSTALL existing installations before installing
     * new ones.
     */
    private File getCurrentInstalledArchive(){
        // Config Folder
        File restoreArchiveFile = new File(getDefaultConfigFolder()+FileContainer.getOSFileSeperator()+defaultRestoreArchive);
        if (restoreArchiveFile.exists()){
            return restoreArchiveFile;
        } else{
            return null;
        }
    }
    
    
    private void clearCurrentInstalledCR(){
        // Archive File
        File thisAFile = new File(getDefaultConfigFolder()+FileContainer.getOSFileSeperator()+defaultRestoreArchive);
        thisAFile.delete();
    }
    
    
    
    private java.io.File archiveFile = null;
    private java.io.File installFolder = null;
    private java.io.File copyInstallFolder = null;
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem aboutMenuItem;
    private javax.swing.JButton btnInstall;
    private javax.swing.JCheckBox chkSkipScripts;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JMenu helpMenu;
    private javax.swing.JTextArea lblCRDescription;
    private javax.swing.JLabel lblCRNumber;
    private javax.swing.JLabel lblCurrentVersion;
    private javax.swing.JLabel lblRequiredVersion;
    private javax.swing.JLabel lblUpdatedVersion;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JPanel panInstall;
    private javax.swing.JPanel panInstall_1;
    private javax.swing.JPanel panInstall_2;
    private javax.swing.JPanel panInstall_3;
    private javax.swing.JPanel panInstall_4;
    private javax.swing.JPanel panInstall_Spacer;
    private javax.swing.JPanel panOverview;
    private javax.swing.JPanel panProgress;
    private javax.swing.JProgressBar pbProgress;
    // End of variables declaration//GEN-END:variables
    
    private javax.swing.JMenuItem selectTestInstallation;
    private javax.swing.JMenuItem selectBuildInstallation;
    private javax.swing.JMenuItem exitMenuItem;
    private int applicationMode=INSTALLCR;
    private static int INSTALLCR=0;
    private static int INSTALLBUILD=1;
    private static int UNINSTALLCR=2;
    private static int BUSY=4;
    private static int NONE=5;
    private InstallAction installCRAction = null;
    private InstallAction installBuildAction = null;
    
    
    private String version="2.0.0";
    private static String defaultConfigFolder = "c:\\Deployment Builder\\";
    private static String defaultRestoreArchive = "restore.arc";
}


/** Internal File Filter class
 */
class MyDeploymentFilter extends javax.swing.filechooser.FileFilter{
    
    public MyDeploymentFilter(String extension, String description){
        this.extension = extension;
        this.description = description;
    }
    
    public boolean accept(File f) {
        if ((f.getName().toUpperCase().endsWith(extension.toUpperCase())==true)
        || (f.isDirectory()))
            return true;
        else
            return false;
    }
    
    public String getDescription(){
        return description;
    }
    
    private String extension;
    private String description;
}