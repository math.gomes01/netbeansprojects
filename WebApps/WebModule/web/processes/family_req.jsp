<%@page contentType="text/html"%>

<%@include file="html5_top.jsp"%>

<%@include file="../html/formatDateScript.txt"%>

<%@include file="html_messages.jsp"%>
<!-- This HAS to be included if messages were generated
and added to the session attributes for this person.
Calling this script will simply display those messages -->

<!--  ******************** 
        Initiates a Family Information Request
      ******************** 
-->

<!-- Now we need to verify that there is a LoginSessionObject -->

<%
            if (za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideRequestChangesFor("Changes to Family Information")) {
                PageSectionGenerator gen = PageSectionGenerator.getInstance();
                out.write(gen.buildHTMLPageHeader("Access Revoked", "Your system was configured not to allow this kind of request to be processed."));
            } else {
                // Can I connect to the database?
                if (!za.co.ucs.lwt.db.DatabaseObject.getInstance().canConnect("select @@version", za.co.ucs.lwt.db.DatabaseObject.getInstance().getNewConnectionFromPool(), true)) {
                    MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                            "Unable to connect to database.  Please contact your network administrator.",
                            false, request.getSession());
                    producer.setMessageInSessionObject();
                    out.write("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");

                } else {

                    PageSectionGenerator gen = PageSectionGenerator.getInstance();
                    if (user.equals(employee)) {
                        out.write(gen.buildHTMLPageHeader("Family Information Request", employee.toString()));
                    } else {
                        out.write(gen.buildHTMLPageHeader("Family Information Request", "On Behalf Of: " + employee.toString()));
                    }

                    // Page Detail
                    PageProducer_FamilyInfoRequest producer = new PageProducer_FamilyInfoRequest(employee);
                    out.write(producer.generateInformationContent());
                }
            }
%>

<%@include file="html_bottom.jsp"%>