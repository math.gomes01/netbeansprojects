package za.co.ucs.ess;

import java.util.LinkedList;
import javax.servlet.http.HttpSession;
import za.co.ucs.accsys.peopleware.Employee;
import za.co.ucs.accsys.peopleware.LeaveHistoryEntry;
import za.co.ucs.accsys.peopleware.LeaveInfo;
import za.co.ucs.accsys.webmodule.WebModulePreferences;
import za.co.ucs.lwt.db.DatabaseObject;

/**
 * <p> PageProducer_FamilyInfoRequest inherits from PageProducer and will
 * display the detail of a LeaveCancellationRequest in HTML All scheduled leave
 * that are future-dated are listed by this page in chronological order. The
 * user will then, by selecting the leave he/she wishes to cancel, trigger the
 * cancellation thereof. </p>
 */
public class PageProducer_LeaveCancellationRequest extends PageProducer {

    /**
     * Constructor
     */
    public PageProducer_LeaveCancellationRequest(Employee employee, HttpSession sessionObject) {
        this.employee = employee;
        this.session = sessionObject;
    }

    /**
     * A LeaveCancellationRequest Page Producer
     */
    @Override
    public String generateInformationContent() {
        try {
            StringBuilder resultPage = new StringBuilder();
            String baseURL = WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Web, "");
            String link = baseURL + "Controller.jsp";
            //resultPage.append("<center><table col=0><tr><td>");

            resultPage.append("<FORM name=\"req_leavecancellation\" METHOD=POST action=\"").append(link).append("\" >");

            // ********************************
            // List leave that can be cancelled
            // ********************************
            LeaveInfo leaveInfo = za.co.ucs.accsys.webmodule.FileContainer.getInstance().getLeaveInfoFromContainer(employee);

            int nbLeaveTypes = leaveInfo.getNumberOfLeaveTypes();
            DatabaseObject object = za.co.ucs.lwt.db.DatabaseObject.getInstance();
            int nbFutureDatedLeaveEntries = 0;


            String[] headers = new String[3];
            headers[0] = "Date";
            headers[1] = "Units Taken";
            headers[2] = "Reason";

            resultPage.append("<div class=\"item\">");
            resultPage.append("<h2 class=\"title\">Future Dated Leave</h2>");
            resultPage.append("<div class=\"outer-grouping\">");
            
            resultPage.append("<table>");

            resultPage.append(PageSectionGenerator.getInstance().getHTMLOuterTableTag());
            {
                // Put the detail inside it's own table
                for (int i = 0; i < leaveInfo.getNumberOfLeaveTypes(); i++) {
                    // Get all the leave entries for dates later than now(*)
                    LinkedList history = leaveInfo.getLeaveHistory(i, new java.util.Date());

                    if (history.size() > 0) {
                        String[][] subsetHistoryDetail = new String[3][history.size()];

                        // Format the contents
                        for (int j = 0; j < history.size(); j++) {
                            // The first column contains a radio button that shows the From and To dates
                            // and passes the HISTORY_ID as the selection value
                            nbFutureDatedLeaveEntries++;
                            LeaveHistoryEntry entry = (LeaveHistoryEntry) history.get(j);

                            String col_0 = "<input type=\"radio\" name=\"historyID\" value=\"" + (entry.getHistoryID()) + "\">"
                                    + DatabaseObject.formatDate(entry.getFromDate()) + " - "
                                    + DatabaseObject.formatDate(entry.getToDate()) + "<br>";

                            subsetHistoryDetail[0][j] = col_0;
                            subsetHistoryDetail[1][j] = String.valueOf(entry.getUnitsTaken()) + " " + entry.getUnit();
                            subsetHistoryDetail[2][j] = entry.getReason();
                        }

                        // Empty line between leave types
                        resultPage.append("<tr><td><h3 class=\"shaddow\" style=\"margin-left: 25px;\">").append(leaveInfo.getProfileName(i)).append("</h3></td></tr>");
                        resultPage.append("<tr><td>").append(PageSectionGenerator.getInstance().buildHTMLTable("displaytable_wide", headers, subsetHistoryDetail)).append("</td></tr>");
                    }
                }
            }
            resultPage.append("</table></div></div>");
            

            // If there aren't any future-dated leave scheduled, skip the 'SUBMIT' section
            if (nbFutureDatedLeaveEntries == 0) {
                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_INFO,
                        "There are no future-dated leave requests which can be cancelled.", false, this.session);
                // Replace the original page for one that simply tells the
                // user that no leave exists that can be cancelled/
                producer.setMessageInSessionObject();
                resultPage.append("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");
            } else {

                // Reason
                {
                    resultPage.append("<div class=\"item\">");
                    resultPage.append("<h2 class=\"title\">Reason for Cancellation</h2>");
                    resultPage.append("<div class=\"grouping\">");
                    resultPage.append("<table>");

//                    resultPage.append("<table  class=\"center_narrow\" >");
                    resultPage.append("<tr><td align=\"left\"><textarea maxlength=500 name=\"cancellationReason\" rows=\"3\" cols=\"50\">");
                    resultPage.append("</textarea></td></tr>");
//                    resultPage.append("</table>");
                }

                // SUBMIT
                {
                    resultPage.append("<!-- Horizontal Ruler -->");
//                    resultPage.append("<hr class=\"center\">");
                    resultPage.append("<tr>");
                    resultPage.append("<td><input type=\"submit\" style=\"margin-right: 0px;\" class=\"gradient-button\" value=\"Submit Leave Cancellation Request\"></td>");
                    resultPage.append("<input type=\"hidden\" name=\"action\" value=\"req_leavecancellation\">");
                    resultPage.append("</tr>");
                    resultPage.append("</table>");
                    resultPage.append("</div></div>");
                }
            }
            resultPage.append("</form>");
            //resultPage.append("</td></tr></table></center>");

            return resultPage.toString();
        } catch (Exception e) {
            return e.getMessage();
        }


    }
    private Employee employee;
    private HttpSession session;
} // end LeavePageProducer

