<%@page contentType="text/html"%>

<%@include file="html5_top.jsp"%>

<%@include file="../html/formatDateScript.txt"%>

<%@include file="html_messages.jsp"%> 
<!-- This HAS to be included if messages were generated
and added to the session attributes for this person.
Calling this script will simply display those messages -->

<!-- Now we need to verify that there is a LoginSessionObject -->

<%  // Can I connect to the database?
    // We need to reset the EMPLOYEE to be the same person as the USER once the process has been committed
    loginSessionObject.setEmployee(user);
    employee = new Employee(user.getCompany(), user.getEmployeeID());
    if (za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideViewInformationFor("Tax Certificates")) {
        PageSectionGenerator gen = PageSectionGenerator.getInstance();
        out.write(gen.buildHTMLPageHeader("Access Revoked", "Your system was configured not to show this page."));
    } else {
        // Can I connect to the database?
        if (!za.co.ucs.lwt.db.DatabaseObject.getInstance().canConnect("select @@version", za.co.ucs.lwt.db.DatabaseObject.getInstance().getNewConnectionFromPool(), true)) {
            MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                    "Unable to connect to database.  Please contact your network administrator.",
                    false, request.getSession());
            producer.setMessageInSessionObject();
            out.write("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");

        } else {

            // Employee
            FileContainer fc = FileContainer.getInstance();
            // Payroll
            Payroll payroll = employee.getPayroll();

            if (payroll == null) {
                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                        "You are not currently linked to a Payroll.",
                        false, request.getSession());
                producer.setMessageInSessionObject();
                out.write("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");

            } else {

                // Submission Period selected through URL
                String submissionPeriod = request.getParameter("submissionPeriod");


                // Submission Periods in database
                java.util.LinkedList submissionPeriods = fc.getTaxCertificateYears(employee);

                // Period Detail
                if (submissionPeriod == null) {
                    // Get the first submission period
                    if (submissionPeriods.size() > 0) {
                        submissionPeriod = (String) submissionPeriods.getFirst();
                    } else {
                        MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                                "Unable to find tax return submissions for " + employee.getPreferredName() + " " + employee.getSurname(),
                                false, request.getSession());
                        producer.setMessageInSessionObject();
                        out.write("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");
                        return;
                    }
                }
                submissionPeriod = submissionPeriod.replace("/", "_");
                //System.out.println("submissionPeriod :" + submissionPeriod);

                // Page Header
                PageSectionGenerator gen = PageSectionGenerator.getInstance();
                out.write(gen.buildHTMLPageHeader("Tax Certificates", employee.toString()));

                // Now we create a tab selection for the different groups
                java.util.TreeMap periodList = new java.util.TreeMap();  // use a TreeMap to sort the dates chronologically
                java.util.Iterator iter = submissionPeriods.iterator();
                while (iter.hasNext()) {
                    String aPeriod = (String) iter.next();
                    periodList.put(aPeriod, aPeriod);
                }


                //out.write("<h3 class=\"shaddow\">Certificate Period: </h3><CENTER class=\"taxCertificatePeriod\">");
                out.write("<CENTER><h3 class=\"shaddow\" style=\"display: inline-block;\">Certificate Period: </h3>");

                StringBuilder selection = new StringBuilder();
                selection.append("<SELECT  style=\"display: inline-block; margin-left: 5px;\" name=\"balanceDate\" ONCHANGE=\"location = this.options[this.selectedIndex].value;\">");

                // Once all the periods are loaded, we can construct the tab buttons
                // We will list a maximum of 6 periods in a line
                java.util.Iterator periodIter = periodList.values().iterator();
                while (periodIter.hasNext()) {
                    String periodDate = (String) periodIter.next();
                    // For fear of messing up the URL string, we'll replace '2010/08' with '2010_08'


                    if (submissionPeriod.compareTo(periodDate.replace("/", "_")) == 0) {
                        selection.append(" <option value=\"taxcertificate_prev.jsp?submissionPeriod=" + periodDate.replace("/", "_") + " \" selected  >" + periodDate + "</option>");
                    } else {
                        selection.append(" <option value=\"taxcertificate_prev.jsp?submissionPeriod=" + periodDate.replace("/", "_") + " \" >" + periodDate + "</option>");
                    }
                }
                selection.append("</select></CENTER>");
                out.write(selection.toString());

                // Page Detail
                if (submissionPeriod == null) {
                    System.out.println("Null submissionPeriod");
                } else {
                    out.write("<CENTER><hr style=\"width: 1210px;\">");
                    String pathAndTemplateFileName = request.getSession().getServletContext().getRealPath("/") + "html/TaxCertificateTemplate.html";
                    //System.out.println(pathAndTemplateFileName);
                    PageProducer_TaxCertificatePreview producer = new PageProducer_TaxCertificatePreview(employee, submissionPeriod, pathAndTemplateFileName);
                    out.write(producer.generateInformationContent());
                    out.write("</CENTER>");
                }

            }
        }
    }

%>

<%@include file="html_bottom.jsp"%>