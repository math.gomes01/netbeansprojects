/*
 * The Request class is used to parse XML requests that will then 
 * be actioned upon by the ESS
 */
package za.co.ucs.accsys.tools;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import za.co.ucs.accsys.peopleware.Employee;
import za.co.ucs.accsys.webmodule.FileContainer;
import za.co.ucs.accsys.webmodule.WebProcess;
import za.co.ucs.accsys.webmodule.WebProcessDefinition;
import za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition;

/**
 *
 * @author fkleynhans
 */
@XmlRootElement
public class Request {

    String TYPE;
    String LEAVE_TYPE_ID;
    String STARTDATE;
    String ENDDATE;
    String HALFDAY;
    String PROCESS_ID;
    String ACTION;
    String REASON;
    String ADDITIONALDETAIL;
    String INITIALSELECTIONID;
    String ATTACHMENTLINK;

    private RequestType requestType = RequestType.UNDEFINED;

    public String getTYPE() {
        return TYPE;
    }

    @XmlElement
    public void setTYPE(String TYPE) {
        this.TYPE = TYPE;
        if (TYPE.compareToIgnoreCase("LEAVEREQUEST") == 0) {
            this.requestType = RequestType.LEAVEREQUEST;
        }
        if (TYPE.compareToIgnoreCase("ACTIONREQUEST") == 0) {
            this.requestType = RequestType.ACTIONREQUEST;
        }

    }

    public RequestType getRequestType() {
        return this.requestType;
    }

    public String getLEAVE_TYPE_ID() {
        return LEAVE_TYPE_ID;
    }

    @XmlElement
    public void setLEAVE_TYPE_ID(String LEAVE_TYPE_ID) {
        this.LEAVE_TYPE_ID = LEAVE_TYPE_ID;
    }

    public String getSTARTDATE() {
        return STARTDATE;
    }

    @XmlElement
    public void setSTARTDATE(String STARTDATE) {
        this.STARTDATE = STARTDATE;
    }

    public String getENDDATE() {
        return ENDDATE;
    }

    @XmlElement
    public void setENDDATE(String ENDDATE) {
        this.ENDDATE = ENDDATE;
    }

    public String getHALFDAY() {
        return HALFDAY;
    }

    @XmlElement
    public void setHALFDAY(String HALFDAY) {
        this.HALFDAY = HALFDAY;
    }

    public String getPROCESS_ID() {
        return PROCESS_ID;
    }

    @XmlElement
    public void setPROCESS_ID(String PROCESS_ID) {
        this.PROCESS_ID = PROCESS_ID;
    }

    public String getACTION() {
        return ACTION;
    }

    @XmlElement
    public void setACTION(String ACTION) {
        this.ACTION = ACTION;
    }

    public String getREASON() {
        return REASON;
    }
    
    @XmlElement
    public void setREASON(String REASON) {
        this.REASON = REASON;
    }
    
    public String getADDITIONALDETAIL() {
        return ADDITIONALDETAIL;
    }
    
    @XmlElement
    public void setADDITIONALDETAIL(String ADDITIONALDETAIL) {
        this.ADDITIONALDETAIL = ADDITIONALDETAIL;
    }

    public String getATTACHMENTLINK() {
        return ATTACHMENTLINK;
    }

    @XmlElement
    public void setATTACHMENTLINK(String ATTACHMENTLINK) {
        this.ATTACHMENTLINK = ATTACHMENTLINK;
    }
    
    /**
     * This method will, depending on the request type, apply the necessary
     * changes to the ESS
     *
     * @return
     */
    public boolean actionRequest(Employee employee) {

        // Leave Requests
        if (requestType == RequestType.LEAVEREQUEST) {
            return processLeaveRequest(employee);
        }
        // Accept/Reject processes
        if (requestType == RequestType.ACTIONREQUEST) {
            return processActionRequest(employee);
        }

        return true;
    }

    private boolean processActionRequest(Employee employee) {
        try {
            boolean approved = ACTION.compareToIgnoreCase("accept") == 0;

            // Get Process
            WebProcess process = FileContainer.getInstance().getWebProcess(PROCESS_ID);

            // Some validation
            //1. Does this process exist?
            if (process == null) {
                return false;
            }

            //2. Is this process still active?
            if (!process.isActive()) {
                return false;

            }
            //3. Can the current user approve/reject this process?
            if (!employee.equals(process.getCurrentStage().getOwner())) {
                return false;
            }
            // Finalise process
            if (approved) {
                process.acceptProcess("Mobile approval");
            } else {
                process.cancelProcess("Mobile rejection");
            }
            return true;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    private boolean processLeaveRequest(Employee employee) {
        try {
            boolean halfDay = (HALFDAY.compareTo("1") == 0);

            // Generate instance of a WebProcess_LeaveRequisition
            WebProcessDefinition procDef = FileContainer.getInstance().getWebProcessDefinition("za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition");
            WebProcess_LeaveRequisition leaveRequest;

            // Half Day?
            if (halfDay) {
                leaveRequest =
                        new WebProcess_LeaveRequisition(procDef, employee, employee, STARTDATE, true, Integer.parseInt(LEAVE_TYPE_ID), REASON, ADDITIONALDETAIL, null, null);
            } else {
                leaveRequest =
                        new WebProcess_LeaveRequisition(procDef, employee, employee, STARTDATE, ENDDATE, Integer.parseInt(LEAVE_TYPE_ID), REASON, ADDITIONALDETAIL, null, null);
            }

            FileContainer.getInstance().getWebProcesses().add(leaveRequest);
            return true;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public enum RequestType {

        UNDEFINED, LEAVEREQUEST, ACTIONREQUEST
    }
}
