<%-- 
    Document   : onlineShop.jsp
    Created on : 21 Aug 2014, 8:39:07 AM
    Author     : fkleynhans
--%>

<%@page import="java.text.DecimalFormat"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFWorkbook"%>
<%@page import="org.apache.poi.ss.usermodel.Workbook"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.IOException"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>PeoleWare Cloud Pricing- Quick Quote</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>  
        <link rel="stylesheet" type="text/css" href="/WebModule/CSS/style.css" />
        <script type="text/javascript">
            $(document).ready(function() {
                console.log('ready!');

                function showIfChecked($mySelector) {
                    console.log($mySelector);
                    console.log($mySelector.children("input[type='checkbox']"));
                    if ($mySelector.children("input[type='checkbox']").is(':checked'))
                        $mySelector.parent().children("div.other_text").slideDown("slow");
                    else
                        $mySelector.parent().children("div.other_text").slideUp("slow");
                }

                showIfChecked($(".onoffswitch").eq(0)); // first question
                showIfChecked($(".onoffswitch").eq(1)); // second question
                showIfChecked($(".onoffswitch").eq(2));
                showIfChecked($(".onoffswitch").eq(3));
                showIfChecked($(".onoffswitch").eq(4));
                showIfChecked($(".onoffswitch").eq(5));
                showIfChecked($(".onoffswitch").eq(6));

                function changeIfChecked($mySelector1) {
                    $mySelector1.children("input[type='checkbox']").change(function() {
                        console.log($(this).is(':checked'));  // $(this) represents whatever the selector brought back - eg. $("div.question > input[type='checkbox']")
                        if ($(this).is(':checked'))
                            $mySelector1.parent().children("div.other_text").slideDown("slow");
                        else
                            $mySelector1.parent().children("div.other_text").slideUp("slow");
                        $mySelector1.parent().children("div.other_text").children().children("input[type='number']").val('');
                        $mySelector1.parent().children("div.other_text").children().children().children("input[type='number']").val('');
                    });
                }

                changeIfChecked($(".onoffswitch").eq(0));
                changeIfChecked($(".onoffswitch").eq(1));
                changeIfChecked($(".onoffswitch").eq(2));
                changeIfChecked($(".onoffswitch").eq(3));
                changeIfChecked($(".onoffswitch").eq(4));
                changeIfChecked($(".onoffswitch").eq(5));
                changeIfChecked($(".onoffswitch").eq(6));

            });

            function copyTo(obj)
            {
                document.getElementById("RetirementFundIncomeInput").value = obj.value;
            }

            function compute() {
                var a = +$('input[id=TravelAllowanceInput]').val();
                var b = +$('input[id=OtherAllowanceInput]').val();
                var c = +$('input[id=AnnualBonusInput]').val();
                var total = a + b + c;
                $('input[id=NonRetirementFundIncomeInput]').val(total);
            }
        </script>
    </head>
    <body>
        <div id="container">
            <h1 class="title">PeopleWare Cloud Pricing - Quick quote</h1>
            <form action="onlineQuote.jsp" method="GET">
                <div class="outer-grouping">
                    <h2 class="title-nomargin">Number of employees and operators</h2>
                    <div class="grouping">
                        <p style="display: inline;">
                            <label for="StaffCount"><b>What is your staff count: &nbsp;</b></label>
                            <input type="number" name="numberOfEmployees" min="1" max="100000" size="15" value="200" required>
                        </p>
                        <p style="margin-top: 10px;">
                            <b>How many Operators require access to the PeopleWare software:</b>
                            <input type="number" id="PWOperators" name="PWOperators" min="1" max="1000" size="15" value="2" required>
                        </p>
                    </div>
                </div>
                <div class="outer-grouping">
                    <h2 class="title-nomargin">Product Grouping</h2>
                    <div class="grouping">
                        <div class="question">
                            <div style="display: inline-block;">
                                <p>
                                    <a href="http://accsys.co.za/features/payroll" target="_blank" title="Click here for more information on Payroll">
                                        <label for="payrollInput"><b>Payroll</b></label>
                                    </a>
                                </p>
                            </div> 
                            <div class="onoffswitch">
                                <input type="checkbox" name="payrollSwitch" class="onoffswitch-checkbox" id="payrollSwitch" checked="true">
                                <label class="onoffswitch-label" for="payrollSwitch">
                                    <div class="onoffswitch-inner"></div>
                                    <div class="onoffswitch-switch"></div>
                                </label>
                            </div>
                        </div>
                        <div class="question" style="margin-top: 15px;">
                            <div style="display: inline-block;">
                                <p>
                                    <a href="http://accsys.co.za/features/human-resources" target="_blank" title="Click here for more information on Human Resources">
                                        <label for="HrInput"><b>Human Resources</b></label>
                                    </a>
                                </p>
                            </div> 
                            <div class="onoffswitch">
                                <input type="checkbox" name="HrSwitch" class="onoffswitch-checkbox" id="HrSwitch">
                                <label class="onoffswitch-label" for="HrSwitch">
                                    <div class="onoffswitch-inner"></div>
                                    <div class="onoffswitch-switch"></div>
                                </label>
                            </div>
                        </div>
                        <div class="question" style="margin-top: 15px;">
                            <div style="display: inline-block;">
                                <p>
                                    <a href="http://accsys.co.za/features/time-attendance" target="_blank" title="Click here for more information on Time and Attendance">
                                        <label for="TnaInput"><b>Time and Attendance</b></label>
                                    </a>
                                </p>
                            </div> 
                            <div class="onoffswitch">
                                <input type="checkbox" name="TnaSwitch" class="onoffswitch-checkbox" id="TnaSwitch">
                                <label class="onoffswitch-label" for="TnaSwitch">
                                    <div class="onoffswitch-inner"></div>
                                    <div class="onoffswitch-switch"></div>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="outer-grouping">
                    <h2 class="title-nomargin">Employee Self Service</h2>
                    <div class="grouping">
                        <div class="question">
                            <div style="display: inline-block;">
                                <p>
                                    <label for="EssInput"><b>Do you want ESS?</b></label>
                                </p>
                            </div> 
                            <div class="onoffswitch">
                                <input type="checkbox" name="EssSwitch" class="onoffswitch-checkbox" id="EssSwitch">
                                <label class="onoffswitch-label" for="EssSwitch">
                                    <div class="onoffswitch-inner"></div>
                                    <div class="onoffswitch-switch"></div>
                                </label>
                            </div>
                            <div id="Ess" class="other_text" style="display: none; margin-top: 15px;">
                                <div class="inputDiv">
                                    <p>
                                        The ESS works on a Pay Per Transaction basis. These transactions are billed as follows:<br>
                                    <table id="displaytable" style="margin-top: 10px; margin-left: 50px;">
                                        <tr>
                                            <th>
                                                Process Name
                                            </th>
                                            <th style="width: 70px;">
                                                Unit Cost
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                Employee Contact Information
                                            </td>
                                            <td>
                                                1.00
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Family Detail Request
                                            </td>
                                            <td>
                                                1.00
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Leave Cancellation
                                            </td>
                                            <td>
                                                1.00
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Leave Request
                                            </td>
                                            <td>
                                                1.00
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                TnA Daily Signoff
                                            </td>
                                            <td>
                                                0.25
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Training Request
                                            </td>
                                            <td>
                                                1.00
                                            </td>
                                        </tr>
                                    </table>
                                    </p>
                                </div>
                                <hr style="margin-top: 15px;">
                                <div class="inputDiv">
                                    <p>
                                        <label for="NoOfCredits"><b>Number of credits:</b></label>
                                        <input type="radio" name="NoOfCredits" value="1000" style="margin-left: 15px;">1 000
                                        <input type="radio" name="NoOfCredits" value="2500" checked style="margin-left: 15px;">2 500
                                        <input type="radio" name="NoOfCredits" value="5000"style="margin-left: 15px;">5 000
                                        <input type="radio" name="NoOfCredits" value="10000" style="margin-left: 15px;">10 000
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="outer-grouping">
                    <h2 class="title-nomargin">Reporting</h2>
                    <div class="grouping">
                        <div class="question">
                            <div style="display: inline-block;">
                                <p>
                                    <label for="excelInput"><b>Do you want the ability to do Excel extracts?</b></label>
                                </p>
                            </div>
                            <div class="onoffswitch">
                                <input type="checkbox" name="ExcelSwitch" class="onoffswitch-checkbox" id="ExcelSwitch">
                                <label class="onoffswitch-label" for="ExcelSwitch">
                                    <div class="onoffswitch-inner"></div>
                                    <div class="onoffswitch-switch"></div>
                                </label>
                            </div>
                        </div>
                        <hr style="margin-top: 15px; margin-bottom: 10px;">
                        <div class="question">
                            <div style="display: inline-block;">
                                <p style="margin-bottom: 10px;"><b>Crystal Reports Licence</b></p>
                                <div class="inputDiv">
                                    <p>
                                        <label for="crystalNotice"><b>This licence is a legal requirement and is dependant on the exchange rate.</b><br>&nbsp;<br>
<%
        try {

            //Create the input stream from the xlsx/xls file
            String fileName = "PriceList.xls";
            FileInputStream fis = new FileInputStream(fileName);

            //Create Workbook instance for xlsx/xls file input stream
            Workbook workbook = new HSSFWorkbook();
            //if(fileName.toLowerCase().endsWith("xlsx")){
            //    workbook = new Workbook(fis);
            //}else if(fileName.toLowerCase().endsWith("xls")){
            try {
                workbook = new HSSFWorkbook(fis);
            } catch (IOException exc) {
                exc.printStackTrace();
            }
            //}

            //Get the number of sheets in the xlsx file
            //int numberOfSheets = workbook.getNumberOfSheets();
            
            double CrystalCost = workbook.getSheetAt(2).getRow(14).getCell(2).getNumericCellValue();
            
            out.write("This licence is quoted at:<b>&nbsp;$" + new DecimalFormat("0.00").format(CrystalCost) + "*</b>");

            //close file input stream
            fis.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
%>
                                            <br>&nbsp;<br>&nbsp;&nbsp;
                                            * This is a once off cost.
                                        </label>
                                        <br>&nbsp;
                                    </p>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
                <button class="gradient-button">Quick Quote</button>
                <br>&nbsp;
                <br>&nbsp;
                <br>&nbsp;
            </form>
        </div><!-- Container end -->
    </body>
</html>
