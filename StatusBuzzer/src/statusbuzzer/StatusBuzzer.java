/*
 * The purpose of this application is to run as a daemon, 
 * monitoring a certain web server.  Any response (JSON) that is more 
 * than a simple '{}' is interpreted as a valid RESPONSE.
 *
 * By running this service, one can nominate the web service, tracking intervals, sound file, and
 *   trigger conditions (trigger when RESPONSE and/or NO RESPONSE)
 */
package statusbuzzer;

/**
 *
 * @author liam
 */
public class StatusBuzzer {

    /**
     * @param args the command line arguments args[0] - Full URL; args[1] - Check interval (minutes); args[2]
     * - Alarm on response; args[3] - Alarm on NO response; args[4] - Alarm media file; args[5] - show debug
     * info
     */
    public static void main(String[] args) {
        boolean canRun = true;

        String argU = "";
        String argM = "";
        boolean bArg1 = false;
        boolean bArg2 = false;
        boolean bArg3 = false;
        boolean bArg4 = false;
        String sArg3 = "";
        String sArg4 = "";

        System.out.println("***************************************************************************");
        System.out.println("StatusBuzzer is a Java daemon that is used to monitor web service calls.");
        System.out.println("Certain conditions can be flagged.  Should this condition evaluate to TRUE, ");
        System.out.println("  a sound-bite can be played to bring it to the user's attention.");
        System.out.println("***************************************************************************");

        if (args.length < 2) {
            System.out.println("StatusBuzzer requires at least 2 parameters:");
            System.out.println(" -u url [fully defined web service call]");
            System.out.println(" -m *.wav/*.mp3 [media file to be played by the alarm]");
            System.out.println(" Other (optional) parameters:");
            System.out.println("   -a1 [Sound alarm when any JSON object is returned by web service call]");
            System.out.println("   -a2 [Sound alarm when no/or empty JSON object is returned by web service call]");
            System.out.println("   -a3 str [Sound alarm when the returning JSON object contains this text - case sensitive]");
            System.out.println("   -a4 str [Sound alarm when the returning JSON object does not contain this text - case sensitive]");
            System.out.println("\n\nExample:java -jar \"StatusBuzzer.jar\" -u \"http://checkmysite.com?test=true\" -m \"train.wav\" -i 5 -a1 ");
        } else {

            int idx = 0;
            for (String arg : args) {
                // URL
                if (arg.equalsIgnoreCase("-u")) {
                    argU = args[idx + 1];
                }
                // Media
                if (arg.equalsIgnoreCase("-m")) {
                    argM = args[idx + 1];
                }
                // A1 - Any non-empty JSON object
                if (arg.equalsIgnoreCase("-a1")) {
                    bArg1 = true;
                }
                // A2 - No- or empty JSON object
                if (arg.equalsIgnoreCase("-a2")) {
                    bArg2 = true;
                }
                // A3 - Containing text
                if (arg.equalsIgnoreCase("-a3")) {
                    bArg3 = true;
                    sArg3 = args[idx + 1];
                }
                // A4 - Not containing text
                if (arg.equalsIgnoreCase("-a4")) {
                    bArg4 = true;
                    sArg4 = args[idx + 1];
                }
                idx++;
            }

            // 
            // URL
            //
            if (argU.isEmpty()) {
                canRun = false;
                System.out.println("Invalid argument -u: bad/missing URL");
            }

            //
            // Media File
            //
            if ((argM.isEmpty()) || (!new java.io.File(argM).exists())) {
                canRun = false;
                System.out.println("Invalid argument -m: bad/missing media file");
            }

            if (canRun) {
                System.out.println("\n\nChecking web service...");
                System.out.println("\tURL:" + argU);
                System.out.println("\tAlarm On Response:" + bArg1);
                System.out.println("\tAlarm On NO Response:" + bArg2);
                if (bArg3) {
                    System.out.println("\tAlarm On Response containing:\"" + sArg3 + "\"");
                }
                if (bArg4) {
                    System.out.println("\tAlarm On Response not containint:\"" + sArg4 + "\"");
                }
                System.out.println("\tMedia File:" + argM);
                WebMonitor monitor = new WebMonitor();

                monitor.monitorURL(argU, argM, bArg1, bArg2, sArg3, sArg4);
            }
        }
    }

}
