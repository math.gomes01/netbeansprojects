package za.co.ucs.accsys.webmodule;

import za.co.ucs.accsys.peopleware.*;
import za.co.ucs.lwt.db.*;

/**
 * <p>
 * A WebProcess_DailySignoff is an extension of WebProcess that specializes in
 * the signing off of T&A hours on a daily basis
 * </p>
 */
public final class WebProcess_TnaRoster extends WebProcess {

    /**
     * To change Contact information, WebProcess_EmployeeContact requires
     *
     * @param processDefinition - details of reporting structure, etc
     * @param owner - who created/is responsible for this process?
     * @param employee - which employee is requesting leave?
     * @param shiftID
     * @param rosterDay
     */
    public WebProcess_TnaRoster(WebProcessDefinition processDefinition, Employee owner, Employee employee, String shiftID, java.util.Date rosterDay) {
        super(processDefinition, owner, employee);

        this.employee = employee;
        this.shiftID = shiftID;
        this.rosterDay = rosterDay;

        ShiftRosterInfo sri = new ShiftRosterInfo(employee);
        this.currentShift = sri.getEmployeeShiftThisDay(this.rosterDay);
        if (this.shiftID == null || this.shiftID.isEmpty()) {
            this.newShift = sri.getRosterShiftThisDay(rosterDay);
        } else {
            this.newShift = new Shift(this.shiftID);
        }
        escalateInitialProcess();

    }

    /**
     * This method should be implemented by each and every descendent of
     * WebProcess. It returns an instance of ValidationResult which contains
     * both the isValid() property as well as a reason in case of a non-valid
     * result.
     *
     * @return
     */
    @Override
    public ValidationResult validate() {
        if (isActive()) {

            /* DESCRIPTION: Presently, I can see no reason why we cannot simply perform the authorisation/signoff once it's reached this stage
             * 
             */
 /*
            
            
             // Does the employee still exist?
             Connection con = null;
             try {             
            
             con = DatabaseObject.getNewConnectionFromPool();
            
             // Is the payroll still open (for this employee)?
             String openPayroll = "select first closed_yn from p_calc_employeelist with (readuncommitted)  "
             + "where company_id=" + employee.getCompany().getCompanyID()
             + "and employee_id=" + employee.getEmployeeID()
             + "and runtype_id=fn_P_GetOpenRuntype(company_id, payroll_id, payrolldetail_id)";
            
             ResultSet rs1 = DatabaseObject.openSQL(openPayroll, con);
             if (rs1.next()) {
             if (rs1.getString(1).compareToIgnoreCase("Y") == 0) {
             rs1.close();
             DatabaseObject.releaseConnection(con);
             return (new ValidationResult(false, "The payroll for employee (employee_id=" + this.getEmployee().getEmployeeID() + ") is already closed for this period."));
             }
             }
             rs1.close();
             } catch (SQLException e) {
             e.printStackTrace();
             } finally {
             DatabaseObject.releaseConnection(con);
             }
             * 
             */
        }
        return (new ValidationResult(true, ""));
    }

    private void escalateInitialProcess() {
        // Is this a valid leave request?
        ValidationResult valid = validate();
        if (!valid.isValid()) {
            this.cancelProcess(valid.getInvalidReason());
        } else {
            this.escalateProcess(et_INITIAL, "");
        }
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("TnA Roster Change:").append(getEmployee().getSurname()).append(", ").append(getEmployee().getFirstName());
        result.append("\n\tDate: ").append(DatabaseObject.formatDate(this.rosterDay));
        result.append("\n\tFrom: ").append(this.currentShift.getName());
        if (this.newShift == null || this.newShift.getName() == null) {
            result.append("\n\tTo: ");
        } else {
            result.append("\n\tTo: ").append(this.newShift.getName());
        }

        return result.toString();
    }

    // Explanation of process in HTML format
    @Override
    public String toHTMLString() {
        StringBuilder result = new StringBuilder();
        result.append("Process Number: [").append(this.hashCode()).append("]<br>");
        result.append("TnA Roster Change: ").append(getEmployee().getSurname()).append(", ").append(getEmployee().getFirstName());
        result.append("<table class=process_detail><font size='-3'><tr><td>Date:</td><td>").append(DatabaseObject.formatDate(this.rosterDay)).append("</td></tr>");
        // The shift the employee was on
        result.append("<tr><td>From:</td><td>").append(currentShift.getName()).append("</td></tr>");
        // The new selected shift
        if (this.newShift == null || this.newShift.getName() == null) {
            result.append("<tr><td>To:</td><td>&nbsp;</td></tr>");
        } else {
            result.append("<tr><td>To:</td><td>").append(newShift.getName()).append("</td></tr>");
        }
        // Close the table
        result.append("</font></table>");
        return result.toString();
    }

    /**
     * This statement gets executed when the process is to be written to the
     * database.
     */
    @Override
    String getCommitStatement() {
        return getCommitCONSTANTStatement();
    }

    private String getCommitCONSTANTStatement() {
        StringBuilder result = new StringBuilder();
        // If the new shift is null then unlink hte employee and let the shift profile take over
        if (this.shiftID == null || this.shiftID.isEmpty()) {
            result.append("delete from TA_ROSTEREXCEPTION where PERSON_ID = ");
            result.append(this.employee.getPersonID());
            result.append(" and DAY = \"date\"('");
            result.append(DatabaseObject.formatDate(this.rosterDay));
            result.append("')");
        } else {
            // Else add the roster exception
            result.append("insert into TA_ROSTEREXCEPTION (PERSON_ID, DAY, SHIFT_ID)");
            result.append(" on existing update ");
            result.append("values ( ").append(this.employee.getPersonID()).append(", ");
            result.append("         \"date\"('").append(DatabaseObject.formatDate(this.rosterDay)).append("'), ");
            result.append("         ").append(this.shiftID).append("); ");
        }
        return (result.toString());
    }

    @Override
    public String getProcessName() {
        return "TnA Roster Change";
    }

    // Fingerprint of previously compatible version
    /* In order to extract this UID, run the following command in the
     * folder where the current lwt.jar file resides BEFORE changing
     * the contents of the class.
     C:\Apache5\shared\lib>c:\j2sdk1.4.2_03\bin\serialver -classpath lwt.jar za.co.ucs.accsys.webmodule.WebProcess_EmployeeContact
     za.co.ucs.accsys.webmodule.WebProcess_EmployeeContact:    static final long serialVersionUID = -1459535708782221840L;
     */
    static final long serialVersionUID = 6514658877295936829L;
    
    /**
     * Returns the day for which the roster change was logged.
     *
     * @return
     */
    public java.util.Date getRosterDay() {
        return this.rosterDay;
    }
    
    /**
     * Returns the shift which the roster change was logged.
     *
     * @return
     */
    public Shift getNewShift() {
        return this.newShift;
    }
    
    /**
     * Returns the employees rostered.
     *
     * @return
     */
    public Shift getCurrentShift() {
        return this.currentShift;
    }
    

    private final Employee employee;
    private final String shiftID;
    private final java.util.Date rosterDay;
    private final Shift currentShift;
    private final Shift newShift;
} // end LeaveRequisitionProcess

