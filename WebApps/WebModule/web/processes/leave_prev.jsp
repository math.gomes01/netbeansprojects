<%@page contentType="text/html"%>

<%@include file="html5_top.jsp"%>

<%@include file="html_messages.jsp"%> 
<!-- This HAS to be included if messages were generated
and added to the session attributes for this person.
Calling this script will simply display those messages -->

<!-- Now we need to verify that there is a LoginSessionObject -->


<%  // Can I connect to the database?
    // We need to reset the EMPLOYEE to be the same person as the USER once the process has been committed
    if (!user.equals(employee)) {
        loginSessionObject.setEmployee(user);
    } else {
        if (za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideViewInformationFor("Leave Status")) {
            PageSectionGenerator gen = PageSectionGenerator.getInstance();
            out.write(gen.buildHTMLPageHeader("Access Revoked", "Your system was configured not to show this page."));
        } else {

// Leave Balance Date
            String balanceDate = request.getParameter("balanceDate"); // for which period are we to analyse the balances?
            if (balanceDate == null) {
                balanceDate = "Today";
            }

// Leave Info
            LeaveInfo leaveInfo = fileContainer.getLeaveInfoFromContainer(employee);
            if (leaveInfo.getNumberOfLeaveTypes() == 0) {
// Page Header
                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                        "You are not linked to any Leave Profiles.", false, request.getSession());
                producer.setMessageInSessionObject();
                out.write("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");

            } else {
                PageSectionGenerator gen = PageSectionGenerator.getInstance();
                out.write(gen.buildHTMLPageHeader("Leave Preview", employee.toString()));





// Leave Balances
                za.co.ucs.ess.PageProducer_LeaveBalances producer = new za.co.ucs.ess.PageProducer_LeaveBalances(employee, balanceDate, request.getRequestURL().toString());
                out.write(producer.generateInformationContent());
                out.write("<hr width=\"80%\" size=\"1\" color=\"#c0c0c0\">");

// Leave Summary Grid
                PageProducer_LeaveSummaryGrid gridProducer = new PageProducer_LeaveSummaryGrid(employee);
                out.write(gridProducer.generateInformationContent());

                out.write("<hr width=\"80%\" size=\"1\" color=\"#c0c0c0\">");

// Leave History
                out.write("<table width=\"80%\" align=\"center\"><tr><td>");
                out.write("<div id=\"container\"><center>");
                out.write("<div class=\"item\">");
                PageProducer_LeaveHistory historyProducer = new PageProducer_LeaveHistory(employee);
                out.write(historyProducer.generateInformationContent());
                out.write("</div>");
                out.write("</center></div>");
                out.write("</td></tr></table>");

            }
        }
    }
%>

<%@include file="html_bottom.jsp"%>