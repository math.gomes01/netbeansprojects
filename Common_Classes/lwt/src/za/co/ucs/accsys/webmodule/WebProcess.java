package za.co.ucs.accsys.webmodule;

import java.io.File;
import java.io.FileInputStream;
import java.sql.*;
import java.util.*;
import java.util.Date;
import org.apache.commons.io.FilenameUtils;
import za.co.ucs.accsys.peopleware.*;
import za.co.ucs.lwt.db.*;

/**
 * <p>
 * A WebProcess is initiated by an employee that might, for example, request
 * leave. This WebProcess contains an active stage that indicates where the
 * current responsibility for the process lies. If a manager rejects/approves a
 * process, that ProcessStage gets added to the ProcessHistory and a new
 * currentProcess gets created. </p>
 */
public abstract class WebProcess implements java.io.Serializable {

    /**
     * Constructor for a Web Process
     *
     * @param processDefinition
     * @param owner
     * @param employee
     */
    public WebProcess(WebProcessDefinition processDefinition, Employee owner, Employee employee) {
        this.processDefinition = processDefinition;
        this.classVersion = 2; // As of Aug 2012
        //this.owner = owner;
        this.employee = employee;
        this.creationDate = new java.util.Date();
        this.processHistory = new WebProcessHistory();
        // Select the default reporting structure if there isn't one
        if (processDefinition.getProcessClassName().contains("za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition")) {
            if (processDefinition.getReportingStructure() == null) {
                processDefinition.setReportingStructure(FileContainer.getInstance().getWebProcessDefinition("za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition").getReportingStructure());
            }
        }
        // The ProcessStage that is in the constructor, needs to be escalated immediately.
        // If we do not do this, the guy that logged the request in the first place, will
        //   have to be responsible for forwarding it.
        this.currentStage = new WebProcessStage(owner, processDefinition.getReportingStructure().getEmployeeSelection(owner));
        this.active = true;
        //za.co.ucs.accsys.webmodule.FileContainer.getInstance().updateProcessInDB(this);
    }

    /**
     * The ProcessDefinition class defines the reporting structure, process age
     * limits, etc.
     *
     * @return
     */
    public WebProcessDefinition getProcessDefinition() {
        return processDefinition;
    }

    /**
     * The ProcessDefinition class defines the reporting structure, process age
     * limits, etc.
     *
     * @param processDefinition
     */
    public void setProcessDefinition(WebProcessDefinition processDefinition) {
        this.processDefinition = processDefinition;
    }

    /**
     * Every WebProcess has a container ProcessHistory that stores its completed
     * processes
     *
     * @return
     */
    public WebProcessHistory getProcessHistory() {
        return processHistory;
    }

    public void setProcessHistory(WebProcessHistory processHistory) {
        this.processHistory = processHistory;
    }

    /**
     * A WebProcess can only have one ProcessStage at any one point in time.
     *
     * @return
     */
    public WebProcessStage getCurrentStage() {
        return currentStage;
    }

    /**
     * Returns the age of the WebProcess (in HOURS)
     *
     * @return
     */
    public int getProcessAge() {
        long ms_create = this.creationDate.getTime();
        long ms_now = new Date().getTime();
        int age = getWorkingHoursPerDay(ms_create, ms_now);
        //System.out.println("\tTotal Process Age (Hours): " + age);
        return age;
    } // end getProcessAge

    /**
     * Returns the age of the current ProcessStage (in HOURS)
     *
     * @return
     */
    public int getActiveStageAge() {
        long ms_create = this.getCurrentStage().getCreationDate().getTime();
        long ms_now = new Date().getTime();
        int age = getWorkingHoursPerDay(ms_create, ms_now);
        //System.out.println("\tActive Stage Age (Hours): " + age);
        return age;
    } // end getStageAge

    /**
     * When determining the age of a process stage of a complete process, it is
     * necessary to ignore non-office hours. As a standard, one will thus only
     * look at times expired between Mon-Fri 09:00 - 17:00
     *
     * @param fromTime
     * @param toTime
     * @return
     */
    public int getWorkingHoursPerDay(long fromTime, long toTime) {
        int hrs_START_OF_DAY = 9;
        int hrs_END_OF_DAY = 17;
        int result = 0;
        java.util.Date fromDate = new java.util.Date(fromTime);
        java.util.Date toDate = new java.util.Date(toTime);

        GregorianCalendar calendarFrom = new GregorianCalendar();
        calendarFrom.setTime(fromDate);
        GregorianCalendar calendarTo = new GregorianCalendar();
        calendarTo.setTime(toDate);

        GregorianCalendar calendarDynamicFrom = new GregorianCalendar();
        calendarDynamicFrom.setTime(fromDate);

        while (calendarDynamicFrom.getTime().getTime() < toTime) {

            // Increment date with one hour
            calendarDynamicFrom.add(Calendar.HOUR_OF_DAY, 1);

            // Saturdays & Sundays
            if ((calendarDynamicFrom.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)
                    || (calendarDynamicFrom.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)) {
                continue;
            }

            // Outside Working Hours
            if ((calendarDynamicFrom.get(Calendar.HOUR_OF_DAY) > hrs_END_OF_DAY)
                    || (calendarDynamicFrom.get(Calendar.HOUR_OF_DAY) < hrs_START_OF_DAY)) {
                continue;
            }

            // Add into result
            result++;
        }

        return result;
    }

    /**
     * Returns the original creation date of this Web Process.
     *
     * @return
     */
    public java.util.Date getCreationDate() {
        return this.creationDate;
    }

    /**
     * Randomly selects anyone on the same level as the current owner. This
     * person should NOT be in the WebProcessHistory.
     */
    private Employee getUnusedOwnerOnSameLevel(Employee currentProcessOwner, EmployeeSelection currentEmployeeSelection) {
        Employee result = null;
        LinkedList employeesOnSameLevel = new LinkedList(getProcessDefinition().getReportingStructure().getEmployeesOnSameLevel(currentProcessOwner, currentEmployeeSelection));
        LinkedList possibleEmployees = new LinkedList();
        for (int i = 0; i < employeesOnSameLevel.size(); i++) {
            if (!getProcessHistory().getPreviousProcessOwners().contains(employeesOnSameLevel.get(i))
                    && !employeesOnSameLevel.get(i).equals(currentProcessOwner)) {
                possibleEmployees.add(employeesOnSameLevel.get(i));
            }
        }
        int nbUnusedEmployees = possibleEmployees.size();
        if (nbUnusedEmployees > 0) {
            Random random = new Random();
            int index = random.nextInt(nbUnusedEmployees);
            result = (Employee) possibleEmployees.get(index);
        }
        return result;
    }

    /**
     * Randomly selects anyone on the next level as the current owner. This
     * person should NOT be in the WebProcessHistory.
     */
    private Employee getUnusedOwnerOnNextLevel(Employee currentProcessOwner, EmployeeSelection currentEmployeeSelection) {
        Employee result = null;
        if (getProcessDefinition().getReportingStructure() == null) {
            System.out.println("No Reporting Structure found for WebProcess:" + getProcessDefinition());
            return null;
        }
        //System.out.println("currentProcessOwner:" + currentProcessOwner);
        //System.out.println("currentEmployeeSelection:" + currentEmployeeSelection);
        //System.out.println("parentEmployeeSelection:" + getProcessDefinition().getReportingStructure().getParentSelection(currentEmployeeSelection));
        WebProcessDefinition procDef = getProcessDefinition();
        ReportingStructure structure = procDef.getReportingStructure();

        while (currentEmployeeSelection != null) {
            int nbUnusedEmployees = 100;
            LinkedList employeesOnNextLevel = new LinkedList(structure.getParentEmployees(currentProcessOwner, currentEmployeeSelection));
            LinkedList possibleEmployees = new LinkedList();
            for (int i = 0; i < employeesOnNextLevel.size(); i++) {
                if (!getProcessHistory().getPreviousProcessOwners().contains(employeesOnNextLevel.get(i))
                        && !employeesOnNextLevel.get(i).equals(currentProcessOwner)) {
                    // Check if the current owner and the possible employee is on the same level
                    EmployeeSelection possibleOwnerEmployeeSelection = structure.getEmployeeSelection((Employee) employeesOnNextLevel.get(i));
                    EmployeeSelection currentOwnerEmployeeSelection = structure.getEmployeeSelection(currentProcessOwner);
                    if (possibleOwnerEmployeeSelection.equals(currentOwnerEmployeeSelection)) {
                        return null;
                    } else {
                        possibleEmployees.add(employeesOnNextLevel.get(i));
                    }
                    //System.out.println("Employee in parentEmployeeSelection:" + employeesOnNextLevel.get(i));
                }
            }
            nbUnusedEmployees = possibleEmployees.size();
            if (nbUnusedEmployees > 0) {
                Random random = new Random();
                int index = random.nextInt(nbUnusedEmployees);
                result = (Employee) possibleEmployees.get(index);
                break;
            }

            if (nbUnusedEmployees == 0) {
                //System.out.println("currentEmployeeSelection (" + currentEmployeeSelection + ") has no available parent employees.  Looking higher up in the hierarchy...");
                // No employees on this level.  Jump one level up to see if someone on that level can take care of this
                currentEmployeeSelection = structure.getParentSelection(currentEmployeeSelection);
                //System.out.println("currentEmployeeSelection is now '" + currentEmployeeSelection + "'.");
            }
        }

        return result;
    }

    /**
     * Returns the next person to take ownership of this process. In order to
     * properly escalate a process, we need to: <br>For
     * escalationType=et_FORWARD/et_ACCEPT, find a person on the next level in
     * the reporting hierarchy. <br>For escalationType=et_TIMEOUT, first try to
     * find an employee on the same level in the reporting hierarchy. If none is
     * found, go one level up. <br>For escalationType=et_INITIAL, treat the same
     * as et_FORWARD
     */
    private Employee getEscalatedOwner(String escalationType, Employee currentProcessOwner, EmployeeSelection currentEmployeeSelection, WebProcessDefinition processDefinition) {
        Employee result;
        // Forwarding
        if ((escalationType.compareTo(et_ACCEPT) == 0) || (escalationType.compareTo(et_FORWARD) == 0) || (escalationType.compareTo(et_INITIAL) == 0)) {
            result = getUnusedOwnerOnNextLevel(currentProcessOwner, currentEmployeeSelection);
            if (!getOriginalOwner().equals(getEmployee())) {
                EmployeeSelection resultParentES = getProcessDefinition().getReportingStructure().getEmployeeSelection(result);
                EmployeeSelection originParentES = getProcessDefinition().getReportingStructure().getEmployeeSelection(getOriginalOwner());
                if (resultParentES != null && originParentES != null) {
                    ArrayList<Employee> resultParents = getProcessDefinition().getReportingStructure().getAllEmployeesInEmployeeSelection(resultParentES);
                    ArrayList<Employee> originParents = getProcessDefinition().getReportingStructure().getAllEmployeesInEmployeeSelection(originParentES);
                    boolean isRelated = false;
                    for (Employee resultParent : resultParents) {
                        if (originParents.contains(resultParent)) {
                            isRelated = true;
                        }
                    }
                    if (isRelated) {
                        result = getUnusedOwnerOnNextLevel(getOriginalOwner(), processDefinition.getReportingStructure().getParentSelection(currentEmployeeSelection));
                    }
                }
            }
        } else {
            result = getUnusedOwnerOnSameLevel(currentProcessOwner, currentEmployeeSelection);
            if (result == null) {
                result = getUnusedOwnerOnNextLevel(currentProcessOwner, currentEmployeeSelection);
            }
        }
        return result;
    }

    /**
     * Returns the Employee Selection of the next person to take ownership of
     * this process. This works closely with getEscalatedOwner and returns the
     * corresponding EmployeeSelection of the selected employee
     */
    private EmployeeSelection getEscalatedOwnersEmployeeSelection(String escalationType, Employee currentProcessOwner, EmployeeSelection currentEmployeeSelection, WebProcessDefinition processDefinition) {
        EmployeeSelection result;
        // Forwarding
        if ((escalationType.compareTo(et_ACCEPT) == 0) || (escalationType.compareTo(et_FORWARD) == 0) || (escalationType.compareTo(et_INITIAL) == 0)) {
            result = processDefinition.getReportingStructure().getParentSelection(currentEmployeeSelection);
        } else {
            result = currentEmployeeSelection;
            if (result == null) {
                result = processDefinition.getReportingStructure().getParentSelection(currentEmployeeSelection);
            }
        }
        return result;
    }

    /**
     * When a process escalates up the hierarchy, and, say, the immediate
     * supervisor approves the request. When the process times-out later in the
     * hierarchy, up the the very last person, we would need to approve the
     * final step as if it has been approved by the top person in the reporting
     * structure. This is to address scenario where the Payroll Administrator
     * goes on leave, and nobody is taking care of her 'Inbox'. The rule states
     * that, should at least one manager approve the process, we no longer need
     * to cancel it if it times-out at the top.
     */
    private boolean processWasApprovedAtLeastOnce() {
        // Get The Process History
        //System.out.println("DEBUG:  processWasApprovedAtLeastOnce=" + getProcessHistory().getNumberOfApprovedStages());
        // The initial process logged by the employee gets approved by the system if all
        // prerequisite rules apply.  We therefore need to check for more than one approval
        // in the whole process
        return (getProcessHistory().getNumberOfApprovedStages() > 1);

    }

    /**
     * Escalation happens on four occasions. <br>1. The very first escalation is
     * from the guy who logged the request. Escalation Type = et_INITIAL <br>2.
     * The owner of the current ProcessStage accepts the process and it needs to
     * be passed on to the next person in the hierarchy. Escalation Type =
     * et_ACCEPT <br>3. The ProcessStage timed-out. Escalation Type = et_TIMEOUT
     * <br>4. The Process gets forwarded because the current owner is on leave,
     * etc.
     *
     * @param escalationType
     * @param reason
     */
    public void escalateProcess(String escalationType, String reason) {

        //System.out.println("\n\nEscalating Process:" + escalationType + ", " + reason);
        //System.out.println("CurrentEmployeeSelection:" + getCurrentStage().getEmployeeSelection());
        // Determine the next logical employee in the reporting structure
        Employee originalOwner = getOriginalOwner();
        Employee thisEmployee = getEmployee();
        Employee newOwner = getEscalatedOwner(escalationType, getCurrentStage().getOwner(), getCurrentStage().getEmployeeSelection(), getProcessDefinition());
        EmployeeSelection newEmployeeSelection = getEscalatedOwnersEmployeeSelection(escalationType, getCurrentStage().getOwner(), getCurrentStage().getEmployeeSelection(), getProcessDefinition());

        // CR 6698 - Allow for the option to bypass the remainder of the reporting structure and gets applied directly to the database
        boolean canApplyAfterFirstApproval = WebModulePreferences.getPreferences().getBoolean(WebModulePreferences.WORKFLOW_ApplyAfterFirstApproval, false);
        boolean canJumpToTopAfterFirstApproval = WebModulePreferences.getPreferences().getBoolean(WebModulePreferences.WORKFLOW_JumpToTopAfterFirstApproval, false);

        if ((escalationType.equals(et_ACCEPT)) && (canApplyAfterFirstApproval)) {
            getCurrentStage().accept(reason);
            commitProcess(getCommitStatement());
            return;
        }
        // If the Manager applied on my behalf, and APPLY_AFTER_FIRST_APPROVAL, Accept and Commit the process
        if ((!originalOwner.equals(thisEmployee)) && (escalationType.equals(et_INITIAL)) && (canApplyAfterFirstApproval)) {
            //System.out.println("** Employee did not log process himself: Applied in db");
            getCurrentStage().accept(reason);
            commitProcess(getCommitStatement());
            return;
        }

        //
        // If we try to escalate this process and are unable to find a person to escalate it to:
        //
        if (newOwner == null) // <editor-fold defaultstate="collapsed" desc="NO NEW OWNER ">
        {
            //System.out.println("****** No NEW OWNER ******");

            // Is this the original request, with no one to approve it?
            if ((escalationType.equals(et_INITIAL))
                    && (getCurrentStage().getOwner() == thisEmployee)) {
                cancelProcess("Nobody available to approve your request");
                return;
            }

            // Is this the first escalation after the original request
            // AND the user that requested the process did it on behalf of one of his/her employees?
            if (((escalationType.equals(et_INITIAL)) || (escalationType.equals(et_ACCEPT)))
                    && (getCurrentStage().getOwner() != thisEmployee)) {
                getCurrentStage().accept(reason + "\nApproved by manager.  Unable to escalate further.");
                commitProcess(getCommitStatement());
            }

            // Did the previous process time-out ?
            if (escalationType.equals(et_TIMEOUT)) {
                // Did someone else aproved this process earlier?
                if (getProcessHistory().getNumberOfApprovedStages() > 0) {
                    getCurrentStage().timeout(reason);
                    commitProcess(getCommitStatement());
                } else {
                    cancelProcess("Nobody available to approve your request");
                }
            }

            if (escalationType.equals(et_FORWARD)) {
                if (getProcessHistory().getNumberOfApprovedStages() > 0) {
                    getCurrentStage().forward(reason);
                    commitProcess(getCommitStatement());
                } else {
                    cancelProcess("Nobody available to approve your request");
                }
            }
        } // </editor-fold>
        //
        // We did find someone to escalate the process to:
        //
        else // <editor-fold defaultstate="collapsed" desc=" NEW OWNER ">
        {
            //System.out.println("****** NEW OWNER:" + newOwner + "  ******");

            // Special Case #1 - Allow for the option to bypass the remainder of the reporting structure and jump straight to the top level
            if (canJumpToTopAfterFirstApproval) {
                if ((escalationType.equals(et_ACCEPT)) || (getProcessHistory().getNumberOfApprovedStages() > 0)) {
                    // Is the new owner already on top?
                    FileContainer fc = za.co.ucs.accsys.webmodule.FileContainer.getInstance();
                    if (!fc.isEmployeeInTopLevel(newOwner, getProcessDefinition())) {
                        ArrayList topList = getProcessDefinition().getReportingStructure().getTopNodes();
                        if (topList.get(0) != null) {
                            newEmployeeSelection = (EmployeeSelection) (topList.get(0));
                            // Only of possible
                            if (getUnusedOwnerOnSameLevel(getOriginalOwner(), newEmployeeSelection) != null) {
                                newOwner = getUnusedOwnerOnSameLevel(getOriginalOwner(), newEmployeeSelection);
                            }
                        }
                    }
                }
            }

            // Special Case #2 - If a manager logged the process on behalf of an employee
            if (canJumpToTopAfterFirstApproval) {
                if ((!originalOwner.equals(thisEmployee)) && (escalationType.equals(et_INITIAL))) {
                    ArrayList topList = getProcessDefinition().getReportingStructure().getTopNodes();
                    if (topList.get(0) != null) {
                        newEmployeeSelection = (EmployeeSelection) (topList.get(0));
                        if (getUnusedOwnerOnSameLevel(getOriginalOwner(), newEmployeeSelection) != null) {
                            newOwner = getUnusedOwnerOnSameLevel(getOriginalOwner(), newEmployeeSelection);
                        }
                    }
                }
            }

            // Send notification messages
            sendEscalationNotifications(originalOwner, newOwner, reason);

            //   Escalate the process.
            if (escalationType.equals(et_INITIAL)) {
                if (reason.trim().length() == 0) {
                    reason = "Initial Escalation";
                }
                getCurrentStage().accept(reason);
            }
            if (escalationType.equals(et_ACCEPT)) {
                if (reason.trim().length() == 0) {
                    reason = "Accepted";
                }
                getCurrentStage().accept(reason);
            }
            if (escalationType.equals(et_FORWARD)) {
                if (reason.trim().length() == 0) {
                    reason = "Forwarded to next level";
                }
                getCurrentStage().forward(reason);
            }
            if (escalationType.equals(et_TIMEOUT)) {
                if (reason.trim().length() == 0) {
                    reason = "Operation Timed Out - Escalated";
                }
                getCurrentStage().timeout(reason);
            }
            getProcessHistory().addProcessStage(getCurrentStage());

            // Create a new CurrentStage
            WebProcessStage newStage = new WebProcessStage(newOwner, newEmployeeSelection);
            this.setCurrentStage(newStage);

        }

// </editor-fold>
    } // end escalateProcess

    private void sendEscalationNotifications(Employee originalOwner, Employee newOwner, String reason) {
        // We have a new owner to forward the process to
        // Notify original owner of status
        String message;
        if (originalOwner.equals(employee)) {
            message = "<h4>Dear " + getOriginalOwner().toString() + "</h4>"
                    + "<br>The following ESS Request has been escalated:<br><i>" + this.toHTMLString() + "</i>"
                    + "<br>" + newOwner.toString() + " is now taking care of it.<br>"
                    + "<i>Reason:</i>" + reason;
        } else {
            message = "<h4>Dear " + employee.toString() + "</h4>"
                    + "<br>The following ESS Request (logged on your behalf by " + getOriginalOwner().toString() + ") has been escalated:<br><i>" + this.toHTMLString() + "</i>"
                    + "<br>" + newOwner.toString() + " is now taking care of it.<br>"
                    + "<i>Reason:</i>" + reason;
            notifyEmployee(employee, message);

            message = "<h4>Dear " + getOriginalOwner().toString() + "</h4>"
                    + "<br>The following ESS Request (logged by you on behalf of " + employee.toString() + ") has been actioned:<br><i>" + this.toHTMLString() + "</i>"
                    + "<br>" + newOwner.toString() + " is now taking care of it.<br>"
                    + "<i>Reason:</i>" + reason;

        }

        notifyEmployee(originalOwner, message);

        // CR 6909: Notify all employees that were part of this process
        Iterator iter = this.getProcessHistory().getPreviousProcessOwners().iterator();
        while (iter.hasNext()) {
            Employee previousOwner = (Employee) (iter.next());
            if (!previousOwner.equals(originalOwner)) {
                notifyEmployee(previousOwner, "<h4>Dear " + previousOwner.toString() + "</h4>"
                        + "<br>The following ESS Request - for " + employee.toString() + " - has been actioned:<br><i>" + this.toHTMLString() + "</i>"
                        + "<br><br>" + newOwner.toString() + " is now taking care of it.<br><i>Reason:</i>" + reason);
            }
        }
    }

    /**
     * Returns the employee that logged the process.
     */
    private Employee getOriginalOwner() {
        WebProcessStage originStage = processHistory.getOriginProcessStage();
        if (originStage == null) {
            originStage = getCurrentStage();
        }
        Employee originalOwner = originStage.getOwner();
        return originalOwner;
    }

    /**
     * Returns the list of managers directly above the employee that the process
     * is for.
     */
    private LinkedList getParentApprovers() {
        ReportingStructure rs = getProcessDefinition().getReportingStructure();
        EmployeeSelection esEmployee = rs.getEmployeeSelection(employee);
        EmployeeSelection esLineManager = rs.getParentSelection(esEmployee);

        LinkedList employeesOnSameLevel = new LinkedList(getProcessDefinition().getReportingStructure().getEmployeesOnSameLevel(employee, esLineManager));
        // The employee parameter in getEmployeesOnSameLevel does not have any effect on the result as it is not used.

        return employeesOnSameLevel;
    }

    /**
     * We need to notify the employee involved in the process, as well as the
     * owner of a process of various events.
     *
     * @param employee
     * @param message
     */
    public void notifyEmployee(Employee employee, String message) {
        //
        // With T&A Authorisations, we do not notify the client of any changes as it would simply be too much!
        //
        if (this.processDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_DailySignoff") != 0) {
            if ((WebModulePreferences.getPreferences().getBoolean(WebModulePreferences.SMTP_SendEMAILNotifications, false))) {
                E_Mail.getInstance().send(employee.getEMail(), "", WebModulePreferences.getInstance().getPreference(WebModulePreferences.SMTP_EMailNotificationSubjectString, "") + " [" + this.hashCode() + "]", message, null);
            }
        } else {
            // System.out.println("Notifications for \"WebProcess_DailySignoff\" is surpressed by the WebProcess class.");
        }
    }

    /**
     * We need to notify the employee involved in the process, as well as the
     * owner of a process of various events. Attaches an ICal file for Outlook
     * integration. Can only be done for Leave and Training requests
     *
     * @param employee
     * @param message
     */
    public void notifyEmployeeWithCalendar(Employee employee, String message) {

        if ((WebModulePreferences.getPreferences().getBoolean(WebModulePreferences.SMTP_SendEMAILNotifications, false))) {
            //
            // Depending on the process type, we might like to send a calendar notification with (IF the person has an e-mail address
            //
            if ((employee.getEMail() != null) && (employee.getEMail().trim().contains("@"))) {
                if (this.processDefinition.getProcessClassName().contains("za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition")) {
                    //
                    // Leave Request - Create a Calendar Entry from 08:00 to 17:00 OR 08:00 to 13:00 for half-days
                    //
                    WebProcess_LeaveRequisition leaveRequest = (WebProcess_LeaveRequisition) this;
                    Date fromDate = leaveRequest.getFromDate();
                    Date toDate = leaveRequest.getToDate();
                    java.util.Calendar startDateCal = new GregorianCalendar();
                    java.util.Calendar endDateCal = new GregorianCalendar();
                    startDateCal.setTime(fromDate);
                    endDateCal.setTime(toDate);
                    startDateCal.set(Calendar.HOUR, 8);
                    if (leaveRequest.isHalfday()) {
                        endDateCal.set(Calendar.HOUR, 13);
                    } else {
                        endDateCal.set(Calendar.HOUR, 17);
                    }
                    iCalendar ical = new iCalendar();

                    String smtpFrom = WebModulePreferences.getInstance().getPreference(WebModulePreferences.SMTP_FromAddress, "");
                    java.io.File iCalAtt = ical.getICalRequest(smtpFrom, this.employee.toString() + " - On Leave: " + leaveRequest.getLeaveType(), startDateCal, endDateCal, "Accsys PeopleWare - Employee Self Service", leaveRequest.toString(), employee.getEMail(),
                            "", this.hashCode() + ".ics");
                    E_Mail.getInstance().send(employee.getEMail(), "", WebModulePreferences.getInstance().getPreference(WebModulePreferences.SMTP_EMailNotificationSubjectString, "") + " [" + this.hashCode() + "]", message, iCalAtt);

                } else if (this.processDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_TrainingRequisition") == 0) {
                    //
                    // Training Requests - Create a Calendar Entry from 08:00 to 17:00 OR 08:00 to 13:00 for half-days
                    //
                    WebProcess_TrainingRequisition trianingRequest = (WebProcess_TrainingRequisition) this;
                    int trainingEventID = trianingRequest.getEventID();
                    TREvent event = new TREvent(trainingEventID);

                    Date fromDate = event.getFromDate();
                    Date toDate = event.getToDate();
                    java.util.Calendar startDateCal = new GregorianCalendar();
                    java.util.Calendar endDateCal = new GregorianCalendar();

                    startDateCal.setTime(fromDate);
                    endDateCal.setTime(toDate);
                    startDateCal.set(Calendar.HOUR, 8);
                    endDateCal.set(Calendar.HOUR, 17);

                    iCalendar ical = new iCalendar();
                    String smtpFrom = WebModulePreferences.getInstance().getPreference(WebModulePreferences.SMTP_FromAddress, "");
                    java.io.File iCalAtt = ical.getICalRequest(smtpFrom, "Accsys PeopleWare - Employee Self Service", startDateCal, endDateCal, "On Training: " + event.getCourseName(), trianingRequest.toString(), employee.getEMail(),
                            event.getVenue(), this.hashCode() + ".ics");
                    E_Mail.getInstance().send(employee.getEMail(), "", WebModulePreferences.getInstance().getPreference(WebModulePreferences.SMTP_EMailNotificationSubjectString, "") + " [" + this.hashCode() + "]", message, iCalAtt);
                }
            }
        }
    }

    /**
     * The applying of a process to the database happens in the abstract class,
     * but the generation of the commitString will have to be done in the
     * descendent classes and then passed to this method.
     *
     * @param commitString
     */
    public void commitProcess(String commitString) {
        // We can only commit the process if the original request is still valid
        if (!validate().isValid()) {
            cancelProcess(validate().getInvalidReason());
        } else if (isActive()) {
            Connection con = null;
            FileContainer fc = za.co.ucs.accsys.webmodule.FileContainer.getInstance();

            try {
                // We need to inform the user of the commit BEFORE the actual apply in the database,
                // as, once the changes are applied, we are unable to include the detail of the change
                // in the message.
                // Notify owner of commiting of process into database.
                sendCommitNotifications(employee);

                con = DatabaseObject.getNewConnectionFromPool();
                //System.out.println("\n\n****** COMMIT *******\n" + commitString);
                DatabaseObject.executeSQL(commitString, con);

                // After applying the commit, reload the employee's data from the database into
                //  the objects stored in the FileContainer
                Employee fcEmployee = FileContainer.getEmployeeFromContainer(
                        employee.getCompany().getCompanyID(),
                        employee.getEmployeeID());
                fcEmployee.loadFromDB();

                fc.resetLeaveInfoForEmployee(fcEmployee);
                this.active = false;

                fc.updateProcessInDerbyAndSybase(this);

            } catch (SQLException e) {
                // Notify the owner of the problem
                Employee originalOwner = getOriginalOwner();
                notifyEmployee(originalOwner, "<h3><font color=\"red\">ERROR in Accsys Web Module:</font></h3>"
                        + "<br>" + e.getMessage());
                cancelProcess("Error in applying process into Peopleware.<br>" + e.getMessage());
                active = false;
            } catch (Exception ge) {
                za.co.ucs.accsys.webmodule.FileContainer.getInstance().writeExceptionToLog(ge);
                // Notify the owner of the problem
                Employee originalOwner = getOriginalOwner();
                notifyEmployee(originalOwner, "<h3><font color=\"red\">ERROR in Accsys Web Module:</font></h3>"
                        + "<br>" + ge.getMessage());
                cancelProcess("Error in applying process into Peopleware.<br>" + ge.getMessage());
                active = false;
            } finally {
                DatabaseObject.releaseConnection(con);
            }

            // If there is an attachment we need to save it
            if (this.getProcessDefinition().getProcessClassName().contains("za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition")) {
                try {
                    WebProcess_LeaveRequisition leaveReq = (WebProcess_LeaveRequisition) this;
                    con = DatabaseObject.getNewConnectionFromPool();
                    con.setAutoCommit(false);
                    String attachmentLink = leaveReq.getAttachmentLink();
                    if (!attachmentLink.isEmpty()) {
                        File attachment = new File(attachmentLink);

                        String sql = "update L_HISTORY set SICK_NOTE_FILEEXT=?, SICK_NOTE=? where REFERENCE_NUMBER like ?";

                        PreparedStatement stmt = con.prepareStatement(sql);
                        stmt.setString(1, "." + FilenameUtils.getExtension(attachmentLink));
                        // Parse file as binary stream
                        FileInputStream fis = new FileInputStream(attachment);
                        stmt.setBinaryStream(2, fis, (int) attachment.length());
                        stmt.setString(3, "%" + Integer.toString(this.hashCode()) + "%");
                        stmt.executeUpdate();

                        con.commit();
                        fis.close();
                    }
                } catch (Exception ge) {
                    ge.printStackTrace();
                } finally {
                    DatabaseObject.releaseConnection(con);
                }
            }
            ///////////////////////////////////////////////
        }
    } // end commitProcess

    private void sendCommitNotifications(Employee originalOwner) {
        // We have a new owner to forward the process to
        // Notify original owner of status
        String message;
        if (originalOwner.equals(employee)) {
            message = "<h4>Dear " + getOriginalOwner().toString() + "</h4>"
                    + "<br>The following ESS Request has been approved:<br><br><i>" + this.toHTMLString() + "</i><br>";
        } else {
            message = "<h4>Dear " + employee.toString() + "</h4>"
                    + "<br>The following ESS Request (logged on your behalf by " + getOriginalOwner().toString() + ") has been approved:"
                    + "<br><br><i>" + this.toHTMLString() + "</i><br>";
            notifyEmployee(employee, message);

            message = "<h4>Dear " + getOriginalOwner().toString() + "</h4>"
                    + "<br>The following ESS Request (logged by you on behalf of " + employee.toString() + ") has been approved:"
                    + "<br><br><i>" + this.toHTMLString() + "</i><br>";
        }

        notifyEmployee(originalOwner, message);

        // CR 6909: Notify all employees that were part of this process
        Iterator iter = this.getProcessHistory().getPreviousProcessOwners().iterator();
        while (iter.hasNext()) {
            Employee previousOwner = (Employee) (iter.next());
            if (!previousOwner.equals(originalOwner)) {
                notifyEmployee(previousOwner, "<h4>Dear " + previousOwner.toString() + "</h4>"
                        + "<br>The following ESS Request - for " + employee.toString() + " - has been approved:"
                        + "<br><br><i>" + this.toHTMLString() + "</i><br>");
            }
        }

        // Finally, send a calendar entry to the employee
        // CR 12784 - ONLY for Training and Leave requests
        if (WebModulePreferences.getInstance().getPreference(WebModulePreferences.SMTP_SendICalAttachmentOnApproval, "true").compareToIgnoreCase("true") == 0) {
            if ((this.processDefinition.getProcessClassName().contains("za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition"))
                    || (this.processDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_TrainingRequisition") == 0)) {
                notifyEmployeeWithCalendar(employee, "Dear " + employee.toString() + "\n\nPlease find attached a calendar entry for your ESS Request.\n" + this.toString());
                if (WebModulePreferences.getInstance().getPreference(WebModulePreferences.SMTP_SendICalAttachmentToLineManagers, "true").compareToIgnoreCase("true") == 0) {
                    // Send to original owners if applicable
                    LinkedList<Employee> parentApprovers = getParentApprovers();
                    if (parentApprovers != null) {
                        for (Employee emp : parentApprovers) {
                            notifyEmployeeWithCalendar(emp, "Dear " + emp + "\n\nPlease find attached a calendar entry for the ESS Request of " + employee.toString() + ".\n" + this.toString());
                        }
                    }
                }
            }
        }
    }

    /**
     * Canceling a WebProcess cancels the active ProcessStage
     *
     * @param reason
     */
    public void cancelProcess(String reason) {
        if (isActive()) {
            notifyEmployee(getCurrentStage().getOwner(),
                    "<h4>Dear " + getCurrentStage().getOwner().toString() + "</h4>"
                    + "<br>The following ESS Request - for " + employee.toString() + " - has been cancelled:"
                    + "<br><br><i>" + this.toHTMLString() + "</i>"
                    + "<br>On: " + (DatabaseObject.formatDate(new Date()))
                    + "<br>Reason:" + reason + "<br><i>" + this.toHTMLString() + "</i>");

            getCurrentStage().cancel(reason);
            active = false;
            // Notify owner of commiting of process into database.
            Employee originalOwner = getOriginalOwner();
            sendCancellationNotifications(originalOwner, reason);
            FileContainer fc = za.co.ucs.accsys.webmodule.FileContainer.getInstance();
            fc.updateProcessInDerbyAndSybase(this);

        }
    } // end cancelProcess

    private void sendCancellationNotifications(Employee originalOwner, String reason) {
        // We have a new owner to forward the process to
        // Notify original owner of status
        String message;
        if (originalOwner.equals(employee)) {
            message = "<h4>Dear " + getOriginalOwner().toString() + "</h4>"
                    + "<br>The following ESS Request has been declined:<br><i>" + this.toHTMLString() + "</i>"
                    + "<br><br>Reason:" + reason;
        } else {
            message = "<h4>Dear " + employee.toString() + "</h4>"
                    + "<br>The following ESS Request (logged on your behalf by " + getOriginalOwner().toString() + ") has been declined:<br><i>" + this.toHTMLString() + "</i>"
                    + "<br><i>Reason:</i>" + reason;
            notifyEmployee(employee, message);

            message = "<h4>Dear " + getOriginalOwner().toString() + "</h4>"
                    + "<br>The following ESS Request (logged by you on behalf of " + employee.toString() + ") has been declined:<br><i>" + this.toHTMLString() + "</i>"
                    + "<br><i>Reason:</i>" + reason;
        }

        notifyEmployee(originalOwner, message);

        // CR 6909: Notify all employees that were part of this process
        Iterator iter = this.getProcessHistory().getPreviousProcessOwners().iterator();
        while (iter.hasNext()) {
            Employee previousOwner = (Employee) (iter.next());
            if (!previousOwner.equals(originalOwner)) {
                notifyEmployee(previousOwner, "<h4>Dear " + previousOwner.toString() + "</h4>"
                        + "<br>The following ESS Request - for " + employee.toString() + " - has been declined:<br><i>" + this.toHTMLString() + "</i>"
                        + "<br><i>Reason:</i>" + reason);
            }
        }
    }

    /**
     * The process timedOut
     *
     * @param reason
     */
    public void timeOutProcess(String reason) {
        if (isActive()) {
            getCurrentStage().timeout(reason);
            escalateProcess(et_TIMEOUT, reason);
        }
    }

    /**
     * The process is accepted by the current owner
     *
     * @param reason
     */
    public void acceptProcess(String reason) {
        if (isActive()) {
            notifyEmployee(getCurrentStage().getOwner(),
                    "<h4>Dear " + getCurrentStage().getOwner().toString() + "</h4>"
                    + "<br>The following ESS Request - for " + employee.toString() + " - has been actioned by you on " + (new Date()) + "<br><i>" + this.toHTMLString() + "</i>");
            getCurrentStage().accept(reason);
            escalateProcess(et_ACCEPT, reason);
        }
    }

    /**
     * This method must be implemented by the descendent classes
     *
     * @return
     */
    public abstract ValidationResult validate();

    /**
     * Dynamically generated SQL statement that will perform the commit at the
     * end of the process.
     */
    abstract String getCommitStatement();

    /**
     * Returns a descriptive string assigned to this type of process.
     */
    @Override
    public abstract String toString();

    /**
     * Returns an HTML formatted version of toString()
     *
     * @return
     */
    public abstract String toHTMLString();

    public abstract String getProcessName();

    /**
     * Returns the status of this process
     *
     * @return
     */
    public String getStatus() {
        if (this.isActive()) {
            return ("Active");
        } else {
            return ("Closed");
        }
    }

    @Override
    public int hashCode() {
        int result;
        // Owner of Process
        int v1 = this.employee.getEmployeeID().intValue();

        // Addresses Compatibility with previous version of WebProcess
        if (classVersion == 2) { // August 2012 and later
            String aCode; //employee_id[3]||time(ms)[5]
            // EmployeeID
            String emID = "00000".concat(this.getEmployee().getEmployeeID().toString());
            emID = emID.substring(emID.length() - 3, emID.length());
            String createdMS = new Long(this.creationDate.getTime()).toString();
            createdMS = createdMS.substring(createdMS.length() - 6, createdMS.length());

            aCode = emID.concat(createdMS);

            result = new Integer(aCode).intValue();

        } else {  // Before August 2012
            long v2 = new Long(this.creationDate.getTime() - (30 * 365 * 24 * 60 * 60 * 1000)).longValue();
            v2 = v2 % Integer.MAX_VALUE;

            int ans = new Float(v2).intValue() + v1;
            if (v2 > Integer.MAX_VALUE) {
                ans = ans - Integer.MAX_VALUE;
            }
            result = ans;

        }

        //System.out.println("hashCode:" + result);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WebProcess other = (WebProcess) obj;
        if (this.creationDate != other.creationDate && (this.creationDate == null || !this.creationDate.equals(other.creationDate))) {
            return false;
        }
        return this.employee == other.employee || (this.employee != null && this.employee.equals(other.employee));
    }

    /**
     * Is this Web Process still active? WebProcesses gets deactivated by a
     * 'cancelProcess()'
     *
     * @return
     */
    public boolean isActive() {
        return this.active;
    }

    /**
     * Returns the employee on whom this Process is based.
     *
     * @return
     */
    public za.co.ucs.accsys.peopleware.Employee getEmployee() {
        return employee;
    }

    /**
     * Returns the employee who logged the request.
     *
     * @return
     */
    public za.co.ucs.accsys.peopleware.Employee getLogger() {
        Employee result;
        if (getProcessHistory().getProcessStages().size() > 0) {
            if (getProcessHistory().getOriginProcessStage() == null) {
                result = currentStage.getOwner();
            } else {
                result = getProcessHistory().getOriginProcessStage().getOwner();
            }
        } else {
            result = getCurrentStage().getOwner();
        }
        return result;
    }

    /**
     * Base of toString() to be used by extended classes
     *
     * @return
     */
    protected String webProcessToString() {
        return "";
    }

// Converts the given date into our standard date format
    public String formatDate(java.util.Date date) {
        if (date != null) {
            return (new java.text.SimpleDateFormat("yyyy/MM/dd")).format(date);
        } else {
            return "Unknown";
        }
    }

    private void setCurrentStage(WebProcessStage newStage) {
        this.currentStage = newStage;
        // Notify new owner of pending process that must be taken care of.
        String baseURL = WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Web, "");
        String link = "<a href=\"" + baseURL + "processes/proc_action.jsp?hashValue=" + this.hashCode() + "\" target=\"_blank\">" + this.getProcessName() + "</a>";
        Employee thisEmployee = getEmployee();
        Employee originalOwner = getOriginalOwner();
        String message;
        if (originalOwner.equals(thisEmployee)) {
            if (WebModulePreferences.getPreferences().getBoolean(WebModulePreferences.Email_Suppress_Hyperlinks, false)) {
                message = "<h4>Dear " + newStage.getOwner().toString() + "</h4>"
                        + "<br>You have been requested to handle the following ESS request:<br><i>" + this.toHTMLString() + "</i>"
                        + "<br>The original process was logged by " + getOriginalOwner().toString() + ".";
            } else {
                message = "<h4>Dear " + newStage.getOwner().toString() + "</h4>"
                        + "<br>You have been requested to handle the following ESS request:<br><i>" + this.toHTMLString() + "</i>"
                        + "<br>The original process was logged by " + getOriginalOwner().toString() + "."
                        + "<br><br>Follow this link for more information:" + link;
            }
        } else if (WebModulePreferences.getPreferences().getBoolean(WebModulePreferences.Email_Suppress_Hyperlinks, false)) {
            message = "<h4>Dear " + newStage.getOwner().toString() + "</h4>"
                    + "<br>You have been requested to handle the following ESS request:<br><i>" + this.toHTMLString() + "</i>"
                    + "<br>The original process was logged by " + getOriginalOwner().toString() + ", on behalf of " + thisEmployee.toString() + ".";
        } else {
            message = "<h4>Dear " + newStage.getOwner().toString() + "</h4>"
                    + "<br>You have been requested to handle the following ESS request:<br><i>" + this.toHTMLString() + "</i>"
                    + "<br>The original process was logged by " + getOriginalOwner().toString() + ", on behalf of " + thisEmployee.toString() + "."
                    + "<br><br>Follow this link for more information:" + link;
        }

        notifyEmployee(newStage.getOwner(), message);
        FileContainer.getInstance()
                .updateProcessInDerbyAndSybase(this);
    }

    /**
     * We need to know if an object has changed, without having to worry about
     * possible null strings.
     *
     * @param object1
     * @param object2
     * @return
     */
    public boolean isDifferent(String object1, String object2) {
        if ((object1 == null) || (object1.trim().length() == 0) || (object1.trim().compareTo("null") == 0)) {
            object1 = "[empty]";
        }

        if ((object2 == null) || (object2.trim().length() == 0) || (object1.trim().compareTo("null") == 0)) {
            object2 = "[empty]";
        }

        return ((object1.trim().compareTo(object2.trim()) != 0));
    }
    /*
     *
     * C:\Apache5\webapps\WebModule\WEB-INF\lib>c:\j2sdk1.4.2_03\bin\serialver
     * -classpath lwt.jar za.co.ucs.accsys.webmodule.WebProcess
     * za.co.ucs.accsys.webmodule.WebProcess: static final long serialVersionUID
     * = 8245584326728561585L;
     *
     *
     */
    private static final long serialVersionUID = 8245584326728561585L;
    int classVersion = 1;
    private WebProcessDefinition processDefinition;
    private final Date creationDate;
    private final Employee employee;
    private WebProcessStage currentStage;
    private WebProcessHistory processHistory;
    private boolean active;
    protected String changesPlain = null;
    protected String changesHTML = null;
    protected boolean didUpdateChanges = false;
    /*
     * <br>1. The owner of the current ProcessStage accepts the process and it
     * needs to be passed on to the next person in the hierarchy. Escalation
     * Type = et_FORWARD <br>2. The ProcessStage timed-out. Escalation Type =
     * et_TIMEOUT
     */
    public static String et_FORWARD = "FORWARD";
    public static String et_TIMEOUT = "TIMEOUT";
    public static String et_INITIAL = "INITIAL";
    public static String et_ACCEPT = "ACCEPT";
} // end Process

