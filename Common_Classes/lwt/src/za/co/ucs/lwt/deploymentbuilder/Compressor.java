/*
 * Compressor.java
 *
 * Created on November 27, 2003, 8:23 AM
 */

package za.co.ucs.lwt.deploymentbuilder;
import java.util.zip.*;
import java.util.*;
import java.io.*;

/**
 * Compressor handles the zip/unzip tasks required in creating/extracting archived files.
 * @author  liam
 */
public class Compressor{
    
    public Compressor(){
    }
    
    public void compressFile(File uncompressedFile, File compressedFile){
        try {
            FileInputStream fis = new FileInputStream(uncompressedFile);
            FileOutputStream dest = new FileOutputStream(compressedFile);
            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest));
            BufferedInputStream origin = null;
            byte data[] = new byte[BUFFER];
            
            origin = new BufferedInputStream(fis, BUFFER);
            ZipEntry entry = new ZipEntry(uncompressedFile.getName());
            out.putNextEntry(entry);
            
            int count;
            while((count = origin.read(data, 0, BUFFER)) != -1) {
                out.write(data, 0, count);
            }
            origin.close();
            out.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public void uncompressFile(File compressedFile, File uncompressedFile){
        try {
            BufferedOutputStream dest = null;
            FileInputStream fis = new FileInputStream(compressedFile);
            ZipInputStream zis = new  ZipInputStream(new BufferedInputStream(fis));
            ZipEntry entry;
            while((entry = zis.getNextEntry()) != null) {
                System.out.println("Extracting: " +entry);
                int count;
                byte data[] = new byte[BUFFER];
                // write the files to the disk
                FileOutputStream fos = new FileOutputStream(uncompressedFile);
                dest = new  BufferedOutputStream(fos, BUFFER);
                while ((count = zis.read(data, 0, BUFFER)) != -1) {
                    dest.write(data, 0, count);
                }
                dest.flush();
                dest.close();
            }
            zis.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
        
    }
    
    
    static final int BUFFER = 2048;
    
}
