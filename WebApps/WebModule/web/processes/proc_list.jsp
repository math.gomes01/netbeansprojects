<%@page contentType="text/html"%>

<%@include file="html5_top.jsp"%>

<%@include file="html_messages.jsp"%> 
<!-- This HAS to be included if messages were generated
and added to the session attributes for this person.
Calling this script will simply display those messages -->

<!-- Now we need to verify that there is a LoginSessionObject -->

<%  // Can I connect to the database?
    // We need to reset the EMPLOYEE to be the same person as the USER once the process has been committed
    if (!user.equals(employee)) {
        loginSessionObject.setEmployee(user);
    } else {
        if (loginSessionObject == null) {
            response.sendRedirect(response.encodeURL("Login.jsp"));
        } else {
            PageSectionGenerator gen = PageSectionGenerator.getInstance();
            out.write(gen.buildHTMLPageHeader("Web Processes", loginSessionObject.getUser().toString()));

            // Page Detail
            out.write("<div id=\"container\"><center>");
            out.write("<div class=\"item\">");
            PageProducer_Processes producer = new PageProducer_Processes(loginSessionObject.getUser(), false);
            out.write(producer.generateInformationContent());
            out.write("</div>");
            out.write("</center></div>");
        }
    }
%>

<%@include file="html_bottom.jsp"%>