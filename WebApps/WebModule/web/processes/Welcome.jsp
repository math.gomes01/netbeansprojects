<%@page contentType="text/html"%>
<%@page import="za.co.ucs.accsys.webmodule.*" %>
<%@include file="html5_top.jsp"%>

<%@include file="html_messages.jsp"%> 
<!-- This HAS to be included if messages were generated
and added to the session attributes for this person.
Calling this script will simply display those messages -->

<!-- Now we need to verify that there is a LoginSessionObject -->

<% // Page Top
        if (loginSessionObject == null) {
           out.write("<meta http-equiv=\"refresh\" content=\"3;Login.jsp\"> ");
        } else {
            PageSectionGenerator gen = PageSectionGenerator.getInstance();
            if (user != null) {
                out.write(gen.buildHTMLPageHeader("Welcome", loginSessionObject.getUser().toString()));
            } else {
                if (session.getAttribute("essMessage") == null) {
                    out.write(gen.buildHTMLPageHeader("Welcome to Accsys PeopleWare Self Service", "Please <b>Sign In</b> to proceed"));
                }
            }
        }

%>

<%@include file="html_bottom.jsp"%>