/*
 * This class is used in the WebModule (time_stats.jsp) to display time spent per variable over a period of time
 */
package za.co.ucs.accsys.webmodule;

import java.util.*;
import za.co.ucs.lwt.db.DatabaseObject;

/**
 *
 * @author lterblanche
 */
public class TimeSpentPerVariableHelperClass {

    private final Queue<TimeSpentPerVariable> TimeSpentPerVariableQueue = new LinkedList<>();

    public void addTimeSpentPerVariable(java.util.Date theDay, String theVariable, int minutesSpent, String timeSpentFormatted) {
        TimeSpentPerVariableQueue.add(new TimeSpentPerVariable(theDay, theVariable, minutesSpent, timeSpentFormatted));
    }

    /*
     * Returns a HashSet with the Variables (String)
     */
    public HashSet getVariables() {
        HashSet result = new HashSet();
        for (TimeSpentPerVariable anItem : TimeSpentPerVariableQueue) {
            result.add(anItem.theVariable);
        }
        return result;
    }

    /** Returns the time spent (in minutes) for a given Variable on a given date
     * @param theDay
     * @param theVariable
     * @return 
     */
    public int getMinutesSpentPerDayPerVariable(String theDay, String theVariable) {
        int result = 0;
        for (TimeSpentPerVariable ts : TimeSpentPerVariableQueue) {
            if ((ts.theVariable.equals(theVariable)) && (DatabaseObject.formatDate(ts.theDay).equals(theDay))) {
                // FailSafe
                if (ts.minutesSpent > (24 * 60 * 60)) {
                    return -(10);
                } else {
                    return ts.minutesSpent;
                }
            }
        }
        return result;
    }

    /** Returns the time spent (in minutes) for a given Variable on a given date
     * @param theDay
     * @param theVariable
     * @return 
     */
    public String getDisplayMinutesSpentPerDayPerVariable(String theDay, String theVariable) {
        String result = "";
        for (TimeSpentPerVariable ts : TimeSpentPerVariableQueue) {
            if ((ts.theVariable.equals(theVariable)) && (DatabaseObject.formatDate(ts.theDay).equals(theDay))) {
                // FailSafe
                if (ts.minutesSpent > (24 * 60 * 60)) {
                    return "Out of Range";
                } else {
                    if (ts.minutesSpent == 0) {
                        return "00:00";
                    } else {
                        return ts.timeSpentFormatted;
                    }
                }
            }
        }
        return result;
    }

    /**
     * Returns a HashSet (String) of all the unique days
     * @return 
     */
    public HashSet<String> getDays() {
        HashSet result = new HashSet();
        for (TimeSpentPerVariable anItem : TimeSpentPerVariableQueue) {
            result.add((String) DatabaseObject.formatDate(anItem.theDay));
        }
        return result;
    }

    private class TimeSpentPerVariable {

        private TimeSpentPerVariable(java.util.Date theDay, String theVariable, int minutesSpent, String timeSpentFormatted) {
            this.minutesSpent = minutesSpent;
            this.theVariable = theVariable;
            this.theDay = theDay;
            this.timeSpentFormatted = timeSpentFormatted;
        }
        private java.util.Date theDay;
        private String theVariable;
        private int minutesSpent;
        private String timeSpentFormatted;

        /**
         * @return the theDay
         */
        public java.util.Date getTheDay() {
            return theDay;
        }

        /**
         * @param theDay the theDay to set
         */
        public void setTheDay(java.util.Date theDay) {
            this.theDay = theDay;
        }

        /**
         * @return the theVariable
         */
        public String getTheVariable() {
            return theVariable;
        }

        /**
         * @param theVariable the theVariable to set
         */
        public void setTheVariable(String theVariable) {
            this.theVariable = theVariable;
        }

        /**
         * @return the minutesSpent
         */
        public int getMinutesSpent() {
            return minutesSpent;
        }

        /**
         * @param minutesSpent the minutesSpent to set
         */
        public void setMinutesSpent(int minutesSpent) {
            this.minutesSpent = minutesSpent;
        }

        /**
         * @return the timeSpentFormatted
         */
        public String getTimeSpentFormatted() {
            return timeSpentFormatted;
        }

        /**
         * @param timeSpentFormatted the timeSpentFormatted to set
         */
        public void setTimeSpentFormatted(String timeSpentFormatted) {
            this.timeSpentFormatted = timeSpentFormatted;
        }
    }
}
