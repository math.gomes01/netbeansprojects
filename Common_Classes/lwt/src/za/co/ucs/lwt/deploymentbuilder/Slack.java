package za.co.ucs.lwt.deploymentbuilder;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Business layer of the Slack entitity.
 * <P>
 * Singleton
 *
 * @author Liam Terblanche Note: By its nature, E_Mails need only be created.
 * The saving and handling of the object should thereafter be done automatically
 */
public class Slack {

    private Slack() {
        URL = DeploymentBuilderPreferences.getInstance().getPreference(DeploymentBuilderPreferences.SLACK_URL, "");
    }

    /**
     * Returns the static instance of Slack
     *
     * @return Slack
     */
    public static Slack getInstance() {
        if (slack == null) {
            slack = new Slack();
        }
        return slack;
    }

    /**
     * Sends a message using the SLACK API to specified address Returns true if successful.
     * The address is read from a
     * configuration file called 'dbPreferences.xml'
     *
     * @param body
     * @return
     */
    public boolean send(String body) {
        boolean rslt;
        try {
            String Data = "{\"text\": \"" + body + "\"}";
            URL url = new URL(this.URL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setDoOutput(true);
            conn.getOutputStream().write(Data.getBytes("UTF-8"));
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println(output);
            }

            conn.disconnect();
            rslt = true;

        } catch (Exception e) {
            rslt = false;
            //e.printStackTrace();
            System.out.println("\n   ***   Slack notification failed:" + new java.util.Date() + "\n" + e);
        }
        return rslt;
    }

    static String URL;
    static Slack slack = null;
}
