/*
 * TRProvider.java
 *
* Created on October 19 2006
 */

package za.co.ucs.accsys.peopleware;
import za.co.ucs.lwt.db.*;
import java.sql.*;

/**
 * TRProvider maps to dba.SD_PROVIDER
 * @Author  liam
 */
public final class TRProvider implements AccsysObjectInterface, java.io.Serializable {
    
    /** Creates an instance of an existing provider in SD_PROVIDER
     * @param providerID As the name suggests. 
     */
    public TRProvider(Integer providerID) {
        this.setProviderID(providerID);
        if (isValid())
            loadFromDB();
    }
    
    public void applyToDB() {
        // Not implemented for SD_PROVIDER.  To be managed through the Skills Development Module only.
    }
    
    public void loadFromDB() {
        if (this.getProviderID() == null)
            return;
        else {
            Connection con = null;
            try{
                con = DatabaseObject.getNewConnectionFromPool();
                ResultSet rs = DatabaseObject.openSQL("SELECT * FROM TR_PROVIDER with (nolock) where provider_id="+this.getProviderID(), con);
                if (rs.next()){
                    this.setAccreditationNumber(rs.getString("ACCREDITATION_NUMBER"));
                    this.setContactName(rs.getString("CONTACT_NAME"));
                    this.setEMail(rs.getString("E_MAIL"));
                    this.setFaxNumber(rs.getString("FAX_NUMBER"));
                    this.setIsAccredited(rs.getBoolean("ACCREDITED"));
                    this.setIsAssessmentProvider(rs.getBoolean("ASSESSMENT_PROVIDER"));
                    this.setIsCompliant(rs.getBoolean("ISCOMPLIANT"));
                    this.setIsInternal(rs.getBoolean("INTERNAL"));
                    this.setIsTrainingProvider(rs.getBoolean("TRAINING_PROVIDER"));
                    this.setName(rs.getString("NAME"));
                    this.setProviderID(new Integer(rs.getInt("PROVIDER_ID")));
                    this.setRegNumber(rs.getString("REG_NUMBER"));
                    this.setTelNumber(rs.getString("TEL_NUMBER"));
                    
                }
            }
            catch (SQLException e){
                e.printStackTrace();
            }
            finally{
                DatabaseObject.releaseConnection(con);
            }
        }
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public String getName(){
        return this.name;
    }
    
    public boolean isValid() {
        return ((this.getProviderID() != null));
    }

    @Override
    public String toString(){
      return ("Provider:"+getName());
    }

    private Integer providerID; //
    private boolean isInternal; //
    private boolean isAccredited; //
    private boolean isCompliant; //
    private boolean isTrainingProvider;
    private boolean isAssessmentProvider;
    private String name;
    private String regNumber;
    private String accreditationNumber;
    private String contactName;
    private String telNumber;
    private String faxNumber;
    private String eMail;

    public Integer getProviderID() {
        return providerID;
    }

    public void setProviderID(Integer providerID) {
        this.providerID = providerID;
    }

    public String getRegNumber() {
        return regNumber;
    }

    public void setRegNumber(String regNumber) {
        this.regNumber = regNumber;
    }

    public String getAccreditationNumber() {
        return accreditationNumber;
    }

    public void setAccreditationNumber(String accreditationNumber) {
        this.accreditationNumber = accreditationNumber;
    }

    /**
     * @return the isInternal
     */
    public boolean isIsInternal() {
        return isInternal;
    }

    /**
     * @param isInternal the isInternal to set
     */
    public void setIsInternal(boolean isInternal) {
        this.isInternal = isInternal;
    }

    /**
     * @return the isAccredited
     */
    public boolean isIsAccredited() {
        return isAccredited;
    }

    /**
     * @param isAccredited the isAccredited to set
     */
    public void setIsAccredited(boolean isAccredited) {
        this.isAccredited = isAccredited;
    }

    /**
     * @return the isCompliant
     */
    public boolean isIsCompliant() {
        return isCompliant;
    }

    /**
     * @param isCompliant the isCompliant to set
     */
    public void setIsCompliant(boolean isCompliant) {
        this.isCompliant = isCompliant;
    }

    /**
     * @return the isTrainingProvider
     */
    public boolean isIsTrainingProvider() {
        return isTrainingProvider;
    }

    /**
     * @param isTrainingProvider the isTrainingProvider to set
     */
    public void setIsTrainingProvider(boolean isTrainingProvider) {
        this.isTrainingProvider = isTrainingProvider;
    }

    /**
     * @return the isAssessmentProvider
     */
    public boolean isIsAssessmentProvider() {
        return isAssessmentProvider;
    }

    /**
     * @param isAssessmentProvider the isAssessmentProvider to set
     */
    public void setIsAssessmentProvider(boolean isAssessmentProvider) {
        this.isAssessmentProvider = isAssessmentProvider;
    }

    /**
     * @return the contactName
     */
    public String getContactName() {
        return contactName;
    }

    /**
     * @param contactName the contactName to set
     */
    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    /**
     * @return the telNumber
     */
    public String getTelNumber() {
        return telNumber;
    }

    /**
     * @param telNumber the telNumber to set
     */
    public void setTelNumber(String telNumber) {
        this.telNumber = telNumber;
    }

    /**
     * @return the faxNumber
     */
    public String getFaxNumber() {
        return faxNumber;
    }

    /**
     * @param faxNumber the faxNumber to set
     */
    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    /**
     * @return the eMail
     */
    public String getEMail() {
        return eMail;
    }

    /**
     * @param eMail the eMail to set
     */
    public void setEMail(String eMail) {
        this.eMail = eMail;
    }
}
