CREATE OR REPLACE PROCEDURE "DBA"."sp_ESS_TaxCalc"
  /*  
  <purpose>  
  sp_ESS_TaxCalc calculates the tax for the given employee input
  </purpose>  
  <history>
  Date          CR#     Programmer      Changes
  ----          ---     ----------      -------
  16/01/2014    13827    Francois      Original
  </history>
  */
-------------------------------------------------------------------------------
-- INPUTS
-------------------------------------------------------------------------------
(
in @inGROSS_INCOME numeric(10,2),
in @inRETIREMENT_FUND_INCOME numeric(10,2),
in @inNON_RETIREMENT_FUND_INCOME numeric(10,2),
in @inANNUAL_BONUS numeric(10,2),
in @inNO_OF_PAY_PERIODS_IN_YEAR integer,
in @inTAX_YEAR_END_DATE char(9),
in @inEmployee_Age integer,
in @inTravel_Allowance numeric(10,2),
in @inOther_Allowance numeric(10,2),
in @inPENSION numeric(10,2),
in @inRETIREMENT numeric(10,2),
in @inPROVIDENT numeric(10,2),
in @inINCOME_REPLACEMENT numeric(10,2),
in @inHasMedical integer,
in @inNumberOfDependants integer,
in @inPersonal_Contribution numeric(10,2),
in @inCompany_Contribution numeric(10,2)
)
result
(
	TOTAL_PAYE numeric(10,2),
	TOTAL_PAYE_BONUS numeric(10,2),
	ANNUAL_TAX numeric(10,2),
	ANNUAL_TAX_BONUS numeric(10,2),
	MEDICAL_TAX_CREDIT numeric(10,2)
)

begin
-------------------------------------------------------------------------------
-- VARIABLES
-------------------------------------------------------------------------------
declare @outTOTAL_PAYE numeric(10,2);
declare @outTOTAL_PAYE_BONUS numeric(10,2);
declare @outANNUAL_TAX numeric(10,2);
declare @outANNUAL_TAX_BONUS numeric(10,2);
declare @outMEDICAL_TAX_CREDIT numeric(10,2);
declare @TAX_YEAR_END date;
declare @PENSION numeric(10,2);
declare @PAYROLL_TYPE char(15);
declare @ANNUALISED_EARNINGS numeric(10,2);
declare @ANNUALISED_EARNINGS_BONUS numeric(10,2);
declare @TAXABLE_EARNINGS numeric(10,2);
declare @DEDUCTIONS numeric(10,2);
declare @RA_MAX_RULE1 numeric(10,2);
declare @RA_MAX_RULE2 numeric(10,2);
declare @RA_MAX_RULE3 numeric(10,2);
declare @RA_MAX numeric(10,2);
declare @RA_DEDUCTION numeric(10,2);

-------------------------------------------------------------------------------
-- INITIALISATIONS
-------------------------------------------------------------------------------
set @outTOTAL_PAYE = 0;
set @outTOTAL_PAYE_BONUS = 0;
set @outANNUAL_TAX = 0;
set @outANNUAL_TAX_BONUS = 0;
set @outMEDICAL_TAX_CREDIT = 0;
set @TAX_YEAR_END = '2014/02/28';
set @PENSION = 0;
set @PAYROLL_TYPE = 'monthly';
set @ANNUALISED_EARNINGS = 0;
set @ANNUALISED_EARNINGS_BONUS = 0;
set @TAXABLE_EARNINGS = 0;
set @DEDUCTIONS = 0;
set @RA_MAX_RULE1 = 0;
set @RA_MAX_RULE2 = 0;
set @RA_MAX_RULE3 = 0;
set @RA_MAX = 0;
set @RA_DEDUCTION = 0;

-------------------------------------------------------------------------------
set @TAX_YEAR_END = "date"(right(@inTAX_YEAR_END_DATE,8));
-------------------------------------------------------------------------------
-- PENSION
-------------------------------------------------------------------------------
if @inRETIREMENT_FUND_INCOME > 0 then
    set @PENSION = lesser(coalesce(@inPENSION,0),greater((@inRETIREMENT_FUND_INCOME * 0.075),(1750/@inNO_OF_PAY_PERIODS_IN_YEAR)));
endif;
-------------------------------------------------------------------------------
-- RETIREMENT ANNUITY
-------------------------------------------------------------------------------
if @inNON_RETIREMENT_FUND_INCOME > 0 then
  set @RA_MAX_RULE1 = 1750/@inNO_OF_PAY_PERIODS_IN_YEAR;
  set @RA_MAX_RULE2 = 3500/@inNO_OF_PAY_PERIODS_IN_YEAR - @PENSION;
  set @RA_MAX_RULE3 = (0.15 * @inNON_RETIREMENT_FUND_INCOME) + (0.15 * @inRETIREMENT_FUND_INCOME);

  set @RA_MAX =  greater(@RA_MAX_RULE1, greater(@RA_MAX_RULE2, @RA_MAX_RULE3));

  set @RA_DEDUCTION = lesser(@RA_MAX, @inRETIREMENT);
endif;
-------------------------------------------------------------------------------
-- MEDICAL AID TAX CREDIT
-------------------------------------------------------------------------------
set @PAYROLL_TYPE =
CASE @inNO_OF_PAY_PERIODS_IN_YEAR
      WHEN 12 then 'month'
      WHEN 26 then 'fortnight'
      WHEN 52 then 'week'
      ELSE 'month'
END;
   
set @outMEDICAL_TAX_CREDIT = fn_P_GetMedCreditAmount(null,null,null,null,null,0,@PAYROLL_TYPE,@inNumberOfDependants,datepart(year,@TAX_YEAR_END));
-------------------------------------------------------------------------------
-- TAX DEDUCTABLES
-------------------------------------------------------------------------------
if @inEmployee_Age > 65 then
	set @DEDUCTIONS = @PENSION + @inINCOME_REPLACEMENT + @RA_DEDUCTION + @inPersonal_Contribution + @inCompany_Contribution;
	set @outMEDICAL_TAX_CREDIT = 0;
else
	set @DEDUCTIONS = @PENSION + @inINCOME_REPLACEMENT + @RA_DEDUCTION;
endif;
-------------------------------------------------------------------------------
-- EARNINGS
-------------------------------------------------------------------------------
set @TAXABLE_EARNINGS = @inGROSS_INCOME + @inTravel_Allowance + @inOther_Allowance + @inCompany_Contribution - @DEDUCTIONS;

set @ANNUALISED_EARNINGS = @TAXABLE_EARNINGS * @inNO_OF_PAY_PERIODS_IN_YEAR;

set @ANNUALISED_EARNINGS_BONUS = @ANNUALISED_EARNINGS + @inANNUAL_BONUS;
-------------------------------------------------------------------------------
-- TAX
-------------------------------------------------------------------------------
if @inHasMedical = 1 then
		set @outANNUAL_TAX = sp_P_GetTax_SouthAfrica(1, 1, 0, 0, @ANNUALISED_EARNINGS, 1, datepart(year,@TAX_YEAR_END)) -
		  fn_P_GetRebate_SouthAfrica(1, 1, 0, 0, 1, datepart(year,@TAX_YEAR_END),@inEmployee_Age) - (@outMEDICAL_TAX_CREDIT * @inNO_OF_PAY_PERIODS_IN_YEAR); 

		set @outANNUAL_TAX_BONUS = sp_P_GetTax_SouthAfrica(1, 1, 0, 0, @ANNUALISED_EARNINGS_BONUS, 1, datepart(year,@TAX_YEAR_END)) -
		  fn_P_GetRebate_SouthAfrica(1, 1, 0, 0, 1, datepart(year,@TAX_YEAR_END),@inEmployee_Age) - (@outMEDICAL_TAX_CREDIT * @inNO_OF_PAY_PERIODS_IN_YEAR); 
ELSE 
    set @outANNUAL_TAX = sp_P_GetTax_SouthAfrica(1, 1, 0, 0, @ANNUALISED_EARNINGS, 1, datepart(year,@TAX_YEAR_END)) -
      fn_P_GetRebate_SouthAfrica(1, 1, 0, 0, 1, datepart(year,@TAX_YEAR_END),@inEmployee_Age); 
	  
    set @outANNUAL_TAX_BONUS = sp_P_GetTax_SouthAfrica(1, 1, 0, 0, @ANNUALISED_EARNINGS_BONUS, 1, datepart(year,@TAX_YEAR_END)) -
      fn_P_GetRebate_SouthAfrica(1, 1, 0, 0, 1, datepart(year,@TAX_YEAR_END),@inEmployee_Age); 
endif;	  
    set @outTOTAL_PAYE = @outANNUAL_TAX / @inNO_OF_PAY_PERIODS_IN_YEAR;

    set @outTOTAL_PAYE_BONUS = @outANNUAL_TAX_BONUS - @outANNUAL_TAX;
-------------------------------------------------------------------------------
-- OUTPUT
-------------------------------------------------------------------------------
if @outANNUAL_TAX < 0 then
    set @outANNUAL_TAX = 0.00;
endif;

if @outANNUAL_TAX_BONUS < 0 then
    set @outANNUAL_TAX_BONUS = 0.00;
    set @outTOTAL_PAYE_BONUS = 0.00;
endif;

if @outTOTAL_PAYE < 0 then
    set @outTOTAL_PAYE = 0.00;
endif;

if @outTOTAL_PAYE_BONUS < 0 then
    set @outTOTAL_PAYE_BONUS = 0.00;
endif;

select
  @outTOTAL_PAYE,
  @outTOTAL_PAYE_BONUS,
  @outANNUAL_TAX,
  @outANNUAL_TAX_BONUS,
  @outMEDICAL_TAX_CREDIT
end;

commit;