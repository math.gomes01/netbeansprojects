<%@page contentType="text/html"%>

<%@include file="html5_top.jsp"%>

<%@include file="html_messages.jsp"%> 

<!-- This HAS to be included if messages were generated
and added to the session attributes for this person.
Calling this script will simply display those messages -->

<!-- Now we need to verify that there is a LoginSessionObject -->
<%  // Page Top
    if (loginSessionObject == null) {
        response.sendRedirect(response.encodeURL("Login.jsp"));
    } else {
        boolean multiEmployeeSelection = false; // Should we allow for the user to select more than one employee?

        PageSectionGenerator gen = PageSectionGenerator.getInstance();
        String selectAllAction = request.getParameter("action");
        String leaveView = request.getParameter("leaveView");
        if (request.getParameter("MultiEmployee") != null) {
            multiEmployeeSelection = (request.getParameter("MultiEmployee").compareToIgnoreCase("true") == 0);
        }
        if (multiEmployeeSelection) {
            out.write(gen.buildHTMLPageHeader("Multiple Employee Selection", employee.toString()));
        } else {
            out.write(gen.buildHTMLPageHeader("Employee Selection", employee.toString()));
        }

        // Page Detail
        String refDate = request.getParameter("referenceDate"); // for which period?
        String webProcessClassName = request.getParameter("webProcessClassName"); // Which WebProcess is being used?
        
        // Clear the previous employee selections
        request.getSession().removeAttribute("selectedEmployeeHashCodes") ;

        //
        // Group of selections 'under' this one in the form of a TreeList
        //
        EmployeeSelection displaySelection = null;  // The displaySelection is the class used to generate the detailed information of the specific page.

        // We also need to traverse the sub-groups to instantiate Get a list of all the selections underneath the current user.
        // We need this to forward the appropriate selected selection
        // Page Detail
        WebProcessDefinition webProcessDefinition = za.co.ucs.accsys.webmodule.FileContainer.getInstance().getWebProcessDefinition(webProcessClassName);

        if (webProcessDefinition != null) {
            PageProducer_EmployeeSelection producer = null;
            out.write("<center><div id=\"container\">");
            out.write("<div class=\"item\" style=\"min-width:50%; max-width:85%;\">");

            //////////////////////////////////////////////////////////////////////////////
            availableProcesses = new java.util.ArrayList(fileContainer.getWebProcessDefinitionsWithSubordinates(loginSessionObject.getUser()).values());
            // Sort by name
            Collections.sort(availableProcesses);
            LinkedList<Employee> allChildEmployees = new LinkedList();
            for (int j = 0; j < availableProcesses.size(); j++) { // Get the next process
                webProcessDefinition = (WebProcessDefinition) availableProcesses.get(j);
                if (webProcessDefinition.getProcessClassName().contains(za.co.ucs.accsys.webmodule.FileContainer.getInstance().getWebProcessDefinition(webProcessClassName).getProcessClassName())) {
                    if (fileContainer.getSubordinates(user, webProcessDefinition).size() > 0) {

                        boolean isBeforeValidPeriod = ((webProcessDefinition.getActiveFromDayOfMonth() > 0) && (webProcessDefinition.getActiveFromDayOfMonth() > DatabaseObject.getDayOfMonth(new java.util.Date())));
                        boolean isAfterValidPeriod = ((webProcessDefinition.getActiveToDayOfMonth() > 0) && (webProcessDefinition.getActiveToDayOfMonth() < DatabaseObject.getDayOfMonth(new java.util.Date())));
                        if ((isBeforeValidPeriod) || (isAfterValidPeriod)) {
                            System.out.println("*** Process outside valid period ***:" + webProcessDefinition + " valid between " + webProcessDefinition.getActiveFromDayOfMonth() + " and " + webProcessDefinition.getActiveToDayOfMonth() + " of the month.");
                            continue;
                        }

                        // Add all child emplyees to the list
                        Collection<Employee> childEmployeesInStructure = fileContainer.getSubordinates(user, webProcessDefinition).values();
                        // Only add each emplyee once
                        for (Employee emp : childEmployeesInStructure) {
                            if (!allChildEmployees.contains(emp)) {
                                allChildEmployees.add(emp);
                            }
                        }
                    }
                }
            }
            //////////////////////////////////////////////////////////////////////////////
            producer = new PageProducer_EmployeeSelection(user, webProcessClassName, request.getSession(), multiEmployeeSelection, selectAllAction, allChildEmployees, leaveView);
            out.write(producer.generateInformationContent());

            out.write("</div>");
            out.write("</div></center>");
        } else {
            MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                    "No Web Process Definition available.", true, request.getSession());
            producer.setMessageInSessionObject();
            out.write("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");
        }
    }
%>

<%@include file="html_bottom.jsp"%>