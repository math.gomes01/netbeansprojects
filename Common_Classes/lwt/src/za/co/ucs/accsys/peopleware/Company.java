/*
 * Company.java
 *
 * Created on January 15, 2004, 12:34 PM
 */
package za.co.ucs.accsys.peopleware;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import za.co.ucs.lwt.db.DatabaseObject;
/**
 * Company maps to dba.C_MASTER
 * @author  liam
 */
public final class Company implements AccsysObjectInterface, java.io.Serializable {

    static final long serialVersionUID = -8702285259074777839L;   
    
    /** Creates a new instance of Company
     * <br>If companyID = null, a new Company records will be created.
     * @param companyID
     */
    public Company(Integer companyID) {
        this.companyID = companyID;        
        if (isValid()) {
            loadFromDB();
        }
    }

    @Override
    public void applyToDB() {
        if (!isValid()) {
            System.out.println("Unable to save company values to db. No COMPANY_ID");
        } else {
            Connection con = null;
            try {
                con = DatabaseObject.getNewConnectionFromPool();
                String updateSQL = "update c_master set name='" + getName() + "', TAX_NUMBER='"
                        + getTaxNumber() + "' where company_id=" + companyID;
                DatabaseObject.executeSQL(updateSQL, con);
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DatabaseObject.releaseConnection(con);
            }
        }
    }

    @Override
    public void loadFromDB() {
        if (this.companyID == null) {
        } else {
            Connection con = null;
            try {
                con = DatabaseObject.getNewConnectionFromPool();
                ResultSet rs = DatabaseObject.openSQL("select * from c_master with (nolock) where company_id=" + this.companyID, con);
                //System.out.println("reading from database...");
                if (rs.next()) {
                    this.name = rs.getString("NAME");
                    this.taxNumber = rs.getString("TAX_NUMBER");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DatabaseObject.releaseConnection(con);
            }
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTaxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    public String getName() {
        return this.name;
    }

    public String getTaxNumber() {
        return this.taxNumber;
    }

    public Integer getCompanyID() {
        return this.companyID;
    }

    @Override
    public boolean isValid() {
        return (this.companyID != null);
    }

    /** Returns a LinkedList of type BankAccount
     * @return 
     */
    public LinkedList getBankAccounts() {
        LinkedList result = new LinkedList();
        Connection con = DatabaseObject.getNewConnectionFromPool();

        try {
            ResultSet rs = DatabaseObject.openSQL("select account_id from c_bankdet with (nolock) where "
                    + "Company_id=" + this.getCompanyID()
                    + " order by account_name", con);
            while (rs.next()) {
                BankAccount account = new BankAccount(this, rs.getInt(1));
                result.add(account);
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return result;
    }    
    /**
     * Loads a list of Passport Countries as stored in A_PASSPORT_COUNTRYLIST
     * @return LinkedList of Strings (country names) contained in A_PASSPORT_COUNTRYLIST
     */
    
    public LinkedList getPassportCountryNameList() {
        LinkedList result = new LinkedList();
        Connection con = DatabaseObject.getNewConnectionFromPool();
        try {
            ResultSet rs = DatabaseObject.openSQL("select COUNTRY from A_PASSPORT_COUNTRYLIST with (nolock) order by COUNTRY", con);
            while (rs.next()) {
                //add entries (counrty names) to to the LinkedList
                result.add(rs.getString(1));                                
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return result;        
    }    
    private final Integer companyID;
    private String name;
    private String taxNumber;   
}
