package za.co.ucs.ess;

import javax.servlet.http.HttpSession;

/**
 * <p>
 * MessageGenerator inherits from PageProducer and will be used as a standard
 * message display
 * </p>
 */
public class MessageGenerator{
    
    /** Constructor
     * @param messageType msg_INFO or msg_ERROR
     * @param message Message to be displayed
     * @param goBack adds a link to go back to previous screen
     */
    public MessageGenerator(int messageType, String message, boolean goBack, HttpSession sessionObject){
        this.messageType = messageType;
        this.message = message;
        this.goBack = goBack;
        this.sessionObject = sessionObject;

    }
    
    /** Sets up the message in a session attribute called "essMessage"
     *  This message gets displayed as part of the html_top.jsp script
     */
    public void setMessageInSessionObject() {
        StringBuilder resultPage = new StringBuilder();
        
        resultPage.append("<TABLE id=\"htmlPageHeader\"><tr><td>").append(message).append("</td></tr></table>");
        
        if (this.goBack){
            resultPage.append("<TABLE id=\"htmlPageHeader\"><tr><td>");                        
            resultPage.append("Please <a href=\"javascript:history.back()\"> go back</a> to previous page</td></tr></table>");            
        }

            this.sessionObject.setAttribute("essMessage", resultPage.toString());
    }
    

    
    private String message;
    private int messageType;
    private boolean goBack;
    private HttpSession sessionObject;
    
    // Message Types
    public static int msg_INFO = 0;
    public static int msg_ERROR = 1;
} // end LeavePageProducer



