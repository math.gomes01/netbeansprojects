<%@page import="org.apache.commons.fileupload.util.Streams"%>
<%@page contentType="text/html"%>
<%@ page import="java.io.*,java.util.*, javax.servlet.*" %>
<%@ page import="javax.servlet.http.*" %>
<%@ page import="org.apache.commons.fileupload.*" %>
<%@ page import="org.apache.commons.fileupload.disk.*" %>
<%@ page import="org.apache.commons.fileupload.servlet.*" %>
<%@ page import="org.apache.commons.io.output.*" %>
<%@page import="javazoom.upload.*" %>
<%@page import="za.co.ucs.lwt.deploymentbuilder.*" %>
<%--<jsp:useBean id="upBean" scope="page" class="javazoom.upload.UploadBean"></jsp:useBean>--%> 



<%
    File file;
    int maxFileSize = 1024 * 1000 * 20;
    int maxMemSize = 1024 * 1000 * 20;
    String savePath = FileContainer.getTemporaryFolder().getAbsolutePath();

    ServletContext context = pageContext.getServletContext();
    ChangeRequest alternativeChangeRequest = null;
    LinkedList<String> filesAlreadyInUse = new LinkedList();
    FileContainer fileContainer = FileContainer.getInstance(FileContainer.getContainerFile());
    ChangeRequest changeRequest = null;
    // Get Change Request
    if (session.getAttribute("crnumber") != null) {
        changeRequest = new ChangeRequest((String) session.getAttribute("crnumber"));
    }

    // Verify the content type
    String contentType = request.getContentType();
    if ((contentType.indexOf("multipart/form-data") >= 0)) {
        DiskFileItemFactory factory = new DiskFileItemFactory();
        // maximum size that will be stored in memory
        factory.setSizeThreshold(maxMemSize);
        // Location to save data that is larger than maxMemSize.
        factory.setRepository(new File(savePath));

        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);
        // maximum file size to be uploaded.
        upload.setSizeMax(maxFileSize);
        try {
            // Parse the request to get file items.
            List fileItems = upload.parseRequest(request);
            // Process the uploaded file items
            Iterator i = fileItems.iterator();
            int installType = InstallGroup.PROGRAM;

            while (i.hasNext()) {
                FileItem fi = (FileItem) i.next();

                if (fi.isFormField()) {
                    // What type of file is this?
                    String fileType = Streams.asString(fi.getInputStream());
                    if (fileType.compareToIgnoreCase("Program File") == 0) {
                        installType = InstallGroup.PROGRAM;
                    }
                    if (fileType.compareToIgnoreCase("Script") == 0) {
                        installType = InstallGroup.SCRIPT;
                    }
                    if (fileType.compareToIgnoreCase("Dll") == 0) {
                        installType = InstallGroup.DLL;
                    }
                    if (fileType.compareToIgnoreCase("Config") == 0) {
                        installType = InstallGroup.CONFIG;
                    }
                    if (fileType.compareToIgnoreCase("Help") == 0) {
                        installType = InstallGroup.HELP;
                    }
                    if (fileType.compareToIgnoreCase("Utility") == 0) {
                        installType = InstallGroup.UTILITY;
                    }
                    if (fileType.compareToIgnoreCase("XML") == 0) {
                        installType = InstallGroup.XML;
                    }
                    if (fileType.compareToIgnoreCase("Report") == 0) {
                        installType = InstallGroup.REPORT;
                    }
                    if (fileType.compareToIgnoreCase("Comms") == 0) {
                        installType = InstallGroup.COMMS;
                    }
                    if (fileType.compareToIgnoreCase("System") == 0) {
                        installType = InstallGroup.SYSTEM;
                    }
                }
                if (!fi.isFormField()) {
                    // Get the uploaded file parameters
                    String fieldName = fi.getFieldName();
                    String newFileName = fi.getName();
                    boolean isInMemory = fi.isInMemory();
                    long sizeInBytes = fi.getSize();
                    // Can we use this file, or is it already used in another CR?
                    if (!fileContainer.isFileNameInUse(newFileName)) {
                        // Write the file
                        File uploadedFile = new File(savePath + FileContainer.getOSFileSeperator() + newFileName);
                        fi.write(uploadedFile);
                        QAFile qaFile = new QAFile(uploadedFile, installType);

                        TestInstallation testInstallation = fileContainer.getTestInstallation(changeRequest.getCRNumber());
                        if (testInstallation == null) {
                            testInstallation = new TestInstallation(changeRequest.getCRNumber(), changeRequest);
                        }

                        // Register
                        fileContainer.registerQAFile(qaFile, testInstallation);
                    } else {
                        filesAlreadyInUse.add(newFileName);
                    }
                }
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }
    } else {
        // Return to previous page
        response.sendRedirect("Development_addCRFile.jsp?crnumber=" + changeRequest.getCRNumber());
    }

    // Are there any files that had problems, i.e. already assigned to another CR?
    if (filesAlreadyInUse.size() == 0) {
        // Return to previous page
        response.sendRedirect("Development_addCRFile.jsp?crnumber=" + changeRequest.getCRNumber());
    } else {
%>
<%@include file="../html/top.html"%>

<CENTER>
    <BR>
    <H3>Accsys Deployment Builder - Add a file to a TestInstallation</H3>
    <BR>

    <!-- Hyper links -->
    <%@ include file="../html/top_hyperlinks.txt" %>
    <%  Iterator iter = filesAlreadyInUse.iterator();
            while (iter.hasNext()) {
                String usedFile = (String) iter.next();
                out.write("<br><i>" + usedFile
                        + "</i> is already in use by another Change Request:<b><a href=\"ChangeRequestInfo.jsp?crnumber=" + fileContainer.getCRContainingFile(usedFile) + "\">" + fileContainer.getCRContainingFile(usedFile) + "</a></b>");
            }

            out.write("</CENTER>");
        }
    %>
