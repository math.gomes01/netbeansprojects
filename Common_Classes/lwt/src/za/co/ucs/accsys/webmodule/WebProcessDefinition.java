package za.co.ucs.accsys.webmodule;

import za.co.ucs.accsys.peopleware.Employee;

/**
 * <p>
 * A ProcessDefinition defines new available processes.  It is also used in the configuration
 * of Reporting Structures to specify which Process should use which reporting structure.
 * </p>
 */
public class WebProcessDefinition implements java.io.Serializable, java.lang.Comparable {

    /**
     * Constructor
     * @param name Name of process
     * @param description 
     * @param processClassname 
     */
    public WebProcessDefinition(String name, String description, String processClassname) {
        this.name = name;
        this.description = description;
        this.processClassName = processClassname;
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public java.lang.String getDescription() {
        return description;
    }

    public void setDescription(java.lang.String description) {
        this.description = description;
    }

    /** The reporting structure is a compulsory requirement before instances of a give
     * process can be initiated.
     * @return 
     */
    public za.co.ucs.accsys.webmodule.ReportingStructure getReportingStructure() {
        return reportingStructure;
    }

    /** The reporting structure is a compulsory requirement before instances of a give
     * process can be initiated.
     * @param reportingStructure
     */
    public void setReportingStructure(za.co.ucs.accsys.webmodule.ReportingStructure reportingStructure) {
        this.reportingStructure = reportingStructure;
    }

    /** Maximum total duration in HOURS of a process.
     * @return 
     */
    public int getMaxProcessAge() {
        return maxProcessAge;
    }

    /** Maximum total duration in HOURS of a process.
     */
    public void setMaxProcessAge(int maxProcessAge) {
        this.maxProcessAge = maxProcessAge;
    }

    /** Maximum time in HOURS that a process can linger at one stage before it will
     * automatically be escalated to the next level.
     */
    public int getMaxStageAge() {
        return maxStageAge;
    }

    /** Maximum time in HOURS that a process can linger at one stage before it will
     * automatically be escalated to the next level.
     */
    public void setMaxStageAge(int maxStageAge) {
        this.maxStageAge = maxStageAge;
    }

    /** The ProcessClassName will be used with Reflection to instantiate
     * the correct sub-class of WebProcess when new processes are created
     */
    public java.lang.String getProcessClassName() {
        return this.processClassName;
    }

    /** The ProcessClassName will be used with Reflection to instantiate
     * the correct sub-class of WebProcess when new processes are created
     */
    public void setProcessClassName(java.lang.String processClassName) {
        this.processClassName = processClassName;
    }

    @Override
    public int compareTo(Object o) {
        return this.getName().compareToIgnoreCase(o.toString());
    }

    @Override
    public String toString() {
        return this.getName();
    }

    /** Can this process be instantiated by the employee, or should
     * one of his/her managers do it on their behalf?
     */
    public boolean getCannotInstantiateSelf() {
        return cannotInstantiateSelf;
    }

    public boolean employeeInReportingStructure(Employee employee) {
        if (this.getReportingStructure() == null) {
            return false;
        }

        return (this.getReportingStructure().getEmployeeSelection(employee) != null);
    }

    /** Can this process be instantiated by the employee, or should
     * one of his/her managers do it on their behalf?
     */
    public void setCannotInstantiateSelf(boolean canInstantiateSelf) {
        this.cannotInstantiateSelf = canInstantiateSelf;
    }
    // Fingerprint of previously compatible version
    /* In order to extract this UID, run the following command in the
     * folder where the current jwt.jar file resides BEFORE changing
     * the contents of the class.
    C:\Apache5\shared\lib>c:\j2sdk1.4.2_03\bin\serialver -classpath lwt.jar za.co.ucs.accsys.peopleware.LeaveInfo
    za.co.ucs.accsys.peopleware.LeaveInfo:    static final long serialVersionUID = 1411302484629752550L;

     */
    private static final long serialVersionUID = 8388180665407229590L;
    private String name;
    private String description;
    private ReportingStructure reportingStructure;
    private int maxProcessAge = 24; // default of 8 hours
    private int maxStageAge = 8; // default 1 day
    private String processClassName;
    private int activeFromDayOfMonth = 0; // If > 0, the process will only be available from this day in the month
    private int activeToDayOfMonth = 0;
    private boolean notifyOfPendingActivation = false;
    private boolean notifyOfPendingDeactivation = false;
    private boolean cannotInstantiateSelf = true;   // Can this process be instantiated by 
    // the employee him/herself?

    /**
     * If this value > 0, the process will only be available from the given day of the month.
     * Existing processes will continue to be active until it times-out, etc.
     * @return the ActiveFromDayOfMonth
     */
    public int getActiveFromDayOfMonth() {
        return activeFromDayOfMonth;
    }

    /**
     * If this value > 0, the process will only be available from the given day of the month.
     * Existing processes will continue to be active until it times-out, etc.
     * @param ActiveFromDayOfMonth the ActiveFromDayOfMonth to set
     */
    public void setActiveFromDayOfMonth(int ActiveFromDayOfMonth) {
        this.activeFromDayOfMonth = ActiveFromDayOfMonth;
    }

    /**
     * If this value > 0, the process will only be available up to the given day of the month.
     * Existing processes will continue to be active until it times-out, etc.
     * @return the ActiveToDayOfMonth
     */
    public int getActiveToDayOfMonth() {
        return activeToDayOfMonth;
    }

    /**
     * If this value > 0, the process will only be available up to the given day of the month.
     * Existing processes will continue to be active until it times-out, etc.
     * @param ActiveToDayOfMonth the ActiveToDayOfMonth to set
     */
    public void setActiveToDayOfMonth(int ActiveToDayOfMonth) {
        this.activeToDayOfMonth = ActiveToDayOfMonth;
    }

    /**
     * @return the notifyOfPendingActivation
     */
    public boolean isNotifyOfPendingActivation() {
        return notifyOfPendingActivation;
    }

    /**
     * @param notifyOfPendingActivation the notifyOfPendingActivation to set
     */
    public void setNotifyOfPendingActivation(boolean notifyOfPendingActivation) {
        this.notifyOfPendingActivation = notifyOfPendingActivation;
    }

    /**
     * @return the notifyOfPendingDeactivation
     */
    public boolean isNotifyOfPendingDeactivation() {
        return notifyOfPendingDeactivation;
    }

    /**
     * @param notifyOfPendingDeactication
     */
    public void setNotifyOfPendingDeactivation(boolean notifyOfPendingDeactication) {
        this.notifyOfPendingDeactivation = notifyOfPendingDeactication;
    }
} // end ProcessDefinition

