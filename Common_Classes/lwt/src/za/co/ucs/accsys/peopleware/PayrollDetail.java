/*
 * PayrollDetail.java
 *
 * Created on January 15, 2004, 12:34 PM
 */
package za.co.ucs.accsys.peopleware;

import java.sql.*;
import za.co.ucs.lwt.db.*;

/**
 * PayrollDetail maps to dba.P_PAYROLLDETAIL
 * @author  liam
 */
public final class PayrollDetail implements AccsysObjectInterface, java.io.Serializable {

    static final long serialVersionUID = 4610504674427895391L;

    /** Creates a new instance of Payroll
     * @param payroll Payroll Object
     * @param payrolldetailID Period in the given payroll
     */
    public PayrollDetail(Payroll payroll, Integer payrolldetailID) {
        this.payroll = payroll;
        this.payrolldetailID = payrolldetailID;

        if (isValid()) {
            loadFromDB();
        }
    }

    /** Constructs a PayrollDetail object for the given 'Start - End' range
     */
    public PayrollDetail(Payroll payroll, String periodRange) {
        this.payroll = payroll;
        this.payrolldetailID = getPayrollDetailID(payroll, periodRange);
        if (isValid()) {
            loadFromDB();
        }
    }

    /** Locate the payroll period with the given date range
     */
    private Integer getPayrollDetailID(Payroll payroll, String periodRange) {
        Integer result = new Integer(-1);
        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            ResultSet rs = DatabaseObject.openSQL("select payrolldetail_id from p_payrolldetail with (nolock) where payroll_id=" + payroll.getPayrollID(), con);
            while (rs.next()) {
                PayrollDetail pd = new PayrollDetail(payroll, new Integer(rs.getInt(1)));
                if (pd.getPeriodRange().compareTo(periodRange) == 0) {
                    result = new Integer(rs.getInt(1));
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return result;
    }

    @Override
    public void applyToDB() {
        if (!isValid()) {
            System.out.println("Unable to save payrolldetail values to db.");
        } else {
            Connection con = null;
            try {
                con = DatabaseObject.getNewConnectionFromPool();
                StringBuilder updateSQL = new StringBuilder();
                updateSQL.append("update p_payrolldetail set ");
                updateSQL.append("STARTDATE='").append(startDate).append("', ");
                updateSQL.append("ENDDATE='").append(endDate).append("', ");
                updateSQL.append("RUNDATE='").append(runDate).append("', ");
                updateSQL.append("PAYDATE='").append(payDate).append("', ");
                updateSQL.append("MONTHEND='").append(monthEnd).append("', ");
                updateSQL.append("FINANCIAL_MONTH=").append(financialMonth).append(", ");
                updateSQL.append("COMBINE_GROUP='").append(combineGroup).append("' ");
                updateSQL.append("where payroll_id=").append(this.payroll.getPayrollID());
                updateSQL.append(" and payrolldetail_id=").append(this.payrolldetailID);
                DatabaseObject.executeSQL(updateSQL.toString(), con);
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DatabaseObject.releaseConnection(con);
            }
        }
    }

    @Override
    public void loadFromDB() {
        if (!isValid()) {
            System.out.println("Unable to load payrolldetail values from db.");
        } else {
            Connection con = null;
            try {
                con = DatabaseObject.getNewConnectionFromPool();
                ResultSet rs = DatabaseObject.openSQL("select * from p_payrolldetail with (nolock) where payroll_id=" + this.payroll.getPayrollID()
                        + " and payrolldetail_id=" + this.payrolldetailID, con);
                if (rs.next()) {
                    this.startDate = rs.getDate("STARTDATE");
                    this.endDate = rs.getDate("ENDDATE");
                    this.runDate = rs.getDate("RUNDATE");
                    this.payDate = rs.getDate("PAYDATE");
                    this.monthEnd = rs.getDate("MONTHEND");
                    this.combineGroup = rs.getString("COMBINE_GROUP");
                    this.financialMonth = new Integer(rs.getInt("FINANCIAL_MONTH"));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DatabaseObject.releaseConnection(con);
            }
        }
    }

    @Override
    public boolean isValid() {
        return ((this.payroll != null) && (this.payrolldetailID != null));
    }

    /**
     * Getter for property payroll.
     * @return Value of property payroll.
     */
    public za.co.ucs.accsys.peopleware.Payroll getPayroll() {
        return payroll;
    }

    /**
     * Getter for property payrolldetailID.
     * @return Value of property payrolldetailID.
     */
    public java.lang.Integer getPayrolldetailID() {
        return payrolldetailID;
    }

    /**
     * Getter for property startDate.
     * @return Value of property startDate.
     */
    public java.util.Date getStartDate() {
        return startDate;
    }

    /** Returns a From - To String representation of the given period
     */
    public String getPeriodRange() {
        String result = DatabaseObject.formatDate(this.startDate) + " - " + DatabaseObject.formatDate(this.endDate);
        return result;
    }

    @Override
    public boolean equals(Object object) {
        return ((this.payrolldetailID.equals(((PayrollDetail) object).getPayrolldetailID()))
                && ((this.getPayroll().getPayrollID().equals(((PayrollDetail) object).getPayroll().getPayrollID()))));
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + (this.payroll != null ? this.payroll.hashCode() : 0);
        hash = 59 * hash + (this.payrolldetailID != null ? this.payrolldetailID.hashCode() : 0);
        return hash;
    }

    /**
     * Setter for property startDate.
     * @param startDate New value of property startDate.
     */
    public void setStartDate(java.util.Date startDate) {
        this.startDate = startDate;
    }

    /**
     * Getter for property endDate.
     * @return Value of property endDate.
     */
    public java.util.Date getEndDate() {
        return endDate;
    }

    /**
     * Setter for property endDate.
     * @param endDate New value of property endDate.
     */
    public void setEndDate(java.util.Date endDate) {
        this.endDate = endDate;
    }

    /**
     * Getter for property runDate.
     * @return Value of property runDate.
     */
    public java.util.Date getRunDate() {
        return runDate;
    }

    /**
     * Setter for property runDate.
     * @param runDate New value of property runDate.
     */
    public void setRunDate(java.util.Date runDate) {
        this.runDate = runDate;
    }

    /**
     * Getter for property payDate.
     * @return Value of property payDate.
     */
    public java.util.Date getPayDate() {
        return payDate;
    }

    /**
     * Setter for property payDate.
     * @param payDate New value of property payDate.
     */
    public void setPayDate(java.util.Date payDate) {
        this.payDate = payDate;
    }

    /**
     * Getter for property monthEnd.
     * @return Value of property monthEnd.
     */
    public java.util.Date getMonthEnd() {
        return monthEnd;
    }

    /**
     * Setter for property monthEnd.
     * @param monthEnd New value of property monthEnd.
     */
    public void setMonthEnd(java.util.Date monthEnd) {
        this.monthEnd = monthEnd;
    }

    /**
     * Getter for property financialMonth.
     * @return Value of property financialMonth.
     */
    public java.lang.Integer getFinancialMonth() {
        return financialMonth;
    }

    /**
     * Setter for property financialMonth.
     * @param financialMonth New value of property financialMonth.
     */
    public void setFinancialMonth(java.lang.Integer financialMonth) {
        this.financialMonth = financialMonth;
    }

    /**
     * Getter for property combineGroup.
     * @return Value of property combineGroup.
     */
    public java.lang.String getCombineGroup() {
        return combineGroup;
    }

    /**
     * Setter for property combineGroup.
     * @param combineGroup New value of property combineGroup.
     */
    public void setCombineGroup(java.lang.String combineGroup) {
        this.combineGroup = combineGroup;
    }
    private Payroll payroll;
    private Integer payrolldetailID;
    private java.util.Date startDate;
    private java.util.Date endDate;
    private java.util.Date runDate;
    private java.util.Date payDate;
    private java.util.Date monthEnd;
    private Integer financialMonth;
    private String combineGroup;
}
