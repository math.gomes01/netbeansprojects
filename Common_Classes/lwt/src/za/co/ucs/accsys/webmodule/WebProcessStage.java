package za.co.ucs.accsys.webmodule;

import java.util.Date;
import za.co.ucs.accsys.peopleware.Employee;

/**
 * <p>
 * Each WebProcess is broken down into various stages.  A process will at any one time always
 * have only one active stage.  Upon creation of a new stage, the old stage gets written
 * to the WebProcess's ProcessHistory class
 * </p>
 */
public class WebProcessStage implements java.io.Serializable {
    /*
     C:\Apache5\webapps\WebModule\WEB-INF\lib>
     c:\j2sdk1.4.2_03\bin\serialver -classpath lwt.jar za.co.ucs.accsys.webmodule.WebProcessStage
     za.co.ucs.accsys.webmodule.WebProcessStage:    static final long serialVersionUID = -2883847192117555627L;
     */
    private static final long serialVersionUID = -2883847192117555627L;
    
    /** owner - Who is the person responsible for the current stage of the WebProcess?
     *  employeeSelection - Where in the reporting structure does this employee reside?
     * @param owner
     * @param employeeSelection
     */
    public WebProcessStage(Employee owner, EmployeeSelection employeeSelection){
        this.creationDate = new java.util.Date();
        this.owner = owner;
        this.employeeSelection = employeeSelection;
    }
    
    /**
     * Date that this stage were created
     * @return 
     */
    public java.util.Date getCreationDate() {
        return creationDate;
    }
    
    /**
     * Date that this stage were created
     * @param creationDate
     */
    public void setCreationDate(java.util.Date creationDate) {
        this.creationDate = creationDate;
    }
    
    /**
     * Owner of this stage (who is responsible for it?)
     * @return 
     */
    public za.co.ucs.accsys.peopleware.Employee getOwner() {
        return owner;
    }
    
    /**
     * Owner of this stage (who is responsible for it?)
     * @param owner
     */
    public void setOwner(za.co.ucs.accsys.peopleware.Employee owner) {
        this.owner = owner;
    }
    
    /**
     * Date when this stage was accepted/rejected
     * @return 
     */
    public java.util.Date getActionDate() {
        return actionDate;
    }
    
    @Override
    public String toString(){
        StringBuilder result = new StringBuilder();
        result.append("\n::Created:").append(getCreationDate());
        result.append("\n::Owner:").append(getOwner());
        if (getActionDate()!=null){
            result.append("\n::ActionDate:").append(getActionDate());
            result.append("\n::Action:").append(getStageAction());
            result.append("\n::Reason:").append(getStageReason());
        }
        return result.toString();
    }
    
    /**
     * "Accept", "Reject", "TimeOut"
     * @return 
     */
    public java.lang.String getStageAction() {
        return stageAction;
    }
    
    /**
     * Reason for "Accept"/"Reject"
     * @return 
     */
    public java.lang.String getStageReason() {
        return stageReason;
    }
    
    /** Accept Stage
     * @param reason
     */
    public void accept(String reason){
        this.actionDate = new Date();
        this.stageReason = reason;
        this.stageAction = sa_ACCEPT;

    }

    /** Accept Stage
     * @param reason
     */
    public void forward(String reason){
        this.actionDate = new Date();
        this.stageReason = reason;
        this.stageAction = sa_FORWARD;
    }
    
    /** Reject Stage
     * @param reason
     */
    public void reject(String reason){
        this.actionDate = new Date();
        this.stageReason = reason;
        this.stageAction = sa_REJECT;
    }
    
    /** TimedOut
     * @param reason
     */
    public void timeout(String reason){
        this.actionDate = new Date();
        this.stageReason = reason;
        this.stageAction = sa_TIMEOUT;
    }
    
    /** TimeOut
     * @param reason
     */
    public void cancel(String reason){
        this.actionDate = new Date();
        this.stageReason = reason;
        this.stageAction = sa_CANCEL;
    }
    
    /** Getter for property employeeSelection.
     * @return Value of property employeeSelection.
     *
     */
    public EmployeeSelection getEmployeeSelection() {
        return employeeSelection;
    }
    
    /** Setter for property employeeSelection.
     * @param employeeSelection New value of property employeeSelection.
     *
     */
    public void setEmployeeSelection(EmployeeSelection employeeSelection) {
        this.employeeSelection = employeeSelection;
    }
    
    private Date actionDate;
    private Date creationDate;
    private String stageAction="";
    private String stageReason="";
    private Employee owner;
    private EmployeeSelection employeeSelection;
    
    public static String sa_ACCEPT="ACCEPT";
    public static String sa_REJECT="REJECT";
    public static String sa_TIMEOUT="TIMEOUT";
    public static String sa_CANCEL="CANCEL";
    public static String sa_FORWARD="FORWARD";
    
    
} // end ProcessStage



