package za.co.ucs.ess;

import java.util.*;
import za.co.ucs.accsys.peopleware.*;
import za.co.ucs.accsys.webmodule.FileContainer;
import za.co.ucs.lwt.db.DatabaseObject;

/**
 * <p>
 * PageProducer_TrainingInfo inherits from PageProducer and will display the
 * full training history of this employee
 * </p>
 */
public class PageProducer_TrainingInfo extends PageProducer {

    /**
     * Constructor
     */
    public PageProducer_TrainingInfo(Employee employee) {
        this.employee = employee;
    }

    /**
     * A LeaveInfo Page Producer
     */
    @Override
    public String generateInformationContent() {

        StringBuilder resultPage = new StringBuilder();
        FileContainer fc = FileContainer.getInstance();
        LinkedList trainingHistory = fc.getTrainingHistoryFromContainer(this.employee);
        // Overview Table
        String[] headers = {"Course Date", "Name", "Type", "Provider", "Venue", "Result"};
        String[][] contents = new String[6][trainingHistory.size()];

        // construct the contents matrix
        for (int row = 0; row < trainingHistory.size(); row++) {
            TRHistoricalEvent record = (TRHistoricalEvent) trainingHistory.get(row);
            // Detail
            if (record.getFromDate() != null) {
                contents[0][row] = DatabaseObject.formatDate(DatabaseObject.formatDate(record.getFromDate())) + " - " + DatabaseObject.formatDate(DatabaseObject.formatDate(record.getToDate()));
            } else {
                contents[0][row] = "";
            }

            contents[1][row] = record.getCourseName();
            contents[2][row] = record.getEntityType();
            contents[3][row] = new TRProvider(record.getProviderID()).getName();
            contents[4][row] = record.getVenue();
            // If the person did not attend...
            if (record.getResultDescription() == null) {
                contents[5][row] = "<font color=red>No outcome recorded</font>";
            } else if (record.getResultDescription().compareToIgnoreCase("Not Attended") == 0) {
                // Is this training still in the future?
                if (DatabaseObject.formatDate(record.getFromDate()).after(new java.util.Date())) {
                    contents[5][row] = "<font color=red>" + record.getResultDescription() + " (future event)</font>";
                } else {
                    contents[5][row] = "<font color=red>" + record.getResultDescription() + "</font>";
                }
            } else {
                contents[5][row] = record.getResultDescription();

            }
        }
        
        resultPage.append("<center><h3 class=\"shaddow\">Training History</h3>");
        resultPage.append("<div style=\"width: 1035px;\">");
        resultPage.append(PageSectionGenerator.getInstance().buildHTMLTable("displaytable_wide", headers, contents));
        resultPage.append("</div></center>");

        return resultPage.toString();
    }
    private Employee employee;
} // end LeavePageProducer

