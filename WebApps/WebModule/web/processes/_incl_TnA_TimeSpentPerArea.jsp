
<%@page import="za.co.ucs.lwt.db.DatabaseObject"%>
<%
        // Define Chart
        //String fromDateM1 = za.co.ucs.lwt.db.DatabaseObject.formatDate( za.co.ucs.lwt.db.DatabaseObject.addNDays(new java.util.Date(), -31));
        //String toDateM1 = za.co.ucs.lwt.db.DatabaseObject.formatDate( za.co.ucs.lwt.db.DatabaseObject.addNDays(new java.util.Date(), -1));
        strXML = new String();
        strBXML = new StringBuffer();

 
        strBXML.append("<chart caption='Time spent per area ("+fromDate+" - "+toDate+") ' ");
        strBXML.append("subCaption ='"+dateDaysDiff+" days' ");
        strBXML.append("xAxisName='Day' ");
        strBXML.append("yAxisName='Time (hours)'");
        strBXML.append("startAngX='20' startAngY='-30' ");
        strBXML.append("endAngX='10' endAngY='-6' ");
        strBXML.append("showValues='1' showLabels='1' ");
        strBXML.append("shownames='1' showSum='0' overlapColumns='0' ");
        strBXML.append("showYAxisValues='1' ");
        strBXML.append("showAboutMenuItem='0' ");
        strBXML.append("bgColor='D8E6E9,FFFFFF' ");
        strBXML.append("bgAlpha='100, 100' ");
        strBXML.append("showBorder='1' ");
        strBXML.append("canvasbgAlpha='30' ");
        strBXML.append("labelDisplay='ROTATE' ");
        strBXML.append("outCnvBaseFont='Verdana' ");
        strBXML.append(">");
 
 //       strBXML.append("<chart palette='1' caption='Product Comparison' shownames='1' showvalues='0'  showSum='1' decimals='0' overlapColumns='0'>");


        za.co.ucs.lwt.db.DatabaseObject.setConnectInfo_AccsysJDBC(za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().getPreference(za.co.ucs.accsys.webmodule.WebModulePreferences.URL_Database, ""));


        try {
            sqlStatement = new StringBuffer();
            sqlStatement.append("select \"date\"(theDay) as day, AREA as theArea,");
            sqlStatement.append("TotalTimeSpent as TimeSpentInMinutes, ");
            sqlStatement.append("right('00'||round(TotalTimeSpent/60,0),2)||':'||right('00'||mod(TotalTimeSpent,60),2) as TimeSpentinMinutes_Displ ");
            sqlStatement.append("from vw_TA_TimeSpentPerAreaPerDay vw with (nolock) ");
            sqlStatement.append("where Company_id=" + employee.getCompany().getCompanyID());
            sqlStatement.append(" and person_id=" + employee.getPersonID());
            sqlStatement.append(" and day between '"+fromDate+"' and '"+toDate+"' ");
            sqlStatement.append(" and TotalTimeSpent is not null ");
            sqlStatement.append(" order by day, area ");

            java.sql.ResultSet rs = za.co.ucs.lwt.db.DatabaseObject.openSQL(sqlStatement.toString(), con);

            // Build class that will assist in the generation of this stacked graph
            za.co.ucs.accsys.webmodule.TimeSpentPerAreaHelperClass helperClass = new za.co.ucs.accsys.webmodule.TimeSpentPerAreaHelperClass();
            while (rs.next()) {
                helperClass.addTimeSpentPerArea(rs.getDate("day"), rs.getString("theArea"), rs.getInt("TimeSpentInMinutes"), rs.getString("TimeSpentinMinutes_Displ"));
            }

            // Build Categories - Every day is a category
            java.util.Queue<String> dayCategories= new java.util.LinkedList<String>();
            java.util.Date pointer = DatabaseObject.formatDate(fromDate); 
            while ( (pointer.before(DatabaseObject.formatDate(toDate))) || (pointer.equals(DatabaseObject.formatDate(toDate)))) {
                dayCategories.add(DatabaseObject.formatDate(pointer));
                pointer = DatabaseObject.addOneDay(pointer);
            }

            strBXML.append("<categories>");
            java.util.Iterator iterDays = dayCategories.iterator();
            while (iterDays.hasNext()){
                String aDay = (String)iterDays.next();
                strBXML.append("<category label='" + aDay + "'/>");
            }
            strBXML.append("</categories>");

            // Build Series - Every Area is a series
            java.util.HashSet areaSeries = helperClass.getAreas();
            java.util.Iterator iterArea = areaSeries.iterator();
            while (iterArea.hasNext()){
                String anArea = (String)iterArea.next();
                strBXML.append("<dataset seriesName='"+anArea+"' showValues='0'>");
                //
                // For every area, step through the days and get the time spent in each
                //
                java.util.Iterator dayIterator = dayCategories.iterator();
                while (dayIterator.hasNext()){
                    String day = (String)dayIterator.next();
                    int minutesSpent = helperClass.getMinutesSpentPerDayPerArea(day, anArea);
                    String minutesDisplay = helperClass.getDisplayMinutesSpentPerDayPerArea(day, anArea);
                    strBXML.append("<set toolText='"+anArea+" | "+minutesDisplay+"' value='"+minutesSpent/60+"'/>");
                }
                strBXML.append("</dataset>");
            }


            strBXML.append("<styles><definition>");
            strBXML.append("   <style name='Anim1' type='animation' param='_xscale' start='0' duration='1'/>");
            strBXML.append("   <style name='Anim2' type='animation' param='_alpha' start='0' duration='0.6'/>");
            strBXML.append("   <style name='DataShadow' type='Shadow' alpha='40'/></definition>");
            strBXML.append("<application><apply toObject='DIVLINES' styles='Anim1'/>");
            strBXML.append("<apply toObject='DATALABELS' styles='DataShadow,Anim2'/>");
            strBXML.append("<apply toObject='HGRID' styles='Anim2'/>");
            strBXML.append("</application></styles>");



            strBXML.append("</chart>");
            strXML = strBXML.toString();
            //strXML = "<chart palette='1' caption='Product Comparison' shownames='1' showvalues='0' numberPrefix='$' showSum='1' decimals='0' overlapColumns='0'><categories><category label='Product A'/><category label='Product B'/><category label='Product C'/><category label='Product D'/><category label='Product E'/></categories><dataset seriesName='2004' showValues='0'><set value='25601.34'/><set value='20148.82'/><set value='17372.76'/><set value='35407.15'/><set value='38105.68'/></dataset><dataset seriesName='2005' showValues='0'><set value='57401.85'/><set value='41941.19'/><set value='45263.37'/><set value='117320.16'/><set value='114845.27'/></dataset><dataset seriesName='2006' showValues='0'><set value='45000.65'/><set value='44835.76'/><set value='18722.18'/><set value='77557.31'/><set value='92633.68'/></dataset></chart>";
            

        } finally {
        }

%>
<DIV ALIGN="CENTER"><jsp:include page="../FCharts/Includes/FusionChartsHTMLRenderer.jsp" flush="true">
        <jsp:param name="chartSWF" value="../FCharts/FusionCharts/StackedColumn3D.swf " />
        <jsp:param name="strURL" value="" />
        <jsp:param name="strXML" value="<%=strXML%>" />
        <jsp:param name="chartId" value="myNext" />
        <jsp:param name="chartWidth" value="750" />
        <jsp:param name="chartHeight" value="400" />
        <jsp:param name="debugMode" value="false" />
        <jsp:param name="registerWithJS" value="false" />

    </jsp:include>
</DIV>

