<%@page contentType="text/html"%>
<%@page import="za.co.ucs.lwt.deploymentbuilder.*" %>

<%@include file="../html/top.html"%>


<CENTER>
    <BR>
    <H3>Accsys Deployment Builder - Error Message</H3>
    <hr width="80%" size="1" color="#c0c0c0">
    <BR>

    <!-- Hyper Links -->
    <%@ include file="../html/top_hyperlinks.txt" %>


    <%

        User user = (User) session.getAttribute("user");

        if (user == null) {
            session.setAttribute("loggedinuser", new String("false"));
            response.sendRedirect("/DeploymentBuilder/loginJsp.jsp");
        } else {
            String message = request.getParameter("message");
            out.write("<H4>" + message + "</H4>");
            out.write("<br><br>Please <a href=\"javascript:history.back()\"> go back</a> to previous page.");
        }
    %>


    <%@include file="../html/bottom.html"%>
