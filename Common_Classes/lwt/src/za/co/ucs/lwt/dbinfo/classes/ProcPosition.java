/*
 * ProcPosition.java
 *
 * Created on November 6, 2003, 7:53 AM
 */

package za.co.ucs.lwt.dbinfo.classes;

/** This class is used to store a scripts and it's position on the screen
 */
public class ProcPosition{
    
    public ProcPosition(StoredProcDef script, int x, int y, int width, int height){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.script = script;
    }
    
    public StoredProcDef getScript(){
        return this.script;
    }
    
    public int getX(){
        return this.x;
    }
    
    public int getY(){
        return this.y;
    }
    
    /** Returns TRUE if the current x, y is somewhere within the
     * boundaries of this script's position on the screen
     */
    public boolean inPosition(int x, int y){
        return (( x >= this.x ) && (x <= (this.x + this.width)) &&
        (y >= this.y) && (y <= (this.y + this.height)));
    }
    
    /** Returns TRUE if the current x, y is somewhere within the
     * boundaries of this script's position on the screen, but
     * exaclty in the same line
     */
    public boolean inLine(int x, int y){
        return (( x >= this.x ) && (x <= (this.x + this.width)) &&
        ( y == this.y ) );
    }

    private StoredProcDef script;
    private int x;
    private int y;
    private int width;
    private int height;
}
