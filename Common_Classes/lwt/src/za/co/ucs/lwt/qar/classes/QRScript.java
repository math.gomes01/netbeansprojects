/*
 * QRScript.java
 *
 * Created on October 21, 2003, 1:22 PM
 */

package za.co.ucs.lwt.qar.classes;
import java.io.*;
import java.util.*;

/**
 *
 * @author  liam
 */
public class QRScript extends QRBase {
    
    /** Creates a new instance of QRScript
     * Requires:    name - the name of the Script
     *              scriptFile - Full path and filename of Script
     *              mainContainer - QRContainer belonging to the Base Class.
     *                              We need this to retain a single instance of each class across
     *                              all the scripts
     */
    public QRScript(String name, File scriptFile, QRBaseContainer mainContainer) {
        super(name);
        this.scriptFile = scriptFile;
        this.text = new LinkedList();
        ownContainer = new QRBaseContainer();
        this.mainContainer = mainContainer;
    }
    
    private LinkedList readContentsOfFile(File scriptFile){
        LinkedList resultBuffer = new LinkedList();
        try {
            RandomAccessFile ras1 = new RandomAccessFile(scriptFile,"r");
            while (true) {
                String readLine = ras1.readLine();
                if ((readLine != null) && (readLine.trim().length() != 0)) {
                    resultBuffer.add(readLine);
                }
                if (readLine == null){
                    return resultBuffer;
                }
            }
            
        } catch (FileNotFoundException f) {
            f.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    private String convertStringToHTML(String string){
        StringTokenizer token = new StringTokenizer(string, " (),", true);
        String newString = "<br>";
        while (token.hasMoreTokens()){
            String word = token.nextToken();
            if (isComment(string)){
                return (new String("<br><font color=\"#009900\">"+string+"</font>"));
            }
            
            if (isKeyWord(word)){
                newString = newString + "<font color=\"#0000ff\">" + word + "</font>";
                continue;
            }
            if (isCommand(word)){
                newString = newString + "<font color=\"#800000\">" + word + "</font>";
                continue;
            }
            if (isInQuotes(word)){
                newString = newString + "<font color=\"#ff0000\">" + word + "</font>";
                continue;
            }
            newString = newString + word;
        }
        return newString;
    }
    
    // Used to check the 'type' of word in the script
    private boolean isKeyWord(String string){
        String[] keywords = {"Public", "Function", "Mail", "Whenever", "Call", "End", "EndIf", "If", "Else",
        "For", "To", "Next"};
        for (int i=0; i<keywords.length; i++){
            if (string.compareToIgnoreCase(keywords[i])==0){
                return true;
            }
        }
        return false;
    }
    // Used to check the 'type' of word in the script
    private boolean isCommand(String string){
        String[] keywords = {"Attach", "TextSelect", "Wait", "Run", "LogComment", "Log.Checks", "Check", "Pause"};
        for (int i=0; i<keywords.length; i++){
            if (string.compareToIgnoreCase(keywords[i])==0){
                return true;
            }
        }
        return false;
    }
    private boolean isComment(String string){
        if ((string.trim().length()>1) &&
                ((string.trim().substring(0, 2).compareTo("//") == 0) ||
                (string.trim().substring(0, 1).compareTo(";") == 0)) )
            return true;
        else
            return false;
    }
    private boolean isInQuotes(String string){
        return (string.indexOf("\"")>=0);
    }
    
    public void interpretContentsOfFile(){
        // Start by reading the file
        this.text = readContentsOfFile(this.scriptFile);
        // Now, we'll start 'picking out' the other objects
        buildDependentScriptsFromTextFile(this.text);
        buildObjectsFromTextFile(this.text);
        buildEventsFromTextFile(this.text);
        // convert the script to HTML
        for (int i=0; i<text.size(); i++){
            String interemLine = (String)this.text.get(i);
            this.text.set(i, convertStringToHTML(interemLine));
        }
        // Add another tag to the outer boundaries of the script
        this.text.add(0, new String("<PRE>"));
        this.text.add(new String("</PRE>"));
    }
    
    /** Traverses through the actual file, looking for references to other scripts
     */
    private void buildDependentScriptsFromTextFile(LinkedList textInFile){
        String itemIdentifier = "RUN";
        for (int I = 0; I<textInFile.size(); I++){
            StringTokenizer token = new StringTokenizer((String)textInFile.get(I)," ()\t\n\r\f\""+new Character(Character.forDigit(32, 100)), false);
            
            while (token.hasMoreTokens()){
                String tokenString = token.nextToken();
                if (tokenString.compareToIgnoreCase(itemIdentifier)==0){
                    // We've got a Script!
                    try{
                        String nextToken = token.nextToken();
                        QRScript containerScript = mainContainer.getScript(nextToken);
                        if (containerScript!=null){
                            if (ownContainer.getScript(nextToken) == null){
                                ownContainer.addScript(containerScript);
                                System.out.println(this.toString()+" calls "+containerScript.toString());
                            }
                        }
                    } catch (NoSuchElementException e) {
                        System.out.println("buildDependentScriptsFromTextFile:NoSuchElementException");
                    }
                }
            }
        }
    }
    /** Traverses through the actual file, looking for references to other scripts
     */
    private void buildObjectsFromTextFile(LinkedList textInFile){
        // Attach
        String[] itemIdentifier = {"ATTACH", "CAPTURE"};
        for (int I = 0; I<itemIdentifier.length; I++){
            for (int j=0; j<textInFile.size(); j++){
                String textLine = (String)textInFile.get(j);
                if (isComment(textLine)){
                    continue;
                }
                StringTokenizer token = new StringTokenizer(textLine," ()\",-", false);
                while (token.hasMoreTokens()){
                    String tokenString = token.nextToken();
                    if (tokenString.compareToIgnoreCase(itemIdentifier[I])==0){
                        // We've got an Object!
                        if (token.hasMoreTokens()){
                            String nextObject = token.nextToken();
                            if (nextObject.trim().length()>0){
                                QRObject containerObject = mainContainer.getObject(nextObject);
                                if (containerObject!=null){
                                    ownContainer.addObject(containerObject);
                                } else{
                                    containerObject = new QRObject(nextObject);
                                    mainContainer.addObject(containerObject);
                                    ownContainer.addObject(containerObject);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    /** Traverses through the actual file, looking for references to other scripts
     */
    private void buildEventsFromTextFile(LinkedList textInFile){
        // Identifiers
        String[] itemIdentifier = {"WHENEVER", "WAIT"};
        for (int I = 0; I<itemIdentifier.length; I++){
            for (int j=0; j<textInFile.size(); j++){
                String textLine = (String)textInFile.get(j);
                if (isComment(textLine)){
                    continue;
                }
                StringTokenizer token = new StringTokenizer(textLine," ()\",-", false);
                while (token.hasMoreTokens()){
                    String tokenString = token.nextToken();
                    if (tokenString.compareToIgnoreCase(itemIdentifier[I])==0){
                        // We've got an Object!
                        // Remember that the Syntax for WAIT is Wait(time, , EVENT)
                        if (itemIdentifier[I].compareToIgnoreCase("WAIT")==0){
                            String timeObject = token.nextToken();
                        }
                        
                        if (token.hasMoreTokens()){
                            String eventString = token.nextToken();
                            if (eventString.trim().length()>0){
                                QREvent containerEvent = mainContainer.getEvent(eventString);
                                if (containerEvent!=null){
                                    ownContainer.addEvent(containerEvent);
                                } else{
                                    containerEvent = new QREvent(eventString);
                                    mainContainer.addEvent(containerEvent);
                                    ownContainer.addEvent(containerEvent);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    
    public void setText(LinkedList text){
        this.text = text; }
    
    public String getText() {
        String rslt = new String();
        for (int i=0; i<text.size(); i++){
            rslt = rslt + text.get(i);
        }
        return rslt; }
    
    public File getScriptFile(){
        return this.scriptFile; }
    
    public void setScriptFile(File scriptFile) {
        this.scriptFile = scriptFile; }
    
    public String toString(){
        String removedExtensionName = this.scriptFile.getName().substring(0, this.scriptFile.getName().toUpperCase().indexOf(".AWL"));
        return (removedExtensionName);
    }
    
    
    /** We need to know the direct or indirect number of scripts called from this script
     * in order to equally distribute those scripts on the user Interface
     */
    public int getMaxLevelWidth(){
        int currentNumberOfScripts = this.ownContainer.getScripts().values().size();
        int nextLevelNumberOfScripts = 0;
        Iterator iter = this.ownContainer.getScripts().values().iterator();
        while (iter.hasNext()){
            QRScript script = (QRScript)iter.next();
            nextLevelNumberOfScripts += script.getMaxLevelWidth();
        }
        if (currentNumberOfScripts > nextLevelNumberOfScripts){
            return currentNumberOfScripts;
        } else {
            return nextLevelNumberOfScripts;
        }
    }
    
    /** How many scripts are being called from the 'parent' script?
     * We need to know this to do a proper graphical representation
     */
    public int getCurrentLevelWidth(QRScript parent){
        // Get Parent Script
        int currentNumberOfScripts = parent.getOwnContainer().getScripts().values().size();
        return currentNumberOfScripts;
    }
    
    public QRBaseContainer getOwnContainer(){
        return ownContainer;
    }
    
    private File scriptFile;
    private LinkedList text;
    private QRBaseContainer ownContainer;// Global Container
    private QRBaseContainer mainContainer;
    private static Properties properties;
    
}
