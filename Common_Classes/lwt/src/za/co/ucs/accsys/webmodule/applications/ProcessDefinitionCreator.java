/*
 * ProcessDefinitionCreator.java
 * This application is used to create the list of available processes that get shipped
 * to all the clients.  This list gets saved as a seperate file
 * that contains the processes that the use can 
 * assign reporting structures to.  Whenever new processes get developed, 
 * the 'main' method of this application needs to be updated and the generated file
 * has to be distributed to the client-base.
 * Created on June 29, 2004, 3:47 PM
 */
package za.co.ucs.accsys.webmodule.applications;

import java.util.ArrayList;
import za.co.ucs.accsys.webmodule.*;

/**
 *
 * @author  liam
 */
public class ProcessDefinitionCreator {

    /** Creates a new instance of ProcessDefinitionCreator */
    public ProcessDefinitionCreator() {
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //if (args.length>0){
        
        
        WebProcessDefinition def1 = new WebProcessDefinition("Leave Requests", "Request Leave", "za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition");
        WebProcessDefinition def2 = new WebProcessDefinition("Update Family Information", "Update Family Information", "za.co.ucs.accsys.webmodule.WebProcess_FamilyRequisition");
        WebProcessDefinition def3 = new WebProcessDefinition("Update Employee Contact Information", "Update Employee Contact Information", "za.co.ucs.accsys.webmodule.WebProcess_EmployeeContact");
        WebProcessDefinition def4 = new WebProcessDefinition("Cancel Leave", "Cancel Leave", "za.co.ucs.accsys.webmodule.WebProcess_LeaveCancellation");

        WebProcessDefinition def5 = new WebProcessDefinition("Variable Changes (55001)", "Variable Changes (55001)", "za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55001");
        WebProcessDefinition def6 = new WebProcessDefinition("Variable Changes (55002)", "Variable Changes (55002)", "za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55002");
        WebProcessDefinition def7 = new WebProcessDefinition("Variable Changes (55003)", "Variable Changes (55003)", "za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55003");
        WebProcessDefinition def8 = new WebProcessDefinition("Variable Changes (55004)", "Variable Changes (55004)", "za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55004");
        WebProcessDefinition def9 = new WebProcessDefinition("Variable Changes (55005)", "Variable Changes (55005)", "za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55005");

        WebProcessDefinition def10 = new WebProcessDefinition("Clocking Authorisation", "Clocking Authorisation", "za.co.ucs.accsys.webmodule.WebProcess_DailySignoff");
        WebProcessDefinition def11 = new WebProcessDefinition("Training Requests", "Training Requests", "za.co.ucs.accsys.webmodule.WebProcess_TrainingRequisition");
        WebProcessDefinition def12 = new WebProcessDefinition("TnA Roster Requests", "TnA Roster Requests", "za.co.ucs.accsys.webmodule.WebProcess_TnaRosterRequesition");

        FileContainer fc = FileContainer.getInstance();
        fc.clearAllProcessDefinitionsFromFile();
        fc.addWebProcessDefinitionIntoFile(def1);
        fc.addWebProcessDefinitionIntoFile(def2);
        fc.addWebProcessDefinitionIntoFile(def3);
        fc.addWebProcessDefinitionIntoFile(def4);
        fc.addWebProcessDefinitionIntoFile(def5);
        fc.addWebProcessDefinitionIntoFile(def6);
        fc.addWebProcessDefinitionIntoFile(def7);
        fc.addWebProcessDefinitionIntoFile(def8);
        fc.addWebProcessDefinitionIntoFile(def9);
        fc.addWebProcessDefinitionIntoFile(def10);
        fc.addWebProcessDefinitionIntoFile(def11);
        fc.addWebProcessDefinitionIntoFile(def12);

        ////////////////////////////////////////////////////////////////////////
        // Add leave process for each leave profile
        ////////////////////////////////////////////////////////////////////////
        ArrayList<String> leaveProfiles = fc.getLeaveProfileList();
        for (String profile : leaveProfiles) {
            //System.out.println("Leave Profile = " + profile);
            WebProcessDefinition def = new WebProcessDefinition("Leave Requests_" + profile, "Request Leave", "za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition" + profile);
            fc.addWebProcessDefinitionIntoFile(def);
        }        

        fc.saveWebSetupDetail();
        //fc.saveProcessDetail(true);

        fc = null;

        //}
        // TODO code application logic here
    }
}
