<%@page contentType="text/html"%>
<%@include file="../html/top.html"%>

    <CENTER>
        <BR>
            <H3>Accsys Deployment Builder</H3>
            <hr width="80%" size="1" color="#c0c0c0">
        <BR>

<!-- Hyper Links -->
<%@ include file="../html/top_hyperlinks.txt" %>


<% // Is this the correct user
   // This user could
   // a. be sent from the login page, in which case we'll receive a 'userName' and 'password' parameter
   // b. be sent from another page.  Then we'll have to check the 'loggedinconsultant' session parameter
   
   String userName = request.getParameter("userName");
   String password = request.getParameter("password");
   
  if ((userName != null) && (password != null) && (userName.trim().length()>0) && (password.trim().length()>0)){
 
        za.co.ucs.lwt.deploymentbuilder.User user = new za.co.ucs.lwt.deploymentbuilder.User(userName);
        if (user.getLoginPwd().compareToIgnoreCase(password) == 0){
            session.setAttribute("loggedinuser", new String("true"));
            // We would like to retain this consultant for the duration of this session
            session.setAttribute("user",user);
            out.println("<h4>Welcome "+user.getName()+"</h4>");   
        }
        else{
            session.setAttribute("loggedinuser", new String("false"));
            response.sendRedirect("/DeploymentBuilder/loginJsp.jsp");
        }

  }
  else {
      session.setAttribute("loggedinuser", new String("false"));
      response.sendRedirect("/DeploymentBuilder/loginJsp.jsp");
  }
%>


<%@include file="../html/bottom.html"%>
