package za.co.ucs.ess;

import java.util.ArrayList;
import za.co.ucs.accsys.peopleware.*;
import za.co.ucs.accsys.webmodule.*;

/**
 * <p>
 * PageProducer_GroupSelector inherits from PageProducer and will display the
 * employee selections that are 'under' the current employee selection for the
 * given reporting structure.
 * </p>
 */
public class PageProducer_GroupSelector {

    /**
     * Creates a page from where a user can select to perform requests on behalf
     * of one (or more) of his/her employees
     *
     * @param webProcessDefinition - Which web process needs to be managed after
     * the employee has been selected?
     * @param reportingStructure - What are the reporting structure being used?
     * @param user - Who is the person calling up this form?
     * @param reportingStructureHashValue - Reference to the reporting structure
     * @param refDate - Reference date (where applicable)
     * @param callerJSP - Which JSP called this form, so that we can redirect if
     * necessary
     * @param selectedSelection - Which selection in the reporting structure are
     * we in?
     * @param defaultActionJSP
     * @param menuCaption - What should the menu item be called/displayed as?
     * @param allowMultiEmpSelection - If true, places a check-box next to a
     * person
     */
    public PageProducer_GroupSelector(WebProcessDefinition webProcessDefinition, ReportingStructure reportingStructure, Employee user, String reportingStructureHashValue, String refDate, String callerJSP, EmployeeSelection selectedSelection, String defaultActionJSP, String menuCaption, boolean allowMultiEmpSelection) {
        this(webProcessDefinition, reportingStructure, user, reportingStructureHashValue, refDate, callerJSP, selectedSelection);
        this.defaultActionJSP = defaultActionJSP;
        this.menuCaption = menuCaption;
        this.allowMultiEmpSelection = allowMultiEmpSelection;
    }

    /**
     * Constructor
     */
    public PageProducer_GroupSelector(WebProcessDefinition webProcessDefinition, ReportingStructure reportingStructure, Employee user, String reportingStructureHashValue, String refDate, String callerJSP, EmployeeSelection selectedSelection) {
        this.user = user;
        this.hashValue = reportingStructureHashValue;
        this.refDate = refDate;
        this.callerJSP = callerJSP;
        this.reportingStructure = reportingStructure;
        this.selectedSelection = selectedSelection;
        this.webProcessDefinition = webProcessDefinition;
        this.counter = 0;
        fileContainer = za.co.ucs.accsys.webmodule.FileContainer.getInstance();
        baseURL = WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Web, "");
        this.allowMultiEmpSelection = false;
        this.menuCaption = webProcessDefinition.getName();
        // User veriable name in the managers menu
        za.co.ucs.accsys.peopleware.Variable variableRequested;
        if (menuCaption.contains("55001")) {
            variableRequested = za.co.ucs.accsys.webmodule.WebProcess_VariableChanges.getVariable(user, "55001");
            if (variableRequested != null) {
                menuCaption = variableRequested.getName();
            } else {
                menuCaption = webProcessDefinition.getName();
            }
        }
        if (menuCaption.contains("55002")) {
            variableRequested = za.co.ucs.accsys.webmodule.WebProcess_VariableChanges.getVariable(user, "55002");
            if (variableRequested != null) {
                menuCaption = variableRequested.getName();
            } else {
                menuCaption = webProcessDefinition.getName();
            }
        }
        if (menuCaption.contains("55003")) {
            variableRequested = za.co.ucs.accsys.webmodule.WebProcess_VariableChanges.getVariable(user, "55003");
            if (variableRequested != null) {
                menuCaption = variableRequested.getName();
            } else {
                menuCaption = webProcessDefinition.getName();
            }
        }
        if (menuCaption.contains("55004")) {
            variableRequested = za.co.ucs.accsys.webmodule.WebProcess_VariableChanges.getVariable(user, "55004");
            if (variableRequested != null) {
                menuCaption = variableRequested.getName();
            } else {
                menuCaption = webProcessDefinition.getName();
            }
        }
        if (menuCaption.contains("55005")) {
            variableRequested = za.co.ucs.accsys.webmodule.WebProcess_VariableChanges.getVariable(user, "55005");
            if (variableRequested != null) {
                menuCaption = variableRequested.getName();
            } else {
                menuCaption = webProcessDefinition.getName();
            }
        }
    }

    public String generateWindowsMenuContent(String baseURL) {
        StringBuilder rslt = new StringBuilder();
        rslt.append(listItemStartStatement);
        rslt.append("<a href=\"" + baseURL + "processes/Welcome.jsp\">").append(this.menuCaption).append("</a>");
        //rslt.append("stm_aix(\"mnuWithRightArrow\",\"menuSubItem\",[0,\"").append(this.menuCaption).append("\",\"\",\"\",-1,-1,0,"
        //        + "\"\",\"_self\",\"\",\"\",\"\",\"bullet.gif\",8,8,0,\"mnu_arrow_right.gif\",\"mnu_arrow_right.gif\",10,11],201,0);");
        rslt.append(submenuStartStatement);
        java.util.ArrayList children0 = reportingStructure.getTopNodes();
        if (children0.size() > 0) {
            // ----- LEVEL 0 ------
            EmployeeSelection child0 = (EmployeeSelection) children0.get(0);
            if ((SelectionInterpreter.getInstance().generateEmployeeList(child0).contains(user))
                    && (!reportingStructure.isUserParentOfSelection(user, child0))
                    && (reportingStructure.getAllChildEmployeeSelections(child0).size() > 0)) {
                String submenuItems = generateWindowsSubmenuContent(this.reportingStructure, child0);
                if (submenuItems.trim().length() > 0) {
                    rslt.append(submenuItems);
                }
            }
            // ----- LEVEL 1 ------
            java.util.LinkedList children1 = reportingStructure.getChildEmployeeSelections(child0);
            java.util.Iterator children1Iter = children1.iterator();
            while (children1Iter.hasNext()) {
                EmployeeSelection child1 = (EmployeeSelection) children1Iter.next();
                // Is this user in this employee selection, AND NOT the selection above this one too, AND there are children listed underneath?
                if ((SelectionInterpreter.getInstance().generateEmployeeList(child1).contains(user))
                        && (!reportingStructure.isUserParentOfSelection(user, child1))
                        && (reportingStructure.getAllChildEmployeeSelections(child1).size() > 0)) {
                    String submenuItems = generateWindowsSubmenuContent(this.reportingStructure, child1);
                    if (submenuItems.trim().length() > 0) {
                        rslt.append(submenuItems);
                    }
                }
                // ----- LEVEL 2 ------
                java.util.LinkedList children2 = reportingStructure.getChildEmployeeSelections(child1);
                java.util.Iterator children2Iter = children2.iterator();
                while (children2Iter.hasNext()) {
                    EmployeeSelection child2 = (EmployeeSelection) children2Iter.next();
                    if ((SelectionInterpreter.getInstance().generateEmployeeList(child2).contains(user))
                            && (!reportingStructure.isUserParentOfSelection(user, child2))
                            && (reportingStructure.getAllChildEmployeeSelections(child2).size() > 0)) {

                        String submenuItems = generateWindowsSubmenuContent(this.reportingStructure, child2);
                        if (submenuItems.trim().length() > 0) {
                            rslt.append(submenuItems);
                        }
                    }
                    // ----- LEVEL 3 ------
                    java.util.LinkedList children3 = reportingStructure.getChildEmployeeSelections(child2);
                    java.util.Iterator children3Iter = children3.iterator();
                    while (children3Iter.hasNext()) {
                        EmployeeSelection child3 = (EmployeeSelection) children3Iter.next();
                        if ((SelectionInterpreter.getInstance().generateEmployeeList(child3).contains(user))
                                && (!reportingStructure.isUserParentOfSelection(user, child3))
                                && (reportingStructure.getAllChildEmployeeSelections(child3).size() > 0)) {

                            String submenuItems = generateWindowsSubmenuContent(this.reportingStructure, child3);
                            if (submenuItems.trim().length() > 0) {
                                rslt.append(submenuItems);
                            }
                        }
                        // ----- LEVEL 4 ------
                        java.util.LinkedList children4 = reportingStructure.getChildEmployeeSelections(child3);
                        java.util.Iterator children4Iter = children4.iterator();
                        while (children4Iter.hasNext()) {
                            EmployeeSelection child4 = (EmployeeSelection) children4Iter.next();
                            if ((SelectionInterpreter.getInstance().generateEmployeeList(child4).contains(user))
                                    && (!reportingStructure.isUserParentOfSelection(user, child4))
                                    && (reportingStructure.getAllChildEmployeeSelections(child4).size() > 0)) {

                                String submenuItems = generateWindowsSubmenuContent(this.reportingStructure, child4);
                                if (submenuItems.trim().length() > 0) {
                                    rslt.append(submenuItems);
                                }
                            }
                            // ----- LEVEL 5 ------
                            java.util.LinkedList children5 = reportingStructure.getChildEmployeeSelections(child4);
                            java.util.Iterator children5Iter = children5.iterator();
                            while (children5Iter.hasNext()) {
                                EmployeeSelection child5 = (EmployeeSelection) children5Iter.next();
                                if ((SelectionInterpreter.getInstance().generateEmployeeList(child5).contains(user))
                                        && (!reportingStructure.isUserParentOfSelection(user, child5))
                                        && (reportingStructure.getAllChildEmployeeSelections(child5).size() > 0)) {

                                    String submenuItems = generateWindowsSubmenuContent(this.reportingStructure, child5);
                                    if (submenuItems.trim().length() > 0) {
                                        rslt.append(submenuItems);
                                    }
                                }
                                // ----- LEVEL 6 ------
                                java.util.LinkedList children6 = reportingStructure.getChildEmployeeSelections(child5);
                                java.util.Iterator children6Iter = children6.iterator();
                                while (children6Iter.hasNext()) {
                                    EmployeeSelection child6 = (EmployeeSelection) children6Iter.next();
                                    if ((SelectionInterpreter.getInstance().generateEmployeeList(child6).contains(user))
                                            && (!reportingStructure.isUserParentOfSelection(user, child6))
                                            && (reportingStructure.getAllChildEmployeeSelections(child6).size() > 0)) {

                                        String submenuItems = generateWindowsSubmenuContent(this.reportingStructure, child6);
                                        if (submenuItems.trim().length() > 0) {
                                            rslt.append(submenuItems);
                                        }
                                    }
                                    // ----- LEVEL 7 ------
                                    java.util.LinkedList children7 = reportingStructure.getChildEmployeeSelections(child6);
                                    java.util.Iterator children7Iter = children7.iterator();
                                    while (children7Iter.hasNext()) {
                                        EmployeeSelection child7 = (EmployeeSelection) children7Iter.next();
                                        if ((SelectionInterpreter.getInstance().generateEmployeeList(child7).contains(user))
                                                && (!reportingStructure.isUserParentOfSelection(user, child7))
                                                && (reportingStructure.getAllChildEmployeeSelections(child7).size() > 0)) {

                                            String submenuItems = generateWindowsSubmenuContent(this.reportingStructure, child7);
                                            if (submenuItems.trim().length() > 0) {
                                                rslt.append(submenuItems);
                                            }
                                        }
                                        // ----- LEVEL 8 ------
                                        java.util.LinkedList children8 = reportingStructure.getChildEmployeeSelections(child7);
                                        java.util.Iterator children8Iter = children8.iterator();
                                        while (children8Iter.hasNext()) {
                                            EmployeeSelection child8 = (EmployeeSelection) children8Iter.next();
                                            if ((SelectionInterpreter.getInstance().generateEmployeeList(child8).contains(user))
                                                    && (!reportingStructure.isUserParentOfSelection(user, child8))
                                                    && (reportingStructure.getAllChildEmployeeSelections(child8).size() > 0)) {

                                                String submenuItems = generateWindowsSubmenuContent(this.reportingStructure, child8);
                                                if (submenuItems.trim().length() > 0) {
                                                    rslt.append(submenuItems);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        rslt.append(submenuEndStatement);
        rslt.append(listItemEndStatement);

        return (rslt.toString());
    }
    
    public String generateWindowsMenuContentForLeave(String baseURL) {
        StringBuilder rslt = new StringBuilder();
        //rslt.append("stm_aix(\"mnuWithRightArrow\",\"menuSubItem\",[0,\"").append(this.menuCaption).append("\",\"\",\"\",-1,-1,0,"
        //        + "\"\",\"_self\",\"\",\"\",\"\",\"bullet.gif\",8,8,0,\"mnu_arrow_right.gif\",\"mnu_arrow_right.gif\",10,11],201,0);");
        java.util.ArrayList children0 = reportingStructure.getTopNodes();
        if (children0.size() > 0) {
            // ----- LEVEL 0 ------
            EmployeeSelection child0 = (EmployeeSelection) children0.get(0);
            if ((SelectionInterpreter.getInstance().generateEmployeeList(child0).contains(user))
                    && (!reportingStructure.isUserParentOfSelection(user, child0))
                    && (reportingStructure.getAllChildEmployeeSelections(child0).size() > 0)) {
                String submenuItems = generateWindowsSubmenuContent(this.reportingStructure, child0);
                if (submenuItems.trim().length() > 0) {
                    rslt.append(submenuItems);
                }
            }
            // ----- LEVEL 1 ------
            java.util.LinkedList children1 = reportingStructure.getChildEmployeeSelections(child0);
            java.util.Iterator children1Iter = children1.iterator();
            while (children1Iter.hasNext()) {
                EmployeeSelection child1 = (EmployeeSelection) children1Iter.next();
                // Is this user in this employee selection, AND NOT the selection above this one too, AND there are children listed underneath?
                if ((SelectionInterpreter.getInstance().generateEmployeeList(child1).contains(user))
                        && (!reportingStructure.isUserParentOfSelection(user, child1))
                        && (reportingStructure.getAllChildEmployeeSelections(child1).size() > 0)) {
                    String submenuItems = generateWindowsSubmenuContent(this.reportingStructure, child1);
                    if (submenuItems.trim().length() > 0) {
                        rslt.append(submenuItems);
                    }
                }
                // ----- LEVEL 2 ------
                java.util.LinkedList children2 = reportingStructure.getChildEmployeeSelections(child1);
                java.util.Iterator children2Iter = children2.iterator();
                while (children2Iter.hasNext()) {
                    EmployeeSelection child2 = (EmployeeSelection) children2Iter.next();
                    if ((SelectionInterpreter.getInstance().generateEmployeeList(child2).contains(user))
                            && (!reportingStructure.isUserParentOfSelection(user, child2))
                            && (reportingStructure.getAllChildEmployeeSelections(child2).size() > 0)) {

                        String submenuItems = generateWindowsSubmenuContent(this.reportingStructure, child2);
                        if (submenuItems.trim().length() > 0) {
                            rslt.append(submenuItems);
                        }
                    }
                    // ----- LEVEL 3 ------
                    java.util.LinkedList children3 = reportingStructure.getChildEmployeeSelections(child2);
                    java.util.Iterator children3Iter = children3.iterator();
                    while (children3Iter.hasNext()) {
                        EmployeeSelection child3 = (EmployeeSelection) children3Iter.next();
                        if ((SelectionInterpreter.getInstance().generateEmployeeList(child3).contains(user))
                                && (!reportingStructure.isUserParentOfSelection(user, child3))
                                && (reportingStructure.getAllChildEmployeeSelections(child3).size() > 0)) {

                            String submenuItems = generateWindowsSubmenuContent(this.reportingStructure, child3);
                            if (submenuItems.trim().length() > 0) {
                                rslt.append(submenuItems);
                            }
                        }
                        // ----- LEVEL 4 ------
                        java.util.LinkedList children4 = reportingStructure.getChildEmployeeSelections(child3);
                        java.util.Iterator children4Iter = children4.iterator();
                        while (children4Iter.hasNext()) {
                            EmployeeSelection child4 = (EmployeeSelection) children4Iter.next();
                            if ((SelectionInterpreter.getInstance().generateEmployeeList(child4).contains(user))
                                    && (!reportingStructure.isUserParentOfSelection(user, child4))
                                    && (reportingStructure.getAllChildEmployeeSelections(child4).size() > 0)) {

                                String submenuItems = generateWindowsSubmenuContent(this.reportingStructure, child4);
                                if (submenuItems.trim().length() > 0) {
                                    rslt.append(submenuItems);
                                }
                            }
                            // ----- LEVEL 5 ------
                            java.util.LinkedList children5 = reportingStructure.getChildEmployeeSelections(child4);
                            java.util.Iterator children5Iter = children5.iterator();
                            while (children5Iter.hasNext()) {
                                EmployeeSelection child5 = (EmployeeSelection) children5Iter.next();
                                if ((SelectionInterpreter.getInstance().generateEmployeeList(child5).contains(user))
                                        && (!reportingStructure.isUserParentOfSelection(user, child5))
                                        && (reportingStructure.getAllChildEmployeeSelections(child5).size() > 0)) {

                                    String submenuItems = generateWindowsSubmenuContent(this.reportingStructure, child5);
                                    if (submenuItems.trim().length() > 0) {
                                        rslt.append(submenuItems);
                                    }
                                }
                                // ----- LEVEL 6 ------
                                java.util.LinkedList children6 = reportingStructure.getChildEmployeeSelections(child5);
                                java.util.Iterator children6Iter = children6.iterator();
                                while (children6Iter.hasNext()) {
                                    EmployeeSelection child6 = (EmployeeSelection) children6Iter.next();
                                    if ((SelectionInterpreter.getInstance().generateEmployeeList(child6).contains(user))
                                            && (!reportingStructure.isUserParentOfSelection(user, child6))
                                            && (reportingStructure.getAllChildEmployeeSelections(child6).size() > 0)) {

                                        String submenuItems = generateWindowsSubmenuContent(this.reportingStructure, child6);
                                        if (submenuItems.trim().length() > 0) {
                                            rslt.append(submenuItems);
                                        }
                                    }
                                    // ----- LEVEL 7 ------
                                    java.util.LinkedList children7 = reportingStructure.getChildEmployeeSelections(child6);
                                    java.util.Iterator children7Iter = children7.iterator();
                                    while (children7Iter.hasNext()) {
                                        EmployeeSelection child7 = (EmployeeSelection) children7Iter.next();
                                        if ((SelectionInterpreter.getInstance().generateEmployeeList(child7).contains(user))
                                                && (!reportingStructure.isUserParentOfSelection(user, child7))
                                                && (reportingStructure.getAllChildEmployeeSelections(child7).size() > 0)) {

                                            String submenuItems = generateWindowsSubmenuContent(this.reportingStructure, child7);
                                            if (submenuItems.trim().length() > 0) {
                                                rslt.append(submenuItems);
                                            }
                                        }
                                        // ----- LEVEL 8 ------
                                        java.util.LinkedList children8 = reportingStructure.getChildEmployeeSelections(child7);
                                        java.util.Iterator children8Iter = children8.iterator();
                                        while (children8Iter.hasNext()) {
                                            EmployeeSelection child8 = (EmployeeSelection) children8Iter.next();
                                            if ((SelectionInterpreter.getInstance().generateEmployeeList(child8).contains(user))
                                                    && (!reportingStructure.isUserParentOfSelection(user, child8))
                                                    && (reportingStructure.getAllChildEmployeeSelections(child8).size() > 0)) {

                                                String submenuItems = generateWindowsSubmenuContent(this.reportingStructure, child8);
                                                if (submenuItems.trim().length() > 0) {
                                                    rslt.append(submenuItems);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return (rslt.toString());
    }

    private String generateWindowsSubmenuContent(ReportingStructure reportingStructure, EmployeeSelection employeeSelection) {
        StringBuilder rslt = new StringBuilder();
        java.util.LinkedList childrenMenuItems = reportingStructure.getChildEmployeeSelections(employeeSelection);
        java.util.Iterator childrenItemIter = childrenMenuItems.iterator();
        while (childrenItemIter.hasNext()) {
            EmployeeSelection childMenuItem = (EmployeeSelection) childrenItemIter.next();
            String submenuItems = generateWindowsSubmenuContent(this.reportingStructure, childMenuItem);
            String multiEmp = "&MultiEmployee=false";
            if (allowMultiEmpSelection) {
                multiEmp = "&MultiEmployee=true";
            }

            if (submenuItems.trim().length() > 0) {
                rslt.append(listItemStartStatement);
                rslt.append("<a href=\" ").append(baseURL).append("processes/").append(defaultActionJSP).append("?webProcessClassName=").append(webProcessDefinition.getProcessClassName()).append(multiEmp).append("&hashValue=").append(reportingStructure.hashCode()).append("&selSelection=").append(childMenuItem.hashCode()).append(" \">").append(childMenuItem.toString()).append("</a>");
                //rslt.append("\n\t\t" + "stm_aix(\"\",\"menuSubItem\",[0,\"").append(childMenuItem.toString()).append("\",\"\",\"\",-1,-1,0,\"").append(baseURL).append("processes/").append(defaultActionJSP).append("?webProcessClassName=").append(webProcessDefinition.getProcessClassName()).append(multiEmp).append("&hashValue=").append(reportingStructure.hashCode()).append("&selSelection=").append(childMenuItem.hashCode()).append(" \",\"_self\",\"\",\"\",\"\",\"bullet.gif\",8,8,0,\"mnu_arrow_right.gif\",\"mnu_arrow_right.gif\",10,11],201,0);");
                rslt.append(submenuStartStatement);
                rslt.append(submenuItems);
                rslt.append(submenuEndStatement);
                rslt.append(listItemEndStatement);
            } else {
                rslt.append(listItemStartStatement);
                rslt.append("<a href=\" ").append(baseURL).append("processes/").append(defaultActionJSP).append("?webProcessClassName=").append(webProcessDefinition.getProcessClassName()).append(multiEmp).append("&hashValue=").append(reportingStructure.hashCode()).append("&selSelection=").append(childMenuItem.hashCode()).append(" \">").append(childMenuItem.toString()).append("</a>");
                rslt.append(listItemEndStatement);

                //rslt.append("\n\t\t" + "stm_aix(\"\",\"menuSubItem\",[0,\"").append(childMenuItem.toString()).append("\",\"\",\"\",-1,-1,0,\"").append(baseURL).append("processes/").append(defaultActionJSP).append("?webProcessClassName=").append(webProcessDefinition.getProcessClassName()).append(multiEmp).append("&hashValue=").append(reportingStructure.hashCode()).append("&selSelection=").append(childMenuItem.hashCode()).append("  \",\"_self\",\"\",\"\"],201,0);");
            }
        }
        return rslt.toString();
    }

    public String getMenuCaption() {
        return menuCaption;
    }
    
    int counter = 0;
    private EmployeeSelection selectedSelection;
    private Employee user;
    private String hashValue; //the HashValue of the Reporting Structure in use
    private String refDate;
    private String callerJSP;
    private ReportingStructure reportingStructure;
    private WebProcessDefinition webProcessDefinition;
    //private String submenuStartStatement = "stm_bpx(\"p2\",\"p1\",[1,2,0,0,3,4,8,0]);";
    //private String submenuEndStatement = "stm_ep();";
    private String submenuStartStatement = "<ul>";
    private String submenuEndStatement = "</ul>";
    private String listItemStartStatement = "<li>";
    private String listItemEndStatement = "</li>";
    FileContainer fileContainer;
    private String defaultActionJSP = "employeelist_prev.jsp";
    private String menuCaption;
    private boolean allowMultiEmpSelection;
    String baseURL;
} // end LeavePageProducer

