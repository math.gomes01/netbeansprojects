/*
 * DeploymentBuilderPreferences.java
 *
 * Created on 07 September 2006, 07:23
 *
 * This is the replacement of the original 'Setup' class which used an ini-like file to
 * store setup properties.
 * The WebModulePreferences class will make use of OS-dependent storage capabilities which should
 * be more reliable.
 */
package za.co.ucs.lwt.deploymentbuilder;

import java.io.IOException;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

/**
 *
 * @author lwt
 */
public final class DeploymentBuilderPreferences {

    /**
     * Creates a new instance of WebModulePreferences
     */
    private DeploymentBuilderPreferences() {
        prefs = Preferences.userNodeForPackage(this.getClass());
        // Whenever the preferences are updated, we store a copy of it in an XML file.
        // On startup, if that file exist, we'll import the properties from it.
        // If it does not exist, we'll simply create it
        java.io.File fl = new java.io.File(getFileName());
        System.out.println("\n\nDBPreferences.XML at :" + getFileName());

        try {
            // File DeploymentBuilderPreferences.XML' does not exist
            if (!fl.exists()) {
                fl.createNewFile();
                try {
                    prefs.exportNode(new java.io.FileOutputStream(DeploymentBuilderPreferences.getInstance().getFileName()));
                } catch (java.util.prefs.BackingStoreException be) {
                    be.printStackTrace();
                }
            } else {
                // File DeploymentBuilderPreferences.XML does exist
                try {
                    // Clear the content in the registry and re-read it from the DeploymentBuilderPreferences.XML file
                    System.out.println("Clearing internal preferences...");
                    prefs.clear();

                    System.out.println("Preferences.userRoot():" + Preferences.userRoot().toString());
                    System.out.println("Importing from DBPreferences.XML...");
                    Preferences.importPreferences(new java.io.FileInputStream(getFileName()));
                    System.out.println("Preferences Key-Value pairs:");
                    for (String key : prefs.keys()) {
                        System.out.println(key + ":" + prefs.get(key, "Undefined"));
                    }
                } catch (BackingStoreException e) {
                    e.printStackTrace();
                }

            }
        } catch (IOException | java.util.prefs.InvalidPreferencesFormatException io) {
            io.printStackTrace();
            //syncFromSetup();
        }
    }

    public static DeploymentBuilderPreferences getInstance() {
        if (deploymentBuilderPreferences == null) {
            deploymentBuilderPreferences = new DeploymentBuilderPreferences();
        }
        return deploymentBuilderPreferences;
    }



    public static Preferences getPreferences() {
        return DeploymentBuilderPreferences.prefs;
    }

    /**
     * The DeploymentBuilderPreferences are backed up to a file in the user directory. This
     * allows for manipulation of the preferences without accessing the
     * registry. During start-up, all changes in this file will be imported into
     * the preferences.
     * @return 
     */
    public String getFileName() {
        return System.getProperty("user.dir") + FileContainer.getOSFileSeperator() + "dbPreferences.xml";
    }


    private boolean containsKey(String aKey, String[] keys) {
        for (String key : keys) {
            if (key.compareToIgnoreCase(aKey) == 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the preference, and adding it to the preferences class if it does
     * not already exist
     * @param preference
     * @param defaultValue
     * @return 
     */
    public String getPreference(String preference, String defaultValue) {
        String rslt = prefs.get(preference, "");
        if (rslt.equals("")) {
            System.out.println("Note: getPreference(" + preference + "," + defaultValue + ") not found.  Default value returned.");
            rslt = defaultValue;
        }
        // Add to preferences if it does not already exist.
        try {
            if (!containsKey(preference, getPreferences().keys())) {
                getPreferences().put(preference, defaultValue);
                System.out.println("Adding new key:" + preference + " - " + defaultValue);
                try {
                    prefs.exportNode(new java.io.FileOutputStream(DeploymentBuilderPreferences.getInstance().getFileName()));
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        } catch (BackingStoreException be) {
            be.printStackTrace();
        }

        return rslt;
    }
    
    public static String SMTP_Server = "SMTP_Server";
    public static String SMTP_Port = "SMTP_Port";
    public static String SMTP_FromAddress = "SMTP_FromAddress";
    public static String SMTP_ToAddress = "SMTP_ToAddress";
    public static String SLACK_URL = "SLACK_URL";
    public static String SMTP_EMailNotificationSubjectString = "SMTP_EMailNotificationSubjectString";
    public static String SMTP_RequireAuthentication = "SMTP_RequireAuthentication";
    public static String SMTP_AuthenticationUser = "SMTP_AuthenticationUser";
    public static String SMTP_AuthenticationPwd = "SMTP_AuthenticationPwd";
    public static String SMTP_UseSSL = "SMTP_UseSSL";
    public static String URL_Database = "DATABASE_Url";
    private static DeploymentBuilderPreferences deploymentBuilderPreferences = null;
    private static Preferences prefs;
}
