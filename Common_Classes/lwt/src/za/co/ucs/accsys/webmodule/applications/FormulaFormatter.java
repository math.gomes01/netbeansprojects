/*
 * FormulaFormatter.java
 *  This class is used to perform text highlighting in any 
 *    JTextPane component.  The rules of the text formatting is
 *    based on the syntax used in creating employee selections.
 * Created on June 18, 2004, 3:17 PM
 */

package za.co.ucs.accsys.webmodule.applications;
import javax.swing.text.*;
import java.util.*;
import javax.swing.*;
import za.co.ucs.accsys.webmodule.*;

/**
 *
 * @author  liam
 */
public class FormulaFormatter {
    
    /** Creates a new instance of FormulaFormatter
     * @param editorPane */
    public FormulaFormatter(JTextPane editorPane) {
        this.editorPane = editorPane;
    }
    
   /** This is where the work is done.
     */
    public void run() {
        Document document = editorPane.getDocument();
        // Position of cursor:
        int caretPos = editorPane.getCaretPosition();
        try {
            // If nothing changed, keep the cursor where it belongs
            String oldText = editorPane.getText();
            
            // Get unformated text from document
            String docText = (document.getText(0, document.getLength()));
            
            // Get rid of the original "\n"
            if (docText.indexOf("\n")==0){
                docText = docText.replaceFirst("\n","");
            }
            
            // Format Text
            String convertedText = convertStringToHTML(docText, editorPane.getWidth());
            
            // if Look changed, update document
            if (convertedText.trim().compareToIgnoreCase(oldText.trim())!=0){
                editorPane.setText(convertedText);
                if (caretPos>document.getLength()){
                    caretPos = document.getLength();
                } else {
                    editorPane.setCaretPosition(caretPos);
                }
            }
        } catch (BadLocationException ble) {
            System.err.println("Bad Location. Exception:" + ble);
        }
    }
    
    private String convertStringToHTML(String string, int displayWidth){
        StringTokenizer token = new StringTokenizer(string, " ()[]\n", true);
        String newString = "";
        while (token.hasMoreTokens()){
            
            String word = token.nextToken();
            //if (word.compareTo(" ")==0)
            //    continue;
            if (isKeyWord(word)){
                newString = newString + "<font color=\"#009900\">"+word+"</font>";
                continue;
            }
            if (isComparator(word)){
                newString = newString + "<font color=\"#0000ff\">" + word + "</font>";
                continue;
            }
            if (isBrackets(word)){
                newString = newString + "<font color=\"#800000\"><b>" + word + "</b></font>";
                continue;
            }
            if (isLineBreak(word)){
                newString = newString + "<br>";
                continue;
            }
            if (isInQuotes(word)){
                newString = newString + "<font color=\"#ff0000\">" + word + "</font>";
                continue;
            }
            if (isCombiners(word)){
                newString = newString + "<font color=\"#888888\">" + word.toUpperCase() + "</font>";
                continue;
            }
            if (isSquareBraces(word)){
                newString = newString + "<font color=\"#000000\"><b>" + word.toUpperCase() + "</b></font>";
                continue;
            }
            // Empty Space
            if (word.compareTo(" ")==0){
                newString = newString + " ";
                continue;
            }
            // Anything else, print as WRONG by drawing a line through it
            newString = newString + "<font color=\"#ff0000\"><i><strike>"+word+"</strike></i></font>";
        }
        return ("<pre>"+newString+"</pre>");
    }
    
    private boolean isInQuotes(String string){
        return (string.indexOf("'")>=0);
    }
    
    private boolean isLineBreak(String string){
        return (string.indexOf("\n")==0);
    }
    
    private boolean isBrackets(String string){
        return ((string.indexOf("(") + string.indexOf(")"))>=-1);
    }
    
    private boolean isSquareBraces(String string){
        return ((string.indexOf("[") + string.indexOf("]"))>=-1);
    }
    
    private boolean isKeyWord(String string){
        String[] keywords = EmployeeSelectionRule.getKeyWords();
        for (String keyword : keywords) {
            if (string.toUpperCase().indexOf(keyword.toUpperCase()) >= 0) {
                return true;
            }
        }
        return false;
    }
    
    private boolean isComparator(String string){
        String[] comparators = EmployeeSelectionRule.Comparators;
        for (String comparator : comparators) {
            if (string.toUpperCase().indexOf(comparator.toUpperCase()) >= 0) {
                return true;
            }
        }
        return false;
    }
    
    private boolean isCombiners(String string){
        String[] combiners = EmployeeSelectionRule.getCombiners();
        for (String combiner : combiners) {
            if (string.toUpperCase().indexOf(combiner.toUpperCase()) >= 0) {
                return true;
            }
        }
        return false;
    }
    
    
    JTextPane editorPane = null;
    
}
