/*
 * WebModulePreferences.java
 *
 * Created on 07 September 2006, 07:23
 *
 * This is the replacement of the original 'Setup' class which used an ini-like file to
 * store setup properties.
 * The WebModulePreferences class will make use of OS-dependent storage capabilities which should
 * be more reliable.
 */
package za.co.ucs.accsys.webmodule;

import java.io.IOException;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

/**
 *
 * @author lwt
 */
public final class WebModulePreferences {

    /**
     * Creates a new instance of WebModulePreferences
     */
    private WebModulePreferences() {
        prefs = Preferences.userNodeForPackage(this.getClass());
        // Whenever the preferences are updated, we store a copy of it in an XML file.
        // On startup, if that file exist, we'll import the properties from it.
        // If it does not exist, we'll simply create it
        java.io.File fl = new java.io.File(getBackupFileName());
        System.out.println("\n\nPreferences.XML at :" + getBackupFileName());

        try {
            // File Preferences.XML' does not exist
            if (!fl.exists()) {
                fl.createNewFile();
                try {
                    prefs.exportNode(new java.io.FileOutputStream(za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().getBackupFileName()));
                } catch (java.util.prefs.BackingStoreException be) {
                    be.printStackTrace();
                }
            } else {
                // File Preferences.XML does exist
                try {
                    // Clear the content in the registry and re-read it from the Preferences.XML file
                    System.out.println("Clearing internal preferences...");
                    prefs.clear();

                    System.out.println("Preferences.userRoot():" + Preferences.userRoot().toString());
                    System.out.println("Importing from Preferences.XML...");
                    Preferences.importPreferences(new java.io.FileInputStream(getBackupFileName()));
                    System.out.println("Preferences Key-Value pairs:");
                    for (String key : prefs.keys()) {
                        System.out.println(key + ":" + prefs.get(key, "Undefined"));
                    }
                } catch (BackingStoreException e) {
                    e.printStackTrace();
                }

            }
        } catch (IOException | java.util.prefs.InvalidPreferencesFormatException io) {
            io.printStackTrace();
            //syncFromSetup();
        }
        // Get rid of unwanted preferences
        clearUnusedPreferences();
    }

    public static WebModulePreferences getInstance() {
        if (webModulePreferences == null) {
            webModulePreferences = new WebModulePreferences();
        }
        return webModulePreferences;
    }

    /**
     * During the evolution of the Web Module, certain preferences are being
     * deprecated and should be removed from the list.
     */
    private void clearUnusedPreferences() {
        WebModulePreferences.prefs.remove("action");
        WebModulePreferences.prefs.remove("HTML_OuterTableDef");
        WebModulePreferences.prefs.remove("HTML_InnerTableDef");
        WebModulePreferences.prefs.remove("HTML_HeaderRowDef");
        WebModulePreferences.prefs.remove("HTML_EvenRowDef");
        WebModulePreferences.prefs.remove("HTML_OddRowDef");
        WebModulePreferences.prefs.remove("HTML_TopBanner");
        WebModulePreferences.prefs.remove("DISPLAY_UseDHTML_ReportingStructure");
        WebModulePreferences.prefs.remove("DISPLAY_HideViewStaffMembersFor");
        WebModulePreferences.prefs.remove("HTML_MainFontSize");
        WebModulePreferences.prefs.remove("DEBUG_ShowSQL");
        WebModulePreferences.prefs.remove("DEBUG_FullStackTrace");
        WebModulePreferences.prefs.remove("DAEMON_DataRefreshInterval");
        WebModulePreferences.prefs.remove("DAEMON_DataRefreshStartTime");
        WebModulePreferences.prefs.remove("HTML_InnerTableDefn");
        WebModulePreferences.prefs.remove("HTML_HeaderRowDefn");
        WebModulePreferences.prefs.remove("HTML_EvenRowDefn");
        WebModulePreferences.prefs.remove("HTML_OddRowDefn");
        WebModulePreferences.prefs.remove("HTML_OuterTableDefn");
        WebModulePreferences.prefs.remove("DAEMON_ProcessInterval");
    }

    public static Preferences getPreferences() {
        return WebModulePreferences.prefs;
    }

    /**
     * The WebPreferences are backed up to a file in the user directory. This
     * allows for manipulation of the preferences without accessing the
     * registry. During start-up, all changes in this file will be imported into
     * the preferences.
     * @return 
     */
    public String getBackupFileName() {
        return System.getProperty("user.dir") + FileContainer.getOSFileSeperator() + "preferences.xml";
    }

    /**
     * Some clients prefer not to show the leave balance for certain leave types
     * @return 
     */
    public boolean sendProcessReminders() {
        return (getPreference(DAEMON_SendProcessReminders, "True").toUpperCase().compareToIgnoreCase("True") == 0);
    }

    /**
     * Some clients prefer not to show the leave balance for certain leave types
     * @param leaveType
     * @return 
     */
    public boolean hideLeaveBalanceFor(String leaveType) {
        return (getPreference(DISPLAY_HideLeaveBalanceFor, "").toUpperCase().indexOf(leaveType.toUpperCase()) >= 0);
    }

    /**
     * Some clients prefer not to show all the 'View information sections
     * @param viewCaption
     * @return 
     */
    public boolean hideViewInformationFor(String viewCaption) {
        return (getPreference(DISPLAY_HideViewInformationFor, "").toUpperCase().indexOf(viewCaption.toUpperCase()) >= 0);
    }

    /**
     * Some clients prefer not to show all the 'Request changes sections
     * @param viewCaption
     * @return 
     */
    public boolean hideRequestChangesFor(String viewCaption) {
        return (getPreference(DISPLAY_HideRequestChangesFor, "").toUpperCase().indexOf(viewCaption.toUpperCase()) >= 0);
    }

    /**
     * Some clients prefer not to allow for the application of certain leave
     * types
     * @param leaveType
     * @return 
     */
    public boolean hideLeaveRequestsForLeaveType(String leaveType) {
        return (getPreference(DISPLAY_HideLeaveRequestsForLeaveType, "").toUpperCase().trim().indexOf(leaveType.toUpperCase()) >= 0);
    }
    
    /**
     * Display 'vanilla' leave types, or use the profile description in l_profile?
     * @return 
     */
    public boolean useLeaveProfileDescription() {
        return (getPreference(DISPLAY_UseLeaveProfileDescription, "False").toUpperCase().compareToIgnoreCase("True") == 0);
    }    

    private boolean containsKey(String aKey, String[] keys) {
        for (String key : keys) {
            if (key.compareToIgnoreCase(aKey) == 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the preference, and adding it to the preferences class if it does
     * not already exist
     * @param preference
     * @param defaultValue
     * @return 
     */
    public String getPreference(String preference, String defaultValue) {
        String rslt = prefs.get(preference, "");
        if (rslt.equals("")) {
            //System.out.println("Note: getPreference(" + preference + "," + defaultValue + ") not found.  Default value returned.");
            rslt = defaultValue;
        }
        // Add to preferences if it does not already exist.
        try {
            if (!containsKey(preference, getPreferences().keys())) {
                getPreferences().put(preference, defaultValue);
                System.out.println("Adding new key:" + preference + " - " + defaultValue);
                try {
                    prefs.exportNode(new java.io.FileOutputStream(za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().getBackupFileName()));
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        } catch (BackingStoreException be) {
            be.printStackTrace();
        }

        return rslt;
    }
    
    public static String DAEMON_SendProcessReminders = "DAEMON_SendProcessReminders";
    public static String HTML_HeaderRowDefn = "HTML_HeaderRowDefn";
    public static String HTML_EvenRowDefn = "HTML_EvenRowDefn";
    public static String HTML_OddRowDefn = "HTML_OddRowDefn";
    public static String HTML_MenuFontFamily = "HTML_MenuFontFamily";
    public static String HTML_MenuFontSize = "HTML_MenuFontSize";
    public static String HTML_MainFontFamily = "HTML_MainFontFamily";
    public static String HTML_CompanyLogo = "HTML_CompanyLogo";
    public static String HTML_Timeout = "HTML_Timeout";
    public static String SMTP_Server = "SMTP_Server";
    public static String SMTP_Port = "SMTP_Port";
    public static String SMTP_FromAddress = "SMTP_FromAddress";
    public static String SMTP_AdminNotificationAddress = "SMTP_AdminNotificationAddress";
    public static String SMTP_SendEMAILNotifications = "SMTP_SendEMAILNotifications";
    public static String SMTP_SendPasswordNotifications = "SMTP_SendPasswordNotifications";
    public static String SMTP_SendEMAILMsgOnLogin = "SMTP_SendEMAILMsgOnLogin";
    public static String Email_Suppress_Hyperlinks = "Email_Suppress_Hyperlinks";
    public static String SMTP_EMailNotificationSubjectString = "SMTP_EMailNotificationSubjectString";
    public static String SMTP_RequireAuthentication = "SMTP_RequireAuthentication";
    public static String SMTP_UseSSL = "SMTP_UseSSL";
    public static String SMTP_AuthenticationUser = "SMTP_AuthenticationUser";
    public static String SMTP_AuthenticationPwd = "SMTP_AuthenticationPwd";
    public static String SMTP_SendICalAttachmentOnApproval = "SMTP_SendICalAttachmentOnApproval";
    public static String SMTP_SendICalAttachmentToLineManagers = "SMTP_SendICalAttachmentToLineManagers";
    public static String WORKFLOW_JumpToTopAfterFirstApproval = "WORKFLOW_JumpToTopAfterFirstApproval";
    public static String WORKFLOW_ApplyAfterFirstApproval = "WORKFLOW_ApplyAfterFirstApproval";
    public static String WORKFLOW_UseShortestRoute = "WORKFLOW_UseShortestRoute";
    public static String WORKFLOW_IncludeTopLevelInManagement = "WORKFLOW_IncludeTopLevelInManagement";
    public static String DISPLAY_HideLeaveBalanceFor = "DISPLAY_HideLeaveBalanceFor";
    public static String DISPLAY_HideViewInformationFor = "DISPLAY_HideViewInformationFor";
    public static String DISPLAY_HideRequestChangesFor = "DISPLAY_HideRequestChangesFor";
    public static String DISPLAY_HideLeaveRequestsForLeaveType = "DISPLAY_HideLeaveRequestsForLeaveType";
    public static String DISPLAY_UseLeaveProfileDescription = "DISPLAY_UseLeaveProfileDescription";
    public static String DISPLAY_AllowTnAHoursFor = "DISPLAY_AllowTnAHoursFor";
    public static String DISPLAY_UserDefinedFieldForTnAReasons = "DISPLAY_UserDefinedFieldForTnAReasons";
    public static String DISPLAY_canChangeMedAidDependent = "DISPLAY_canChangeMedAidDependent";
    public static String FILTER_FilterProcessesByCreationDate = "FILTER_FilterProcessesByCreationDate";
    public static String URL_Database = "DATABASE_Url";
    public static String MEMORY_OnlyLoadActiveProcesses = "MEMORY_OnlyLoadActiveProcesses";
    public static String URL_Web = "WEB_Url";
    public static String PASSWORDS_ExpireAfterNDays = "PASSWORDS_ExpireAfterNDays";
    public static String PASSWORDS_reUsable = "PASSWORDS_reUsable";
    public static String DAEMON_MobiRefreshInterval = "DAEMON_MobiRefreshInterval";
    public static String FILTER_FilterOnlyPlannedTrainingEvents = "FILTER_FilterOnlyPlannedTrainingEvents";
    private static WebModulePreferences webModulePreferences = null;
    private static Preferences prefs;
}
