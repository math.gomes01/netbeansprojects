/*
 * The EventMonitor class is responsible for polling the server 
 * (once a day) and see who should receive what emails
 */
package za.co.accsys.peopleweb;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

/**
 *
 * @author liam
 */
public class EventMonitor {

    /**
     * Queries [PeopleWeb_URL/get_sysinfo.jsp for a list of accounts that adheres to the particular
     * constraints For each of these, an email is generated through MailChimp (using Mandarill).
     *
     * @param url PeopleWeb URL
     * @param templateName Mandarill/MailChimp template name
     * @param nbDaysOfInactivity
     * @param nbDaysSinceActivation
     * @param mailSubject
     * @param mailFromMail
     * @param mailFromName
     *
     */
    public void sendEmailNotifications(String url, String templateName, int nbDaysOfInactivity,
            int nbDaysSinceActivation, String mailSubject, String mailFromMail, String mailFromName) {

        // Let's build a proper URL
        String fullURL;
        if (url.endsWith("/")) {
            fullURL = url + JSP_NAME;
        } else {
            fullURL = url + "/" + JSP_NAME;
        }
        // Add the necessary parameter
        if (nbDaysOfInactivity >= 0) {
            fullURL += "&" + JSP_PARAM_I + "=" + nbDaysOfInactivity;
        }
        if (nbDaysSinceActivation >= 0) {
            fullURL += "&" + JSP_PARAM_A + "=" + nbDaysSinceActivation;
        }

        // Get the list from the server
        System.out.println("\n\tRequest: " + fullURL);
        ArrayList<MailChimpClientAccount> accounts = getAccountListFromServer(fullURL);
        System.out.println("\n\tResponse: " + accounts.toString());

        // Now that we have the list, let's send out the emails
        if (MailChimpMailer.getInstance().sendMail(accounts, templateName, mailSubject, mailFromMail, mailFromName)) {
            System.out.println("\n\tDone.");
        } else {
            System.out.println("\n\tProblems encountered with MailChimp API");
        }
    }

    /**
     * Calls a jsp on PeopleWeb to extract a list of clients that should be emailed.
     *
     * @param fullURL
     * @return
     */
    public ArrayList<MailChimpClientAccount> getAccountListFromServer(String fullURL) {
        try {
            ArrayList<MailChimpClientAccount> rslt = new ArrayList<>();
            HttpClient client = new DefaultHttpClient();
            System.out.println("getAccountListFromServer:" + fullURL);
            HttpPost post = new HttpPost(fullURL);

            // Reading the response
            StringBuilder httpResponse = new StringBuilder();

            // Executes the web service (with parameters)
            //post.setEntity(new UrlEncodedFormEntity(null));
            HttpResponse response = client.execute(post);
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String line;
            while ((line = rd.readLine()) != null) {
                httpResponse.append(line);
            }

            // Get rid of HTML
            int startAt = httpResponse.lastIndexOf(HTML_BODY_OPEN);
            int endAt = httpResponse.lastIndexOf(HTML_BODY_CLOSE);
            String htmlResponse = httpResponse.toString();
            htmlResponse = htmlResponse.subSequence(startAt + HTML_BODY_OPEN.length(), endAt).toString().trim();

            // Break it up into accounts
            // *accountID|companyName|ownerFirstName|ownerSurname|ownerEmail
            StringTokenizer tokenRow = new StringTokenizer(htmlResponse, TOKEN_ROW);
            while (tokenRow.hasMoreTokens()) {
                MailChimpClientAccount account = new MailChimpClientAccount();
                StringTokenizer tokenRecord = new StringTokenizer(tokenRow.nextToken(), TOKEN_RECORD);
                while (tokenRecord.hasMoreTokens()) {
                    // AccountID
                    account.setAccountID(tokenRecord.nextToken());
                    // CompanyName
                    account.setCompanyName(tokenRecord.nextToken());
                    // OwnerFirstName
                    account.setOwnerFirstname(tokenRecord.nextToken());
                    // OwnerSurname
                    account.setOwnerSurname(tokenRecord.nextToken());
                    // OwnerEmail
                    account.setOwnerEmail(tokenRecord.nextToken());
                }
                rslt.add(account);
            }

            return rslt;
        } catch (IOException ex) {
            Logger.getLogger(EventMonitor.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    private static final String JSP_NAME = "get_sysinfo.jsp?pwd=zapper123";
    private static final String JSP_PARAM_A = "daysSinceActivation";
    private static final String JSP_PARAM_I = "daysOfInactivity";
    private static final String HTML_BODY_OPEN = "<body>";
    private static final String HTML_BODY_CLOSE = "</body>";
    private static final String TOKEN_ROW = "*";
    private static final String TOKEN_RECORD = "|";
    private static final String TOKEN_TIME = ":";

}
