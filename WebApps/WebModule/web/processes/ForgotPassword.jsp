<%@page contentType="text/html"%>
<%@page import="za.co.ucs.accsys.webmodule.*" %>

<%
// Start by resetting the LoginSessionObject
    session.removeAttribute("loginsessionobject");
%>

<%@include file="html5_top.jsp"%>

<%@include file="html_messages.jsp"%> 
<!-- This HAS to be included if messages were generated
and added to the session attributes for this person.
Calling this script will simply display those messages -->

<% // If the File Container is busy loading data from the database, do not allow LOGINS to occur.
    if (!za.co.ucs.accsys.webmodule.FileContainer.getInstance().isESSRegistered()) {
        out.write("<br><span  class=\"standardHeading\">The PeopleWare ESS Module has not been registered.</span>");
        out.write("<br><span  class=\"standardHeading\">Please contact your Payroll/HR administrator to assist.</h5>");

    } else {
        if (za.co.ucs.accsys.webmodule.FileContainer.getInstance().isCachingFromDB()) {
            out.write("<br><h4>The system is in the process of reloading data from the database.  Please try again later.</h4>");
            out.write("<br><h5>" + za.co.ucs.accsys.webmodule.FileContainer.getInstance().getCachePercentage() + "% complete.</h5>");
        } else {
%>
<form METHOD=POST id="forgotPassword" ACTION=../Controller.jsp>
    <h1>Please enter your e-mail address</h1>
    <fieldset id="inputs">
        <input id="email" NAME="email" type="text" placeholder="E-mail" maxlength="50" autofocus required>   
    </fieldset>
    <fieldset id="actions">
        <input type="submit" id="submit" value="Send new password" name="action" style="display: inline-block;">
        <input type="button" id="backBtn" value="Back" name="back" onclick="location.href='../processes/Login.jsp'">
    </fieldset>
        
</form>
<%
        }
    }
%>

<%@include file="html_bottom.jsp"%>
