/*
 * StoredProcBase.java
 *
 * Created on October 21, 2003, 8:25 AM
 */

package za.co.ucs.lwt.dbinfo.classes;

/**
 *
 * @author  liam
 */
public class StoredProcBase {
    
    /**
     * Creates a new instance of StoredProcBase
     */
    public StoredProcBase(String name) {
        this.name = name; 
        this.lineNumber = 0;
    }
    
    public void setName(String name){
        this.name = name; }
    
    public String getName(){
        return this.name; }
    
    public int getLineNumber(){
        return this.lineNumber; }
    
    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber; }
    
    public String toString(){
        return (this.getName());
    }    
    private String name;
    private int lineNumber;    
}