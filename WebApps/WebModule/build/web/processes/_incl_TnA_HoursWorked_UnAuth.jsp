
<%@page import="za.co.ucs.lwt.db.DatabaseObject"%>
<%
        // Define Chart
        sqlStatement = new StringBuffer();
        //String fromDateM2 = za.co.ucs.lwt.db.DatabaseObject.formatDate( za.co.ucs.lwt.db.DatabaseObject.addNDays(new java.util.Date(), -31));
        //String toDateM2 = za.co.ucs.lwt.db.DatabaseObject.formatDate( za.co.ucs.lwt.db.DatabaseObject.addNDays(new java.util.Date(), -1));
        strXML = new String();
        strBXML = new StringBuffer();

 
        strBXML.append("<chart caption='Un-authorised Hours worked ("+fromDate+" - "+toDate+") ' ");
        strBXML.append("subCaption ='"+dateDaysDiff+" days' ");
        strBXML.append("xAxisName='Day' ");
         strBXML.append("startAngX='20' startAngY='-30' ");
        strBXML.append("endAngX='10' endAngY='-6' ");
        strBXML.append("showValues='1' showLabels='1' ");
        strBXML.append("shownames='1' showSum='0' overlapColumns='0' ");
        strBXML.append("showYAxisValues='1' ");
        strBXML.append("showAboutMenuItem='0' ");
        strBXML.append("bgColor='D8E6E9,FFFFFF' ");
        strBXML.append("bgAlpha='100, 100' ");
        strBXML.append("showBorder='1' ");
        strBXML.append("canvasbgAlpha='30' ");
        strBXML.append("labelDisplay='ROTATE' ");
        strBXML.append("outCnvBaseFont='Verdana'  >");
 
 //       strBXML.append("<chart palette='1' caption='Product Comparison' shownames='1' showvalues='0'  showSum='1' decimals='0' overlapColumns='0'>");
        za.co.ucs.lwt.db.DatabaseObject.setConnectInfo_AccsysJDBC(za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().getPreference(za.co.ucs.accsys.webmodule.WebModulePreferences.URL_Database, ""));


        try {

            sqlStatement = new StringBuffer();
            sqlStatement.append("select dayname(day) as theDay, \"date\"(day) as day, \"Variable\" as theVariable,");
            sqlStatement.append("MinutesAfterSCL as TimeSpentInMinutes, ");
            sqlStatement.append("right('00'||round(convert(integer,MinutesAfterSCL)/60,0),2)||':'||right('00'||mod(convert(integer,MinutesAfterSCL),60),2) as TimeSpentinMinutes_Displ ");
            sqlStatement.append("from vw_TA_DailyCalculationDetail vw with (nolock) ");
            sqlStatement.append("where Company_id=" + employee.getCompany().getCompanyID());
            sqlStatement.append(" and employee_id=" + employee.getEmployeeID());
            sqlStatement.append(" and day between '"+fromDate+"' and '"+toDate+"' ");
            sqlStatement.append(" order by day, theVariable ");

            java.sql.ResultSet rs = za.co.ucs.lwt.db.DatabaseObject.openSQL(sqlStatement.toString(), con);

            // Build class that will assist in the generation of this stacked graph
            za.co.ucs.accsys.webmodule.TimeSpentPerVariableHelperClass helperClass = new za.co.ucs.accsys.webmodule.TimeSpentPerVariableHelperClass();
            while (rs.next()) {
                anyHoursWorked = true;
                helperClass.addTimeSpentPerVariable(rs.getDate("day"), rs.getString("theVariable"), rs.getInt("TimeSpentInMinutes"), rs.getString("TimeSpentinMinutes_Displ"));
            }

            // Build Categories - Every day is a category
            java.util.Queue<String> dayCategories= new java.util.LinkedList<String>();
            java.util.Date pointer = DatabaseObject.formatDate(fromDate);
            while ( (pointer.before(DatabaseObject.formatDate(toDate))) || (pointer.equals(DatabaseObject.formatDate(toDate)))) {
                dayCategories.add(DatabaseObject.formatDate(pointer));
                pointer = DatabaseObject.addOneDay(pointer);
            }

            strBXML.append("<categories>");
            java.util.Iterator iterDays = dayCategories.iterator();
            while (iterDays.hasNext()){
                String aDay = (String)iterDays.next();
                java.text.DateFormat f = new java.text.SimpleDateFormat("EEEE");

                String dayName = f.format(DatabaseObject.formatDate(aDay));
                strBXML.append("<category label='" + dayName + "'/>");
            }
            strBXML.append("</categories>");

            // Build Series - Every Variable is a series
            java.util.HashSet variableSeries = helperClass.getVariables();
            java.util.Iterator iterVariable = variableSeries.iterator();
            while (iterVariable.hasNext()){
                String aVariable = (String)iterVariable.next();
                strBXML.append("<dataset seriesName='"+aVariable+"' showValues='0'>");
                //
                // For every Variable, step through the days and get the time spent in each
                //
                java.util.Iterator dayIterator = dayCategories.iterator();
                while (dayIterator.hasNext()){
                    String day = (String)dayIterator.next();
                    int minutesSpent = helperClass.getMinutesSpentPerDayPerVariable(day, aVariable);
                    String minutesDisplay = helperClass.getDisplayMinutesSpentPerDayPerVariable(day, aVariable);
                    strBXML.append("<set toolText='"+aVariable+" | "+minutesDisplay+"' value='"+minutesSpent/60+"'/>");
                }
                strBXML.append("</dataset>");
            }


            strBXML.append("<styles><definition>");
            strBXML.append("   <style name='Anim1' type='animation' param='_xscale' start='0' duration='1'/>");
            strBXML.append("   <style name='Anim2' type='animation' param='_alpha' start='0' duration='0.6'/>");
            strBXML.append("   <style name='DataShadow' type='Shadow' alpha='40'/></definition>");
            strBXML.append("<application><apply toObject='DIVLINES' styles='Anim1'/>");
            strBXML.append("<apply toObject='DATALABELS' styles='DataShadow,Anim2'/>");
            strBXML.append("<apply toObject='HGRID' styles='Anim2'/>");
            strBXML.append("</application></styles>");



            strBXML.append("</chart>");
            strXML = strBXML.toString();
            //strXML = "<chart palette='1' caption='Product Comparison' shownames='1' showvalues='0' numberPrefix='$' showSum='1' decimals='0' overlapColumns='0'><categories><category label='Product A'/><category label='Product B'/><category label='Product C'/><category label='Product D'/><category label='Product E'/></categories><dataset seriesName='2004' showValues='0'><set value='25601.34'/><set value='20148.82'/><set value='17372.76'/><set value='35407.15'/><set value='38105.68'/></dataset><dataset seriesName='2005' showValues='0'><set value='57401.85'/><set value='41941.19'/><set value='45263.37'/><set value='117320.16'/><set value='114845.27'/></dataset><dataset seriesName='2006' showValues='0'><set value='45000.65'/><set value='44835.76'/><set value='18722.18'/><set value='77557.31'/><set value='92633.68'/></dataset></chart>";
            

        } finally {
        }

        if (!anyHoursWorked){
            out.write("<table class=\"center\"><tr><td>No calculated hours exists for this period.</td></tr></table>");
            } else {
%>
<DIV ALIGN="CENTER"><jsp:include page="../FCharts/Includes/FusionChartsHTMLRenderer.jsp" flush="true">
        <jsp:param name="chartSWF" value="../FCharts/FusionCharts/StackedColumn3D.swf " />
        <jsp:param name="strURL" value="" />
        <jsp:param name="strXML" value="<%=strXML%>" />
        <jsp:param name="chartId" value="myNext" />
        <jsp:param name="chartWidth" value="550" />
        <jsp:param name="chartHeight" value="300" />
        <jsp:param name="debugMode" value="false" />
        <jsp:param name="registerWithJS" value="false" />

    </jsp:include>
</DIV>
<% } %>

