/*
 * WebProcess_VariableChanges_55003.java
 *
 * Created on 25 April 2005, 09:25
 */

package za.co.ucs.accsys.webmodule;
import za.co.ucs.accsys.peopleware.*;
/**
 * Specialization of WebProcess_VariableChanges for variables with indicator code 55003
 * @author  lwt
 */
public class WebProcess_VariableChanges_55003 extends WebProcess_VariableChanges {
    
    /** Creates a new instance of WebProcess_VariableChanges_55003*/
    public WebProcess_VariableChanges_55003(WebProcessDefinition processDefinition, Employee owner, Employee employee, float value, String comment) {
        super(processDefinition, owner, employee, "55003", value, comment);
    }
    
}
