<%@page contentType="text/html"%>
<%@page import="java.io.*" %>
<%@page import="java.util.*" %>
<%@page import="za.co.ucs.lwt.deploymentbuilder.*" %>

<%
    User user = (User) session.getAttribute("user");

    if (user == null) {
        session.setAttribute("loggedinuser", new String("false"));
        response.sendRedirect("/DeploymentBuilder/loginJsp.jsp");
    } else {

        FileContainer fileContainer = FileContainer.getInstance(FileContainer.getContainerFile());
        ChangeRequest changeRequest = null;
        String fileName = null;

        // File in question
        if (request.getParameter("fileName") != null) {
            fileName = (request.getParameter("fileName"));
        }

        if (fileName.trim().length() == 0) {
            String message = "No use looking for an empty file name... Duh!";
            response.sendRedirect("/DeploymentBuilder/process/ErrorMessage.jsp?message=" + message);
            return;
        }

        // Create new Find File
        if (fileName.trim().length() > 0) {
            String message = fileContainer.findFilesInTestInstallationsInHTML(fileName);
            if (message.trim().length() == 0) {
                message = "\"" + fileName + "\" is not found";
            }

            response.sendRedirect("/DeploymentBuilder/process/ErrorMessage.jsp?message=" + message);
            return;
        }

    }
%>
<%@include file="../html/bottom.html"%>