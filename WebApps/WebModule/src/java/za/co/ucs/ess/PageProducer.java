package za.co.ucs.ess;

/**
 * <p>
 * The PageProducer is a base class that will have the capability to generate HTML pages
 * based on the Object type being passed to it.
 * </p>
 */
abstract public class PageProducer {
    
    
    /** Constructs the internal contents of a display-webpage.  This
     * excludes the page-formatting sections from the <HTML> tag down to 
     * the <BODY> tag, and anything else required to format the page.
     * <br>The 'generateInformationPage' is thus stripped from most formatting
     * detail in order to allow the look-and-feel of the page to be as
     * configurable as possible.
     * 
     * @param: object - the object that needs displaying.
     */
    abstract public String generateInformationContent();
    
    //protected LoginSessionObject loginSessionObject;
} // end PageProducer



