package za.co.ucs.accsys.webmodule;

import za.co.ucs.accsys.peopleware.*;
import za.co.ucs.lwt.db.*;
import java.sql.*;

/**
 * <p>
 * A WebProcess_LeaveCancellation is an extension of WebProcess that specializes
 * in LeaveCancellation </p>
 */
public final class WebProcess_LeaveCancellation extends WebProcess {

    static final long serialVersionUID = 7486927409473376126L;

    /**
     * LeaveCancellation requires
     *
     * @param processDefinition - details of reporting structure, etc
     * @param owner - who created/is responsible for this process?
     * @param employee - which employee is cancelling leave?
     * @param history_id - entry in L_HISTORY that the employee wishes to cancel
     * @param cancellationReason - [compulsory]
     */
    public WebProcess_LeaveCancellation(WebProcessDefinition processDefinition, Employee owner, Employee employee, String history_id, String cancellationReason) {
        super(processDefinition, owner, employee);
        //System.out.println("WebProcess_LeaveCancellation(" + processDefinition + ", " + owner + ", " + employee + "," + history_id + "," + cancellationReason + "  ) ");

        if (history_id == null) {
            this.cancelProcess("Missing History ID in Leave Cancellation Request. \nPlease contact your ESS Administrator with the following detail:"
                    + "\n processDefinition:" + processDefinition + "\n Owner:" + owner + "\n Employee:" + employee + "\n Reason:" + cancellationReason);
        } else {
            this.historyID = history_id;
            // Reason for cancellation
            if (cancellationReason == null) {
                this.cancellationReason = "Initial Request";
            } else {
                this.cancellationReason = cancellationReason;
            }

            // Is this a valid leave request?
            ValidationResult valid = validate();
            if (!valid.isValid()) {
                this.cancelProcess(valid.getInvalidReason());
            } else {
                this.escalateProcess(et_INITIAL, this.cancellationReason);
            }
        }

    }

    /**
     * This method should be implemented by each and every descendent of
     * WebProcess. It returns an instance of ValidationResult which contains
     * both the isValid() property as well as a reason in case of a non-valid
     * result.
     */
    @Override
    public ValidationResult validate() {
        if (isActive()) {

            Connection con = null;
            try {
                con = DatabaseObject.getNewConnectionFromPool();

                // We start by checking weather there are sufficient leave days available.
                ResultSet rs1 = DatabaseObject.openSQL(prepareStatement(getValidationCONSTANTStatement()), con);
                if (rs1.next()) {
                    int rslt = rs1.getInt(1);
                    if (rslt < 1) {
                        rs1.close();
                        DatabaseObject.releaseConnection(con);
                        return (new ValidationResult(false, "Leave Entry not found in Leave History."));
                    }
                }
                rs1.close();

            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DatabaseObject.releaseConnection(con);
            }
        }
        return (new ValidationResult(true, ""));
    }

    @Override
    public String toString() {
        // Get the leave history
        LeaveInfo leaveInfo = null;
        if (!FileContainer.isBusyCreatingFileContainer) {
            leaveInfo = za.co.ucs.accsys.webmodule.FileContainer.getInstance().getLeaveInfoFromContainer(this.getEmployee());
        }
        if (leaveInfo == null) {
            return "Leave Cancellation: No Detail";
        }

        if (leaveInfo.getLeaveTypeIndexFromHistory(historyID) == -1) {
            return "Leave Cancellation: No Detail in Leave History";
        }

        String leaveType = leaveInfo.getLeaveType(leaveInfo.getLeaveTypeIndexFromHistory(historyID));
        LeaveHistoryEntry entry = leaveInfo.getLeaveHistoryEntry(historyID);
        if (entry == null) {
            return "Leave Cancellation: No Entry found in Leave History";
        }

        StringBuilder result = new StringBuilder();
        result.append("Leave Cancellation for: ").append(leaveType);
        result.append(" [").append(DatabaseObject.formatDate(entry.getFromDate())).append(" - ").append(DatabaseObject.formatDate(entry.getToDate())).append("] ");
        result.append("\n").append(String.valueOf(entry.getUnitsTaken())).append(" ").append(entry.getUnit()).append("\nReason:").append(cancellationReason);
        return result.toString();
    }

    @Override
    public String toHTMLString() {
        // Get the leave history
        LeaveInfo leaveInfo = null;
        int leaveTypeIndex = -1;
        if (!FileContainer.isBusyCreatingFileContainer) {
            leaveInfo = za.co.ucs.accsys.webmodule.FileContainer.getInstance().getLeaveInfoFromContainer(this.getEmployee());
            leaveTypeIndex = leaveInfo.getLeaveTypeIndexFromHistory(historyID);
        }
        if (leaveTypeIndex == -1) {
            return ("Unable to locate the specific leave entry."
                    + "Please wait a few minutes for the database to refresh, before trying again.");
        } else {
            String leaveType = leaveInfo.getLeaveType(leaveTypeIndex);
            LeaveHistoryEntry entry = leaveInfo.getLeaveHistoryEntry(historyID);

            StringBuilder result = new StringBuilder();
            result.append("Process Number: [").append(this.hashCode()).append("]<br>");
            result.append("<table class=process_detail><font size='-3'><tr><td>Leave Cancellation For:</td><td><i>").append(leaveType).append("</i></td>");
            result.append("<td>[").append(DatabaseObject.formatDate(entry.getFromDate())).append(" - ").append(DatabaseObject.formatDate(entry.getToDate())).append("]</td></tr>");
            result.append("<tr><td>Duration:</td><td>").append(String.valueOf(entry.getUnitsTaken()));
            result.append(" ").append(entry.getUnit()).append("</td></tr>");
            result.append("<tr><td>Reason for Cancellation</td><td>").append(this.cancellationReason).append("</td></tr></font></table>");
            return result.toString();
        }
    }

    /**
     * This statement gets executed when the process is to be written to the
     * database.
     */
    @Override
    String getCommitStatement() {
        // Compile the SQL statement used to validate weather the
        // employee can take leave or not.
        return prepareStatement(getCommitCONSTANTStatement());
    }

    /**
     * Internal method to perform parameter replacements
     */
    private String prepareStatement(String aStatementWithParameters) {
        String sqlString = aStatementWithParameters;
        sqlString = sqlString.replaceAll(":companyID", getEmployee().getCompany().getCompanyID().toString());
        sqlString = sqlString.replaceAll(":employeeID", getEmployee().getEmployeeID().toString());
        //System.out.println("===replaceAll=== " + historyID);
        sqlString = sqlString.replaceAll(":historyID", historyID);
        sqlString = sqlString.replaceAll(":cancellationReason", this.cancellationReason.replaceAll("'", "`"));
        return sqlString;
    }
    private String historyID;
    private String cancellationReason;
    private EmployeeSelection initialEmployeeSelection;

    // Does the entry in L_HISTORY really belong to the EMPLOYEE?
    private String getValidationCONSTANTStatement() {
        return ("select count(*) from L_HISTORY  with (NOLOCK) where HISTORY_ID=:historyID and "
                + "company_id=:companyID and employee_id=:employeeID");
    }

    private String getCommitCONSTANTStatement() {
        return ("update l_history set CANCEL_DATE=now(*), REASON=':cancellationReason', TIME_STAMP=now(*) "
                + "where company_id=:companyID and employee_id=:employeeID and history_id=:historyID");
    }

    @Override
    public String getProcessName() {
        return "Leave Cancellation";
    }
} // end LeaveRequisitionProcess

