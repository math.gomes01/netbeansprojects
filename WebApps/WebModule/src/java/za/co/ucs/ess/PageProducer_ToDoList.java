package za.co.ucs.ess;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.TreeMap;
import za.co.ucs.accsys.peopleware.Employee;
import za.co.ucs.accsys.peopleware.TREvent;
import za.co.ucs.accsys.webmodule.*;
import za.co.ucs.lwt.db.DatabaseObject;

/**
 * <p> PageProducer_ToDoList inherits from PageProducer and will display the
 * list of processes assigned to this person to take care of. If 'current' -
 * true, currently assigned processes will be listed. If 'current' - false,
 * historical processes that had been assigned to this employee at one point in
 * time, will be listed. </p>
 */
public class PageProducer_ToDoList extends PageProducer {

    /**
     * Constructor
     *
     * @param owner person assigned to a list of processes
     * @param current should we display the processes currently assigned to this
     * owner, or list processes that were previously assigned to this owner?
     * @param action
     * @param orderBy
     * @param filterByProcess
     * @param sFromDate
     * @param sToDate
     */
    public PageProducer_ToDoList(Employee owner, boolean current, String action, String orderBy, String filterByProcess, String sFromDate, String sToDate) {
        this.owner = owner;
        if (orderBy == null) {
            orderBy = "EmployeeNumber";
        }

        this.orderBy = orderBy.trim();
        this.current = current; // Currently assigned, or previously assigned (historical)
        this.action = action;
        if (filterByProcess == null) {
            filterByProcess = action_NOFILTER;
        }
        this.filterByProcess = filterByProcess;
        // <editor-fold defaultstate="collapsed" desc="INITITATION of START-DATE and END-DATE">

        if ((sFromDate == null)) {
            this.fromDate = DatabaseObject.formatDate(DatabaseObject.removeOneYear(new java.util.Date()));
        } else {
            this.fromDate = sFromDate;
        }

        if ((sToDate == null)) {
            this.toDate = DatabaseObject.formatDate(DatabaseObject.addOneYear(new java.util.Date()));
        } else {
            this.toDate = sToDate;
        }

        if (DatabaseObject.formatDate(this.fromDate).after(DatabaseObject.formatDate(this.toDate))) {
            this.toDate = this.fromDate;
        }

        //</editor-fold>    
    }

    public Employee getOwner() {
        return owner;
    }

    public void setOwner(Employee owner) {
        this.owner = owner;
    }

    /**
     * A ToDo-List Page Producer
     */
    @Override
    public String generateInformationContent() {
        //System.out.println("PageProducer_ToDoList: generateInformationContent");
        boolean filterOnCreationDate = WebModulePreferences.getInstance().getPreference(WebModulePreferences.FILTER_FilterProcessesByCreationDate, "TRUE").compareToIgnoreCase("TRUE") == 0;

        StringBuilder resultPage = new StringBuilder();
        FileContainer fileContainer = FileContainer.getInstance();

        // **** Let us create a list of processes ****
        java.util.TreeMap myProcesses;

        // List of processes currently assigned to this user
        if (current) {
            myProcesses = fileContainer.getWebProcessesForActiveList(this.owner);
        } else {
            myProcesses = fileContainer.getWebProcessesForHistoricalList(this.owner);
        }


        if (myProcesses.size() > 0) {
            String baseURL = WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Web, "");
            String trailingURL = "  >";

            String headerLinkEmpNum = "<a href=\"proc_todo.jsp?orderBy=EmployeeNumber&cbbProcessFilter=" + filterByProcess + "&fromDate=" + this.fromDate + "&toDate=" + this.toDate + "\"" + trailingURL;
            String headerLinkProcessName = "<a href=\"proc_todo.jsp?orderBy=ProcessName&cbbProcessFilter=" + filterByProcess + "&fromDate=" + this.fromDate + "&toDate=" + this.toDate + "\"" + trailingURL;
            String headerLinkCreationDate = "<a href=\"proc_todo.jsp?orderBy=CreationDate&cbbProcessFilter=" + filterByProcess + "&fromDate=" + this.fromDate + "&toDate=" + this.toDate + "\"" + trailingURL;
            String headerLinkProcessAge = "<a href=\"proc_todo.jsp?orderBy=ProcessAge&cbbProcessFilter=" + filterByProcess + "&fromDate=" + this.fromDate + "&toDate=" + this.toDate + "\"" + trailingURL;
            String headerLinkAssignedAge = "<a href=\"proc_todo.jsp?orderBy=AssignedAge&cbbProcessFilter=" + filterByProcess + "&fromDate=" + this.fromDate + "&toDate=" + this.toDate + "\"" + trailingURL;

            // <editor-fold defaultstate="collapsed" desc="/// Creating Filters ///">
            //
            // Creating filters
            //

            if (current) {
                resultPage.append("  <FORM action=proc_todo.jsp method=\"post\" name=\"ToDoList\" >");
                resultPage.append("<center>");
                resultPage.append("<h3 class=\"shaddow\">Process Filters</h3>");
                resultPage.append("<div class=\"grouping\">");
                resultPage.append("  <table width=\"900\" cellpadding=\"10\"><tr><td>");

                // No use having 'Select ALL', etc. if there is only one process
                if ((myProcesses.size() > 1) && (current)) {
                    // Create the table that allows for multiple APPROVE/REJECT
                    resultPage.append("    <table class=\"select_all_select_none\"><tr><td>");
                    resultPage.append("      <a class=\"hintanchor\" onMouseover=\"showhint('Select All',this,event,'80px')\" href=\"proc_todo.jsp?action=1&cbbProcessFilter=").append(filterByProcess).append("&fromDate=").append(this.fromDate).append("&toDate=").append(this.toDate).append("\" class=\".mediumFontNoBackground\" ><img alt=\"Select All\" src=\"../images/selectall.gif\"></a></td><td>");
                    resultPage.append("      <a class=\"hintanchor\" onMouseover=\"showhint('Select None',this,event,'80px')\" href=\"proc_todo.jsp?action=&cbbProcessFilter=").append(filterByProcess).append("&fromDate=").append(this.fromDate).append("&toDate=").append(this.toDate).append("\" class=\".mediumFontNoBackground\" ><img alt=\"Select All\" src=\"../images/selectnone.gif\"></a></td>");
                } else {
                    resultPage.append("      <table class=\"select_all_select_none_wide\"><tr><td>");
                    resultPage.append("      <a class=\"hintanchor\" onMouseover=\"showhint('Select All',this,event,'80px')\" href=\"proc_todo.jsp?action=1&cbbProcessFilter=").append(filterByProcess).append("&fromDate=").append(this.fromDate).append("&toDate=").append(this.toDate).append("\" class=\".mediumFontNoBackground\" ><img alt=\"Select All\" src=\"../images/selectall.gif\"></a></td><td>");
                    resultPage.append("      <a class=\"hintanchor\" onMouseover=\"showhint('Select None',this,event,'80px')\" href=\"proc_todo.jsp?action=0&cbbProcessFilter=").append(filterByProcess).append("&fromDate=").append(this.fromDate).append("&toDate=").append(this.toDate).append("\"  class=\".mediumFontNoBackground\" ><img alt=\"Select All\" src=\"../images/selectnone.gif\"></a></td>");
                }
                resultPage.append("    </td></tr>");
                resultPage.append("    </table>");


                // <editor-fold defaultstate="collapsed" desc="/// Display Process Filter ///">
                resultPage.append("  </td><td>");

                resultPage.append("  <table id=\"displaytable_filter\"><tr>");

                resultPage.append("  <td><b>Filter Process</b><br>");
                resultPage.append("<select style=\"font-size: 11px; margin-top: 5px;\"  name=\"cbbProcessFilter\" >");

                // First row, no filter
                if (filterByProcess.compareToIgnoreCase(action_NOFILTER) == 0) {
                    resultPage.append("<option VALUE=\"").append(action_NOFILTER).append("\" selected>").append(action_NOFILTER).append("</option>");
                } else {
                    resultPage.append("<option VALUE=\"").append(action_NOFILTER).append("\" >").append(action_NOFILTER).append("</option>");
                }


                // List of all processes in this person's INBOX
                LinkedList procNameList = new LinkedList(myProcesses.values());
                HashSet aSet = new HashSet();
                // Place all the processes into a sorted map.
                for (int i = 0; i < procNameList.size(); i++) {
                    String processName = ((WebProcess) procNameList.get(i)).getProcessName();
                    aSet.add(processName);
                }
                Iterator filterIter = aSet.iterator();
                while (filterIter.hasNext()) {
                    String processName = (String) filterIter.next();
                    // Do have a selected Filter
                    if (processName.compareToIgnoreCase(filterByProcess) == 0) {
                        resultPage.append("<option VALUE=\"").append(processName).append("\" selected>").append(processName).append("</option>");
                    } else {
                        resultPage.append("<option VALUE=\"").append(processName).append("\" >").append(processName).append("</option>");
                    }
                }

                resultPage.append("</select></td>");
                //</editor-fold>

                // <editor-fold defaultstate="collapsed" desc="/// Display Date ///">
                if (filterOnCreationDate) {
                    //resultPage.append("<td><table width=\"480\" id=\"displaytable_filter_NoBorder\" ><tr><td>Filter Period (Creation Date)</td></tr><tr>");
                    resultPage.append("<td><fieldset style=\"border: 1px #A3A3A3 solid; margin-left: 10px;\"><legend style=\"margin-left: 15px; font-weight: bold;\">Filter Period (Creation Date)</legend><table margin=\"5px;\" width=\"480\" id=\"displaytable_filter_NoBorder\"><tr>");
                } else {
                    //resultPage.append("<td><table width=\"480\" id=\"displaytable_filter_NoBorder\" ><tr><td>Filter Period (Action Date)</td></tr><tr>");
                    resultPage.append("<td><fieldset style=\"border: 1px #A3A3A3 solid; margin-left: 10px;\"><legend style=\"margin-left: 15px; font-weight: bold;\">Filter Period (Action Date)</legend><table margin=\"5px;\" width=\"480\" id=\"displaytable_filter_NoBorder\"><tr>");
                }

                //
                // From Date and To Date Picker (START)
                //
                resultPage.append("");
                // From Date
                //
                resultPage.append("<td width=\"30\" align=\"right\" >From &nbsp;");

                resultPage.append("<input style=\"font-size: 11px\" value=\"").append(this.fromDate).append("\" type=\"text\" name=\"fromDate\" size=10 maxlength=10");
                //resultPage.append(" onFocus=\"javascript:vDateType='2'\"");
                //resultPage.append(" onKeyUp=\"DateFormat(this,this.value,event,false,'2')\" ");
                //resultPage.append(" onBlur=\"DateFormat(this,this.value,event,false,'2')\" ");
                resultPage.append(" placeholder=\"yyyy/mm/dd\" >");

                resultPage.append(" <a class=\"hintanchor\" onMouseover=\"showhint('Select the From date.', this, event, '130px')\" href=\"javascript:showCal('Calendar7')\"><img style=\"border: 0px solid ; \"  align=\"top\" alt=\"From Date\" src=\"../images/calendar.jpg\"></a></td>");
                // Separator 
                //
                resultPage.append("<td width=\"5\"></td>");
                // To Date
                //
                resultPage.append("<td width=\"30\"  align=\"right\" >To &nbsp;");

                resultPage.append("<input style=\"font-size: 11px\" value=\"").append(this.toDate).append("\" type=\"text\" name=\"toDate\" size=10 maxlength=10");
                //resultPage.append(" onFocus=\"javascript:vDateType='2'\"");
                //resultPage.append(" onKeyUp=\"DateFormat(this,this.value,event,false,'2')\" ");
                //resultPage.append(" onBlur=\"DateFormat(this,this.value,event,false,'2')\" ");
                resultPage.append(" placeholder=\"yyyy/mm/dd\" >");

                resultPage.append(" <a class=\"hintanchor\" onMouseover=\"showhint('Select the To date.', this, event, '130px')\" href=\"javascript:showCal('Calendar8')\"><img style=\"border: 0px solid ; \" align=\"top\"  alt=\"To Date\" src=\"../images/calendar.jpg\"></a></td>");
                resultPage.append("</td></tr></table>");
                //
                // From Date and To Date Picker (END)
                //

                resultPage.append("</tr></table></fieldset>");


                resultPage.append("</td><td  width=\"10%\" ><input class=\"gradient-button-grey\" type=\"submit\" value=\"Apply Filter\" name=\"filterByProcess\"></td>");
                //</editor-fold>
                resultPage.append("</td></tr></table></div></center><br>");
                resultPage.append("</FORM>");
            }

            //</editor-fold>




            // List of all processes in this person's INBOX
            LinkedList unsortedProcesses = new LinkedList(myProcesses.values());

            // <editor-fold defaultstate="collapsed" desc="/// Aplying the Sort/Filter mechanism ///">
            // Place all the processes into a sorted map.
            for (int i = 0; i < unsortedProcesses.size(); i++) {
                String key = null;
                WebProcess process = (WebProcess) unsortedProcesses.get(i);
                //
                // Aply Process Filter
                if ((filterByProcess.compareToIgnoreCase(action_NOFILTER) == 0) || (filterByProcess.compareToIgnoreCase(process.getProcessName()) == 0)) {
                    // 
                    // Apply Date Filter
                    //
                    java.util.Date filterDate;
                    if (!filterOnCreationDate) {
                        // <editor-fold defaultstate="collapsed" desc="/// Filter on ACTION DATE (where applicable) ///">
                        // Note: If a process does not have an Action date, we'll list it every time by assigning the filter date to the From Date value
                        //
                        filterDate = DatabaseObject.addOneDay(DatabaseObject.formatDate(fromDate));

                        //
                        // For Leave Requests: Filter on the 'From Date'
                        //
                        if (process.getProcessDefinition().getProcessClassName().contains("za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition")) {
                            filterDate = ((WebProcess_LeaveRequisition) process).getFromDate();
                        }
                        //
                        // For Training Requests: Filter on the 'From Date'
                        //
                        if (process.getProcessDefinition().getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_TrainingRequisition") == 0) {
                            TREvent event = new TREvent(((WebProcess_TrainingRequisition) process).getEventID());
                            filterDate = event.getFromDate();
                        }
                        //
                        // For Daily Signoff: Filter on the 'From Date'
                        //
                        if (process.getProcessDefinition().getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_DailySignoff") == 0) {
                            filterDate = DatabaseObject.formatDate(((WebProcess_DailySignoff) process).getAuthDay());
                        }
                        //</editor-fold>     
                    } else {
                        // <editor-fold defaultstate="collapsed" desc="/// Filter on CREATION DATE ///">
                        filterDate = process.getCreationDate();
                        //</editor-fold>                        
                    }

                    if (((filterDate.equals(DatabaseObject.formatDate(this.fromDate)))
                            || (filterDate.after(DatabaseObject.formatDate(this.fromDate))))
                            && (filterDate.before(DatabaseObject.addOneDay(DatabaseObject.formatDate(this.toDate))))) {
                        if ((this.orderBy == null) || (this.orderBy.compareToIgnoreCase("EmployeeNumber") == 0)) {
                            key = ((WebProcess) unsortedProcesses.get(i)).getEmployee().getSurname() + ((WebProcess) unsortedProcesses.get(i)).getEmployee().getEmployeeNumber() + ((WebProcess) unsortedProcesses.get(i)).hashCode();
                        }
                        if (this.orderBy.compareToIgnoreCase("ProcessName") == 0) {
                            key = ((WebProcess) unsortedProcesses.get(i)).getProcessName() + ((WebProcess) unsortedProcesses.get(i)).getEmployee().getEmployeeNumber() + ((WebProcess) unsortedProcesses.get(i)).hashCode();
                        }
                        if (this.orderBy.compareToIgnoreCase("CreationDate") == 0) {
                            key = ((WebProcess) unsortedProcesses.get(i)).getCreationDate() + ((WebProcess) unsortedProcesses.get(i)).getEmployee().getEmployeeNumber() + ((WebProcess) unsortedProcesses.get(i)).hashCode();
                        }
                        if (this.orderBy.compareToIgnoreCase("ProcessAge") == 0) {
                            key = ((WebProcess) unsortedProcesses.get(i)).getProcessAge() + ((WebProcess) unsortedProcesses.get(i)).getEmployee().getEmployeeNumber() + ((WebProcess) unsortedProcesses.get(i)).hashCode();
                        }
                        if (this.orderBy.compareToIgnoreCase("AssignedAge") == 0) {
                            key = ((WebProcess) unsortedProcesses.get(i)).getActiveStageAge() + ((WebProcess) unsortedProcesses.get(i)).getEmployee().getEmployeeNumber() + ((WebProcess) unsortedProcesses.get(i)).hashCode();
                        }
                        processes.put(key, unsortedProcesses.get(i));
                    }
                }

            }

            //</editor-fold>
            // Multi-employees
            String[] staticHeaders = {"", "Employee", "Process Name", "Process Detail", "Creation Date", "Process Age (Days)", "Date assigned to you"};
            String[] linkedHeaders = {"", headerLinkEmpNum + "Employee</a>", headerLinkProcessName + "Process Name</a>", "Process Detail", headerLinkCreationDate + "Creation Date</a>", headerLinkProcessAge + "Process Age (Days)</a>", headerLinkAssignedAge + "Date assigned to you</a>"};


            // List of WebProcesses currently owned - or previously owned - by this.owner
            String[][] contents = new String[7][processes.values().size()];

            // construct the contents matrix
            Iterator iter = processes.values().iterator();
            int row = 0;

            while (iter.hasNext()) {
                WebProcess process = (WebProcess) iter.next();
                // Process Name
                // Only allow for editing of process if
                // a. It is still active
                // b. This user is the current owner of that process
                contents[1][row] = process.getEmployee().toString();
                // Process Name
                if ((process.isActive()) && (current)) {
                    String link = "<a href=\"proc_action.jsp?hashValue=" + process.hashCode() + "\" >";
                    contents[2][row] = link + process.getProcessName() + "</a>";

                    if (this.action.equals(action_SELECTALL)) {
                        contents[0][row] = "<input name=\"selected_" + row + "\" checked=\"checked\" value=\"" + process.hashCode() + "\" type=\"checkbox\">";
                    } else {
                        contents[0][row] = "<input name=\"selected_" + row + "\" value=\"" + process.hashCode() + "\" type=\"checkbox\">";
                    }
                } else {
                    String link = "<a href=\"proc_prev.jsp?hashValue=" + process.hashCode() + "\" >";
                    if ((process.isActive())) {
                        contents[2][row] = link + process.getProcessName() + " (Active)</a>";
                    } else {
                        contents[2][row] = link + process.getProcessName() + "<font color=\"#ff0000\"> (Closed)</font></a>";
                    }
                    contents[0][row] = "<input disabled=\"disabled\" name=\"selected_" + process.hashCode() + "\" type=\"checkbox\">";

                }
                // Detail
                contents[3][row] = process.toHTMLString();
                // Creation Date
                contents[4][row] = process.formatDate(process.getCreationDate());
                // Process Age

                contents[5][row] = (new Integer(DatabaseObject.getNumberOfDaysBetween(process.getCreationDate(), new java.util.Date()))).toString();
                // Date assigned to me
                contents[6][row] = process.formatDate(process.getCurrentStage().getCreationDate());
                row++;
            }

            //resultPage.append("<FORM action=proc_todo.jsp method=\"post\" onSubmit=\"return verify()\">");
            if (current) {
                resultPage.append("<FORM action=proc_todo.jsp method=\"post\" onSubmit=\"return confirm('Are you sure?');\">");
            }
            resultPage.append("<center><h3 class=\"shaddow\">Process List</h3>");
            // Add contents
            if (current) {
                resultPage.append(PageSectionGenerator.getInstance().buildHTMLTable("displaytable_wide", linkedHeaders, contents));
            } else {
                resultPage.append(PageSectionGenerator.getInstance().buildHTMLTable("displaytable_wide", staticHeaders, contents));
            }

            if (current) {
                // Action buttons and accompanied <FORM>'s

                resultPage.append("<!-- Horizontal Ruler -->");
                resultPage.append("<hr class=\"center\">");
                resultPage.append("<table class=\"center\"  >");
                resultPage.append("<tr><td width=\"80%\"></td><td align=\"right\" width=100><input class=\"gradient-button-green\" type=\"submit\" name=\"ActionButton\" value=\"Approve\"  ></td>");
                resultPage.append("<td align=\"right\" width=100><input class=\"gradient-button\" type=\"submit\" name=\"ActionButton\" value=\"Reject\"  >");
                resultPage.append("</td></tr></table>");
                resultPage.append("</center>");

                // Close <FORM>
                resultPage.append("</FORM>");

                // Close <FORM>
                resultPage.append("</FORM>");



            }
        } else {
            // No processes to display

            resultPage.append("<br><CENTER><span class=\"standardHeading_NotBold\">There are no processes to display</span></CENTER>");
        }
        return resultPage.toString();
    }
    private Employee owner;
    private String orderBy;
    private boolean current;
    private String action;
    private String filterByProcess;
    private TreeMap processes = new TreeMap();
    public static String action_SELECTNONE = "0";
    public static String action_SELECTALL = "1";
    public static String action_NOFILTER = "< No Filter >";
    private String fromDate;
    private String toDate;
} // end LeavePageProducer

