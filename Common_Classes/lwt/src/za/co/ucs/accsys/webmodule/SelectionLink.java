package za.co.ucs.accsys.webmodule;

/**
 * <p>
 * SelectionLink is a simple class that simply points from one EmployeeSelection
 * to another.  This is used to build up a hierarchy of people reporting to
 * other people.
 * </p>
 */
public class SelectionLink implements java.io.Serializable {
//    private static final long serialVersionUID = -6818491189411835222L;
    
    public SelectionLink(EmployeeSelection reportsFrom, EmployeeSelection reportsTo){
        this.reportsFrom = reportsFrom;
        this.reportsTo = reportsTo;
    }
    
    /**
     * Getter for property reportsFrom.
     * @return Value of property reportsFrom.
     */
    public za.co.ucs.accsys.webmodule.EmployeeSelection getReportsFrom() {
        return reportsFrom;
    }
    
    /**
     * Setter for property reportsFrom.
     * @param reportsFrom New value of property reportsFrom.
     */
    public void setReportsFrom(za.co.ucs.accsys.webmodule.EmployeeSelection reportsFrom) {
        this.reportsFrom = reportsFrom;
    }
    
    /**
     * Getter for property reportsTo.
     * @return Value of property reportsTo.
     */
    public za.co.ucs.accsys.webmodule.EmployeeSelection getReportsTo() {
        return reportsTo;
    }
    
    /**
     * Setter for property reportsTo.
     * @param reportsTo New value of property reportsTo.
     */
    public void setReportsTo(za.co.ucs.accsys.webmodule.EmployeeSelection reportsTo) {
        this.reportsTo = reportsTo;
    }
    
    @Override
    public String toString(){
        return ("From:"+reportsFrom.getName()+" To:"+reportsTo.getName());
    }
    
    @Override
    public int hashCode(){
       int v = toString().compareTo("SelectionLink:");
       return v;
    }

    
   
    private EmployeeSelection reportsFrom;
    private EmployeeSelection reportsTo;
} // end SelectionLink



