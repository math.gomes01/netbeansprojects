<%@page contentType="text/html"%>

<%@include file="html5_top.jsp"%>

<%@include file="html_messages.jsp"%> 
<!-- This HAS to be included if messages were generated
and added to the session attributes for this person.
Calling this script will simply display those messages -->

<!-- Now we need to verify that there is a LoginSessionObject -->

<%  // Page Top
    if (loginSessionObject == null) {
        response.sendRedirect(response.encodeURL("Login.jsp"));
    } else {
        PageSectionGenerator gen = PageSectionGenerator.getInstance();
        out.write(gen.buildHTMLPageHeader("Leave History Overview", employee.toString()));

        // Page Detail
        String refDate = request.getParameter("referenceDate"); // for which period?

        // We also need to traverse the sub-groups to get a list of all the selections underneath the current user.
        // We need this to forward the appropriate selected selection
        // If the request passed on a parameter with a referencedate value, rather use that
        String originalURI = request.getRequestURI();

        PageProducer_LeaveSummaryGrid_SelectionGroup groupProducer;

        ////////////////////////////////////////////////////////////
        //
        // Do we have anything from Pageproducer_EmployeeSelection in the form of multiple-selected employees?
        // If so, I need to convert those request parameters to session objects so that it can be passed on to PageProducer_TnADailySignoff
        //
        // But first, how about those already stored in a session?
        java.util.LinkedList<String> selectedEmployeeHashCodes = new java.util.LinkedList<>();
        if (request.getSession().getAttribute("selectedEmployeeHashCodes") != null) {
            selectedEmployeeHashCodes = (java.util.LinkedList<String>) request.getSession().getAttribute("selectedEmployeeHashCodes");
            //System.out.println("Session's selectedEmployeeHashCodes already exists");
        }

        for (int i = 0; i < 1000; i++) {
            if (request.getParameter("multiEmp_" + i) != null) {
                String selectedEmp = request.getParameter("multiEmp_" + i);
                //System.out.println("request.getParameter(\"multiEmp_" + i + "\")" + request.getParameter("multiEmp_" + i) + "=" + selectedEmp);
                selectedEmployeeHashCodes.add(selectedEmp);
            }
        }
        request.getSession().setAttribute("selectedEmployeeHashCodes", selectedEmployeeHashCodes);
        //////////////////////////////////////////////////////////////////////////////
        if ((refDate == null) || (refDate.compareToIgnoreCase("null") == 0)) {
            groupProducer = new PageProducer_LeaveSummaryGrid_SelectionGroup(selectedEmployeeHashCodes, new java.util.Date(), originalURI, null);
        } else {
            groupProducer = new PageProducer_LeaveSummaryGrid_SelectionGroup(selectedEmployeeHashCodes, za.co.ucs.lwt.db.DatabaseObject.getInstance().formatDate(refDate), originalURI, null);
        }
        out.write(groupProducer.generateInformationContent());
    }
%>

<%@include file="html_bottom.jsp"%>