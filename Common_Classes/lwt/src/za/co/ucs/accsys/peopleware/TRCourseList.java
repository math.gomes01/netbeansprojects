/*
 * TRCourseList.java
 *
 */
package za.co.ucs.accsys.peopleware;

import za.co.ucs.lwt.db.*;
import java.sql.*;
import java.util.LinkedList;

/**
 * TRCourseList lists all the courses, or those linked to a specific provider
 * @Author  liam
 */
public final class TRCourseList {

    /**
     * Creates a LinkedList of all training courses
     */
    public TRCourseList() {
        this(0);
    }
    
    /** 
     * Creates a LinkedList of all training courses
     * @param providerID : ID of training provider
     */
    public TRCourseList(int providerID) {
        this.providerID = providerID;
        courseList = new LinkedList();
        loadFromDB();
    }


    public LinkedList getList(){
        return courseList;
    }

    public void loadFromDB() {
        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            ResultSet rs;
            if (this.providerID==0){
            rs = DatabaseObject.openSQL("SELECT COURSE_ID FROM TR_COURSE with (nolock) order by NAME", con);
            } else {
            rs = DatabaseObject.openSQL("SELECT COURSE_ID FROM TR_COURSE with (nolock) where "+
                                          " PROVIDER_ID="+this.providerID, con);
            }

            while (rs.next()) {
                int courseID = (new Integer(rs.getInt("COURSE_ID")));
                TRCourse course = new TRCourse(courseID);
                courseList.add(course);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
    }

    private java.util.LinkedList courseList;
    private int providerID = 0;
}
