

<%

        // Define Chart
        strBXML = new StringBuffer();
        sqlStatement = new StringBuffer();
        //String fromDateM = za.co.ucs.lwt.db.DatabaseObject.formatDate( za.co.ucs.lwt.db.DatabaseObject.addNDays(new java.util.Date(), -31));
        //String toDateM = za.co.ucs.lwt.db.DatabaseObject.formatDate( za.co.ucs.lwt.db.DatabaseObject.addNDays(new java.util.Date(), -1));


        strBXML.append("<");
        strBXML.append("chart caption='Time in the office ("+fromDate+" - "+toDate+") ' ");
        strBXML.append("subCaption ='"+dateDaysDiff+" days' ");
        strBXML.append("xAxisName='Day' ");
        strBXML.append("yAxisName='Time (hours)'");
        strBXML.append("startAngX='20' startAngY='-30' ");
        strBXML.append("endAngX='10' endAngY='-6' ");
        strBXML.append("showValues='0' ");
        strBXML.append("showYAxisValues='1' ");
        strBXML.append("showAboutMenuItem='0' ");
        strBXML.append("bgColor='D8E6E9,FFFFFF' ");
        strBXML.append("bgAlpha='100, 100' ");
        strBXML.append("showBorder='1' ");
        strBXML.append("canvasbgAlpha='30' ");
        strBXML.append("labelDisplay='ROTATE' ");
        strBXML.append("outCnvBaseFont='Verdana' ");
        strBXML.append(">");


        object.setConnectInfo_AccsysJDBC(WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Database, ""));


        try {
            con = object.getNewConnectionFromPool();
            sqlStatement.append("select dayname(day) as DayName, \"date\"(day) as day, ");
            sqlStatement.append("COALESCE(datediff(minute, \"date\"(FirstIn), FirstIn),0) as  FirstIn_Num, ");
            sqlStatement.append("right('00'||datepart(hh,FirstIn),2)||':'||right('00'||datepart(mi,FirstIn),2) as FirstIn_Displ, ");
            sqlStatement.append("COALESCE(datediff(minute, \"date\"(LastOut), LastOut),0) as  LastOut_Num, ");
            sqlStatement.append("right('00'||datepart(hh,LastOut),2)||':'||right('00'||datepart(mi,LastOut),2) as LastOut_Displ, ");
            sqlStatement.append("COALESCE(TimeAtWork_ActualMinutes,0) as TimeAtWork_ActualMinutes, TimeAtWork_DisplayValue ");
            sqlStatement.append("from vw_TA_Report_FirstInLastOut vw with (nolock) ");
            sqlStatement.append("where Company_id=" + employee.getCompany().getCompanyID());
            sqlStatement.append(" and person_id in ");
            sqlStatement.append("    (select person_id from ta_e_master with (nolock) where ");
            sqlStatement.append("     company_id=vw.company_id and Employee_id=" + employee.getEmployeeID() + ") ");
            sqlStatement.append("and day between '"+fromDate+"' and '"+toDate+"' ");
            //sqlStatement.append("and FirstIn is not null ");
            sqlStatement.append("order by day ");

            java.sql.ResultSet rs = object.openSQL(sqlStatement.toString(), con);

            // Build CATEGORIES
            while (rs.next()) {
                strBXML.append("<set label='" + rs.getString("day").substring(0, 10) + "' value='" + new Float(rs.getInt("TimeAtWork_ActualMinutes") / 60).toString() + "' toolText='Time in office{br}" + rs.getString("TimeAtWork_DisplayValue") + "' displayValue='" + rs.getString("TimeAtWork_DisplayValue") + "' />");
            }

            // Now we add the 'Average' line
            rs.close();
            sqlStatement = new StringBuffer();
            sqlStatement.append("select convert(numeric(6,2),round(avg(TimeAtWork_ActualMinutes)/60,2)) as average  ");
            sqlStatement.append("from vw_TA_Report_FirstInLastOut vw with (nolock) ");
            sqlStatement.append("where Company_id=" + employee.getCompany().getCompanyID());
            sqlStatement.append(" and person_id in ");
            sqlStatement.append("    (select person_id from ta_e_master with (nolock) where ");
            sqlStatement.append("     company_id=vw.company_id and Employee_id=" + employee.getEmployeeID() + ") ");
            sqlStatement.append("and day between dateadd(dd, -31, now()) and dateadd(dd, -21, now()) ");
            sqlStatement.append("and FirstIn is not null ");
            rs = object.openSQL(sqlStatement.toString(), con);
            rs.first();
            String startAvg = rs.getString("average");

            rs.close();
            sqlStatement = new StringBuffer();
            sqlStatement.append("select convert(numeric(6,2),round(avg(TimeAtWork_ActualMinutes)/60,2)) as average  ");
            sqlStatement.append("from vw_TA_Report_FirstInLastOut vw with (nolock) ");
            sqlStatement.append("where Company_id=" + employee.getCompany().getCompanyID());
            sqlStatement.append(" and person_id in ");
            sqlStatement.append("    (select person_id from ta_e_master with (nolock) where ");
            sqlStatement.append("     company_id=vw.company_id and Employee_id=" + employee.getEmployeeID() + ") ");
            sqlStatement.append("and day between dateadd(dd, -11, now()) and dateadd(dd, -1, now()) ");
            sqlStatement.append("and FirstIn is not null ");
            rs = object.openSQL(sqlStatement.toString(), con);
            rs.first();
            String endAvg = rs.getString("average");


            strBXML.append("<trendLines>");
            strBXML.append("<line startValue='"+startAvg+"' endValue='"+endAvg+"' color='FF0000' displayvalue='Average     ' tooltext='Average time in the office'/>");
            strBXML.append("</trendLines>");

            strBXML.append("<styles><definition>");
            strBXML.append("   <style name='Anim1' type='animation' param='_xscale' start='0' duration='1'/>");
            strBXML.append("   <style name='Anim2' type='animation' param='_alpha' start='0' duration='0.6'/>");
            strBXML.append("   <style name='DataShadow' type='Shadow' alpha='40'/></definition>");
            strBXML.append("<application><apply toObject='DIVLINES' styles='Anim1'/>");
            strBXML.append("<apply toObject='DATALABELS' styles='DataShadow,Anim2'/>");
            strBXML.append("<apply toObject='HGRID' styles='Anim2'/>");
            strBXML.append("</application></styles>");



            strBXML.append("</chart>");
            strXML = strBXML.toString();


        } finally {
            object.releaseConnection(con);
        }

%>
<jsp:include page="../FCharts/Includes/FusionChartsHTMLRenderer.jsp" flush="true">
        <jsp:param name="chartSWF" value="../FCharts/FusionCharts/Line.swf " />
        <jsp:param name="strURL" value="" />
        <jsp:param name="strXML" value="<%=strXML%>" />
        <jsp:param name="chartId" value="myNext" />
        <jsp:param name="chartWidth" value="600" />
        <jsp:param name="chartHeight" value="300" />
        <jsp:param name="debugMode" value="false" />
        <jsp:param name="registerWithJS" value="false" />

    </jsp:include>


