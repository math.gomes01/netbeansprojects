<%@page contentType="text/html"%>

<%@include file="html5_top.jsp"%>

<%@include file="../html/formatDateScript.txt"%>

<%@include file="html_messages.jsp"%> 
<!-- This HAS to be included if messages were generated
and added to the session attributes for this person.
Calling this script will simply display those messages -->

<!--  ******************** 
        Initiates a Leave Request
      ******************** 
-->

<%  // Can I connect to the database?
    if (za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideRequestChangesFor("Leave")) {
        PageSectionGenerator gen = PageSectionGenerator.getInstance();
        out.write(gen.buildHTMLPageHeader("Access Revoked", "Your system was configured not to allow this kind of request to be processed."));
    } else {

        // Can I connect to the database?
        if (!za.co.ucs.lwt.db.DatabaseObject.getInstance().canConnect("select @@version", za.co.ucs.lwt.db.DatabaseObject.getInstance().getNewConnectionFromPool(), true)) {
            MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                    "Unable to connect to database.  Please contact your network administrator.",
                    false, request.getSession());
            producer.setMessageInSessionObject();
            out.write("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");

        } else {
            // Leave Info
            LeaveInfo leaveInfo = fileContainer.getLeaveInfoFromContainer(employee);
            if ((leaveInfo == null)) {
                // Page Header
                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                        employee.toString() + " is not linked to any Leave Profiles.  Leave Requests cannot be processed.",
                        false, request.getSession());
                producer.setMessageInSessionObject();
                out.write("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");

            } else {
                PageSectionGenerator gen = PageSectionGenerator.getInstance();
                if (user.equals(employee)) {
                    out.write(gen.buildHTMLPageHeader("Leave Request", employee.toString()));
                } else {
                    out.write(gen.buildHTMLPageHeader("Leave Request", "On Behalf Of: " + employee.toString()));

                    // Leave Summary Grid
                    PageProducer_LeaveSummaryGrid gridProducer = new PageProducer_LeaveSummaryGrid(employee);
                    out.write(gridProducer.generateInformationContent());
                    out.write("<hr width=\"80%\" size=\"1\" color=\"#c0c0c0\">");

                }

                // Leave Balances
                za.co.ucs.ess.PageProducer_LeaveBalances baProducer = new za.co.ucs.ess.PageProducer_LeaveBalances(employee, "Today", request.getRequestURL().toString());
                out.write(baProducer.generateInformationContent());
                out.write("<hr width=\"750px;\" size=\"1\" color=\"#c0c0c0\">");

                // Page Detail
                // Did the user pass on a specific Leave Type?
                out.write("<center>");
                String leaveTypeID = request.getParameter("cbbLeaveTypeID");
                String leaveUnit = request.getParameter("cbbLeaveUnit");
                String webProcessClassName = request.getParameter("webProcessClassName"); // Which WebProcess is being used?
                PageProducer_LeaveRequest producer = new PageProducer_LeaveRequest(leaveInfo, employee, user, leaveTypeID, leaveUnit, webProcessClassName);
                out.write(producer.generateInformationContent());
                out.write("</center>");

            }
        }
    }
%>


<%@include file="html_bottom.jsp"%>