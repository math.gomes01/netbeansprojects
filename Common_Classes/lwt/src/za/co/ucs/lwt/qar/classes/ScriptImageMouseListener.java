/*
 * ScriptImageMouseListener.java
 *
 * Created on November 6, 2003, 7:44 AM
 */

package za.co.ucs.lwt.qar.classes;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

/** Event class to handle mouse-clicks on this Image
 */
public class ScriptImageMouseListener extends Observable  implements MouseListener{
    
    public void mouseClicked(MouseEvent e) {
        // Which script was clicked on?
        QRScript script = null;
        int clickedX = e.getX();
        int clickedY = e.getY();
        LinkedList coordinates = ((ScriptImage)e.getSource()).getScriptCoordinates();
        for (int i=0; i<coordinates.size(); i++){
            if (((ScriptPosition)coordinates.get(i)).inPosition(clickedX, clickedY)){
                script = ((ScriptPosition)coordinates.get(i)).getScript();
                break;
            }
        }
        if (script != null){
            setChanged();
            notifyObservers(script);
        }
    }
    
    public void mouseEntered(MouseEvent e) {
    }
    
    public void mouseExited(MouseEvent e) {
    }
    
    public void mousePressed(MouseEvent e) {
    }
    
    public void mouseReleased(MouseEvent e) {
    }
}

