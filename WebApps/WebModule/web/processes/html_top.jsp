<%@page language="java" session="true" %>
<%@page import="za.co.ucs.accsys.webmodule.*" %>
<%@page import="za.co.ucs.ess.*" %>
<%@page import="za.co.ucs.lwt.db.*" %>
<%@page import="za.co.ucs.accsys.peopleware.*" %>


<!DOCTYPE html>
<HTML>
    <HEAD>
        <meta http-equiv="X-UA-Compatible" content="IE=8">
        <link rel="shortcut icon" href="../images/favicon.ico" />
        <link rel="stylesheet" type="text/css" href="/WebModule/CSS/EssCss.css" />

        <link rel="stylesheet" href="../CSS/jquery-ui-1.8.20.custom.css" type="text/css">

        <script type="text/javascript" src="../js/jquery-1.7.2.min.js"></script>
        <script type="text/javascript" src="../js/jquery-ui-1.8.20.custom.min.js"></script>
        <script type="text/javascript" src="../js/jquery.masonry.min.js"></script>


       

        <script>
            $(function() {
                $( "#tabs" ).tabs();
            });
        </script>
        <script>$(function(){
            $('#container').masonry({
                // options
                itemSelector : '.item',
                isFitWidth: true,
                isAnimated: true,
                animationOptions: {
                    duration: 400,
                    easing: 'linear',
                    queue: false
                },
                // set columnWidth a fraction of the container width
                columnWidth: function( containerWidth ) {
                    return containerWidth / 10;
                },
                gutterWidth:10
            });
        });

        </script>
 

        <%
            LoginSessionObject loginSessionObject = (LoginSessionObject) session.getAttribute("loginsessionobject");
            String sPasswordExpired = (String)session.getAttribute("expiredPassword");
            // If the password expired, reset the login session
            if (sPasswordExpired != null){
                if (sPasswordExpired.trim().compareToIgnoreCase("TRUE")==0){
                    loginSessionObject = null;
                }
            }
            
            if ((loginSessionObject == null)
                    && (request.getRequestURI().compareToIgnoreCase("/WebModule/processes/Login.jsp") != 0)) {
                session.setAttribute("originalURI", request.getRequestURI());
                session.setAttribute("originalQuery", request.getQueryString());
        %>
        <META HTTP-EQUIV="Refresh" CONTENT="1; URL=Login.jsp" />
        <% } else {%>
        <META HTTP-EQUIV="Refresh" CONTENT="600; URL=Login.jsp" />
        <% }%>

        <meta HTTP-EQUIV=Expires CONTENT="0">
        <meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
        <TITLE>Employee Self Service (
            <%
                out.write("   " + za.co.ucs.accsys.webmodule.ESSVersion.getInstance().getJarVersion());
            %>)
        </TITLE>
        <meta http-equiv="content-type" 
              content="text/html;charset=utf-8" />
 
        <script language="javascript" src="../js/cal2.js"></script>
        <script language="javascript" src="../js/cal_conf2.js"></script>

        <script type="text/javascript">

    /***********************************************
     * Show Hint script- ? Dynamic Drive (www.dynamicdrive.com)
     * This notice MUST stay intact for legal use
     * Visit http://www.dynamicdrive.com/ for this script and 100s more.
     ***********************************************/

    var horizontal_offset="9px" //horizontal offset of hint box from anchor link

    /////No further editting needed

    var vertical_offset="0" //horizontal offset of hint box from anchor link. No need to change.
    var ie=document.all
    var ns6=document.getElementById&&!document.all

    function getposOffset(what, offsettype){
        var totaloffset=(offsettype=="left")? what.offsetLeft : what.offsetTop;
        var parentEl=what.offsetParent;
        while (parentEl!=null){
            totaloffset=(offsettype=="left")? totaloffset+parentEl.offsetLeft : totaloffset+parentEl.offsetTop;
            parentEl=parentEl.offsetParent;
        }
        return totaloffset;
    }

    function iecompattest(){
        return (document.compatMode && document.compatMode!=="BackCompat")? document.documentElement : document.body;
    }

    function clearbrowseredge(obj, whichedge){
        var edgeoffset=(whichedge==="rightedge")? parseInt(horizontal_offset)*-1 : parseInt(vertical_offset)*-1
        if (whichedge==="rightedge"){
            var windowedge=ie && !window.opera? iecompattest().scrollLeft+iecompattest().clientWidth-30 : window.pageXOffset+window.innerWidth-40
            dropmenuobj.contentmeasure=dropmenuobj.offsetWidth
            if (windowedge-dropmenuobj.x < dropmenuobj.contentmeasure)
                edgeoffset=dropmenuobj.contentmeasure+obj.offsetWidth+parseInt(horizontal_offset)
        }
        else{
            var windowedge=ie && !window.opera? iecompattest().scrollTop+iecompattest().clientHeight-15 : window.pageYOffset+window.innerHeight-18
            dropmenuobj.contentmeasure=dropmenuobj.offsetHeight
            if (windowedge-dropmenuobj.y < dropmenuobj.contentmeasure)
                edgeoffset=dropmenuobj.contentmeasure-obj.offsetHeight
        }
        return edgeoffset
    }

    function showhint(menucontents, obj, e, tipwidth){
        if ((ie||ns6) && document.getElementById("hintbox")){
            dropmenuobj=document.getElementById("hintbox")
            dropmenuobj.innerHTML=menucontents
            dropmenuobj.style.left=dropmenuobj.style.top=-500
            if (tipwidth!=""){
                dropmenuobj.widthobj=dropmenuobj.style
                dropmenuobj.widthobj.width=tipwidth
            }
            dropmenuobj.x=getposOffset(obj, "left")
            dropmenuobj.y=getposOffset(obj, "top")
            dropmenuobj.style.left=dropmenuobj.x-clearbrowseredge(obj, "rightedge")+obj.offsetWidth+"px"
            dropmenuobj.style.top=dropmenuobj.y-clearbrowseredge(obj, "bottomedge")+"px"
            dropmenuobj.style.visibility="visible"
            obj.onmouseout=hidetip
        }
    }

    function hidetip(e){
        dropmenuobj.style.visibility="hidden"
        dropmenuobj.style.left="-500px"
    }

    function createhintbox(){
        var divblock=document.createElement("div")
        divblock.setAttribute("id", "hintbox")
        document.body.appendChild(divblock)
    }

    if (window.addEventListener)
        window.addEventListener("load", createhintbox, false)
    else if (window.attachEvent)
        window.attachEvent("onload", createhintbox)
    else if (document.getElementById)
        window.onload=createhintbox

        </script>

        <style type="text/css">

            /*Credits: Dynamic Drive CSS Library */
            /*URL: http://www.dynamicdrive.com/style/ */

            .halfmoon{
                margin-bottom: 1em;
            }

            .halfmoon ul{
                padding: 3px 2px 2px 5px;
                margin-left: 15px;
                margin-top: -50px;
                margin-bottom: 0;
                font-weight: bold;
                font-size: 11pt;
                color: rgb(96,0,1);
                list-style-type: none;
                text-align: left; /*set to left, center, or right to align the menu as desired*/
                border-bottom: 2px solid rgb(118,5,1);
            }

            .halfmoon li{
                display: inline;
                margin: 0;
            }

            .halfmoon li a{
                text-decoration: none;
                padding: 3px 9px 2px 5px;
                margin: 0;
                margin-right: 2px; /*distance between each tab*/
                border-left: 1px solid rgb(86,0,1);

                border-top: 1px solid rgb(86,0,1);
                color: rgb(61, 10, 15);
                font-size: 12px;
                background: #dddddd url(media/tabright.gif) top right no-repeat;
            }

            .halfmoon li a:visited{
                color: rgb(161, 10, 15);
            }

            .halfmoon li a:hover{
                background-color: #eeeeee;
                color: rgb(141, 40, 45);
            }

        </style>

             
            
        <!--<link rel="stylesheet" href="css.css" />-->

        <script type="text/javascript" src="../js/js.js">
        </script>
      
    </HEAD>
    <BODY>


        <%
            Employee user = null;
            Employee employee = null;
            FileContainer fileContainer = null;
            java.util.ArrayList availableProcesses;
        %>



        <script type="text/javascript" src="../menu_js/stmenu.js"></script>
        <script type="text/javascript">

            
            <%        // If nobody is logged in, hide most of the menu items
                fileContainer = za.co.ucs.accsys.webmodule.FileContainer.getInstance();
                java.util.prefs.Preferences preferences = za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().getPreferences();
                String baseURL = WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Web, "");
                // Define the Menu script constants
                //
                // MENU_HEADER
                StringBuffer menu_HEADER = new StringBuffer();
                menu_HEADER.append("stm_bm([\"menu45ba\",900,\"" + baseURL + "menu_images\",\"blank.gif\",0,\"\",\"\",0,0,250,0,1000,1,0,0,\"\",\"\",0,0,1,2,\"default\",\"hand\",\"\",1,25]);");
                menu_HEADER.append("\n");
                menu_HEADER.append("stm_bp(\"p0\",[0,4,0,0,3,4,22,9,100,\"\",-2,\"\",-2,50,2,3,\"#999999\",\"transparent\",\"bluefireback.gif\",1,0,0,\"#A9CFDB #93C0CE #155E8C\"]);");

                
                if (loginSessionObject != null) {

                    out.write(menu_HEADER.toString());

                    user = loginSessionObject.getUser();
                    employee = loginSessionObject.getEmployee();

                    // Were these overwritten by request parameters?
                    // If the request is logged by a manager on behalf of an employee, a parameter will
                    // be passed with the Employee's hashCode.  This is initiated from the PageProducer_EmployeeSelection class
                    String hashCode = (String) request.getParameter("forEmployee");
                    if ((hashCode != null) && (hashCode != "")) {
                        employee = za.co.ucs.accsys.webmodule.FileContainer.getInstance().getEmployeeFromContainer(hashCode);
                        if (employee != null) {
                            loginSessionObject.setEmployee(employee);
                        }
                    } else {
                        loginSessionObject.setEmployee(user);
                        employee = user;
                    }

                    availableProcesses = new java.util.ArrayList(fileContainer.getWebProcessDefinitionsWithSubordinates(loginSessionObject.getUser()).values());

                    //
                    //
                    // View...
                    //
                    //
                    out.write("stm_ai(\"mnuMainItem\",[0,\"View\",\"\",\"\",-1,-1,0,\"#\",\"_self\",\"\",\"\",\"mnu_view.gif\",\"mnu_view.gif\",16,16,0,\"mnu_arrow_down.gif\",\"mnu_arrow_down.gif\",9,7,0,0,1,\"#FFFFF7\",1,\"#B5BED6\",1,\"\",\"bluefireback_top_bottom.gif\",3,3,0,0,\"#FFFFF7\",\"#000000\",\"#FFFFFF\",\"#FFFFFF\",\"9pt Verdana\",\"9pt Verdana\",0,0,\"\",\"bluefireback_left.gif\",\"\",\"bluefireback_right.gif\",6,6,24],75,0);");
                    out.write("stm_bpx(\"p1\",\"p0\",[1,4,0,0,3,4,8,10,100,\"\",-2,\"\",-2,50,2,3,\"#999999\",\"#f0f0f0\",\"\",0]);");

                    // Contact Information
                    if (user != null) {
                        out.write("stm_aix(\"menuSubItem\",\"mnuMainItem\",[0,\"Personal Information\","
                                + "\"\",\"\",-1,-1,0,"
                                + "\"" + baseURL + "processes/personal_prev.jsp \""
                                + ",\"_self\",\"\","
                                + "\"Preview personal information\""
                                + ",\"\",\"bullet.gif\",8,8,0,\"\",\"\",0,0,0,0,1,\"#00CCFF\",1,\"#C8C8C8\",1,\"\",\"bluefireback_top_bottom_light.gif\",3,3,0,0,\"#FFFFF7\",\"#000000\",\"#000000\",\"#000000\",\"8pt Verdana\",\"8pt Verdana\",0,0,\"\",\"bluefireback_left_light.gif\",\"\",\"bluefireback_right_light.gif\",4,4],130,0);");
                    }

                    //
                    // Leave Status
                    if (user != null) {
                        out.write("stm_aix(\"\",\"menuSubItem\",[0,\"Leave Status\","
                                + "\"\",\"\",-1,-1,0,"
                                + "\"" + baseURL + "processes/leave_prev.jsp \""
                                + ",\"_self\",\"\","
                                + "\"Display leave balances and history Information\""
                                + ",\"\",\"bullet.gif\",8,8,0,\"\",\"\",0,0,0,0,1,\"#00CCFF\",1,\"#C8C8C8\",1,\"\",\"bluefireback_top_bottom_light.gif\",3,3,0,0,\"#FFFFF7\",\"#000000\",\"#000000\",\"#000000\",\"8pt Verdana\",\"8pt Verdana\",0,0,\"\",\"bluefireback_left_light.gif\",\"\",\"bluefireback_right_light.gif\",4,4],130,0);");
                    }
                    //
                    // T&A Status
                    if (user != null) {
                        out.write("stm_aix(\"\",\"menuSubItem\",[0,\"T&A Status\","
                                + "\"\",\"\",-1,-1,0,"
                                + "\"" + baseURL + "processes/tna_prev.jsp \""
                                + ",\"_self\",\"\","
                                + "\"Display Time & Attendance Information\""
                                + ",\"\",\"bullet.gif\",8,8,0,\"\",\"\",0,0,0,0,1,\"#00CCFF\",1,\"#C8C8C8\",1,\"\",\"bluefireback_top_bottom_light.gif\",3,3,0,0,\"#FFFFF7\",\"#000000\",\"#000000\",\"#000000\",\"8pt Verdana\",\"8pt Verdana\",0,0,\"\",\"bluefireback_left_light.gif\",\"\",\"bluefireback_right_light.gif\",4,4],130,0);");
                    }
                    //
                    // Training History
                    if (user != null) {
                        out.write("stm_aix(\"\",\"menuSubItem\",[0,\"Training History\","
                                + "\"\",\"\",-1,-1,0,"
                                + "\"" + baseURL + "processes/training_prev.jsp \""
                                + ",\"_self\",\"\","
                                + "\"Display history of all training courses attended\""
                                + ",\"\",\"bullet.gif\",8,8,0,\"\",\"\",0,0,0,0,1,\"#00CCFF\",1,\"#C8C8C8\",1,\"\",\"bluefireback_top_bottom_light.gif\",3,3,0,0,\"#FFFFF7\",\"#000000\",\"#000000\",\"#000000\",\"8pt Verdana\",\"8pt Verdana\",0,0,\"\",\"bluefireback_left_light.gif\",\"\",\"bluefireback_right_light.gif\",4,4],130,0);");
                    }

                    //
                    // Tax Certificates
                    if (user != null) {
                        out.write("stm_aix(\"\",\"menuSubItem\",[0,\"Tax Certificates\","
                                + "\"\",\"\",-1,-1,0,"
                                + "\"" + baseURL + "processes/taxcertificate_prev.jsp \""
                                + ",\"_self\",\"\","
                                + "\"Display tax certificates for current and previous years\""
                                + ",\"\",\"bullet.gif\",8,8,0,\"\",\"\",0,0,0,0,1,\"#00CCFF\",1,\"#C8C8C8\",1,\"\",\"bluefireback_top_bottom_light.gif\",3,3,0,0,\"#FFFFF7\",\"#000000\",\"#000000\",\"#000000\",\"8pt Verdana\",\"8pt Verdana\",0,0,\"\",\"bluefireback_left_light.gif\",\"\",\"bluefireback_right_light.gif\",4,4],130,0);");
                    }

                    //                    
                    // Payslips

                    if (user != null) {
                        out.write("stm_aix(\"mnuWithRightArrow\",\"menuSubItem\",[0,\"Payslips\",\"\",\"\",-1,-1,0,"
                                + "\"\",\"_self\",\"\",\"\",\"\",\"bullet.gif\",8,8,0,\"mnu_arrow_right.gif\",\"mnu_arrow_right.gif\",10,11],130,0);");
                        out.write("stm_bpx(\"p2\",\"p1\",[1,2,0,0,3,4,8,0]);");
                        // This Year
                        out.write("stm_aix(\"\",\"menuSubItem\",[0,\"Payslips - This Tax Year\",\"\",\"\",-1,-1,0,\"" + baseURL + "processes/payslip_prev.jsp \",\"_self\",\"\",\"\"],130,0);");
                        // Now, let us add a line for all the archived tax years

                        java.util.LinkedList taxYears = fileContainer.getArchivedPayslipTaxYears(user);
                        java.util.Iterator iter = taxYears.iterator();
                        String fromAndToDate;
                        String fromYear;
                        String toYear;
                        while (iter.hasNext()) {
                            fromAndToDate = (String) iter.next();
                            fromYear = fromAndToDate.substring(0, 4);
                            toYear = fromAndToDate.substring(5, 9);
                            out.write("stm_aix(\"\",\"menuSubItem\",[0,\"Payslips - " + fromYear + " - " + toYear + " \",\"\",\"\",-1,-1,0,\"" + baseURL + "processes/payslip_prev.jsp?fromYear=" + fromYear + "&toYear=" + toYear + " \",\"_self\",\"\",\"\"],130,0);");
                        }

                    }
                    out.write("stm_ep();");


                    //
                    //
                    // Statistics
                    //
                    //
                    if (user != null) {
                        out.write("stm_ep();");
                        out.write("stm_aix(\"\",\"mnuMainItem\",[0,\"Statistics\",\"\",\"\",-1,-1,0,\"#\",\"_self\",\"\","
                                + "\"\",\"mnu_stats.gif\",\"mnu_stats.gif\",21],75,0);");
                        out.write("stm_bpx(\"p3\",\"p2\",[1,4]);");
                        // Leave Stats
                        out.write("stm_aix(\"\",\"menuSubItem\",[0,\"Personal Leave Statistics\","
                                + "\"\",\"\",-1,-1,0,"
                                + "\"" + baseURL + "processes/leave_stats.jsp \""
                                + ",\"_self\",\"\","
                                + "\"View personal leave statistics - This can take some time to load\""
                                + ",\"\",\"bullet.gif\",8,8,0,\"\",\"\",0,0,0,0,1,\"#00CCFF\",1,\"#C8C8C8\",1,\"\",\"bluefireback_top_bottom_light.gif\",3,3,0,0,\"#FFFFF7\",\"#000000\",\"#000000\",\"#000000\",\"8pt Verdana\",\"8pt Verdana\",0,0,\"\",\"bluefireback_left_light.gif\",\"\",\"bluefireback_right_light.gif\",4,4],130,0);");

                        // T&A Stats
                        out.write("stm_aix(\"\",\"menuSubItem\",[0,\"Personal T&A Statistics\","
                                + "\"\",\"\",-1,-1,0,"
                                + "\"" + baseURL + "processes/time_stats.jsp \""
                                + ",\"_self\",\"\","
                                + "\"View personal Time & Attendance statistics - This can take some time to load\""
                                + ",\"\",\"bullet.gif\",8,8,0,\"\",\"\",0,0,0,0,1,\"#00CCFF\",1,\"#C8C8C8\",1,\"\",\"bluefireback_top_bottom_light.gif\",3,3,0,0,\"#FFFFF7\",\"#000000\",\"#000000\",\"#000000\",\"8pt Verdana\",\"8pt Verdana\",0,0,\"\",\"bluefireback_left_light.gif\",\"\",\"bluefireback_right_light.gif\",4,4],130,0);");


                        if (availableProcesses.size() > 0) {
                            out.write("stm_aix(\"\",\"menuSubItem\",[0,\"Company Statistics\","
                                    + "\"\",\"\",-1,-1,0,"
                                    + "\"" + baseURL + "processes/company_stats.jsp \""
                                    + ",\"_self\",\"\","
                                    + "\"Display company statistics - This can take some time to load\""
                                    + ",\"\",\"bullet.gif\",8,8,0,\"\",\"\",0,0,0,0,1,\"#00CCFF\",1,\"#C8C8C8\",1,\"\",\"bluefireback_top_bottom_light.gif\",3,3,0,0,\"#FFFFF7\",\"#000000\",\"#000000\",\"#000000\",\"8pt Verdana\",\"8pt Verdana\",0,0,\"\",\"bluefireback_left_light.gif\",\"\",\"bluefireback_right_light.gif\",4,4],130,0);");
                        }

                        out.write("stm_ep();");
                    }

                    //
                    //
                    // Aply For...
                    //
                    //
                    int creditBalance = DatabaseObject.getAvailableESSCredits();
                    // Warn the user to buy credits
                    if (creditBalance == 0) {
                        session.setAttribute("essMessage", "<i>Your ESS Credits have been depleted.  All workflow processes are disabled.</i>");
                    }
                    if ((user != null) && (creditBalance > 0)) {
                        //out.write("stm_ep();");
                        out.write("stm_aix(\"\",\"mnuMainItem\",[0,\"Apply for ...\",\"\",\"\",-1,-1,0,\"#\",\"_self\",\"\","
                                + "\"\",\"fix.gif\",\"fix.gif\",21],75,0);");
                        out.write("stm_bpx(\"p3\",\"p2\",[1,4]);");

                        java.util.ArrayList allProcesses = fileContainer.getWebProcessDefinitions();
                        if (allProcesses.size() > 0) {
                            for (int a = 0; a < allProcesses.size(); a++) {
                                WebProcessDefinition webProcessDefinition = (WebProcessDefinition) allProcesses.get(a);

                                // Only show those processes than can be instantiated by the employee him/herself
                                boolean cannotInstantiateSelf = webProcessDefinition.getCannotInstantiateSelf();
                                boolean isBeforeValidPeriod = ((webProcessDefinition.getActiveFromDayOfMonth() > 0) && (webProcessDefinition.getActiveFromDayOfMonth() > DatabaseObject.getDayOfMonth(new java.util.Date())));
                                boolean isAfterValidPeriod = ((webProcessDefinition.getActiveToDayOfMonth() > 0) && (webProcessDefinition.getActiveToDayOfMonth() < DatabaseObject.getDayOfMonth(new java.util.Date())));
                                if (cannotInstantiateSelf) {
                                    //System.out.println("*** cannotInstantiateSelf ***:" + webProcessDefinition);
                                    continue;
                                }
                                if ((isBeforeValidPeriod) || (isAfterValidPeriod)) {
                                    System.out.println("*** Process outside valid period ***:" + webProcessDefinition + " valid between " + webProcessDefinition.getActiveFromDayOfMonth() + " and " + webProcessDefinition.getActiveToDayOfMonth() + " of the month.");
                                    continue;
                                }

                                if ((webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_EmployeeContact") == 0) && (!za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideRequestChangesFor("Contact Detail"))) {
                                    // Contact Information
                                    out.write("stm_aix(\"\",\"menuSubItem\",[0,\"Changes to Personal Information\","
                                            + "\"\",\"\",-1,-1,0,"
                                            + "\"" + baseURL + "processes/contact_req.jsp \""
                                            + ",\"_self\",\"\","
                                            + "\"Change Personal Information\""
                                            + ",\"\",\"bullet.gif\",8,8,0,\"\",\"\",0,0,0,0,1,\"#00CCFF\",1,\"#C8C8C8\",1,\"\",\"bluefireback_top_bottom_light.gif\",3,3,0,0,\"#FFFFF7\",\"#000000\",\"#000000\",\"#000000\",\"8pt Verdana\",\"8pt Verdana\",0,0,\"\",\"bluefireback_left_light.gif\",\"\",\"bluefireback_right_light.gif\",4,4],130,0);");
                                }

                                if ((webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition") == 0) && (!za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideRequestChangesFor("Leave"))) {
                                    // Leave
                                    out.write("stm_aix(\"\",\"menuSubItem\",[0,\"Leave\","
                                            + "\"\",\"\",-1,-1,0,"
                                            + "\"" + baseURL + "processes/leave_req.jsp?forEmployee= \""
                                            + ",\"_self\",\"\","
                                            + "\"Apply for leave\""
                                            + ",\"\",\"bullet.gif\",8,8,0,\"\",\"\",0,0,0,0,1,\"#00CCFF\",1,\"#C8C8C8\",1,\"\",\"bluefireback_top_bottom_light.gif\",3,3,0,0,\"#FFFFF7\",\"#000000\",\"#000000\",\"#000000\",\"8pt Verdana\",\"8pt Verdana\",0,0,\"\",\"bluefireback_left_light.gif\",\"\",\"bluefireback_right_light.gif\",4,4],130,0);");
                                }

                                if ((webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_LeaveCancellation") == 0) && (!za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideRequestChangesFor("Leave Cancellation"))) {
                                    // Leave Cancellation
                                    out.write("stm_aix(\"\",\"menuSubItem\",[0,\"Leave Cancellation\","
                                            + "\"\",\"\",-1,-1,0,"
                                            + "\"" + baseURL + "processes/leavecancel_req.jsp \""
                                            + ",\"_self\",\"\","
                                            + "\"Cancel future-dated leave\""
                                            + ",\"\",\"bullet.gif\",8,8,0,\"\",\"\",0,0,0,0,1,\"#00CCFF\",1,\"#C8C8C8\",1,\"\",\"bluefireback_top_bottom_light.gif\",3,3,0,0,\"#FFFFF7\",\"#000000\",\"#000000\",\"#000000\",\"8pt Verdana\",\"8pt Verdana\",0,0,\"\",\"bluefireback_left_light.gif\",\"\",\"bluefireback_right_light.gif\",4,4],130,0);");
                                }

                                if ((webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_FamilyRequisition") == 0) && (!za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideRequestChangesFor("Family Detail"))) {
                                    // Family
                                    out.write("stm_aix(\"\",\"menuSubItem\",[0,\"Changes to Family Information\","
                                            + "\"\",\"\",-1,-1,0,"
                                            + "\"" + baseURL + "processes/family_req.jsp \""
                                            + ",\"_self\",\"\","
                                            + "\"Add/delete/update family related information\""
                                            + ",\"\",\"bullet.gif\",8,8,0,\"\",\"\",0,0,0,0,1,\"#00CCFF\",1,\"#C8C8C8\",1,\"\",\"bluefireback_top_bottom_light.gif\",3,3,0,0,\"#FFFFF7\",\"#000000\",\"#000000\",\"#000000\",\"8pt Verdana\",\"8pt Verdana\",0,0,\"\",\"bluefireback_left_light.gif\",\"\",\"bluefireback_right_light.gif\",4,4],130,0);");
                                }

                                if ((webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_TrainingRequisition") == 0) && (!za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideRequestChangesFor("Training"))) {
                                    // Training
                                    out.write("stm_aix(\"\",\"menuSubItem\",[0,\"Training Requests\","
                                            + "\"\",\"\",-1,-1,0,"
                                            + "\"" + baseURL + "processes/training_req.jsp \""
                                            + ",\"_self\",\"\","
                                            + "\"Application to attend a training course\""
                                            + ",\"\",\"bullet.gif\",8,8,0,\"\",\"\",0,0,0,0,1,\"#00CCFF\",1,\"#C8C8C8\",1,\"\",\"bluefireback_top_bottom_light.gif\",3,3,0,0,\"#FFFFF7\",\"#000000\",\"#000000\",\"#000000\",\"8pt Verdana\",\"8pt Verdana\",0,0,\"\",\"bluefireback_left_light.gif\",\"\",\"bluefireback_right_light.gif\",4,4],130,0);");
                                }

                                if ((webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_DailySignoff") == 0) && (!za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideRequestChangesFor("Clocking Authorisation"))) {
                                    // TnA Clocking Authorisation
                                    out.write("stm_aix(\"\",\"menuSubItem\",[0,\"Clocking Authorisation\","
                                            + "\"\",\"\",-1,-1,0,"
                                            + "\"" + baseURL + "processes/tna_signoff_req.jsp?self=true \""
                                            + ",\"_self\",\"\","
                                            + "\"Application to approve/modify Time & Attendance calculation results\""
                                            + ",\"\",\"bullet.gif\",8,8,0,\"\",\"\",0,0,0,0,1,\"#00CCFF\",1,\"#C8C8C8\",1,\"\",\"bluefireback_top_bottom_light.gif\",3,3,0,0,\"#FFFFF7\",\"#000000\",\"#000000\",\"#000000\",\"8pt Verdana\",\"8pt Verdana\",0,0,\"\",\"bluefireback_left_light.gif\",\"\",\"bluefireback_right_light.gif\",4,4],130,0);");
                                }

                                //
                                // Variable : 55001
                                //
                                if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55001") == 0) {
                                    // Indicator Code = 55001
                                    za.co.ucs.accsys.peopleware.Variable variableRequested = za.co.ucs.accsys.webmodule.WebProcess_VariableChanges.getVariable(loginSessionObject.getEmployee(), "55001");
                                    //System.out.println("WebProcess_VariableChanges_55001 - Variable:" + variableRequested);

                                    if ((variableRequested != null) && (!za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideRequestChangesFor(variableRequested.getDescription().trim()))) {
                                        out.write("stm_aix(\"\",\"menuSubItem\",[0,\"Apply for - " + variableRequested.getDescription() + "\","
                                                + "\"\",\"\",-1,-1,0,"
                                                + "\"" + baseURL + "processes/variable_req.jsp?indCode=55001 \""
                                                + ",\"_self\",\"\","
                                                + ",\"\",\"bullet.gif\",8,8,0,\"\",\"\",0,0,0,0,1,\"#00CCFF\",1,\"#C8C8C8\",1,\"\",\"bluefireback_top_bottom_light.gif\",3,3,0,0,\"#FFFFF7\",\"#000000\",\"#000000\",\"#000000\",\"8pt Verdana\",\"8pt Verdana\",0,0,\"\",\"bluefireback_left_light.gif\",\"\",\"bluefireback_right_light.gif\",4,4],130,0);");
                                    } else {
                                        //System.out.println("WebProcess_VariableChanges_55001 - Opted to Hide Requests:");
                                    }

                                }
                                //
                                // Variable : 55002
                                //
                                if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55002") == 0) {
                                    // Indicator Code = 55002
                                    za.co.ucs.accsys.peopleware.Variable variableRequested = za.co.ucs.accsys.webmodule.WebProcess_VariableChanges.getVariable(loginSessionObject.getEmployee(), "55002");

                                    if ((variableRequested != null) && (!za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideRequestChangesFor(variableRequested.getDescription().trim()))) {
                                        out.write("stm_aix(\"\",\"menuSubItem\",[0,\"Apply for - " + variableRequested.getDescription() + "\","
                                                + "\"\",\"\",-1,-1,0,"
                                                + "\"" + baseURL + "processes/variable_req.jsp?indCode=55002 \""
                                                + ",\"_self\",\"\","
                                                + ",\"\",\"bullet.gif\",8,8,0,\"\",\"\",0,0,0,0,1,\"#00CCFF\",1,\"#C8C8C8\",1,\"\",\"bluefireback_top_bottom_light.gif\",3,3,0,0,\"#FFFFF7\",\"#000000\",\"#000000\",\"#000000\",\"8pt Verdana\",\"8pt Verdana\",0,0,\"\",\"bluefireback_left_light.gif\",\"\",\"bluefireback_right_light.gif\",4,4],130,0);");
                                    } else {
                                        //System.out.println("WebProcess_VariableChanges_55002 - Opted to Hide Requests:");
                                    }

                                }
                                //
                                // Variable : 55003
                                //
                                if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55003") == 0) {
                                    // Indicator Code = 55002
                                    za.co.ucs.accsys.peopleware.Variable variableRequested = za.co.ucs.accsys.webmodule.WebProcess_VariableChanges.getVariable(loginSessionObject.getEmployee(), "55003");
                                    //System.out.println("WebProcess_VariableChanges_55003 - Variable:" + variableRequested);

                                    if ((variableRequested != null) && (!za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideRequestChangesFor(variableRequested.getDescription().trim()))) {
                                        out.write("stm_aix(\"\",\"menuSubItem\",[0,\"Apply for - " + variableRequested.getDescription() + "\","
                                                + "\"\",\"\",-1,-1,0,"
                                                + "\"" + baseURL + "processes/variable_req.jsp?indCode=55002 \""
                                                + ",\"_self\",\"\","
                                                + ",\"\",\"bullet.gif\",8,8,0,\"\",\"\",0,0,0,0,1,\"#00CCFF\",1,\"#C8C8C8\",1,\"\",\"bluefireback_top_bottom_light.gif\",3,3,0,0,\"#FFFFF7\",\"#000000\",\"#000000\",\"#000000\",\"8pt Verdana\",\"8pt Verdana\",0,0,\"\",\"bluefireback_left_light.gif\",\"\",\"bluefireback_right_light.gif\",4,4],130,0);");
                                    } else {
                                        //System.out.println("WebProcess_VariableChanges_55003 - Opted to Hide Requests:");
                                    }

                                }

                                //
                                // Variable : 55004
                                //
                                if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55004") == 0) {
                                    // Indicator Code = 55004
                                    za.co.ucs.accsys.peopleware.Variable variableRequested = za.co.ucs.accsys.webmodule.WebProcess_VariableChanges.getVariable(loginSessionObject.getEmployee(), "55004");
                                    //System.out.println("WebProcess_VariableChanges_55004 - Variable:" + variableRequested);

                                    if ((variableRequested != null) && (!za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideRequestChangesFor(variableRequested.getDescription().trim()))) {
                                        out.write("stm_aix(\"\",\"menuSubItem\",[0,\"Apply for - " + variableRequested.getDescription() + "\","
                                                + "\"\",\"\",-1,-1,0,"
                                                + "\"" + baseURL + "processes/variable_req.jsp?indCode=55004 \""
                                                + ",\"_self\",\"\","
                                                + ",\"\",\"bullet.gif\",8,8,0,\"\",\"\",0,0,0,0,1,\"#00CCFF\",1,\"#C8C8C8\",1,\"\",\"bluefireback_top_bottom_light.gif\",3,3,0,0,\"#FFFFF7\",\"#000000\",\"#000000\",\"#000000\",\"8pt Verdana\",\"8pt Verdana\",0,0,\"\",\"bluefireback_left_light.gif\",\"\",\"bluefireback_right_light.gif\",4,4],130,0);");
                                    } else {
                                        //System.out.println("WebProcess_VariableChanges_55004 - Opted to Hide Requests:");
                                    }

                                }

                                //
                                // Variable : 55005
                                //
                                if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_VariableChanges_55005") == 0) {
                                    // Indicator Code = 55005
                                    za.co.ucs.accsys.peopleware.Variable variableRequested = za.co.ucs.accsys.webmodule.WebProcess_VariableChanges.getVariable(loginSessionObject.getEmployee(), "55005");
                                    //System.out.println("WebProcess_VariableChanges_55005 - Variable:" + variableRequested);

                                    if ((variableRequested != null) && (!za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().hideRequestChangesFor(variableRequested.getDescription().trim()))) {
                                        out.write("stm_aix(\"\",\"menuSubItem\",[0,\"Apply for - " + variableRequested.getDescription() + "\","
                                                + "\"\",\"\",-1,-1,0,"
                                                + "\"" + baseURL + "processes/variable_req.jsp?indCode=55005 \""
                                                + ",\"_self\",\"\","
                                                + ",\"\",\"bullet.gif\",8,8,0,\"\",\"\",0,0,0,0,1,\"#00CCFF\",1,\"#C8C8C8\",1,\"\",\"bluefireback_top_bottom_light.gif\",3,3,0,0,\"#FFFFF7\",\"#000000\",\"#000000\",\"#000000\",\"8pt Verdana\",\"8pt Verdana\",0,0,\"\",\"bluefireback_left_light.gif\",\"\",\"bluefireback_right_light.gif\",4,4],130,0);");
                                    } else {
                                        //System.out.println("WebProcess_VariableChanges_55005 - Opted to Hide Requests:");
                                    }

                                }



                            }
                        }
                        out.write("stm_ep();");
                    }
                    //
                    //
                    // Processes
                    //
                    //
                    if (user != null) {
                        out.write("stm_aix(\"\",\"mnuMainItem\",[0,\"Processes\",\"\",\"\",-1,-1,0,\"#\",\"_self\",\"\",\"\","
                                + "\"mnu_email.gif\",\"mnu_email.gif\",20,16],75,0);");
                        out.write("stm_bpx(\"p4\",\"p3\",[]);");
                        //fileContainer.getWebProcessesForActiveList(this.owner)
                        // --- Inbox
                        if (fileContainer.getWebProcessesForActiveList(user).size() > 0) {
                            out.write("stm_aix(\"\",\"menuSubItem\",[0,\"Inbox (Management)\","
                                    + "\"\",\"\",-1,-1,0,"
                                    + "\"" + baseURL + "processes/proc_todo.jsp \""
                                    + ",\"_self\",\"\","
                                    + "\"You have " + fileContainer.getWebProcessesForActiveList(user).size() + " process(es) in your Inbox\""
                                    + ",\"bullet_inbox.gif\",\"\",16,16,0,\"\",\"\",0,0,0,0,1,\"#00CCFF\",1,\"#C8C8C8\",1,\"\",\"bluefireback_top_bottom_light.gif\",3,3,0,0,\"#FFFFF7\",\"#000000\",\"#000000\",\"#000000\",\"8pt Verdana\",\"8pt Verdana\",0,0,\"\",\"bluefireback_left_light.gif\",\"\",\"bluefireback_right_light.gif\",4,4],130,0);");
                        } else {
                            out.write("stm_aix(\"\",\"menuSubItem\",[0,\"Inbox (Management)\","
                                    + "\"\",\"\",-1,-1,0,"
                                    + "\"" + baseURL + "processes/proc_todo.jsp \""
                                    + ",\"_self\",\"\","
                                    + "\"List processes that requires your attention \""
                                    + ",\"\",\"bullet.gif\",8,8,0,\"\",\"\",0,0,0,0,1,\"#00CCFF\",1,\"#C8C8C8\",1,\"\",\"bluefireback_top_bottom_light.gif\",3,3,0,0,\"#FFFFF7\",\"#000000\",\"#000000\",\"#000000\",\"8pt Verdana\",\"8pt Verdana\",0,0,\"\",\"bluefireback_left_light.gif\",\"\",\"bluefireback_right_light.gif\",4,4],130,0);");
                        }
                        // --- Handled Processes

                        out.write("stm_aix(\"\",\"menuSubItem\",[0,\"Processed (Management)\","
                                + "\"\",\"\",-1,-1,0,"
                                + "\"" + baseURL + "processes/proc_diddo.jsp \""
                                + ",\"_self\",\"\","
                                + "\"List processes that you have already actioned\""
                                + ",\"\",\"bullet.gif\",8,8,0,\"\",\"\",0,0,0,0,1,\"#00CCFF\",1,\"#C8C8C8\",1,\"\",\"bluefireback_top_bottom_light.gif\",3,3,0,0,\"#FFFFF7\",\"#000000\",\"#000000\",\"#000000\",\"8pt Verdana\",\"8pt Verdana\",0,0,\"\",\"bluefireback_left_light.gif\",\"\",\"bluefireback_right_light.gif\",4,4],130,0);");
                        // --- My Active Requests
                        out.write("stm_aix(\"\",\"menuSubItem\",[0,\"My active requests\","
                                + "\"\",\"\",-1,-1,0,"
                                + "\"" + baseURL + "processes/proc_activelist.jsp \""
                                + ",\"_self\",\"\","
                                + "\"List your requests that are still in progress\""
                                + ",\"\",\"bullet.gif\",8,8,0,\"\",\"\",0,0,0,0,1,\"#00CCFF\",1,\"#C8C8C8\",1,\"\",\"bluefireback_top_bottom_light.gif\",3,3,0,0,\"#FFFFF7\",\"#000000\",\"#000000\",\"#000000\",\"8pt Verdana\",\"8pt Verdana\",0,0,\"\",\"bluefireback_left_light.gif\",\"\",\"bluefireback_right_light.gif\",4,4],130,0);");
                        // --- My Closed Requests
                        out.write("stm_aix(\"\",\"menuSubItem\",[0,\"All my processes\","
                                + "\"\",\"\",-1,-1,0,"
                                + "\"" + baseURL + "processes/proc_list.jsp \""
                                + ",\"_self\",\"\","
                                + "\"List all the requests logged by you\""
                                + ",\"\",\"bullet.gif\",8,8,0,\"\",\"\",0,0,0,0,1,\"#00CCFF\",1,\"#C8C8C8\",1,\"\",\"bluefireback_top_bottom_light.gif\",3,3,0,0,\"#FFFFF7\",\"#000000\",\"#000000\",\"#000000\",\"8pt Verdana\",\"8pt Verdana\",0,0,\"\",\"bluefireback_left_light.gif\",\"\",\"bluefireback_right_light.gif\",4,4],130,0);");

                        //
                        //
                        // If this user is on top of the hierarchy with a given web process, he/she will be able to see detailed information on these
                        // processes 
                        //
                        for (int j = 0; j < availableProcesses.size(); j++) {
                            WebProcessDefinition webProcessDefinition = (WebProcessDefinition) availableProcesses.get(j);
                            //ReportingStructure reportingStructure = webProcessDefinition.getReportingStructure();
                            if (fileContainer.isEmployeeInTopLevel(loginSessionObject.getUser(), webProcessDefinition)) {
                                String description = webProcessDefinition.getName();
                                if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_EmployeeContact") == 0) {
                                    description = "Contact Detail";
                                }
                                if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition") == 0) {
                                    description = "Leave";
                                }
                                if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_LeaveCancellation") == 0) {
                                    description = "Leave Cancellation";
                                }
                                /*
                                 * if
                                 * (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_TrainingRequisition")
                                 * == 0) { description = "Training"; }
                                 */
                                if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_FamilyRequisition") == 0) {
                                    description = "Family Detail";
                                }

                                out.write("stm_aix(\"\",\"menuSubItem\",[0,\"Detailed Report:" + description + "\","
                                        + "\"\",\"\",-1,-1,0,"
                                        + "\"" + baseURL + "processes/proc_list_toplevel.jsp?webProcessClassName=" + webProcessDefinition.getProcessClassName() + " \""
                                        + ",\"_self\",\"\","
                                        + "\"List all the requests logged by you\""
                                        + ",\"\",\"bullet.gif\",8,8,0,\"\",\"\",0,0,0,0,1,\"#00CCFF\",1,\"#C8C8C8\",1,\"\",\"bluefireback_top_bottom_light.gif\",3,3,0,0,\"#FFFFF7\",\"#000000\",\"#000000\",\"#000000\",\"8pt Verdana\",\"8pt Verdana\",0,0,\"\",\"bluefireback_left_light.gif\",\"\",\"bluefireback_right_light.gif\",4,4],130,0);");
                            }
                        }


                        out.write("stm_ep();");
                    }
                    //
                    //
                    // View/Apply for staff (Managers)
                    //
                    //
                    if (user != null) {
                        out.write("stm_aix(\"\",\"mnuMainItem\",[0,\"Managers\",\"\",\"\",-1,-1,0,\"#\",\"_self\",\"\",\"\",\"mnu_book.gif\",\"mnu_book.gif\",16,16],75,0);");
                        out.write("stm_bpx(\"p1\",\"p0\",[1,4,0,0,3,4,8,0,100,\"\",-2,\"\",-2,50,2,3,\"#999999\",\"#f0f0f0\",\"\",0]);");

                        String submenuStartStatement = "stm_bpx(\"p2\",\"p1\",[1,2]);";
                        String submenuEndStatement = "stm_ep();";

                        for (int j = 0; j < availableProcesses.size(); j++) {
                            WebProcessDefinition webProcessDefinition = (WebProcessDefinition) availableProcesses.get(j);
                            boolean isBeforeValidPeriod = ((webProcessDefinition.getActiveFromDayOfMonth() > 0) && (webProcessDefinition.getActiveFromDayOfMonth() > DatabaseObject.getDayOfMonth(new java.util.Date())));
                            boolean isAfterValidPeriod = ((webProcessDefinition.getActiveToDayOfMonth() > 0) && (webProcessDefinition.getActiveToDayOfMonth() < DatabaseObject.getDayOfMonth(new java.util.Date())));
                            if ((isBeforeValidPeriod) || (isAfterValidPeriod)) {
                                System.out.println("*** Process outside valid period ***:" + webProcessDefinition + " valid between " + webProcessDefinition.getActiveFromDayOfMonth() + " and " + webProcessDefinition.getActiveToDayOfMonth() + " of the month.");
                                continue;
                            }

                            //
                            // Standard Manager menu options for web processes
                            //
                            // BUT - Not for T&A Clocking Authorisation, as we have created a special form for that
                            ReportingStructure reportingStructure = webProcessDefinition.getReportingStructure();
                            PageProducer_GroupSelector gen = new PageProducer_GroupSelector(webProcessDefinition, reportingStructure, user, new Integer(reportingStructure.hashCode()).toString(), "Today", null, null); // Scan the existing list of process definitions
                            if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_DailySignoff") == 0) {
                                // Authorise T&A hours for multiple employees
                                PageProducer_GroupSelector genMultiEmpTnAAuth = new PageProducer_GroupSelector(webProcessDefinition, reportingStructure, user, new Integer(reportingStructure.hashCode()).toString(), "Today", null, null, "employeelist_prev.jsp", "Clocking Authorisation", true); // Scan the existing list of process definitions
                                out.write(genMultiEmpTnAAuth.generateWindowsMenuContent());
                            } else {
                                out.write(gen.generateWindowsMenuContent());
                            }
                            //
                            // Extra options for Leave related items
                            //
                            if (webProcessDefinition.getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition") == 0) {
                                // View Leave History
                                PageProducer_GroupSelector genLeaveHistory = new PageProducer_GroupSelector(webProcessDefinition, reportingStructure, user, new Integer(reportingStructure.hashCode()).toString(), "Today", null, null, "leaveHistory_prev.jsp", "View Leave History", false); // Scan the existing list of process definitions
                                out.write(genLeaveHistory.generateWindowsMenuContent());
                                // View Leave Balances
                                PageProducer_GroupSelector genLeaveBalances = new PageProducer_GroupSelector(webProcessDefinition, reportingStructure, user, new Integer(reportingStructure.hashCode()).toString(), "Today", null, null, "leaveBalances_prev.jsp", "View Leave Balances", false); // Scan the existing list of process definitions
                                out.write(genLeaveBalances.generateWindowsMenuContent());
                            }


                        }
                    }

                    out.write("stm_ep();");
                    //
                    //
                    // (Admin)
                    //
                    //
                    if (fileContainer.isEmployeeWebAdministrator(user)) {
                        out.write("stm_aix(\"\",\"mnuMainItem\",[0,\"Admin\",\"\",\"\",-1,-1,0,\"#\",\"_self\",\"\",\"\","
                                + "\"mnu_config.gif\",\"mnu_config.gif\",16,16],75,0);");
                        out.write("stm_bpx(\"p4\",\"p3\",[]);");
                        //fileContainer.getWebProcessesForActiveList(this.owner)
                        // --- Inbox
                        out.write("stm_aix(\"\",\"menuSubItem\",[0,\"Settings\","
                                + "\"\",\"\",-1,-1,0,"
                                + "\"" + baseURL + "processes/admin.jsp\""
                                + ",\"_self\",\"\","
                                + "\"Change the Look & Feel of the ESS\""
                                + ",\"\",\"bullet.gif\",8,8,0,\"\",\"\",0,0,0,0,1,\"#00CCFF\",1,\"#C8C8C8\",1,\"\",\"bluefireback_top_bottom_light.gif\",3,3,0,0,\"#FFFFF7\",\"#000000\",\"#000000\",\"#000000\",\"8pt Verdana\",\"8pt Verdana\",0,0,\"\",\"bluefireback_left_light.gif\",\"\",\"bluefireback_right_light.gif\",4,4],130,0);");

                        // --- Handled Processes

                        out.write("stm_aix(\"\",\"menuSubItem\",[0,\"View Reporting Structures\","
                                + "\"\",\"\",-1,-1,0,"
                                + "\"" + baseURL + "processes/websetup.jsp \""
                                + ",\"_self\",\"\","
                                + "\"List the ESS Reporting Structures\""
                                + ",\"\",\"bullet.gif\",8,8,0,\"\",\"\",0,0,0,0,1,\"#00CCFF\",1,\"#C8C8C8\",1,\"\",\"bluefireback_top_bottom_light.gif\",3,3,0,0,\"#FFFFF7\",\"#000000\",\"#000000\",\"#000000\",\"8pt Verdana\",\"8pt Verdana\",0,0,\"\",\"bluefireback_left_light.gif\",\"\",\"bluefireback_right_light.gif\",4,4],130,0);");
                        out.write("stm_ep();");
                    }

                    //
                    //
                    // Log Off
                    //
                    //
                    if (user != null) {
                        out.write("stm_aix(\"\",\"mnuMainItem\",[0,\"Sign Out\","
                                + "\"\",\"\",-1,-1,0,"
                                + "\"" + baseURL + "Controller.jsp?action=logout\",\"_self\",\"\",\"\",\"mnu_lock.gif\",\"mnu_lock.gif\",21,16,0,\"\",\"\",0,0,0,2]);");
                        out.write("stm_aix(\"\",\"mnuMainItem\",[0,\"Change Password\","
                                + "\"\",\"\",-1,-1,0,"
                                + "\"" + baseURL + "processes/ChangePassword.jsp \",\"_self\",\"Changes your ESS password\",\"\",\"mnu_changepwd.gif\",\"mnu_changepwd.gif\",21,16,0,\"\",\"\",0,0,0,2]);");
                    }
                    out.write("stm_em();");
                } else {

                    out.write(menu_HEADER.toString());
            %>
        // Log On
        stm_ai("mnuMainItem",[0,"Sign In","","",-1,-1,0,"Login.jsp","_self","","","mnu_lock.gif","mnu_lock.gif",16,16,0,"mnu_arrow_down.gif","mnu_arrow_down.gif",9,7,0,0,1,"#FFFFF7",1,"#B5BED6",1,"","bluefireback_top_bottom.gif",3,3,0,0,"#FFFFF7","#000000","#FFFFFF","#FFFFFF","9pt Verdana","9pt Verdana",0,0,"","bluefireback_left.gif","","bluefireback_right.gif",6,6,4],75,0);
        stm_bpx("p1","p0",[1,4,0,0,3,4,8,10,100,"",-2,"",-2,50,2,3,"#999999","#f0f0f0","",0]);
        stm_ep();
        stm_em();
            <%        //}
                }

            %>
                
        </script>
        <br>

