/*
 * ProcLink.java
 *
 * Created on November 4, 2003, 8:12 AM
 */

package za.co.ucs.lwt.dbinfo.classes;

/**
 *
 * @author  liam
 */
public class ProcLink {
    
    /**
     * Creates a new instance of ProcLink
     */
    public ProcLink(StoredProcDef from, StoredProcDef to) {
        this.from = from;
        this.to = to;
    }
    
    public StoredProcDef getFrom(){
        return from;
    }
    
    public StoredProcDef getTo(){
        return to;
    }
    
    public String toString(){
        return ("From: "+(from.toString())+" - To: "+(to.toString()));
    }
    
    private StoredProcDef from;
    private StoredProcDef to;
}
