/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.ucs.accsys.peopleware;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import za.co.ucs.lwt.db.DatabaseObject;

public class ShiftRosterInfo implements AccsysObjectInterface, java.io.Serializable {

    private final Employee employee;
    private Shift[] availableShifts;
    // Fingerprint of previously compatible version
    /* In order to extract this UID, run the following command in the
     * folder where the current lwt.jar file resides BEFORE changing
     * the contents of the class.
     C:\Apache5\shared\lib>c:\j2sdk1.4.2_03\bin\serialver -classpath lwt.jar za.co.ucs.accsys.webmodule.WebProcess_EmployeeContact
     za.co.ucs.accsys.webmodule.WebProcess_EmployeeContact:    static final long serialVersionUID = -1459535708782221840L;
     */
    static final long serialVersionUID = -5788725162942402443L;

    public ShiftRosterInfo(Employee employee) {
        this.employee = employee;
        if (this.isValid()) {
            this.loadFromDB();
        }
    }

    public void applyToDB() {
        if (!this.isValid()) {
            System.out.println("Unable to save shiftRosterInfo values to db.");
        }
    }

    public void loadFromDB() {
        if (!this.isValid()) {
            System.out.println("Unable to load shifts from db.");
        } else {
            Connection con = null;
            try {
                con = DatabaseObject.getNewConnectionFromPool();
                ResultSet rs = DatabaseObject.openSQL((String) "select SHIFT_ID from TA_SHIFT with (nolock) ORDER BY NAME", (Connection) con);
                int numberOfShifts = DatabaseObject.getInt((String) "select count(*) from TA_SHIFT with (nolock)", (Connection) con);
                this.availableShifts = new Shift[numberOfShifts];
                int row = 0;
                while (rs.next()) {
                    this.availableShifts[row] = new Shift(rs.getString("SHIFT_ID"));
                    ++row;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DatabaseObject.releaseConnection((Connection) con);
            }
        }
    }

    public boolean isValid() {
        return this.employee != null;
    }

    public Employee getEmployee() {
        return this.employee;
    }

    public Shift[] getAvailableShifts() {
        return this.availableShifts;
    }

    public Shift getEmployeeShiftThisDay(Date day) {
        Connection con = null;
        Shift empShift = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            String shiftProfileID = DatabaseObject.getString((String) ("select SHIFTPROFILE_ID from TA_E_MASTER with (nolock) where PERSON_ID = " + this.employee.getPersonID()), (Connection) con);
            String shiftID = DatabaseObject.getString((String) ("select fn_TA_GetPersonRosterShift(" + this.employee.getPersonID() + ", date('" + DatabaseObject.formatDate((Date) day) + "')," + shiftProfileID + ")"), (Connection) con);
            empShift = new Shift(shiftID);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection((Connection) con);
        }
        return empShift;
    }
    
    public Shift getRosterShiftThisDay(Date day) {
        Connection con = null;
        Shift rosterShift = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            String shiftProfileID = DatabaseObject.getString((String) ("select SHIFTPROFILE_ID from TA_E_MASTER with (nolock) where PERSON_ID = " + this.employee.getPersonID()), (Connection) con);
            String shiftID = DatabaseObject.getString((String) ("select shift_id from ta_rosterprofile_breakdown with (nolock) where ShiftProfile_Id = " + shiftProfileID + " and \"date\"(DAY) = date('" + DatabaseObject.formatDate((Date) day) + "')"), (Connection) con);
            if (shiftID != null && !shiftID.isEmpty()) {
                rosterShift = new Shift(shiftID);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection((Connection) con);
        }
        return rosterShift;
    }
}
