<%@page contentType="text/html"%>
<%@page import="java.io.*" %>
<%@page import="java.util.*" %>
<%@page import="za.co.ucs.lwt.deploymentbuilder.*" %>
<%@include file="../html/top.html"%>
<%
    User user = (User) session.getAttribute("user");

    if (user == null) {
        session.setAttribute("loggedinuser", new String("false"));
        response.sendRedirect("/DeploymentBuilder/loginJsp.jsp");
    } else {

        FileContainer fileContainer = FileContainer.getInstance(FileContainer.getContainerFile());
        ChangeRequest changeRequest = null;
        if (request.getParameter("crnumber") != null) {
            changeRequest = new ChangeRequest(request.getParameter("crnumber"));
        }
        History.getInstance().addEvent(new HistoryEvent(user, HistoryEvent.EVT_DISSASEMBLETESTINSTALLATION, fileContainer.getTestInstallation(request.getParameter("crnumber")).getName()));
        fileContainer.dismantleTestInstallation(fileContainer.getTestInstallation(request.getParameter("crnumber")), user, true);
        fileContainer.save();

        // Return to previous page
        response.sendRedirect("Development.jsp");
    }
%>
<%@include file="../html/bottom.html"%>