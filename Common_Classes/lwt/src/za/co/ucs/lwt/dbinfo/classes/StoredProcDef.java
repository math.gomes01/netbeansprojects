/*
 * StoredProcDef.java
 *
 * Created on October 21, 2003, 1:22 PM
 */

package za.co.ucs.lwt.dbinfo.classes;
import java.util.*;

/**
 *
 * @author  liam
 */
public class StoredProcDef extends StoredProcBase {
    
    /**
     * Creates a new instance of StoredProcDef
     * Requires:    name - the name of the StoredProcedure
     *
     *              scriptFile - Full path and filename of Script
     *              mainContainer - StoredProcBaseContainer belonging to the Base Class.
     *                              We need this to retain a single instance of each class across
     *                              all the scripts
     */
    public StoredProcDef(String name, String definition, String type, StoredProcBaseContainer mainContainer) {
        super(name);
        this.text = new StringBuffer(definition);
        this.type = type;
        ownContainer = new StoredProcBaseContainer();
            this.mainContainer = mainContainer;
    }
    
    public StoredProcDef(String name, String definition, String type) {
        super(name);
        if (definition != null){
          this.text = new StringBuffer(definition);
        } else {
          this.text = new StringBuffer("");
        }
        this.type = type;
        ownContainer = new StoredProcBaseContainer();
        this.mainContainer = null;
    }
    
    private String convertStringToHTML(String string){
        String newString = string;
        
        // Replace Keywords
        String[] keywords = {" in ","numeric ", "integer ", " char", "if ", "end if","delete ", "before","after",
        " then", " or ", " and ", "insert","update","values",
        "for ", " to ", " next","select ","into", "alter ","return","from "," where","group by","cursor", "is null"};
        for (int i=0; i<keywords.length; i++){
            newString = newString.replaceAll(keywords[i],"<font color=\"#0000ff\"><b>" + keywords[i] + "</b></font>");
        }
        
        // Commands
        String[] commands = {"create ", "declare ", "procedure","function", "view ","trigger", "begin", "end", "returns "};
        for (int j=0; j<commands.length; j++){
            newString = newString.replaceAll(commands[j],"<font color=\"#800000\"><b>" + commands[j] + "</b></font>");
        }
        
        // Comments #1
        String[] comments = {"--"};
        for (int j=0; j<comments.length; j++){
            newString = newString.replaceAll(comments[j],"<font color=\"#009900\"><b>" + comments[j] + "</b></font>");
        }
        // Comments #2
        int startPos = 0;
        int endPos = 0;
        while (newString.indexOf("/*",startPos)>=0){
            startPos = newString.indexOf("/*",startPos);
            endPos = newString.indexOf("*/",startPos);
            if ((startPos>=0) && (endPos>=0)){
                newString = newString.substring(0,startPos-1)+"<font color=\"#009900\">"+newString.substring(startPos,endPos+1)+"</font>"+newString.substring(endPos,newString.length());
            } else {
                break;
            }
            startPos = endPos;
        }
        
        return newString;
    }
    
    
    // Used to check the 'type' of word in the script
    private boolean isKeyWord(String string){
        String[] keywords = {"in", "numeric", "integer", "char", "if", "end if","delete",
        "For", "To", "Next","select","into","alter","return","from","where","group by","cursor"};
        for (int i=0; i<keywords.length; i++){
            if (string.compareToIgnoreCase(keywords[i])==0){
                return true;
            }
        }
        return false;
    }
    // Used to check the 'type' of word in the script
    private boolean isCommand(String string){
        String[] keywords = {"create", "declare", "procedure", "function", "begin", "end", "returns"};
        for (int i=0; i<keywords.length; i++){
            if (string.compareToIgnoreCase(keywords[i])==0){
                return true;
            }
        }
        return false;
    }
    private boolean isComment(String string){
        if ((string.trim().length()>1) &&
                ((string.trim().substring(0, 2).compareTo("//") == 0) ||
                (string.trim().substring(0, 1).compareTo(";") == 0)) )
            return true;
        else
            return false;
    }
    private boolean isInQuotes(String string){
        return (string.indexOf("\'")>=0);
    }
    
    
    public void interpretContentsOfFile(){
        // Now, we'll start 'picking out' the other objects
        buildDependentScriptsFromScript(this.text);
        // convert the script to HTML
        this.text = new StringBuffer(convertStringToHTML(this.text.toString()));
        // Add another tag to the outer boundaries of the script
        this.text.insert(0,"<PRE>");
        this.text.append("</PRE>");
    }
    
    /** Traverses through the actual stored procedure, looking for references to other scripts
     */
    private void buildDependentScriptsFromScript(StringBuffer textInProcedure){
        StringTokenizer tokenizer = new StringTokenizer(textInProcedure.toString(), " \t\n\r\f (),",true);
        
        while (tokenizer.hasMoreTokens()){
            String tokenString = tokenizer.nextToken();
            StoredProcDef containerScript = mainContainer.getStoredProc(tokenString);
            if (containerScript!=null){
                if (containerScript.getName().compareToIgnoreCase(this.getName())!=0){
                    if (ownContainer.getStoredProc(tokenString) == null){
                        ownContainer.addStoredProc(containerScript);
                    }
                }
            }
        }
    }
    
    public void setText(StringBuffer text){
        this.text = text;
    }
    
    public StringBuffer getText() {
        return this.text;
    }
    
    public String toString(){
        return (this.getName());
    }
    
    
    /** We need to know the direct or indirect number of scripts called from this script
     * in order to equally distribute those scripts on the user Interface
     */
    public int getMaxLevelWidth(){
        int currentNumberOfScripts = this.ownContainer.getStoredProcs().values().size();
        int nextLevelNumberOfScripts = 0;
        Iterator iter = this.ownContainer.getStoredProcs().values().iterator();
        while (iter.hasNext()){
            StoredProcDef script = (StoredProcDef)iter.next();
            nextLevelNumberOfScripts += script.getMaxLevelWidth();
        }
        if (currentNumberOfScripts > nextLevelNumberOfScripts){
            return currentNumberOfScripts;
        } else {
            return nextLevelNumberOfScripts;
        }
    }
    
    /** How many scripts are being called from the 'parent' script?
     * We need to know this to do a proper graphical representation
     */
    public int getCurrentLevelWidth(StoredProcDef parent){
        // Get Parent Script
        int currentNumberOfScripts = parent.getOwnContainer().getStoredProcs().values().size();
        return currentNumberOfScripts;
    }
    
    public StoredProcBaseContainer getOwnContainer(){
        return ownContainer;
    }
    
    public StoredProcBaseContainer getBaseContainer(){
        return mainContainer;
    }
    
    public String getType(){
        return this.type;
    }
    
    public void setType(String type){
        this.type = type;
    }
    
    public void setBaseContainer(StoredProcBaseContainer container){
        this.mainContainer  = container;
    }
    
    private StringBuffer text;
    private String type;
    private StoredProcBaseContainer ownContainer;// Global Container
    private StoredProcBaseContainer mainContainer;
    private static Properties properties;
    
}
