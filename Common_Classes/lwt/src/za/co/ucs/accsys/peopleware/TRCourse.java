/*
 * TRProvider.java
 *
* Created on October 19 2006
 */

package za.co.ucs.accsys.peopleware;
import java.sql.*;
import za.co.ucs.lwt.db.*;

/**
 * TRCourse maps to dba.TR_COURSE
 * @Author  liam
 */
public final class TRCourse implements AccsysObjectInterface, java.io.Serializable {
    
    /** Creates an instance of an existing provider in SD_PROVIDER
     * @param providerID As the name suggests. 
     */
    public TRCourse(Integer courseID) {
        this.setCourseID(courseID);
        if (isValid()) {
            loadFromDB();
        }
    }
    
    @Override
    public void applyToDB() {
        // Not implemented for TR_COURSE.  To be managed through the Skills Development Module only.
    }
    
    @Override
    public void loadFromDB() {
        if (this.getCourseID() == null) {
        }
        else {
            Connection con = null;
            try{
                con = DatabaseObject.getNewConnectionFromPool();
                String queryString = "SELECT c.COURSE_ID, c.PROVIDER_ID, c.NAME AS CourseName, c.DESCRIPTION AS CourseDescription, "+
                                     "c.COURSE_CODE AS CourseCode,c.ACCREDITED as isAccredited, c.DURATION AS Duration,c.PREQUALIFICATIONS AS PreQualifications, "+
                                     "ct.DESCRIPTION AS CourseType,cl.COURSE_LEVEL_NAME AS CourseLevel,ce.FROM_DATE AS EventFromDate, "+
                                     "ce.TO_DATE AS EventToDate,ce.VENUE AS EventVenue "+
                                     "FROM ( ( DBA.TR_COURSE AS c with (nolock) LEFT OUTER JOIN DBA.TR_COURSE_TYPE AS ct with (nolock) "+
                                     "         ON c.COURSE_TYPE_ID = ct.COURSE_TYPE_ID ) "+
                                     "   LEFT OUTER JOIN DBA.TR_COURSE_LEVEL AS cl with (nolock) ON c.COURSE_LEVEL_ID = cl.COURSE_LEVEL_ID ) "+
                                     "   LEFT OUTER JOIN DBA.TR_EVENT AS ce with (nolock) ON c.COURSE_ID = ce.COURSE_ID "+
                                     " where c.COURSE_ID="+this.getCourseID();
                                    // " ORDER BY CourseType ASC ,CourseName ASC ";
                ResultSet rs = DatabaseObject.openSQL(queryString, con);
                if (rs.next()){
                    this.setCourseCode(rs.getString("CourseCode"));
                    this.setCourseLevel(rs.getString("CourseLevel"));
                    this.setCourseType(rs.getString("CourseType"));
                    this.setDescription(rs.getString("CourseDescription"));
                    this.setIsAccredited(rs.getBoolean("isAccredited"));
                    this.setName(rs.getString("CourseName"));
                    this.setNextAvailableDate(rs.getString("EventFromDate")+" - "+rs.getString("EventToDate") );
                    this.setPreQualifications(rs.getString("PREQUALIFICATIONS"));
                    this.setVenue(rs.getString("EventVenue"));
                    
                }
            }
            catch (SQLException e){
                e.printStackTrace();
            }
            finally{
                DatabaseObject.releaseConnection(con);
            }
        }
    }
    
    @Override
    public boolean isValid() {
        return ((this.getCourseID() != null));
    }

    /**
     * @return the courseID
     */
    public Integer getCourseID() {
        return courseID;
    }

    /**
     * @param courseID the courseID to set
     */
    public void setCourseID(Integer courseID) {
        this.courseID = courseID;
    }

    @Override
    public String toString(){
      return ("Course:"+getName());
    }

    /**
     * @return the isAccredited
     */
    public boolean isIsAccredited() {
        return isAccredited;
    }

    /**
     * @param isAccredited the isAccredited to set
     */
    public void setIsAccredited(boolean isAccredited) {
        this.isAccredited = isAccredited;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the courseCode
     */
    public String getCourseCode() {
        return courseCode;
    }

    /**
     * @param courseCode the courseCode to set
     */
    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    /**
     * @return the duration
     */
    public String getDuration() {
        return duration;
    }

    /**
     * @param duration the duration to set
     */
    public void setDuration(String duration) {
        this.duration = duration;
    }

    /**
     * @return the venue
     */
    public String getVenue() {
        return venue;
    }

    /**
     * @param venue the venue to set
     */
    public void setVenue(String venue) {
        this.venue = venue;
    }

    /**
     * @return the courseType
     */
    public String getCourseType() {
        return courseType;
    }

    /**
     * @param courseType the courseType to set
     */
    public void setCourseType(String courseType) {
        this.courseType = courseType;
    }

    /**
     * @return the preQualifications
     */
    public String getPreQualifications() {
        return preQualifications;
    }

    /**
     * @param preQualifications the preQualifications to set
     */
    public void setPreQualifications(String preQualifications) {
        this.preQualifications = preQualifications;
    }

    /**
     * @return the nextAvailableDate
     */
    public String getNextAvailableDate() {
        return nextAvailableDate;
    }

    /**
     * @param nextAvailableDate the nextAvailableDate to set
     */
    public void setNextAvailableDate(String nextAvailableDate) {
        this.nextAvailableDate = nextAvailableDate;
    }

    private Integer courseID; //
    private boolean isAccredited; //
    private String name;
    private String description;
    private String courseCode;
    private String courseLevel;
    private String duration;
    private String venue;
    private String courseType;
    private String preQualifications;
    private String nextAvailableDate;

    /**
     * @return the courseLevel
     */
    public String getCourseLevel() {
        return courseLevel;
    }

    /**
     * @param courseLevel the courseLevel to set
     */
    public void setCourseLevel(String courseLevel) {
        this.courseLevel = courseLevel;
    }
   
}
