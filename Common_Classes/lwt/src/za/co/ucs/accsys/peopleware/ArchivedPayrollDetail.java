/*
 * ArchivedPayrollDetail.java
 *
 * Created on January 15, 2004, 12:34 PM
 */

package za.co.ucs.accsys.peopleware;
import java.sql.*;
import za.co.ucs.lwt.db.*;

/**
 * ArchivedPayrollDetail maps to dba.P_PAYROLLDETAIL_OLD and is used in archived payslips
 * @author  liam
 */
public final class ArchivedPayrollDetail implements AccsysObjectInterface, java.io.Serializable {
    
    /** Creates a new instance of Payroll
     * @param payroll Payroll Object
     * @param payrolldetailID Period in the given payroll
     * @param taxYearStart ('yyyy' e.g. '2006')
     */
    public ArchivedPayrollDetail(Payroll payroll, Integer payrolldetailID, String taxYearStart) {
        this.payroll = payroll;
        this.payrolldetailID = payrolldetailID;
        this.taxYearStart = taxYearStart;
        
        if (isValid()) {
            loadFromDB();
        }
    }
    
    
    @Override
    public void loadFromDB() {
        if (!isValid()){
            System.out.println("Unable to load payrolldetail values from db.");
        }
        else {
            Connection con = null;
            try{
                con = DatabaseObject.getNewConnectionFromPool();
                ResultSet rs = DatabaseObject.openSQL("select * from p_payrolldetail_old with (nolock) where payroll_id="+this.payroll.getPayrollID()+
                " and payrolldetail_id="+this.payrolldetailID+
                " and datepart(yy,TAX_YEAR_START)="+taxYearStart, con);
                if (rs.next()){
                    this.startDate = rs.getDate("STARTDATE");
                    this.endDate = rs.getDate("ENDDATE");
                    this.runDate = rs.getDate("RUNDATE");
                    this.payDate = rs.getDate("PAYDATE");
                    this.monthEnd = rs.getDate("MONTHEND");
                    this.combineGroup = rs.getString("COMBINE_GROUP");
                    this.financialMonth = rs.getInt("FINANCIAL_MONTH");
                }
            }
            catch (SQLException e){
                e.printStackTrace();
            }
            finally{
                DatabaseObject.releaseConnection(con);
            }
        }
    }
    
    @Override
    public void applyToDB() {
            System.out.println("ApplyToDB not implemented for ArchivedPayrollDetail.");
    }
    
    
    @Override
    public boolean isValid() {
        return ((this.payroll != null) && (this.payrolldetailID != null));
    }
    
    /**
     * Getter for property payroll.
     * @return Value of property payroll.
     */
    public za.co.ucs.accsys.peopleware.Payroll getPayroll() {
        return payroll;
    }
    
    /**
     * Getter for property payrolldetailID.
     * @return Value of property payrolldetailID.
     */
    public java.lang.Integer getPayrolldetailID() {
        return payrolldetailID;
    }
    
    /**
     * Getter for property startDate.
     * @return Value of property startDate.
     */
    public java.util.Date getStartDate() {
        return startDate;
    }
    
    /** Returns a From - To String representation of the given period
     * @return 
     */
    public String getPeriodRange(){
        String result = DatabaseObject.formatDate(this.startDate)+" - "+DatabaseObject.formatDate(this.endDate);
        return result;
    }
    
    /**
     * Setter for property startDate.
     * @param startDate New value of property startDate.
     */
    public void setStartDate(java.util.Date startDate) {
        this.startDate = startDate;
    }
    
    /**
     * Getter for property endDate.
     * @return Value of property endDate.
     */
    public java.util.Date getEndDate() {
        return endDate;
    }
    
    /**
     * Setter for property endDate.
     * @param endDate New value of property endDate.
     */
    public void setEndDate(java.util.Date endDate) {
        this.endDate = endDate;
    }
    
    /**
     * Getter for property runDate.
     * @return Value of property runDate.
     */
    public java.util.Date getRunDate() {
        return runDate;
    }
    
    /**
     * Setter for property runDate.
     * @param runDate New value of property runDate.
     */
    public void setRunDate(java.util.Date runDate) {
        this.runDate = runDate;
    }
    
    /**
     * Getter for property payDate.
     * @return Value of property payDate.
     */
    public java.util.Date getPayDate() {
        return payDate;
    }
    
    /**
     * Setter for property payDate.
     * @param payDate New value of property payDate.
     */
    public void setPayDate(java.util.Date payDate) {
        this.payDate = payDate;
    }
    
    /**
     * Getter for property monthEnd.
     * @return Value of property monthEnd.
     */
    public java.util.Date getMonthEnd() {
        return monthEnd;
    }
    
    /**
     * Setter for property monthEnd.
     * @param monthEnd New value of property monthEnd.
     */
    public void setMonthEnd(java.util.Date monthEnd) {
        this.monthEnd = monthEnd;
    }
    
    /**
     * Getter for property financialMonth.
     * @return Value of property financialMonth.
     */
    public java.lang.Integer getFinancialMonth() {
        return financialMonth;
    }
    
    /**
     * Setter for property financialMonth.
     * @param financialMonth New value of property financialMonth.
     */
    public void setFinancialMonth(java.lang.Integer financialMonth) {
        this.financialMonth = financialMonth;
    }
    
    /**
     * Getter for property combineGroup.
     * @return Value of property combineGroup.
     */
    public java.lang.String getCombineGroup() {
        return combineGroup;
    }
    
    /**
     * Setter for property combineGroup.
     * @param combineGroup New value of property combineGroup.
     */
    public void setCombineGroup(java.lang.String combineGroup) {
        this.combineGroup = combineGroup;
    }
    
    /**
     * Returns the start (yyyy e.g. '2006') of the tax year in which this period resides
     * @return 
     */
    public String getTaxYearStart(){
        return this.taxYearStart;
    }
    
    private final Payroll payroll;
    private final Integer payrolldetailID;
    private java.util.Date startDate;
    private java.util.Date endDate;
    private java.util.Date runDate;
    private java.util.Date payDate;
    private java.util.Date monthEnd;
    private Integer financialMonth;
    private String combineGroup;
    private final String taxYearStart;
    
}
