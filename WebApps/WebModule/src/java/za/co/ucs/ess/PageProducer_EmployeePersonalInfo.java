package za.co.ucs.ess;

import za.co.ucs.accsys.peopleware.*;

/**
 * <p>
 * PageProducer_EmployeePersonalInfo inherits from PageProducer and will display all the
 * contact information of a given employee in HTML
 * </p>
 */
public class PageProducer_EmployeePersonalInfo extends PageProducer{
    
    /** Constructor
     */
    public PageProducer_EmployeePersonalInfo(Employee employee){
        this.employee = employee;
    }
    
    /** A LeaveInfo Page Producer
     */
    @Override
    public String generateInformationContent() {
        Contact contact = employee.getContact();
        StringBuilder resultPage = new StringBuilder();
        
        // Line 1
        {
            String[] headers1 = {"Tel Home", "Fax Home","Cell","E-Mail"};
            String[][] contents1 = new String[4][1];
            contents1[0][0] = contact.getTelHome();
            contents1[1][0] = contact.getFaxHome();
            contents1[2][0] = contact.getCellNo();
            contents1[3][0] = contact.getEMail();
            resultPage.append(PageSectionGenerator.getInstance().buildHTMLTable(headers1, contents1));
        }
        
        resultPage.append("<br>");
        // Line 2
        {
            String[] headers2 = {"Tel Office", "Fax Office"};
            String[][] contents2 = new String[2][1];
            contents2[0][0] = contact.getTelWork();
            contents2[1][0] = contact.getFaxWork();
            resultPage.append(PageSectionGenerator.getInstance().buildHTMLTable(headers2, contents2));
        }
        
        resultPage.append("<br>");
        // Line 3
        {
            String[] headers3 = {"Unit No", "Complex", "Street No", "Street"};
            String[][] contents3 = new String[4][1];
            contents3[0][0] = contact.getPhys_HOME_UNITNO();
            contents3[1][0] = contact.getPhys_HOME_COMPLEX();
            contents3[2][0] = contact.getPhys_HOME_STREETNO();
            contents3[3][0] = contact.getPhys_HOME_STREET();
            resultPage.append(PageSectionGenerator.getInstance().buildHTMLTable(headers3, contents3));
        }

        resultPage.append("<br>");
        // Line 4
        {
            String[] headers4 = {"Suburb", "City", "Province", "Country"};
            String[][] contents4 = new String[4][1];
            contents4[0][0] = contact.getPhys_HOME_SUBURB();
            contents4[1][0] = contact.getPhys_HOME_CITY();
            contents4[2][0] = contact.getPhys_HOME_PROVINCE();
            contents4[3][0] = contact.getPhys_HOME_COUNTRY();
            resultPage.append(PageSectionGenerator.getInstance().buildHTMLTable(headers4, contents4));
        }


        resultPage.append("<br>");
        // Line 5
        {
            String[] headers5 = {"PO Box", "Area", "City", "Code"};
            String[][] contents5 = new String[4][1];
            contents5[0][0] = contact.getHomePostal1();
            contents5[1][0] = contact.getHomePostal2();
            contents5[2][0] = contact.getHomePostal3();
            contents5[3][0] = contact.getHomePostalCode();
            resultPage.append(PageSectionGenerator.getInstance().buildHTMLTable(headers5, contents5));
        }
        
        return resultPage.toString();
    }
    
    private Employee employee;
} // end LeavePageProducer



