/*
 * QREvent.java
 *
 * Created on October 21, 2003, 1:07 PM
 */

package za.co.ucs.lwt.qar.classes;

/**
 *
 * @author  liam
 */
public class QREvent extends QRBase {
    
    /** Creates a new instance of QREvent */
    public QREvent(String name) {
        super(name);
        timeOut = 0;
    }
    
    public void setTimeOut(int seconds) {
        timeOut = seconds; }
    
    
    private int timeOut;
    
}
