

<%

        // Define Chart
        strBXML = new StringBuffer();
        sqlStatement = new StringBuffer();

        strBXML.append("<");
        strBXML.append("chart caption='Sick Leave: By Category - current cycle' ");
        strBXML.append("showPercentageValues='1' ");
        strBXML.append("showValues='1' ");
        strBXML.append("formatNumberScale='0' ");
        strBXML.append("showAboutMenuItem='0' ");
        strBXML.append("bgColor='D8E6E9,FFFFFF' ");
        strBXML.append("bgAlpha='100, 100' ");
        strBXML.append("showBorder='1' ");
        strBXML.append("canvasbgAlpha='30' ");
        strBXML.append("outCnvBaseFont='Verdana' ");
        strBXML.append("showToolTip='1'");
        strBXML.append(">");


za.co.ucs.lwt.db.DatabaseObject.setConnectInfo_AccsysJDBC(za.co.ucs.accsys.webmodule.WebModulePreferences.getInstance().getPreference(za.co.ucs.accsys.webmodule.WebModulePreferences.URL_Database, ""));
        try {
            con = object.getNewConnectionFromPool();
            sqlStatement.append("select sum(nb_unit) as DaysTaken, trim(reason) as theReason ");
            sqlStatement.append("from l_history with (nolock) where ");
            sqlStatement.append("   Company_id=" + employee.getCompany().getCompanyID());
            sqlStatement.append("   and Employee_id=" + employee.getEmployeeID());
            sqlStatement.append(" and leave_type_id=3 ");
            sqlStatement.append(" and end_date>=fn_L_GetCycleStart(fn_L_GetLeaveCycleDate(company_id, employee_id, 3, 1),  fn_L_GetCycleLength(company_id, employee_id, 3),now(*)) ");
            sqlStatement.append("  group by theReason order by theReason");


            java.sql.ResultSet rs = object.openSQL(sqlStatement.toString(), con);

            // Build the SETS
            while (rs.next()) {
                String daysTaken = new Float(rs.getFloat("DaysTaken")).toString();
                String category = rs.getString("theReason");
                strBXML.append("<set label='" + category + "' displayValue='"+category+"' value='" + daysTaken + "'  toolText='"+daysTaken+"{br}Days taken' />");
            }



            strBXML.append("</chart>");
            strXML = strBXML.toString();
            rs.close();
        } finally {
            object.releaseConnection(con);
        }

%>
<jsp:include page="../FCharts/Includes/FusionChartsHTMLRenderer.jsp" flush="true">
        <jsp:param name="chartSWF" value="../FCharts/FusionCharts/Doughnut2D.swf " />
        <jsp:param name="strURL" value="" />
        <jsp:param name="strXML" value="<%=strXML%>" />
        <jsp:param name="chartId" value="myNext" />
        <jsp:param name="chartWidth" value="550" />
        <jsp:param name="chartHeight" value="350" />
        <jsp:param name="debugMode" value="false" />
        <jsp:param name="registerWithJS" value="false" />

    </jsp:include>


