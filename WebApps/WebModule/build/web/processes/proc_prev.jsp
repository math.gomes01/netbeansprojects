<%@page contentType="text/html"%>

<%@include file="html5_top.jsp"%>

<%@include file="html_messages.jsp"%> 
<!-- This HAS to be included if messages were generated
and added to the session attributes for this person.
Calling this script will simply display those messages -->

<!-- Now we need to verify that there is a LoginSessionObject -->

<%  // Can I connect to the database?
    // We need to reset the EMPLOYEE to be the same person as the USER once the process has been committed
    if (!user.equals(employee)) {
        loginSessionObject.setEmployee(user);
    } else {
        if (loginSessionObject == null) {
            response.sendRedirect(response.encodeURL("Login.jsp"));
        } else {
            // Process In Question
            String hashValue = request.getParameter("hashValue");
            WebProcess process = za.co.ucs.accsys.webmodule.FileContainer.getInstance().getWebProcess(hashValue);
            // Page Detail
            if (process == null) {
                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                        "No Web Process available with given hashCode.", true, request.getSession());
                producer.setMessageInSessionObject();
                out.write("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");

            } else {
                // Page Top
                PageSectionGenerator gen = PageSectionGenerator.getInstance();
                out.write(gen.buildHTMLPageHeader("Web Process Preview", loginSessionObject.getUser().toString()));

                out.write("<div id=\"container\"><center>");
                out.write("<div class=\"item\">");
                PageProducer_ProcessPreview producer = new PageProducer_ProcessPreview(process, loginSessionObject);
                out.write(producer.generateInformationContent());
                out.write("</div>");
                out.write("</center></div>");
            }
        }
    }
%>

<%@include file="html_bottom.jsp"%>