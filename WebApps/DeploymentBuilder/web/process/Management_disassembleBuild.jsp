<%@page contentType="text/html"%>
<%@page import="java.io.*" %>
<%@page import="java.util.*" %>
<%@page import="za.co.ucs.lwt.deploymentbuilder.*" %>

<%
    User user = (User) session.getAttribute("user");

    if (user == null) {
        session.setAttribute("loggedinuser", new String("false"));
        response.sendRedirect("/DeploymentBuilder/loginJsp.jsp");
    } else {

        FileContainer fileContainer = FileContainer.getInstance(FileContainer.getContainerFile());
        BuildInstallation buildInstallation = null;
        if (request.getParameter("buildname") != null) {
            buildInstallation = fileContainer.getBuildInstallation(request.getParameter("buildname"));
            History.getInstance().addEvent(new HistoryEvent(user, HistoryEvent.EVT_DISSASEMBLEBUILDINSTALLATION, buildInstallation.getBuildName()));
            fileContainer.dismantleBuildInstallation(buildInstallation, user);
            fileContainer.save();
        }

        // Return to previous page
        response.sendRedirect("Management.jsp");
    }
%>
<%@include file="../html/bottom.html"%>
