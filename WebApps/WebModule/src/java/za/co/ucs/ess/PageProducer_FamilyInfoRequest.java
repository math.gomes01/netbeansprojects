package za.co.ucs.ess;

import java.util.LinkedList;
import za.co.ucs.accsys.peopleware.Employee;
import za.co.ucs.accsys.peopleware.Family;
import za.co.ucs.accsys.webmodule.WebModulePreferences;

/**
 * <p>
 * PageProducer_FamilyInfoRequest inherits from PageProducer and will display
 * the detail of a FamilyInformationRequest in HTML The generated page allows
 * for adding/removing and changing of family members </p>
 */
public class PageProducer_FamilyInfoRequest extends PageProducer {

    /**
     * Constructor
     */
    public PageProducer_FamilyInfoRequest(Employee employee) {
        this.employee = employee;
    }

    /**
     * A FamilyInfoRequest Page Producer
     */
    @Override
    public String generateInformationContent() {
        try {
            StringBuilder resultPage = new StringBuilder();
            LinkedList familyMembers = employee.getFamilyMembers();
            String baseURL = WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Web, "");
            String link = baseURL + "Controller.jsp";
            int column = 0;
            int columnCount = 11;
            boolean canChangeMedAidDependent = WebModulePreferences.getPreferences().getBoolean(WebModulePreferences.DISPLAY_canChangeMedAidDependent, false);
            // If the employee can change Med Aid Dependent add the extra column
            if (canChangeMedAidDependent) {
                columnCount++;
            }

            resultPage.append("<center><div class=\"outer-grouping\">");
            resultPage.append("<h3 class=\"shaddow\" style=\"padding-left: 5px; margin-top: 5px;\">Change information for existing family members</h3>");
            resultPage.append("<FORM name=\"req_family\" METHOD=POST action=\"").append(link).append("\">");
            resultPage.append("<table col=0><tr><td>");

            // ***********************
            // Existing Family Members
            // ***********************
            String[] headers = {"First Name", "Second Name", "Surname", "Relation", "Gender", "Birth Date", "Tel Home", "Tel Work", "Cell", "E-mail", "Action"};
            if (canChangeMedAidDependent) {
                String[] headersMed = {"First Name", "Second Name", "Surname", "Relation", "Gender", "Birth Date", "Medical Aid Dependent", "Tel Home", "Tel Work", "Cell", "E-mail", "Action"};
                headers = headersMed;
            }
            String[][] contents = new String[columnCount][familyMembers.size()];
            // construct the contents matrix
            int maxFamilyID = 0;
            for (int row = 0; row < familyMembers.size(); row++) {
                // Family class
                Family family = (Family) familyMembers.get(row);
                if (family.getFamilyID().intValue() > maxFamilyID) {
                    maxFamilyID = family.getFamilyID().intValue();
                }
                column = 0;
                contents[column++][row] = "<input type=\"text\" name=\"edtFirstName_" + family.getFamilyID() + "\" placeholder=\"First Name\" size=\"12\" maxlength=\"30\" value=\"" + family.getFirstName() + "\">";
                contents[column++][row] = "<input type=\"text\" name=\"edtSecondName_" + family.getFamilyID() + "\" placeholder=\"Second Name\" size=\"12\" maxlength=\"30\" value=\"" + family.getSecondName() + "\">";
                contents[column++][row] = "<input type=\"text\" name=\"edtSurname_" + family.getFamilyID() + "\" placeholder=\"Surname\" size=\"12\" maxlength=\"30\" value=\"" + family.getSurname() + "\">";
                contents[column++][row] = getRelationTag(family);
                contents[column++][row] = getGenderTag(family);
                contents[column++][row] = getBirthDateTag(family);
                if (canChangeMedAidDependent) {
                    contents[column++][row] = getMedTag(family);
                }
                contents[column++][row] = "<input type=\"text\" name=\"edtTelHome_" + family.getFamilyID() + "\" placeholder=\"Tel Home\" size=\"12\" maxlength=\"30\" value=\"" + family.getTelHome() + "\">";
                contents[column++][row] = "<input type=\"text\" name=\"edtTelWork_" + family.getFamilyID() + "\" placeholder=\"Tel Work\" size=\"12\" maxlength=\"30\" value=\"" + family.getTelWork() + "\">";
                contents[column++][row] = "<input type=\"text\" name=\"edtTelCell_" + family.getFamilyID() + "\" placeholder=\"Cell\" size=\"12\" maxlength=\"30\" value=\"" + family.getTelCell() + "\">";
                contents[column++][row] = "<input type=\"text\" name=\"edtEMail_" + family.getFamilyID() + "\" placeholder=\"E-mail\" size=\"12\" maxlength=\"30\" value=\"" + family.getEMail() + "\">";
                contents[column++][row] = "<font color=\"#ff0000\" style=\"white-space: nowrap; padding: 5px;\"><input type=\"radio\" name=\"cbOption_" + family.getFamilyID() + "\" value=\"Delete\" >Delete<br></font>"
                        + "<font color=\"#000000\" style=\"white-space: nowrap; padding: 5px;\"><input type=\"radio\" name=\"cbOption_" + family.getFamilyID() + "\" value=\"Change\" >Change<br></font>"
                        + "<font color=\"#808080\" style=\"white-space: nowrap; padding: 5px;\"><input type=\"radio\" name=\"cbOption_" + family.getFamilyID() + "\" value=\"None\" checked>None</font>";

            }
            resultPage.append(PageSectionGenerator.getInstance().buildHTMLTable("displaytable_wide", headers, contents));

            // ***********************
            // New Family Members (Up to 5)
            // ***********************
            String[][] contentsNew = new String[columnCount][5];
            // construct the contents matrix
            for (int row = 0; row < 5; row++) {
                // Family class
                column = 0;
                int newFamilyID = maxFamilyID + row + 1;
                contentsNew[column++][row] = "<input type=\"text\" name=\"edtFirstName_" + newFamilyID + "\" placeholder=\"First Name\" size=\"12\" maxlength=\"30\" value=\"\">";
                contentsNew[column++][row] = "<input type=\"text\" name=\"edtSecondName_" + newFamilyID + "\" placeholder=\"Second Name\" size=\"12\" maxlength=\"30\" value=\"\">";
                contentsNew[column++][row] = "<input type=\"text\" name=\"edtSurname_" + newFamilyID + "\" placeholder=\"Surname\" size=\"12\" maxlength=\"30\" value=\"\">";
                contentsNew[column++][row] = getRelationTag(newFamilyID);
                contentsNew[column++][row] = getGenderTag(newFamilyID);
                contentsNew[column++][row] = getBirthDateTag(newFamilyID);
                if (canChangeMedAidDependent) {
                    contentsNew[column++][row] = getMedTag(newFamilyID);
                }
                contentsNew[column++][row] = "<input type=\"text\" name=\"edtTelHome_" + newFamilyID + "\" placeholder=\"Tel Home\" size=\"12\" maxlength=\"30\" value=\"\">";
                contentsNew[column++][row] = "<input type=\"text\" name=\"edtTelWork_" + newFamilyID + "\" placeholder=\"Tel Work\" size=\"12\" maxlength=\"30\" value=\"\">";
                contentsNew[column++][row] = "<input type=\"text\" name=\"edtTelCell_" + newFamilyID + "\" placeholder=\"Cell\" size=\"12\" maxlength=\"30\" value=\"\">";
                contentsNew[column++][row] = "<input type=\"text\" name=\"edtEMail_" + newFamilyID + "\" placeholder=\"E-mail\" size=\"12\" maxlength=\"30\" value=\"\">";
                contentsNew[column++][row] = "<font color=\"#808080\" style=\"white-space: nowrap; padding: 15px 20px 15px 10px;\"><input type=\"checkbox\" name=\"cbAdd_" + newFamilyID + "\" value=\" Add\" > Add</font>";
            }
            resultPage.append("</td></tr><tr><td>");
            resultPage.append("<center><h3 class=\"shaddow\" style=\"margin-bottom: 15px;\">Add new family members</h3></center>");
            resultPage.append(PageSectionGenerator.getInstance().buildHTMLTable("displaytable_wide", headers, contentsNew));

            // SUBMIT
            {
                resultPage.append("<!-- Horizontal Ruler -->");
                resultPage.append("<hr class=\"center\" style=\"margin-left: 10px; margin-right: 10px; margin-top: 15px;\">");
                resultPage.append("<input class=\"gradient-button\" style=\"margin-right: 50px;\" type=\"submit\" value=\"Submit Family Information Request\">");
                resultPage.append("<input type=\"hidden\" name=\"action\" value=\"req_family\">");
            }
            resultPage.append("</td></tr></table></form></div></center>");
            return resultPage.toString();
        } catch (Exception e) {
            return e.getMessage();
        }

    }

    private String getRelationTag(Family family) {
        // Relation
        StringBuilder relation = new StringBuilder();
        relation.append("<select name=\"cbbRelation_").append(family.getFamilyID()).append("\">");
        // Adult Family Member
        {
            if (family.getRelation().compareToIgnoreCase("Adult Family Member") == 0) {
                relation.append("<option value=\"Adult Family Member\" selected>Adult Family Member</option>");
            } else {
                relation.append("<option value=\"Adult Family Member\" >Adult Family Member</option>");
            }
            // Child
            if (family.getRelation().compareToIgnoreCase("Child") == 0) {
                relation.append("<option value=\"Child\" selected>Child</option>");
            } else {
                relation.append("<option value=\"Child\" >Child</option>");
            }
            // Spouse
            if (family.getRelation().compareToIgnoreCase("Spouse") == 0) {
                relation.append("<option value=\"Spouse\" selected>Spouse</option>");
            } else {
                relation.append("<option value=\"Spouse\" >Spouse</option>");
            }
            // Unselected
            if ((family.getRelation().compareToIgnoreCase("Spouse") != 0)
                    && (family.getRelation().compareToIgnoreCase("Child") != 0)
                    && (family.getRelation().compareToIgnoreCase("Adult Family Member") != 0)) {
                relation.append("<option value=\"Unselected\" selected>-Select-</option>");
            }
            relation.append("</select>");
        }
        return relation.toString();
    }

    private String getRelationTag(int familyID) {
        // Relation
        StringBuilder relation = new StringBuilder();
        relation.append("<select name=\"cbbRelation_").append(familyID).append("\">");
        // Adult Family Member
        {
            relation.append("<option value=\"Adult Family Member\" >Adult Family Member</option>");
            relation.append("<option value=\"Child\" >Child</option>");
            relation.append("<option value=\"Spouse\" >Spouse</option>");
            // Unselected
            relation.append("<option value=\"Unselected\" selected>-Select-</option>");
            relation.append("</select>");
        }
        return relation.toString();
    }

    private String getBirthDateTag(Family family) {
        // Date
        StringBuilder date = new StringBuilder();

        date.append("<input type=\"text\" name=\"edtBirthDate_").append(family.getFamilyID()).append("\" size=10 maxlength=10");
        date.append(" onFocus=\"javascript:vDateType='2'\"");
        date.append(" onKeyUp=\"DateFormat(this,this.value,event,false,'2')\" ");
        //date.append(" onBlur=\"DateFormat(this,this.value,event,false,'2')\" ");
        date.append(" placeholder=\"yyyy/mm/dd\" value=\"").append(family.getBirthDate()).append("\">");
        return date.toString();
    }

    private String getBirthDateTag(int familyID) {
        // Date
        StringBuilder date = new StringBuilder();
        date.append("<input type=\"text\" name=\"edtBirthDate_").append(familyID).append("\" size=10 maxlength=10");
        //date.append(" onFocus=\"javascript:vDateType='2'\"");
        //date.append(" onKeyUp=\"DateFormat(this,this.value,event,false,'2')\" ");
        //date.append(" onBlur=\"DateFormat(this,this.value,event,false,'2')\" ");
        date.append(" placeholder=\"yyyy/mm/dd\"  value=\"\">");
        return date.toString();
    }

    private String getGenderTag(Family family) {
        // Gender
        StringBuilder gender = new StringBuilder();
        gender.append("<select name=\"cbbGender_").append(family.getFamilyID()).append("\">");
        {
            // Female
            if (family.getGender().compareToIgnoreCase("F") == 0) {
                gender.append("<option value=\"F\" selected>F</option>");
            } else {
                gender.append("<option value=\"F\" >F</option>");
            }
            // Male
            if (family.getGender().compareToIgnoreCase("M") == 0) {
                gender.append("<option value=\"M\" selected>M</option>");
            } else {
                gender.append("<option value=\"M\" >M</option>");
            }
            gender.append("<option value=\"Unselected\" >-Select-</option>");
            // Unselected
            if ((family.getGender().compareToIgnoreCase("M") != 0)
                    && (family.getGender().compareToIgnoreCase("F") != 0)) {
                gender.delete(gender.length() - 45, gender.length());
                gender.append("<option value=\"Unselected\" selected>-Select-</option>");
            }
            gender.append("</select>");
        }
        return gender.toString();
    }

    private String getGenderTag(int familyID) {
        // Gender
        StringBuilder gender = new StringBuilder();
        gender.append("<select name=\"cbbGender_").append(familyID).append("\">");
        {
            // Female
            gender.append("<option value=\"F\" >F</option>");
            // Male
            gender.append("<option value=\"M\" >M</option>");
            // Unselected
            gender.append("<option value=\"Unselected\" selected>-Select-</option>");
            gender.append("</select>");
        }
        return gender.toString();
    }

    private String getMedTag(Family family) {
        // Gender
        StringBuilder medYN = new StringBuilder();
        medYN.append("<center><select name=\"cbbMedDependent_").append(family.getFamilyID()).append("\">");
        {
            if (family.isMedDependent()) {
                medYN.append("<option value=\"Y\" selected>&nbsp;&nbsp;&nbsp;Y&nbsp;&nbsp;&nbsp;</option>");
                medYN.append("<option value=\"N\" >&nbsp;&nbsp;&nbsp;N&nbsp;&nbsp;&nbsp;</option>");
            } else {
                medYN.append("<option value=\"Y\">&nbsp;&nbsp;&nbsp;Y&nbsp;&nbsp;&nbsp;</option>");
                medYN.append("<option value=\"N\" selected>&nbsp;&nbsp;&nbsp;N&nbsp;&nbsp;&nbsp;</option>");
            }
            medYN.append("</select></center>");
        }
        return medYN.toString();
    }

    private String getMedTag(int familyID) {
        // Gender
        StringBuilder medYN = new StringBuilder();
        medYN.append("<center><select name=\"cbbMedDependent_").append(familyID).append("\">");
        {
            // Yes
            medYN.append("<option value=\"Y\" >&nbsp;&nbsp;&nbsp;Y&nbsp;&nbsp;&nbsp;</option>");
            // No Default
            medYN.append("<option value=\"N\" selected>&nbsp;&nbsp;&nbsp;N&nbsp;&nbsp;&nbsp;</option>");
            medYN.append("</select></center>");
        }
        return medYN.toString();
    }

    private Employee employee;
} // end LeavePageProducer

