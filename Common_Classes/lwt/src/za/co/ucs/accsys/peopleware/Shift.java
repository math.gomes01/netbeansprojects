/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.ucs.accsys.peopleware;

import java.awt.Color;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import za.co.ucs.lwt.db.DatabaseObject;

public final class Shift implements AccsysReadOnlyObjectInterface, java.io.Serializable {

    private final String shiftID;
    private String name = "";
    private String color = "";
    // Fingerprint of previously compatible version
    /* In order to extract this UID, run the following command in the
     * folder where the current lwt.jar file resides BEFORE changing
     * the contents of the class.
     C:\Apache5\shared\lib>c:\j2sdk1.4.2_03\bin\serialver -classpath lwt.jar za.co.ucs.accsys.webmodule.WebProcess_EmployeeContact
     za.co.ucs.accsys.webmodule.WebProcess_EmployeeContact:    static final long serialVersionUID = -1459535708782221840L;
     */
    static final long serialVersionUID = -8205184029142052224L;

    public Shift(String shiftID) {
        this.shiftID = shiftID;
        if (this.isValid()) {
            this.loadFromDB();
        }
    }

    @Override
    public void loadFromDB() {
        if (this.shiftID != null) {
            Connection con = null;
            try {
                con = DatabaseObject.getNewConnectionFromPool();
                ResultSet rs = DatabaseObject.openSQL((String) ("select NAME, COLOUR from TA_SHIFT with (nolock) WHERE SHIFT_ID = " + this.shiftID), (Connection) con);
                if (rs.next()) {
                    this.name = rs.getString("NAME");
                    Color hexColor = new Color(new Integer(rs.getString("COLOUR")));
                    this.color = "rgb(" + hexColor.getBlue() + "," + hexColor.getGreen() + "," + hexColor.getRed() + ")";
                }
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DatabaseObject.releaseConnection((Connection) con);
            }
        }
    }

    public int getPersonsOnShiftForDate(Date date) {
        int result = 0;
        Connection con = null;
        try {
            con = DatabaseObject.getNewConnectionFromPool();
            ResultSet rs = DatabaseObject.openSQL((String) ("SELECT count(*) as CNT FROM vw_TA_ShiftRoster with (nolock) where DAY = '" + DatabaseObject.formatDate((Date) date) + "' AND SHIFT_ID = " + this.shiftID), (Connection) con);
            if (rs.next()) {
                result = rs.getInt("CNT");
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection((Connection) con);
        }
        return result;
    }

    public boolean isValid() {
        return this.shiftID != null;
    }

    public String getShiftID() {
        return this.shiftID;
    }

    public String getName() {
        return this.name;
    }

    public String getColor() {
        return this.color;
    }
}
