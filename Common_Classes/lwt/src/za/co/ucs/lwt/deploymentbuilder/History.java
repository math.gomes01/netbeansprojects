/*
 * History.java
 *
 * Created on December 19, 2003, 12:45 PM
 */

package za.co.ucs.lwt.deploymentbuilder;
import java.util.*;
import java.io.*;

/**
 * The History Class will be used to retain a full record of activities taken place in
 * the Deployment Manager.  Singleton
 * @author  liam
 */
public class History implements java.io.Serializable{
    
    /** Creates a new instance of History */
    private History() {
        historyFile = new File(FileContainer.getArchiveFolder().getAbsolutePath()+FileContainer.getOSFileSeperator()+"history.dat");
        historyFile.mkdirs();
        historyEvents = new LinkedList();
    }
    
    public static History getInstance(){
        //System.out.println("History.getInstance()");
        if (history == null)
            History.createInstance();
        System.out.println("                      :"+history);
        return history;
    }
    
    private static synchronized void createInstance(){
        if (history == null){
            history = new History();
            history.load();
        }
    }
    
    /** Loads the mappings from a file
     */
    private void load(){
        try{
            // Temporary uncompressed file
           File tempFile = new File(FileContainer.getTemporaryFolder().getAbsolutePath()+FileContainer.getOSFileSeperator()+"hist");
           System.out.println("Loading history from: "+historyFile);
             //if (tempFile.exists()){
                TestInstallationArchiver.uncompressFile(historyFile, tempFile);
                
                FileInputStream fis = new FileInputStream(tempFile);
                ObjectInputStream ois = new ObjectInputStream(fis);
                
                // Load each HistoryEvent
                historyEvents.clear();
                int numberOfEvents = ois.readInt();
                for (int i=0; i<numberOfEvents; i++){
                    HistoryEvent event = (HistoryEvent)ois.readObject();
                    historyEvents.add(event);
                }
                
                // Close Streams
                ois.close();
                fis.close();
            //}
        }
        catch (FileNotFoundException fnfe){
            fnfe.printStackTrace();
        }
        catch (EOFException eofe){
            
        }
        catch (IOException ioe){
            ioe.printStackTrace();
        }
        catch (ClassNotFoundException cnfe){
            cnfe.printStackTrace();
        }
    }
    
    /** Saves the mappings to a file
     */
    public void save(){
        try{
            // Temporary uncompressed file
            File tempFile = new File(FileContainer.getTemporaryFolder().getAbsolutePath()+FileContainer.getOSFileSeperator()+"hist");
            tempFile.mkdirs();
            
            // Clears the current contents
            historyFile.delete();
            tempFile.delete();
            
            System.out.println("Saving history to: "+tempFile);
            FileOutputStream fos = new FileOutputStream(tempFile);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            // Save each HistoryEvent
            oos.writeInt(historyEvents.size());
            for (int i=0; i<historyEvents.size(); i++){
                oos.writeObject(historyEvents.get(i));
            }
            
            // Close Streams
            oos.close();
            fos.close();
            
            // Let's compress the darn file!
            TestInstallationArchiver.compressFile(tempFile, historyFile);
            
        }
        catch (FileNotFoundException fnfe){
            fnfe.printStackTrace();
        }
        catch (IOException ioe){
            ioe.printStackTrace();
        }
    }
    
    public void addEvent(HistoryEvent event){
        historyEvents.add(event);
        save();
    }
    
    public LinkedList getEvents(){
        return historyEvents;
    }
    
    
    private static LinkedList historyEvents;
    private static History history = null;
    private static File historyFile = null;
}

