<%-- 
    Document   : tax_calc
    Created on : 06 Nov 2013 = ""; 10:23:16 AM
    Author     : fkleynhans
--%>

<%@page import="java.text.DecimalFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.*"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="za.co.ucs.accsys.webmodule.*" %>
<%@page import="za.co.ucs.ess.*" %>
<%@page import="za.co.ucs.lwt.db.*" %>
<%@page import="za.co.ucs.accsys.peopleware.*" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="/WebModule/CSS/style.css" />
        <script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>  
        <title>Tax Calculation Result</title>
        <script type="text/javascript">
            // Goes back
            function back() {
                act = 'Back';
                history.back();
            }

            $(document).ready(function () {
                console.log('ready!');

                function showIfChecked($mySelector) {
                    console.log($mySelector);
                    console.log($mySelector.children("input[type='checkbox']"));
                    if ($mySelector.children("input[type='checkbox']").is(':checked'))
                        $mySelector.parent().children("div.other_text").slideDown("slow");
                    else
                        $mySelector.parent().children("div.other_text").slideUp("slow");
                }

                showIfChecked($(".onoffswitch").eq(0)); // first question

                function changeIfChecked($mySelector1) {
                    $mySelector1.children("input[type='checkbox']").change(function () {
                        console.log($(this).is(':checked'));  // $(this) represents whatever the selector brought back - eg. $("div.question > input[type='checkbox']")
                        if ($(this).is(':checked'))
                            $mySelector1.parent().children("div.other_text").slideDown("slow");
                        else
                            $mySelector1.parent().children("div.other_text").slideUp("slow");
                        $mySelector1.parent().children("div.other_text").children().children("input[type='number']").val();
                        $mySelector1.parent().children("div.other_text").children().children().children("input[type='number']").val();
                    });
                }

                changeIfChecked($(".onoffswitch").eq(0));

            });

            function copyTo(obj)
            {
                document.getElementById("RetirementFundIncomeInput").value = obj.value;
            }
        </script>
    </head>
    <body>
        <div id="container">

            <%
                String GrossIncome = request.getParameter("GrossIncome");
                if (GrossIncome == null || GrossIncome.equalsIgnoreCase("")) {
                    GrossIncome = "0";
                }

                String RetirementFundIncome = request.getParameter("RetirementFundIncome");
                if (RetirementFundIncome == null || RetirementFundIncome.equalsIgnoreCase("")) {
                    RetirementFundIncome = "0";
                }
                String NonRetirementFundIncome = request.getParameter("NonRetirementFundIncome");
                if (NonRetirementFundIncome == null || NonRetirementFundIncome.equalsIgnoreCase("")) {
                    NonRetirementFundIncome = "0";
                }
                String AnnualBonus = request.getParameter("AnnualBonus");
                if (AnnualBonus == null || AnnualBonus.equalsIgnoreCase("")) {
                    AnnualBonus = "0";
                }

                String NoOfPayPeriodsInYear = request.getParameter("NoOfPayPeriodsInYear");
                if (NoOfPayPeriodsInYear == null || NoOfPayPeriodsInYear.equalsIgnoreCase("")) {
                    NoOfPayPeriodsInYear = "0";
                }

                String paymentFrequency = "";
                if (NoOfPayPeriodsInYear.equalsIgnoreCase("12")) {
                    paymentFrequency = "Monthly";

                } else if (NoOfPayPeriodsInYear.equalsIgnoreCase("26")) {
                    paymentFrequency = "Fortnightly";

                } else {
                    paymentFrequency = "Weekly";
                };

                String TaxYearEndDate = request.getParameter("TaxYearEndDate");
                if (TaxYearEndDate == null || TaxYearEndDate.equalsIgnoreCase("")) {
                    TaxYearEndDate = "020170228";
                }

                String TaxYearEndDate_Previous = request.getParameter("TaxYearEndDate_Previous");
                if (TaxYearEndDate_Previous == null || TaxYearEndDate_Previous.equalsIgnoreCase("")) {
                    TaxYearEndDate_Previous = "020160229";
                }

                String EmployeeAge = request.getParameter("EmployeeAge");
                if (EmployeeAge == null || EmployeeAge.equalsIgnoreCase("")) {
                    EmployeeAge = "0";
                }
                String TravelAllowance = request.getParameter("TravelAllowance");
                if (TravelAllowance == null || TravelAllowance.equalsIgnoreCase("")) {
                    TravelAllowance = "0";
                }
                String taxPercentage = request.getParameter("taxPercentage");
                if (taxPercentage == null || taxPercentage.equalsIgnoreCase("")) {
                    taxPercentage = "0";
                }

                ////////////////////////////////////////////////////////////////
                //   Mutliply travel allowance by taxable percentage 
                ////////////////////////////////////////////////////////////////
                double temp = Double.parseDouble(TravelAllowance) * Double.parseDouble(taxPercentage);
                TravelAllowance = Double.toString(temp);

                String OtherAllowance = request.getParameter("OtherAllowance");
                if (OtherAllowance == null || OtherAllowance.equalsIgnoreCase("")) {
                    OtherAllowance = "0";
                }
                String Pension = request.getParameter("Pension");
                if (Pension == null || Pension.equalsIgnoreCase("")) {
                    Pension = "0";
                }
                String PensionCompany = request.getParameter("PensionCompany");
                if (PensionCompany == null || PensionCompany.equalsIgnoreCase("")) {
                    PensionCompany = "0";
                }
                String Retirement = request.getParameter("Retirement");
                if (Retirement == null || Retirement.equalsIgnoreCase("")) {
                    Retirement = "0";
                }
                String RetirementCompany = request.getParameter("RetirementCompany");
                if (RetirementCompany == null || RetirementCompany.equalsIgnoreCase("")) {
                    RetirementCompany = "0";
                }
                String Provident = request.getParameter("Provident");
                if (Provident == null || Provident.equalsIgnoreCase("")) {
                    Provident = "0";
                }
                String ProvidentCompany = request.getParameter("ProvidentCompany");
                if (ProvidentCompany == null || ProvidentCompany.equalsIgnoreCase("")) {
                    ProvidentCompany = "0";
                }
                String NumberOfDependants = request.getParameter("NumberOfDependants");
                if (NumberOfDependants == null || NumberOfDependants.equalsIgnoreCase("")) {
                    NumberOfDependants = "0";
                }
                String PersonalContribution = request.getParameter("PersonalContribution");
                if (PersonalContribution == null || PersonalContribution.equalsIgnoreCase("")) {
                    PersonalContribution = "0";
                }
                String CompanyContribution = request.getParameter("CompanyContribution");
                if (CompanyContribution == null || CompanyContribution.equalsIgnoreCase("")) {
                    CompanyContribution = "0";
                }
                String incomeReplacement = request.getParameter("incomeReplacement");
                if (incomeReplacement == null || incomeReplacement.equalsIgnoreCase("")) {
                    incomeReplacement = "0";
                }
                String medSwitch = request.getParameter("medicalAidSwitch");
                if (medSwitch == null || medSwitch.equalsIgnoreCase("")) {
                    medSwitch = "off";
                }

                int hasMedical = 0;
                if (medSwitch.equalsIgnoreCase("on")) {
                    if (!PersonalContribution.equalsIgnoreCase("0") || !CompanyContribution.equalsIgnoreCase("0")) {
                        hasMedical = 1;
                    }
                }

                FileContainer fileContainer = FileContainer.getInstance();
                ResultSet rs = fileContainer.calculateTax(GrossIncome, RetirementFundIncome, NonRetirementFundIncome, AnnualBonus, NoOfPayPeriodsInYear, TaxYearEndDate, EmployeeAge, TravelAllowance, OtherAllowance, Pension, PensionCompany, Retirement, RetirementCompany, Provident, ProvidentCompany, incomeReplacement, hasMedical, NumberOfDependants, PersonalContribution, CompanyContribution);

                String TOTAL_PAYE = "0";
                String TOTAL_PAYE_BONUS = "0";
                String ANNUAL_TAX = "0";
                String ANNUAL_TAX_BONUS = "0";
                String MEDICAL_TAX_CREDIT = "0";

                while (rs.next()) {
                    TOTAL_PAYE = rs.getBigDecimal(1).toString();
                    TOTAL_PAYE_BONUS = rs.getBigDecimal(2).toString();
                    ANNUAL_TAX = rs.getBigDecimal(3).toString();
                    ANNUAL_TAX_BONUS = rs.getBigDecimal(4).toString();
                    MEDICAL_TAX_CREDIT = rs.getBigDecimal(5).toString();
                }

                DecimalFormat df = new DecimalFormat("#0.00");
                double totalTax = Double.parseDouble(TOTAL_PAYE) + Double.parseDouble(TOTAL_PAYE_BONUS);

                rs.close();

                ResultSet rs_presvious = fileContainer.calculateTax(GrossIncome, RetirementFundIncome, NonRetirementFundIncome, AnnualBonus, NoOfPayPeriodsInYear, TaxYearEndDate_Previous, EmployeeAge, TravelAllowance, OtherAllowance, Pension, PensionCompany, Retirement, RetirementCompany, Provident, ProvidentCompany, incomeReplacement, hasMedical, NumberOfDependants, PersonalContribution, CompanyContribution);

                String TOTAL_PAYE_PREVIOUS = "0";
                String TOTAL_PAYE_BONUS_PREVIOUS = "0";
                String ANNUAL_TAX_PREVIOUS = "0";
                String ANNUAL_TAX_BONUS_PREVIOUS = "0";
                String MEDICAL_TAX_CREDIT_PREVIOUS = "0";

                while (rs_presvious.next()) {
                    TOTAL_PAYE_PREVIOUS = rs_presvious.getBigDecimal(1).toString();
                    TOTAL_PAYE_BONUS_PREVIOUS = rs_presvious.getBigDecimal(2).toString();
                    ANNUAL_TAX_PREVIOUS = rs_presvious.getBigDecimal(3).toString();
                    ANNUAL_TAX_BONUS_PREVIOUS = rs_presvious.getBigDecimal(4).toString();
                    MEDICAL_TAX_CREDIT_PREVIOUS = rs_presvious.getBigDecimal(5).toString();
                }

                double totalTaxPrevious = Double.parseDouble(TOTAL_PAYE_PREVIOUS) + Double.parseDouble(TOTAL_PAYE_BONUS_PREVIOUS);

                rs_presvious.close();

            %>

            <h1 class="title">Tax Calculation Result</h1>
            <div class="outer-grouping">
                <h2 class="title-nomargin" style="margin-top: 5px; margin-bottom: 5px;">Earnings and deductions summary</h2>
                <div class="grouping">
                    <h2 class="title-nomargin">Earnings</h2>
                    <hr>
                    <div style="display: inline-block">
                        <table style="margin-left: 15px; width: 335px;">
                            <tr>
                                <td style="width: 230px;">Gross Income:</td>
                                <td style="font-weight: bold;"><%out.write(" R " + GrossIncome.toUpperCase());%></td>
                            </tr>
                        </table>
                    </div>
                    <div style="display: inline-block">
                        <table style="margin-left: 15px; width: 335px;">
                            <tr>
                                <td style="width: 230px;">Retirement Fund Income:</td>
                                <td style="font-weight: bold;"><%out.write(" R " + RetirementFundIncome.toUpperCase());%></td>
                            </tr>
                        </table>
                    </div>
                    <div style="display: inline-block">
                        <table style="margin-left: 15px; width: 335px;">
                            <tr>
                                <td style="width: 230px;">Bonus:</td>
                                <td style="font-weight: bold;"><%out.write(" R " + AnnualBonus.toUpperCase());%></td>
                            </tr>
                        </table>
                    </div>
                    <div style="display: inline-block">
                        <table style="margin-left: 15px; width: 335px;">
                            <tr>
                                <td style="width: 230px;">Non Retirement Fund Income:</td>
                                <td style="font-weight: bold;"><%out.write(" R " + NonRetirementFundIncome.toUpperCase());%></td>
                            </tr>
                        </table>
                    </div>
                    <%
                        if (!TravelAllowance.equalsIgnoreCase("0.0")) {
                    %>
                    <div style="display: inline-block">
                        <table style="margin-left: 15px; width: 335px;">
                            <tr>
                                <td style="width: 230px;">Travel Allowance:</td>
                                <td style="font-weight: bold;"><%out.write(" R " + TravelAllowance.toUpperCase());%></td>
                            </tr>
                        </table>
                    </div>
                    <%
                        }
                        if (!OtherAllowance.equalsIgnoreCase("0")) {
                    %>
                    <div style="display: inline-block">
                        <table style="margin-left: 15px; width: 335px;">
                            <tr>
                                <td style="width: 230px;">Other Taxable Allowances:</td>
                                <td style="font-weight: bold;"><%out.write(" R " + OtherAllowance.toUpperCase());%></td>
                            </tr>
                        </table>
                    </div>
                    <%
                        }
                        if (!PensionCompany.equalsIgnoreCase("0")) {
                    %>
                    <div style="display: inline-block">
                        <table style="margin-left: 15px; width: 335px;">
                            <tr>
                                <td style="width: 230px;">Pension Fund Benefit:</td>
                                <td style="font-weight: bold;"><%out.write(" R " + PensionCompany.toUpperCase());%></td>
                            </tr>
                        </table>
                    </div>
                    <%
                        }
                        if (!ProvidentCompany.equalsIgnoreCase("0")) {
                    %>
                    <div style="display: inline-block">
                        <table style="margin-left: 15px; width: 335px;">
                            <tr>
                                <td style="width: 230px;">Provident Fund Benefit:</td>
                                <td style="font-weight: bold;"><%out.write(" R " + ProvidentCompany.toUpperCase());%></td>
                            </tr>
                        </table>
                    </div>
                    <%
                        }
                        if (!RetirementCompany.equalsIgnoreCase("0")) {
                    %>
                    <div style="display: inline-block">
                        <table style="margin-left: 15px; width: 335px;">
                            <tr>
                                <td style="width: 230px;">Retirement Fund Benefit:</td>
                                <td style="font-weight: bold;"><%out.write(" R " + RetirementCompany.toUpperCase());%></td>
                            </tr>
                        </table>
                    </div>
                    <%
                        }
                        if (!CompanyContribution.equalsIgnoreCase("0")) {
                    %>
                    <div style="display: inline-block">
                        <table style="margin-left: 15px; width: 335px;">
                            <tr>
                                <td style="width: 230px;">Med. Aid Company Contribution:</td>
                                <td style="font-weight: bold;"><%out.write(" R " + CompanyContribution.toUpperCase());%></td>
                            </tr>
                        </table>
                    </div>
                    <%
                        }
                        if (!Pension.equalsIgnoreCase("0") || !Provident.equalsIgnoreCase("0")
                                || !Retirement.equalsIgnoreCase("0") || !incomeReplacement.equalsIgnoreCase("0")
                                || !PersonalContribution.equalsIgnoreCase("0") || !CompanyContribution.equalsIgnoreCase("0")) {
                    %>
                    <h2 class="title-nomargin">Deductions</h2>
                    <hr>
                    <%
                        }
                        if (!Pension.equalsIgnoreCase("0")) {
                    %>
                    <div style="display: inline-block">
                        <table style="margin-left: 15px; width: 335px;">
                            <tr>
                                <td style="width: 230px;">Pension Fund Contribution:</td>
                                <td style="font-weight: bold;"><%out.write(" R " + Pension.toUpperCase());%></td>
                            </tr>
                        </table>
                    </div>
                    <%
                        }
                        if (!Provident.equalsIgnoreCase("0")) {
                    %>
                    <div style="display: inline-block">
                        <table style="margin-left: 15px; width: 335px;">
                            <tr>
                                <td style="width: 230px;">Provident Fund Contribution:</td>
                                <td style="font-weight: bold;"><%out.write(" R " + Provident.toUpperCase());%></td>
                            </tr>
                        </table>
                    </div>
                    <%
                        }
                        if (!Retirement.equalsIgnoreCase("0")) {
                    %>
                    <div style="display: inline-block">
                        <table style="margin-left: 15px; width: 335px;">
                            <tr>
                                <td style="width: 230px;">Retirement Fund Contribution:</td>
                                <td style="font-weight: bold;"><%out.write(" R " + Retirement.toUpperCase());%></td>
                            </tr>
                        </table>
                    </div>
                    <%
                        }
                        if (!incomeReplacement.equalsIgnoreCase("0")) {
                    %>
                    <div style="display: inline-block">
                        <table style="margin-left: 15px; width: 335px;">
                            <tr>
                                <td style="width: 230px;">Income Replacement Policy:</td>
                                <td style="font-weight: bold;"><%out.write(" R " + incomeReplacement.toUpperCase());%></td>
                            </tr>
                        </table>
                    </div>
                    <%
                        }
                        if (!PersonalContribution.equalsIgnoreCase("0")) {
                    %>
                    <div style="display: inline-block">
                        <table style="margin-left: 15px; width: 335px;">
                            <tr>
                                <td style="width: 230px;">Med. Aid Personal Contribution:</td>
                                <td style="font-weight: bold;"><%out.write(" R " + PersonalContribution.toUpperCase());%></td>
                            </tr>
                        </table>
                    </div>
                    <%
                        }
                    %>
                </div>
            </div>
            <h2 class="title-nomargin" style="margin-top: 20px;">Tax RESULT</h2>
            <!---------------------------
            -- Current Year
            --------------------------->            
            <div class="outer-grouping">
                <h2 class="title-nomargin">Current Year 2016/2017</h2>
                <%
                    if (hasMedical == 1) {

                %>
                <div class="grouping">
                    <table>
                        <tr>
                            <td style="width: 180px;">&nbsp;</td>
                            <td style="width: 180px; font-weight: bold;">Applied Tax Credit:</td>
                            <td style="width: 180px;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td><h3 class="title-nomargin">MEDICAL AID</h3></td>
                            <td><%out.write(" R " + MEDICAL_TAX_CREDIT);%></td>
                            <td style="width: 180px;">&nbsp;</td>
                        </tr>
                    </table>
                </div>
                <%
                    }
                %>
                <div class="grouping">
                    <table>
                        <tr>
                            <td style="width: 180px;">&nbsp;</td>
                            <td style="width: 180px; font-weight: bold;">Tax:</td>
                            <td style="width: 180px; font-weight: bold;">Tax on bonus:</td>
                            <td style="width: 180px; font-weight: bold;">Total Tax:</td>
                        </tr>
                        <tr>
                            <td><h3 class="title-nomargin"><%out.write(paymentFrequency.toUpperCase());%></h3></td>
                            <td><%out.write(" R " + TOTAL_PAYE);%></td>
                            <td><%out.write(" R " + TOTAL_PAYE_BONUS);%></td>
                            <td><%out.write(" R " + df.format(totalTax));%></td>
                        </tr>
                        <tr>
                            <td><h3 class="title-nomargin">ANNUAL</h3></td>
                            <td><%out.write(" R " + ANNUAL_TAX);%></td>
                            <td><%
                                if (AnnualBonus.equalsIgnoreCase("0")) {
                                    out.write(" R 0.00");
                                } else {
                                    out.write(" R " + ANNUAL_TAX_BONUS);
                                }
                                %></td>
                        </tr>
                    </table>
                </div>
            </div>
            <!---------------------------
            -- Previous Year
            --------------------------->
            <div class="outer-grouping">
                <div style="display: inline-block;">
                    <p style="margin-left: 80px; margin-bottom: 10px; margin-top: 10px;">Compare with previous tax year?</p>
                </div> 
                <div class="onoffswitch" style="margin-right: 80px; margin-bottom: 10px; margin-top: 3px;">
                    <input type="checkbox" name="previous" class="onoffswitch-checkbox" id="previous">
                    <label class="onoffswitch-label" for="previous">
                        <div class="onoffswitch-inner"></div>
                        <div class="onoffswitch-switch"></div>
                    </label>
                </div>
                <div id="bonus" class="other_text" style="display: none; margin-top: 10px;">
                    <h2 class="title-nomargin">Previous Year 2015/2016</h2>
                    <%
                        if (hasMedical == 1) {

                    %>
                    <div class="grouping">
                        <table>
                            <tr>
                                <td style="width: 180px;">&nbsp;</td>
                                <td style="width: 180px; font-weight: bold;">Applied Tax Credit:</td>
                                <td style="width: 180px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td><h3 class="title-nomargin">MEDICAL AID</h3></td>
                                <td><%out.write(" R " + MEDICAL_TAX_CREDIT_PREVIOUS);%></td>
                                <td style="width: 180px;">&nbsp;</td>
                            </tr>
                        </table>
                    </div>
                    <%
                        }
                    %>
                    <div class="grouping">
                        <table>
                            <tr>
                                <td style="width: 180px;">&nbsp;</td>
                                <td style="width: 180px; font-weight: bold;">Tax:</td>
                                <td style="width: 180px; font-weight: bold;">Tax on bonus:</td>
                                <td style="width: 180px; font-weight: bold;">Total Tax:</td>
                            </tr>
                            <tr>
                                <td><h3 class="title-nomargin"><%out.write(paymentFrequency.toUpperCase());%></h3></td>
                                <td><%out.write(" R " + TOTAL_PAYE_PREVIOUS);%></td>
                                <td><%out.write(" R " + TOTAL_PAYE_BONUS_PREVIOUS);%></td>
                                <td><%out.write(" R " + df.format(totalTaxPrevious));%></td>
                            </tr>
                            <tr>
                                <td><h3 class="title-nomargin">ANNUAL</h3></td>
                                <td><%out.write(" R " + ANNUAL_TAX_PREVIOUS);%></td>
                                <td><%
                                    if (AnnualBonus.equalsIgnoreCase("0")) {
                                        out.write(" R 0.00");
                                    } else {
                                        out.write(" R " + ANNUAL_TAX_BONUS_PREVIOUS);
                                    }
                                    %></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <button  class="gradient-button" style="width: 80px;" onclick="back();">Back</button>
        </div>
    </body>
</html>