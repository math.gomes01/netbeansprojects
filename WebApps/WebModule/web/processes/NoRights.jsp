<%@page contentType="text/html"%>
<%@page import="za.co.ucs.accsys.webmodule.*" %>

<%@include file="html5_top.jsp"%>

<!-- Now we need to verify that there is a LoginSessionObject -->

<% // Page Top

        PageSectionGenerator gen = PageSectionGenerator.getInstance();
        out.write(gen.buildHTMLPageHeader("Access Revoked", "Your system was configured not to show this page."));

%>

<%@include file="html_messages.jsp"%> 
<!-- This HAS to be included if messages were generated
and added to the session attributes for this person.
Calling this script will simply display those messages -->

<%@include file="html_bottom.jsp"%>