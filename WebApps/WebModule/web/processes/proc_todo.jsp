<%@page contentType="text/html"%>

<%@include file="html5_top.jsp"%>

<%@include file="../html/formatDateScript.txt"%>

<%@include file="html_messages.jsp"%> 
<!-- This HAS to be included if messages were generated
and added to the session attributes for this person.
Calling this script will simply display those messages -->

<!-- Now we need to verify that there is a LoginSessionObject -->

<%  // Can I connect to the database?
    // We need to reset the EMPLOYEE to be the same person as the USER once the process has been committed
    if (loginSessionObject == null) {
        response.sendRedirect(response.encodeURL("Login.jsp"));
    } else {
        if (!user.equals(employee)) {
            loginSessionObject.setEmployee(user);

        } else {  // Page Top
            if (loginSessionObject != null) {
                PageSectionGenerator gen = PageSectionGenerator.getInstance();
                out.write(gen.buildHTMLPageHeader("To-Do List", loginSessionObject.getUser().toString()));

                // Any special action?
                String action = request.getParameter("action"); // for which period are we to analyse the balances?
                if (action == null) {
                    action = "";
                }
                String actionValue = request.getParameter("ActionButton");
                if (actionValue == null) {
                    actionValue = "";
                }
                String orderBy = request.getParameter("orderBy");
                String filterByProcess = request.getParameter("cbbProcessFilter");
                String fromDate = request.getParameter("fromDate");
                String toDate = request.getParameter("toDate");
                // If no dates were passed, look at the last 7 days
                if ((fromDate == null) || (toDate == null)) {
                    toDate = DatabaseObject.formatDate(DatabaseObject.addOneYear(new java.util.Date()));

                    fromDate = DatabaseObject.formatDate(DatabaseObject.removeOneYear(new java.util.Date()));
                }
                // Are we to 'Approve' processes?
                int j = 0;
                for (int i = 0; i < 500; i++) {
                    String processHashCode = request.getParameter("selected_" + i);
                    if (processHashCode != null) {
                        if (processHashCode.trim().length() > 0) {
                            WebProcess process = fileContainer.getWebProcess(processHashCode);
                            if (process != null) {
                                if (process.isActive()) {
                                    if (actionValue.equals("Approve")) {
                                        process.acceptProcess("Bulk Approval");
                                    }
                                    if (actionValue.equals("Reject")) {
                                        process.cancelProcess("Bulk Rejection");
                                    }
                                }
                            }
                        }
                    } 
                }

                // Page Detail
                out.write("<center>");
                out.write("<div id=\"container\">");
                out.write("<div class=\"item\">");
                PageProducer_ToDoList producer = new PageProducer_ToDoList(loginSessionObject.getUser(), true, action, orderBy, filterByProcess, fromDate, toDate);
                out.write(producer.generateInformationContent());
                out.write("</div>");
                out.write("</div>");
                out.write("</center>");
            } else {
                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                        "Your session has timed out, please log in again.", false, request.getSession());
                producer.setMessageInSessionObject();
                out.write("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");

            }
        }
    }
%>

<%@include file="html_bottom.jsp"%>