/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.accsys.ess;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Dialogs;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import static za.co.accsys.ess.FXMLDocumentController.contentsChanged;
import javafx.event.EventHandler;
import javafx.stage.WindowEvent;
import javafx.application.Platform;
import za.co.ucs.accsys.webmodule.FileContainer;

/**
 *
 * @author Liam
 */
public class ESSSetup extends Application {

    public static ObservableList names = FXCollections.observableArrayList();
    public static Stage stage;

    @Override
    public void start(final Stage stage) throws Exception {

        FileContainer.getInstance().loadWebProcessDefinitionList();
        Parent root;
        try {
            root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
            Scene scene = new Scene(root);

            stage.setScene(scene);
            stage.initStyle((StageStyle.DECORATED));
            stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent e) {
                    if (contentsChanged) {
                        Dialogs.DialogResponse responseToSave = Dialogs.showWarningDialog(stage,
                                "You have unsaved changes.\nUnsaved changes will be lost!", "Unsaved changes will be lost!", "Close ESSConfig utility?", Dialogs.DialogOptions.OK_CANCEL);
                        if (responseToSave == Dialogs.DialogResponse.CANCEL) {
                            e.consume();
                        }
                        if (responseToSave == Dialogs.DialogResponse.OK) {
                            Platform.exit();
                        }
                    }
                }
            });
            
            stage.setTitle("ESS Configuration");
            stage.show();
            
        } catch (IOException ex) {
            Logger.getLogger(ESSSetup.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            Thread.sleep(3000); // the parameter is in milliseconds
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
        // begin with the interactive portion of the program
        launch(args);
    }
}
