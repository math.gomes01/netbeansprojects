<%-- 
    Document   : onlineQuote
    Created on : 28 Aug 2014, 10:56:41 AM
    Author     : fkleynhans
--%>

<%@page import="za.co.ucs.accsys.webmodule.WebModulePreferences"%>
<%@page import="za.co.ucs.accsys.webmodule.E_Mail"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.io.IOException"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFWorkbook"%>
<%@page import="org.apache.poi.ss.usermodel.Workbook"%>
<%@page import="java.io.FileInputStream"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="/WebModule/CSS/style.css" />
        <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>  
        <title>PeoleWare Cloud Pricing- Quick Quote</title>
        <script type="text/javascript">
            // Goes back
            function back() {
                act = 'Back';
                history.back();
            }
        </script>
    </head>
    <body>
        <div id="container">
            <h1 class="title">PeopleWare Cloud Pricing - Quick quote</h1>
            <form enctype="multipart/form-data" method="GET">
                <%
                    int numberOfEmployees = Integer.parseInt(session.getAttribute("numberOfEmployees").toString());

                    int PWOperators = Integer.parseInt(session.getAttribute("PWOperators").toString());

                    String payrollSwitch = session.getAttribute("payrollSwitch").toString();

                    String HrSwitch = session.getAttribute("HrSwitch").toString();

                    String TnaSwitch = session.getAttribute("TnaSwitch").toString();

                    String EssSwitch = session.getAttribute("EssSwitch").toString();

                    String ExcelSwitch = session.getAttribute("ExcelSwitch").toString();

                    int NoOfCredits = Integer.parseInt(session.getAttribute("NoOfCredits").toString());

                %>
                <div class="outer-grouping">
                    <h2 class="title-nomargin">Contact Information</h2>
                    <%
                        if (request.getParameter("btnSubmit") == null) {
                    %>     
                    <div class="grouping">
                        <p><b>Name:</b><input style="margin-left: 29px;" id="name" name="name" type="text" maxlength="50" required><span style="color: red">*</span></p>
                        <p><b>E-mail:</b><input style="margin-left: 24px;" id="email" name="email" type="email" maxlength="50" required><span style="color: red">*</span></p>
                        <p><b>Tel/Cell:</b><input style="margin-left: 13px;" id="tel" name="tel" type="tel" maxlength="15" required><span style="color: red">*</span></p>
                        <p style="display: inline-block; float: left;"><b>Message:</b></p>
                        <%    StringBuilder message = new StringBuilder();
                            message.append("Dear Sales Consultant\n\nPlease find below a request for a formal quote.\n");
                            message.append("\n\tNumber of employees:\t\t").append(numberOfEmployees);
                            message.append("\n\tNumber of PW Operators:\t\t").append(PWOperators);
                            if (ExcelSwitch.contains("on")) {
                                message.append("\n\twith Excel Extract Licence/s \t");
                            }
                            if (payrollSwitch.contains("on") || HrSwitch.contains("on") || TnaSwitch.contains("on")) {
                                message.append("\n\n\tModules:");
                            }
                            if (payrollSwitch.contains("on")) {
                                message.append("\n\t\tPayroll");
                            }
                            if (HrSwitch.contains("on")) {
                                message.append("\n\t\tHuman Resources");
                            }
                            if (TnaSwitch.contains("on")) {
                                message.append("\n\t\tTime and Attendance");
                            }
                            if (EssSwitch.contains("on")) {
                                message.append("\n\n\tEmployee Self Service with ").append(NoOfCredits).append(" credits.\n");
                            }
                            message.append("\n\nRegards\n*");
                            out.append("<textarea style=\" margin-left: 5px; display: inline-block; max-width: 667px;\" maxlength=\"500\"  rows=\"20\" cols=\"80\" name=\"emailMsgs\" tesxt-align=\"left\" id=\"emailMsgs\">");
                            out.append(message);
                        %>
                        </textarea>
                        <p><b>How did you find out about Accsys:</b><textarea style=" margin-left: 77px; margin-top: 10px; max-width: 667px;" maxlength="500"  rows="5" cols="80" name="howText" tesxt-align="left" id="howText"></textarea></p>
                        <p style="font-size: 10px;">* Your contact details will be added to the message automatically.</p>
                        <p><span style="font-size: 10px; color: red">* These are required fields</span></p>
                    </div>
                </div>
                <%
                    }
                    if (request.getParameter("btnSubmit") != null) {
                        //Build the email message
                        StringBuilder emailMsgs = new StringBuilder();
                        emailMsgs.append(request.getParameter("emailMsgs"));
                        //Delete the *
                        emailMsgs.delete(emailMsgs.indexOf("*"), emailMsgs.length());
                        //Add the contanct details
                        emailMsgs.append(request.getParameter("name"));
                        emailMsgs.append("\n").append(request.getParameter("email"));
                        emailMsgs.append("\n").append(request.getParameter("tel"));
                        
                        emailMsgs.append("\n\nHow did I find out about Accsys:<br>");
                        emailMsgs.append(request.getParameter("howText"));
                        
                        String htmlEmail = emailMsgs.toString();
                        htmlEmail = htmlEmail.replaceAll("\n", "<br>");
                        htmlEmail = htmlEmail.replaceAll("\t", "&emsp;");
                        htmlEmail = htmlEmail.replaceAll("Number of employees:", "Number of employees:&emsp;");

                        String subject = "Cloud Quick Quote for: " + request.getParameter("name");
                        String toAdress = WebModulePreferences.getInstance().getPreference(WebModulePreferences.SMTP_AdminNotificationAddress, "");
                        
                        if (E_Mail.getInstance().send(toAdress, "", subject, htmlEmail)) {
                        %>
                                <div class="grouping">
                                    <p style="text-align: center;"><b>Thank you for contacting us.<br>Your message has been sent to our Sales team.</b></p>
                                </div>
                            </div>
                        <%
                        } else {
                        %>
                                <div class="grouping">
                                    <p style="text-align: center;"><b>Your message FAILED to send.<br>Please go back and try again.</b></p>
                                </div>
                            </div>
                        <%
                        }
                    }
                    if (request.getParameter("btnSubmit") == null) {
                %>                                
                        <input class="gradient-button" type="submit" id="btnSubmit" name="btnSubmit" value="Send to Sales"/>
                <%
                    }
                %>
                <input class="gradient-button" value="Back" onclick="back();" style="width: 40px;">
            </form>
        </div><!-- Container end -->
    </body>
</html>
