<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="org.apache.commons.lang.time.DateUtils"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="application/pdf"%>
<%@page trimDirectiveWhitespaces="true"%>

<%@page import="net.sf.jasperreports.view.JasperViewer"%>
<%@page import="net.sf.jasperreports.engine.*"%>
<%@page import="net.sf.jasperreports.engine.export.*"%>
<%@page import="java.util.*"%>
<%@page import="za.co.ucs.accsys.webmodule.*" %>
<%@page import="za.co.ucs.ess.*" %>
<%@page import="za.co.ucs.lwt.db.*" %>
<%@page import="za.co.ucs.accsys.peopleware.*" %>

<%  // Generate and print report
    LoginSessionObject loginSessionObject = (LoginSessionObject) session.getAttribute("loginsessionobject");

    Employee user = null;

    user = loginSessionObject.getUser();

    List<String> subordinate_list = new ArrayList();

    String reportNb = request.getParameter("reportNum");
    if (reportNb == null) {
        reportNb = "0";
    }

    String fromDate = request.getParameter("fromDate");
    if (fromDate == null) {
        fromDate = DatabaseObject.formatDate(DatabaseObject.addNDays(new java.util.Date(), -30));
    }

    String toDate = request.getParameter("toDate");
    if (toDate == null) {
        toDate = DatabaseObject.getCurrentDate();
    }

    String rsSelection = request.getParameter("rsSelection");
    if (rsSelection == null) {
        rsSelection = "All";
    }

    List<String> processNameList = new ArrayList();
    String processName = request.getParameter("processName");
    if (processName == null) {
        processNameList.add("Leave Request");
    } else {
        processNameList.add(processName);
    }

    List<String> activeYNList = new ArrayList();
    String activeYN = request.getParameter("activeYN");
    if (activeYN == null) {
        activeYNList.add("Y");
    } else {
        activeYNList.add(activeYN);
    }

    String payroll_ID = request.getParameter("payrollID");
    if (payroll_ID == null) {
        payroll_ID = "1";
    }

    String payrollDetail_ID = request.getParameter("payrollDetailID");
    if (payrollDetail_ID == null) {
        payrollDetail_ID = "1";
    }

    String batch_Count = request.getParameter("batchCount");
    if (batch_Count == null) {
        batch_Count = "0";
    }

    String endDate = request.getParameter("endDate");
    if (endDate == null) {
        endDate = DatabaseObject.getCurrentDate();
    }

    String runtype_ID = request.getParameter("runtypeID");
    if (runtype_ID == null) {
        runtype_ID = "1";
    }

    String submissionPeriod = request.getParameter("submissionPeriod");
    if (submissionPeriod == null) {
        submissionPeriod = "2014";
    }

    HashMap<String, Object> map = new HashMap();
    FileContainer fileContainer = FileContainer.getInstance();

    String baseURL = getServletContext().getRealPath("/");
    String subReportPath = baseURL + "/Reports/";

    // <editor-fold defaultstate="collapsed" desc="EMPLOYEE SPECIFIC REPORTS ">
    if (reportNb.compareTo("ESSP1102") == 0) {
        fileContainer.setReportAnalysisDate(user.getCompany().getCompanyID(), user.getEmployeeID(), -2, reportNb);
        //out.write("Payslip**************************************************");

        map.put("REPORT_LOGO", "../images/CompanyLogo.png");
        map.put("LOGON_ID", "-2");
        map.put("REPORT_NAME", "ESSP1102");
        map.put("COMPANY_ID", user.getCompany().getCompanyID());
        map.put("EMPLOYEE_ID", user.getEmployeeID());
        map.put("PAYROLL_ID", payroll_ID);
        map.put("PAYROLLDETAIL_ID", payrollDetail_ID);
        map.put("BATCH_COUNT", batch_Count);
        map.put("RUNTYPE_ID", runtype_ID);
        map.put("SUBREPORT_DIR", subReportPath);

    } else if (reportNb.compareTo("ESSP1102_OLD") == 0) {
        fileContainer.setReportAnalysisDate(user.getCompany().getCompanyID(), user.getEmployeeID(), -2, reportNb);
        //out.write("Payslip**************************************************");

        map.put("REPORT_LOGO", "../images/CompanyLogo.png");
        map.put("LOGON_ID", "-2");
        map.put("REPORT_NAME", "ESSP1102_OLD");
        map.put("COMPANY_ID", user.getCompany().getCompanyID());
        map.put("EMPLOYEE_ID", user.getEmployeeID());
        map.put("PAYROLL_ID", payroll_ID);
        map.put("PAYROLLDETAIL_ID", payrollDetail_ID);
        map.put("PERIOD_END_DATE", endDate);
        map.put("SUBREPORT_DIR", subReportPath);

    } else if (reportNb.compareTo("ESS_IRP5") == 0) {

        map.put("REPORT_LOGO", "../images/SARS.bmp");
        map.put("COMPANY_ID", user.getCompany().getCompanyID());
        map.put("EMPLOYEE_ID", user.getEmployeeID());
        map.put("PAYROLL_ID", user.getPayroll().getPayrollID());
        map.put("SUBREPORT_DIR", subReportPath);
        map.put("TRANSACTION_YEAR", StringUtils.substring(submissionPeriod, 0, 4));
//</editor-fold>
        // <editor-fold defaultstate="collapsed" desc="MULTI-EMPLOYEE REPORTS - Choose correct Employees to add to employee list">
    } else { // Choose the correct employee selections for the selected report

        // Populate subordinate list with employee numbers
        LinkedList<EmployeeSelectionRule> employeeSelectionRules = new LinkedList();
        ReportingStructure repStruct = null;
        HashSet<Employee> employeesInES = new HashSet();
        //subordinate_list.add(user.getEmployeeNumber()); This was decided to not be such a good idea
        if (reportNb.contains("ESSL")) { // Leave reports
            WebProcessDefinition webProcessDefinition = fileContainer.getWebProcessDefinition("za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition");
            repStruct = webProcessDefinition.getReportingStructure();
            employeeSelectionRules = fileContainer.getChildEmployeeSelectionsForReporting(repStruct, user);
        } else if (reportNb.contains("ESSTM")) { // Tna reports
            WebProcessDefinition webProcessDefinition = fileContainer.getWebProcessDefinition("za.co.ucs.accsys.webmodule.WebProcess_DailySignoff");
            repStruct = webProcessDefinition.getReportingStructure();
            employeeSelectionRules = fileContainer.getChildEmployeeSelectionsForReporting(repStruct, user);
            if (reportNb.contains("ESSTM1206")) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
                Date date = sdf.parse(toDate);
                date = DateUtils.addDays(date, 1);
                toDate = sdf.format(date);
            }
        } else {
            try {
                ArrayList<ReportingStructure> reportingStructures = fileContainer.getReportingStructures();
                subordinate_list.clear();
                for (ReportingStructure rs : reportingStructures) {
                    employeeSelectionRules.addAll(fileContainer.getChildEmployeeSelectionsForReporting(rs, user));
                }
            } catch (Exception e) {
                System.out.println("Report error = " + e);
            }
        }
        // </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Add the employees form the employee selection to the list">
        if (!reportNb.contains("ESS_AUDIT")) { // Add the employees form the employee selection to the list
            subordinate_list.clear();
            for (EmployeeSelectionRule es : employeeSelectionRules) {
                if (es.getName().equalsIgnoreCase(rsSelection) || rsSelection.equalsIgnoreCase("All")) {
                    employeesInES = es.getEmployees();
                    // Populate the list that goes to jasper
                    for (Employee emp : employeesInES) {
                        subordinate_list.add(emp.getEmployeeNumber());
                    }
                }
            }
        }
        //</editor-fold>

        //
        // ESS Audit report
        //
        // <editor-fold defaultstate="collapsed" desc="Add ALL employees form the employee selection to the list">
        if (reportNb.contains("ESS_AUDIT")) { // Add the employees form the employee selection to the list
            ArrayList<ReportingStructure> reportingStructures = fileContainer.getReportingStructures();
            LinkedList<EmployeeSelectionRule> employeeSelectionRulesAll = new LinkedList();
            subordinate_list.clear();
            // Get each reporting struncture
            for (ReportingStructure rs : reportingStructures) {
                // Get all the top Nodes
                ArrayList<EmployeeSelection> topNodes = rs.getTopNodes();
                // For every top node get all the childEmployee selections
                for (EmployeeSelection topNode : topNodes) {
                    employeeSelectionRulesAll.addAll(repStruct.getAllChildEmployeeSelections(topNode));
                }
                for (EmployeeSelectionRule es : employeeSelectionRulesAll) {
                    employeesInES = es.getEmployees();
                    for (Employee emp : employeesInES) {
                        subordinate_list.add(emp.getEmployeeNumber());
                    }
                }
            }
        }
            //</editor-fold>

            //
            // Process History report
            //
            // <editor-fold defaultstate="collapsed" desc="Process History report">
            if (reportNb.compareTo("ESSP1190") == 0) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
                Date date = sdf.parse(toDate);
                date = DateUtils.addDays(date, 1);
                toDate = sdf.format(date);
                // Process type selection
                if (processNameList.contains("All")) {
                    processNameList.remove("All");
                    DatabaseObject.setConnectInfo_AccsysJDBC(WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Database, ""));
                    Connection con = null;
                    try {
                        con = DatabaseObject.getNewConnectionFromPool();
                        ResultSet rs = DatabaseObject.openSQL("SELECT DISTINCT PROCESS_NAME FROM ESS_PROCESS with (nolock) ORDER BY PROCESS_NAME", con);
                        while (rs.next()) {
                            processNameList.add(rs.getString(1));
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    } finally {
                        DatabaseObject.releaseConnection(con);
                    }
                }
                if (activeYNList.contains("All")) {
                    activeYNList.remove("All");
                    activeYNList.add("Y");
                    activeYNList.add("N");
                }
            }
            //</editor-fold>

            map.put("REPORT_LOGO", "../images/CompanyLogo.png");
            map.put("COMPANY_ID", user.getCompany().getCompanyID());
            map.put("EMPLOYEE_ID", user.getEmployeeID());
            map.put("LOGON_ID", "-2");
            map.put("REPORT_NAME", reportNb);
            map.put("FROM_DATE", fromDate);
            map.put("TO_DATE", toDate);
            map.put("PROCESS_NAME", processNameList);
            map.put("ACTIVE_YN", activeYNList);
            map.put("EMPLOYEE_LIST", subordinate_list);
            map.put("SUBREPORT_DIR", subReportPath);

            //System.out.println("Company_ID = " + user.getCompany().getCompanyID());
            //System.out.println("Employee_ID = " + user.getEmployeeID());
            //System.out.println("ReportNB = " + reportNb);
            //System.out.println("FromDate = " + fromDate);
            //System.out.println("ToDate = " + toDate);
            //System.out.println("SubordinateList = " + subordinate_list);
            //System.out.println("processNameList = " + processNameList);
            //System.out.println("activeYNList = " + activeYNList);
        }

        String reportPath = baseURL + "/Reports/" + reportNb + ".jasper";
        //System.out.println("Jasper path:" + reportPath);

        JasperPrint print = JasperFillManager.fillReport(
                reportPath,
                map,
                za.co.ucs.lwt.db.DatabaseObject.getInstance().getNewConnectionFromPool());

        JasperExportManager.exportReportToPdfStream(print, response.getOutputStream());
%>
