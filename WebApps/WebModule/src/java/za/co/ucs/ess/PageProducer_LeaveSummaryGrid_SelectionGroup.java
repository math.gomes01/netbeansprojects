package za.co.ucs.ess;

import java.util.*;
import za.co.ucs.accsys.peopleware.*;
import za.co.ucs.accsys.webmodule.*;
import za.co.ucs.lwt.db.DatabaseObject;

/**
 * <p>
 * PageProducer_LeaveSummaryGrid_SelectionGroup inherits from PageProducer and
 * will display a summary of leave taken for all employees that reports to a
 * given person in the leave request reporting structures. The purpose of this
 * display is to empower the manager to see a complete breakdown of which
 * employees that reports to him/her are requesting for leave in and around the
 * same time-frame.
 * </p>
 */
public class PageProducer_LeaveSummaryGrid_SelectionGroup extends PageProducer {

    /**
     * Constructor
     *
     * @param unsortedEmployees
     * @param forwardingURL
     * @param referenceDate The date for which the calendar should be set up
     */
    public PageProducer_LeaveSummaryGrid_SelectionGroup(LinkedList unsortedEmployees, java.util.Date referenceDate, String forwardingURL, String hashValue) {

        this.forwardingURL = forwardingURL;

        // The original group to which the logger of the request belonged to
        //originalSelection = employeeSelection;
        // The Start_Date of the requested leave
        this.referenceDate = referenceDate;
        if (this.referenceDate == null) {
            this.referenceDate = new java.util.Date();
        }

        // Place all the employees into a sorted map.
        for (Object unsortedEmployee : unsortedEmployees) {
            String empString = (String) unsortedEmployee;
            Employee employee = new Employee(new Company(new Integer(empString.substring(0, 5))), new Integer(empString.substring(5)));
            String key = employee.getSurname() + employee.getEmployeeNumber();
            employees.put(key, employee);
        }
        
        this.hashValue = hashValue;
    }

    /**
     * A LeaveInfo Page Producer
     *
     * @return
     */
    @Override
    public String generateInformationContent() {

        // No use building info if there are no employees that has requested leave
        // during the same period.
        if (this.employees.size() == 0) {
            return "";
        }

        StringBuilder resultPage = new StringBuilder();

        // Array that will store the last 10 weeks (names)
        java.util.Date[] daysInMonth = DatabaseObject.getDaysOfMonth(this.referenceDate);

        // Table Header
        if (this.employees.size() > 1) {
            resultPage.append("<CENTER><h3 class=\"shaddow\">Leave Taken by <span style=\"color:#f02229\">selected employees</span></h3>");
        } else {
            Employee emp = (Employee) employees.get(employees.firstKey());
            resultPage.append("<CENTER><h3 class=\"shaddow\">Leave Taken by <span style=\"color:#f02229\">").append(emp.toString()).append("</span></h3>");
        }
        resultPage.append("<div><br></div><div class=\"outer-grouping\" style=\"display: inline-block;\"><table style=\"min-width: 800px;\">");
        // 1st Row: 'Previous Month', 'Selected Month Name', 'Next Month'
        resultPage.append("<tr><td><table style=\"width: 100%;\">");

        resultPage.append("<tr>");

        java.util.Date referredDate;
        String referredURL;

        // **** PREVIOUS MONTH ****
        if (this.forwardingURL != null) { // No use creating a forwarding session if we do not know where it came from
            referredDate = DatabaseObject.removeOneMonth(this.referenceDate);
            referredURL = forwardingURL + "?referenceDate=" + DatabaseObject.formatDate(referredDate) + "&hashValue=" + this.hashValue;
            //System.out.println("newURL:" + referredURL);
            resultPage.append("<td width=\"10%\" style=\"text-align: left;\"><a href=\"").append(referredURL).append("\"><img  style=\"border: 0px\" alt=\"Previous Month\" src=\"").append(WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Web, "")).append("images/mvLeft.png\"></a></td>");
        }

        // **** CURRENTLY SELECTED MONTH DISPLAYED ****
        resultPage.append("<td width=\"80%\" style=\"text-align: center;\" class=\"standardHeading_NotBold\"><b>").append(DatabaseObject.formatDateToMonthYear(referenceDate)).append("</b></td>");

        // **** NEXT MONTH ****
        if (this.forwardingURL != null) {
            referredDate = DatabaseObject.addOneMonth(this.referenceDate);
            referredURL = forwardingURL + "?referenceDate=" + DatabaseObject.formatDate(referredDate) + "&hashValue=" + this.hashValue;
            //System.out.println("newURL:" + referredURL);
            resultPage.append("<td width=\"10%\" style=\"text-align: right;\"><a href=\"").append(referredURL).append("\"><img style=\"border: 0px\" alt=\"Next Month\" src=\"").append(WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Web, "")).append("images/mvRight.png\"></a></td>");
        }

        resultPage.append("</tr>");
        resultPage.append("</table></td></tr>");

        if (didGroupTakeLeaveBetween(this.employees, daysInMonth[0], daysInMonth[daysInMonth.length - 1])) {
            // 2nd Row: Days of month
            resultPage.append("<tr><td><div class=\"grouping\" style=\"display: inline-block;\"><table style=\"min-width: 780px;\">");
            resultPage.append("  <tr><td></td>");
            for (Date daysInMonth1 : daysInMonth) {
                GregorianCalendar calendar = new GregorianCalendar();
                calendar.setTime(daysInMonth1);
                resultPage.append("    <td width=\"2%\" class=\"smallFont\">").append(calendar.get(Calendar.DAY_OF_MONTH)).append("</td>");
            }
            resultPage.append("  </tr>");

            // Remaining rows, one per leave type, per employee
            Iterator iter = this.employees.values().iterator();
            while (iter.hasNext()) {
                Employee employee = (Employee) iter.next();

                LeaveInfo leaveInfo = za.co.ucs.accsys.webmodule.FileContainer.getInstance().getLeaveInfoFromContainer(employee);

                // We are only interested in those ho have taken leave in this period
                if ((leaveInfo != null) && (leaveInfo.didTakeLeaveBetween(daysInMonth[0], daysInMonth[daysInMonth.length - 1]))) {

                    // Get the holiday Info for this employee
                    HolidayInfo holidayInfo = za.co.ucs.accsys.webmodule.FileContainer.getInstance().getHolidayInfoFromContainer(employee);

                    // Message to console
                    //System.out.println(employee.toString() + " took leave between " + daysInMonth[0] + " and " + daysInMonth[daysInMonth.length - 1]);
                    // Add employee to left of table
                    resultPage.append("  <tr><td><small>").append(employee.toString()).append("</small></td>");

                    // Step through all the days of this period, listing the appropriate leave
                    for (int idxOfDayOfMonth = 0; idxOfDayOfMonth <= daysInMonth.length - 1; idxOfDayOfMonth++) {
                        resultPage.append(getDayString(daysInMonth[idxOfDayOfMonth], employee, leaveInfo, holidayInfo));
                    }
                    resultPage.append("  </tr>");
                }
            }

            // Table Footer
            resultPage.append("</table></div></td></tr>");
        } else if (this.employees.size() > 1) {
            resultPage.append("<tr><td style=\"text-align: center;\"> No leave was taken by the selected employees. </td></tr>");
        } else {
            Employee emp = (Employee) employees.get(employees.firstKey());
            resultPage.append("<tr><td style=\"text-align: center;\"> No leave was taken by ").append(emp.toString()).append(". </td></tr>");
        }
        resultPage.append("<tr><td><input class=\"gradient-button\" value=\"Back\" onclick=\"location.href='../processes/employeelist_prev.jsp?webProcessClassName=za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition&MultiEmployee=true&leaveView=History'\" style=\"width: 40px;\"></td></tr>");
        resultPage.append("</table></div></CENTER>");

        return resultPage.toString();
    }

    private String getBgColor(String sLeaveTypeID) {
        char leaveTypeID = sLeaveTypeID.charAt(0);

        switch (leaveTypeID) {
            // Annual
            case '1':
                return "#33E851";
            // Study
            case '2':
                return "#93A3FF";
            // Sick
            case '3':
                return "#FF6B6B";
            // Maternity
            case '4':
                return "#FF89FD";
            // Family
            case '5':
                return "#9729F2";
            // Special
            case '6':
                return "#D8A149";
            // Unpaid
            case '7':
                return "#B5BFC9";
            // Long Service
            default:
                return "#ffff33";
        }
    }

    // Returns the HTML entry for a single cell for an employee on a given day
    private String getDayString(java.util.Date aDay, Employee employee, LeaveInfo leaveInfo, HolidayInfo holidayInfo) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(aDay);

        // Get the leave taken / holiday info on that day
        LeaveHistoryEntry leaveTaken = leaveInfo.didTakeLeaveOn(aDay);
        HolidayDetail holidayDetail = holidayInfo.didHaveHolidayOn(aDay);

        // If no leave was taken but this was a public holiday, indicate it as such
        if ((leaveTaken == null) && (holidayDetail != null)) {
            StringBuilder leaveDetail = new StringBuilder();
            StringBuilder result = new StringBuilder();
            leaveDetail.append("<b>Holiday:</b>").append(holidayDetail.getHolidayName());
            leaveDetail.append("<br><b>Date:</b>").append(DatabaseObject.formatDate(holidayDetail.getDate()));

            result.append("<td bgcolor=\"#8888dd\">");
            result.append("<a class=\"hintanchor\" onMouseover=\"showhint('").append(leaveDetail.toString()).append("', this, event, '150px')\">");
            result.append("<small>H</small></a></td>");

            return result.toString();
        }
        // If leave was taken AND this was a public holiday, indicate it as such
        if ((leaveTaken != null) && (holidayDetail != null)) {
            StringBuilder leaveDetail = new StringBuilder();
            StringBuilder result = new StringBuilder();
            leaveDetail.append("<b>Leave Type:</b>").append(leaveInfo.getLeaveTypeName(leaveTaken.getLeaveTypeID()));
            leaveDetail.append("<br><b>Reason:</b>").append(leaveTaken.getReason());
            leaveDetail.append("<br><b>From:</b>").append(leaveTaken.getFromDate());
            leaveDetail.append("<br><b>To:</b>").append(leaveTaken.getToDate());
            leaveDetail.append("<br><b>Holiday:</b>").append(holidayDetail.getHolidayName());
            leaveDetail.append("<br><b>Date:</b>").append(DatabaseObject.formatDate(holidayDetail.getDate()));

            result.append("<td bgcolor=\"").append(getBgColor(leaveTaken.getLeaveTypeID())).append("\">");
            result.append("<a class=\"hintanchor\" onMouseover=\"showhint('").append(leaveDetail.toString()).append("', this, event, '150px')\">");
            result.append("<small>").append(leaveInfo.getLeaveTypeName(leaveTaken.getLeaveTypeID()).charAt(0)).append("H</small></a></td>");

            return result.toString();
        }
        // If leave was taken and this is not a public holiday
        if ((leaveTaken != null) && (holidayDetail == null)) {
            StringBuilder leaveDetail = new StringBuilder();
            StringBuilder result = new StringBuilder();
            leaveDetail.append("<b>Leave Type:</b>").append(leaveInfo.getLeaveTypeName(leaveTaken.getLeaveTypeID()));
            leaveDetail.append("<br><b>Reason:</b>").append(leaveTaken.getReason());
            leaveDetail.append("<br><b>From:</b>").append(leaveTaken.getFromDate());
            leaveDetail.append("<br><b>To:</b>").append(leaveTaken.getToDate());

            result.append("<td bgcolor=\"").append(getBgColor(leaveTaken.getLeaveTypeID())).append("\">");
            result.append("<a class=\"hintanchor\" onMouseover=\"showhint('").append(leaveDetail.toString()).append("', this, event, '150px')\">");
            result.append("<small>").append(leaveInfo.getLeaveTypeName(leaveTaken.getLeaveTypeID()).charAt(0)).append("</small></a></td>");

            return result.toString();
        }
        // If no leave was taken and this is not a public holiday and it falls on a weekend
        if ((leaveTaken == null) && (holidayDetail == null)
                && ((calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY)
                || (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY))) {
            StringBuilder result = new StringBuilder();
            result.append("<td bgcolor=\"#cfcfcf\"></td>");
            return result.toString();
        }
        // If no leave was taken and this is not a public holiday and not a weekend
        if ((leaveTaken == null) && (holidayDetail == null)
                && ((calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY)
                || (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY))) {
            StringBuilder result = new StringBuilder();
            result.append("<td bgcolor=\"#eeeeee\"></td>");
            return result.toString();
        }
        return ("");
    }

// Checks to see if any-one of the employees in EmployeeSelection took leave during this month
    private boolean didGroupTakeLeaveBetween(TreeMap employees, java.util.Date fromDate, java.util.Date toDate) {
        Iterator iter = employees.values().iterator();
        while (iter.hasNext()) {
            Employee employee = (Employee) iter.next();
            LeaveInfo leaveInfo = za.co.ucs.accsys.webmodule.FileContainer.getInstance().getLeaveInfoFromContainer(employee);
            if (leaveInfo != null) {
                if (leaveInfo.didTakeLeaveBetween(fromDate, toDate)) {
                    return true;
                }
            }
        }
        return false;
    }
// The employees that reports to the given manager
    private final TreeMap employees = new TreeMap();
    private java.util.Date referenceDate;
    private final String forwardingURL;
    private final String hashValue;
} // end LeavePageProducer

