package za.co.ucs.ess;

import java.text.DecimalFormat;
import za.co.ucs.accsys.peopleware.Employee;
import za.co.ucs.accsys.peopleware.LeaveInfo;
import za.co.ucs.accsys.webmodule.FileContainer;
import za.co.ucs.accsys.webmodule.WebModulePreferences;
import za.co.ucs.accsys.webmodule.WebProcessDefinition;

/**
 * <p>
 * PageProducer_LeaveRequest inherits from PageProducer and will display the
 * detail of a Leave Request in HTML </p>
 */
public class PageProducer_LeaveRequest extends PageProducer {

    /**
     * Constructor
     *
     * @param leaveInfo
     * @param employee
     * @param user
     * @param selectedLeaveTypeID
     * @param leaveUnit
     * @param webProcessClassName
     */
    public PageProducer_LeaveRequest(LeaveInfo leaveInfo, Employee employee, Employee user, String selectedLeaveTypeID, String leaveUnit, String webProcessClassName) {
        this.leaveInfo = leaveInfo;
        this.employee = employee;
        this.user = user;
        if (selectedLeaveTypeID == null) {
            selectedLeaveTypeID = "0";
        }
        this.selectedLeaveTypeID = new Integer(selectedLeaveTypeID);
        if (leaveUnit == null) {
            leaveUnit = "0";
        }
        this.leaveUnit = new Integer(leaveUnit);
        this.webProcessClassName = webProcessClassName;
    }

    /**
     * A LeaveInfo Page Producer
     *
     * @return
     */
    @Override
    public String generateInformationContent() {

        StringBuilder resultPage = new StringBuilder();

        String baseURL = WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Web, "");
        String formLink = baseURL + "Controller.jsp";
        String leaveTypeSelectionLink = baseURL + "processes/leave_req.jsp";

        // SELECT LEAVE TYPE AND UNIT
        // If selectedLeaveType = null, default to first
        {
            resultPage.append("<div>");
            //resultPage.append("<div class=\"item\">");
            resultPage.append("<div class=\"grouping\" style=\"width: 320px; display: inline-block; text-align: left;\">");
            resultPage.append("<h3 class=\"shaddow\" style=\"margin-left: 5px; margin-right: 5px;\">Please select your Leave type and unit</h3>");
            resultPage.append("<form name=\"req_leave_type\" id=\"req_leave_type\" METHOD=POST action=\"").append(leaveTypeSelectionLink).append("\"> ");
            resultPage.append("<table class=\"center\">");
            // Start with the selection of leave types.  If the user already selected a leave type,
            // we'll populate the list of reasons (if required)
            {
                resultPage.append("<tr>");
                resultPage.append("<td>Select Leave Type</td>");
                resultPage.append("<td>");
                resultPage.append("<select name=\"cbbLeaveTypeID\" onchange=\"this.form.submit()\">");
                //Add deafault selection
                //
                resultPage.append("<option VALUE=\"0\" selected>--None--</option>");
                // Add Leave Types
                //
                for (int row = 0; row < leaveInfo.getNumberOfLeaveTypes(); row++) {
                    String leaveType = leaveInfo.getLeaveType(row);
                    int leaveTypeID = leaveInfo.getLeaveTypeID(row);
                    if (!WebModulePreferences.getInstance().hideLeaveRequestsForLeaveType(leaveType)) {
                        String leaveHeading = leaveInfo.getLeaveType(row);
                        if (WebModulePreferences.getInstance().useLeaveProfileDescription()) {
                            leaveHeading = leaveInfo.getLeaveCaption(row);
                        }

                        String leaveTypeWebProcessClassName = "za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition" + leaveInfo.getLeaveProfileID(leaveTypeID) + "_" + leaveType + "_" + leaveInfo.getProfileName(row);
                        WebProcessDefinition webProcessDefinition = FileContainer.getInstance().getWebProcessDefinition(leaveTypeWebProcessClassName);
                        // If the specific leave structure is not set up use the default
                        if (webProcessDefinition == null || webProcessDefinition.getReportingStructure() == null) {
                            webProcessDefinition = FileContainer.getInstance().getWebProcessDefinition("za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition");
                        }
                        if (!webProcessDefinition.getCannotInstantiateSelf() || (!user.equals(employee))) {
                            // No selected Leave Type
                            if (this.selectedLeaveTypeID == 0) {
                                resultPage.append("<option VALUE=\"").append(leaveTypeID).append("\" >").append(leaveHeading).append("</option>");
                            }
                            // Do have a selected Leave Type
                            if (this.selectedLeaveTypeID != 0) {
                                if (this.selectedLeaveTypeID == leaveTypeID) {
                                    resultPage.append("<option VALUE=\"").append(leaveTypeID).append("\" selected>").append(leaveHeading).append("</option>");
                                } else {
                                    resultPage.append("<option VALUE=\"").append(leaveTypeID).append("\" >").append(leaveHeading).append("</option>");
                                }
                            }
                        }
                    }
                }
                if (!user.equals(employee)) {
                    resultPage.append("<input type=\"hidden\" name=\"forEmployee\" value=\"").append(employee.longHashCode()).append("\">");
                    resultPage.append("<input type=\"hidden\" name=\"webProcessClassName\" value=\"").append(webProcessClassName).append("\">");
                    //System.out.println("******************************");
                    //System.out.println("Employee hash code:");
                    //System.out.println(employee.longHashCode());

                }
                resultPage.append("</select>");
                resultPage.append("</td>");
                resultPage.append("</tr>");
                //
                // Use hours or days?
                //
                resultPage.append("<tr>");
                resultPage.append("<td>Select Leave Unit:</td><td>");
                resultPage.append("<select type=\"input\" name=\"cbbLeaveUnit\" id=\"cbbLeaveUnit\" onchange=\"this.form.submit()\">");
                // Add Leave Unit Selections
                if (this.leaveUnit == 0) {
                    resultPage.append("<option VALUE=\"0\" selected>Days</option>");
                    resultPage.append("<option VALUE=\"1\">Hours</option>");
                } else if (this.leaveUnit == 1) {
                    resultPage.append("<option VALUE=\"0\">Days</option>");
                    resultPage.append("<option VALUE=\"1\" selected>Hours</option>");
                }
                resultPage.append("</select>");
                resultPage.append("</td></tr>");
            }
            resultPage.append("</table>");

            resultPage.append("</form>");
            resultPage.append("</div>");

            if (this.selectedLeaveTypeID != 0) {

                resultPage.append("<div class=\"grouping\" style=\"display: inline-block; text-align: left;\">");
                resultPage.append("<h3 class=\"shaddow\" style=\"margin-left: 5px; margin-right: 5px;\">Please complete your Leave request</h3>");
                if (this.selectedLeaveTypeID == 3) {// Only do file upload for Sick leave
                    resultPage.append("<form name=\"req_leave\" METHOD=POST action=\"").append(formLink).append("\" enctype=\"MULTIPART/FORM-DATA\">");
                } else {
                    resultPage.append("<form name=\"req_leave\" METHOD=POST action=\"").append(formLink).append("\">");
                }

                // Next, we provide the list of possible reasons
                // IF the leave is of type Family, Special, Unpaid, Sick
                {
                    resultPage.append("<table class=\"center_narrow\" >");
                    resultPage.append("<tr style=\"align: center;\">");
                    String[][] leaveReasons = leaveInfo.getLeaveReasons(this.selectedLeaveTypeID);
                    //1. *** No selected Leave Type, OR no leave reasons are available: Provide optional text box ***
                    if ((this.selectedLeaveTypeID == 0) || (leaveReasons.length == 0)) {
                        resultPage.append("<td>Reason</td>");
                        resultPage.append("<td><textarea maxlength=500 name=\"reason\" cols=\"35\" rows=\"3\">");
                        resultPage.append("</textarea>");
                        resultPage.append("</td>");
                    }
                    //2. *** No selected Leave Type, OR Annual Leave.  Provide optional text box ***
                    if (leaveReasons.length > 0) {
                        resultPage.append("<td width=\"20%\">Reason</td>");
                        resultPage.append("<td width=\"60%\">");
                        resultPage.append("<select name=\"incidentID\" >");
                        // Add Reasons
                        for (int row = 0; row < leaveReasons.length; row++) {
                            if (row == 0) {
                                resultPage.append("<option value=\"").append(leaveReasons[row][0]).append("\" selected>").append(leaveReasons[row][1]).append("</option>");
                            } else {
                                resultPage.append("<option value=\"").append(leaveReasons[row][0]).append("\" >").append(leaveReasons[row][1]).append("</option>");
                            }
                        }
                        resultPage.append("</select>");
                        resultPage.append("</td>");
                        if (this.selectedLeaveTypeID == 5 || this.selectedLeaveTypeID == 6) {
                            resultPage.append("</tr><tr><td width=\"20%\">Additional Detail</td>");
                            resultPage.append("<td width=\"60%\"><textarea maxlength=500 name=\"additionalDetail\" cols=\"35\" rows=\"3\">");
                            resultPage.append("</textarea>");
                            resultPage.append("</td>");
                        }
                    }

                    resultPage.append("</tr>");
                    //////////////////////////////////////////////////////////////////////
                    //  Sick note attachment
                    //////////////////////////////////////////////////////////////////////
                    if (this.selectedLeaveTypeID == 3) {
                        resultPage.append("<tr><td colspan=\"2\">");
                        resultPage.append("<INPUT style=\"margin-top: 5px\" TYPE=\"file\" id=\"files\" NAME=\"files\">");
                        resultPage.append("</td></tr>");
                    }
                    resultPage.append("</table>");

                }
                // Horizontal Ruler
                resultPage.append("<hr class=\"center\">");

                // SELECT FROM DATE & TO DATE
                // Detail of leave request
                resultPage.append("<!-- Horizontal Ruler -->");
                resultPage.append("<table class=\"center_narrow\" >");

                if (this.leaveUnit == 0) {
                    // Can this Employee/LeaveType take half days?
                    if (leaveInfo.canTakeHalfDayLeave(selectedLeaveTypeID)) {
                        resultPage.append("<tr>");
                        resultPage.append("<td width=\"100%\"><input type=\"checkbox\" name=\"cbHalfDay\" value=\"HalfDay\">");
                        resultPage.append("Half Day  (<i><b>To Date</b> not required</i>)<br>");
                        resultPage.append("</td>");
                        resultPage.append("</tr>");
                    }

                    resultPage.append("<tr><td>");
                    //
                    // From Date and To Date Picker (START)
                    //
                    resultPage.append("<table style=\"width: 435px;\"><tr>");
                    // From Date
                    //
                    resultPage.append("<td>From</td>");

                    resultPage.append("<td><input type=\"text\" name=\"fromDate\" size=10 maxlength=10");
                    resultPage.append(" onFocus=\"javascript:vDateType='2'\"");
                    resultPage.append(" onKeyUp=\"DateFormat(this,this.value,event,false,'2')\" ");
                    //resultPage.append(" onBlur=\"DateFormat(this,this.value,event,false,'2')\" ");
                    resultPage.append(" placeholder=\"yyyy/mm/dd\">");

                    resultPage.append(" <a class=\"hintanchor\" onMouseover=\"showhint('Select the From date.', this, event, '130px')\" href=\"javascript:showCal('Calendar1')\"><img style=\"border: 0px solid ; \"  align=\"top\" alt=\"From Date\" src=\"../images/calendar.jpg\"></a></td>");

                    // Separator 
                    //
                    resultPage.append("<td width=\"20px;\"></td>");
                    // To Date
                    //
                    resultPage.append("<td align=\"right\" >To</td>");

                    resultPage.append("<td><input type=\"text\" name=\"toDate\" size=10 maxlength=10");
                    resultPage.append(" onFocus=\"javascript:vDateType='2'\"");
                    resultPage.append(" onKeyUp=\"DateFormat(this,this.value,event,false,'2')\" ");
                    //resultPage.append(" onBlur=\"DateFormat(this,this.value,event,false,'2')\" ");
                    resultPage.append(" placeholder=\"yyyy/mm/dd\">");

                    resultPage.append(" <a class=\"hintanchor\" onMouseover=\"showhint('Select the To date.', this, event, '130px')\" href=\"javascript:showCal('Calendar2')\"><img style=\"border: 0px solid ; \" align=\"top\"  alt=\"To Date\" src=\"../images/calendar.jpg\"></a>");

                    resultPage.append("</td></tr></table>");
                    //
                    // From Date and To Date Picker (END)
                    //
                } else {
                    resultPage.append("<tr><td>");
                    //
                    // From Date and To Date Picker (START)
                    //
                    resultPage.append("<table style=\"width: 435px;\"><tr>");
                    //
                    // From Date
                    //
                    resultPage.append("<td>Date</td>");

                    resultPage.append("<td><input type=\"text\" name=\"fromDate\" size=10 maxlength=10");
                    resultPage.append(" onFocus=\"javascript:vDateType='2'\"");
                    resultPage.append(" onKeyUp=\"DateFormat(this,this.value,event,false,'2')\" ");
                    //resultPage.append(" onBlur=\"DateFormat(this,this.value,event,false,'2')\" ");
                    resultPage.append(" placeholder=\"yyyy/mm/dd\">");

                    resultPage.append(" <a class=\"hintanchor\" onMouseover=\"showhint('Select the From date.', this, event, '130px')\" href=\"javascript:showCal('Calendar1')\"><img style=\"border: 0px solid ; \"  align=\"top\" alt=\"From Date\" src=\"../images/calendar.jpg\"></a></td>");
                    //
                    // Separator 
                    //
                    resultPage.append("<td width=\"20px;\"></td>");
                    //
                    // Duration
                    //
                    double hoursPerDay = employee.getHoursPerDay();
                    double fractionalPart = hoursPerDay % 1;
                    double integralPart = hoursPerDay - fractionalPart;
                    resultPage.append("<td>Hours&nbsp;</td><td>");
                    resultPage.append("<select name=\"cbbHours\" id=\"cbbHours\" style=\"width:45px;\">");
                    String value = "";
                    DecimalFormat df = new DecimalFormat("00");
                    for (int i = 0; i <= integralPart; i++) {
                        value = df.format(i);
                        resultPage.append("<option value=\"").append(value).append("\">").append(value).append("</option>");
                    }

                    resultPage.append("</select>&nbsp;&nbsp;Minutes&nbsp;");
                    resultPage.append("<select name=\"cbbMinutes\" id=\"cbbMinutes\" style=\"width:45px;\">");
                    for (int i = 0; i < 60; i++) {
                        value = df.format(i);
                        resultPage.append("<option value=\"").append(value).append("\">").append(value).append("</option>");
                    }
                    resultPage.append("</select>");
                    resultPage.append("</td></tr></table>");
                    //
                    // From Date and To Date Picker (END)
                    //
                }
                resultPage.append("</td></tr></table>");

                ////////////////////////////////////////////////////////////////
//                resultPage.append("<script>");
//                resultPage.append("function setValue(id){"
//                        + "var select = document.getElementById(id);\n"
//                        + "select.onchange = function() {\n"
//                        + "    var selIndex = select.selectedIndex;}");
//                resultPage.append("</td></tr></table>");
//                resultPage.append("</td></tr></table>");
//                resultPage.append("</td></tr></table>");
//                resultPage.append("</td></tr></table>");
//                resultPage.append("</script>");
                ////////////////////////////////////////////////////////////////
                // SUBMIT
                {
                    resultPage.append("<!-- Horizontal Ruler -->");
                    resultPage.append("<hr class=\"center\">");
                    resultPage.append("<input type=\"hidden\" name=\"action\" value=\"req_leave\">");
                    resultPage.append("<input type=\"hidden\" name=\"cbbLeaveTypeID\" value=\"").append(selectedLeaveTypeID).append("\">");
                    resultPage.append("<input type=\"hidden\" name=\"cbbLeaveUnit\" value=\"").append(leaveUnit).append("\">");
                    resultPage.append("<input type=\"hidden\" name=\"longHash\" value=\"").append(employee.longHashCode()).append("\">");
                    resultPage.append("<input type=\"hidden\" name=\"webProcessClassName\" value=\"").append(webProcessClassName).append("\">");
                    resultPage.append("<input class=\"gradient-button\" style=\"margin-right: 10px;\" type=\"submit\" value=\"Submit Leave Request\">");
                }

                resultPage.append("</form>");

            } //   if (this.selectedLeaveType != null)
            resultPage.append("</div></div>");
        }

        return resultPage.toString();
    }
    private final LeaveInfo leaveInfo;
    private final int selectedLeaveTypeID;
    private final int leaveUnit;
    private final Employee employee;
    private final Employee user;
    private final String webProcessClassName;
} // end LeavePageProducer

