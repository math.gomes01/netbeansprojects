package za.co.ucs.ess;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import javax.swing.text.DateFormatter;
import za.co.ucs.accsys.peopleware.Employee;
import za.co.ucs.accsys.webmodule.FileContainer;
import za.co.ucs.accsys.webmodule.WebModulePreferences;
import za.co.ucs.accsys.webmodule.WebProcess;
import za.co.ucs.accsys.webmodule.WebProcess_DailySignoff;
import za.co.ucs.lwt.db.DatabaseObject;

/**
 * <p>
 * PageProducer_TnADailySignoff inherits from PageProducer and allows for the
 * approval/rejection of calculated hours </p>
 */
public class PageProducer_TnADailySignoff extends PageProducer {

    /**
     * Creates an instance of PageProducer_TnADailySignoff for a selection of
     * employees
     *
     * @param owner
     * @param sFromDate
     * @param sToDate
     * @param sessionObject - we can retrieve the list of selected employees
     * from the "selectedEmployeeHashCodes" session object
     */
    public PageProducer_TnADailySignoff(Employee owner, String sFromDate, String sToDate, HttpSession sessionObject) {
        this.owner = owner;
        this.sessionObject = sessionObject;
        this.canAutoriseHours = true;
        this.selectedEmployees = new LinkedList<>();
        this.authMultipleEmployees = true;
        //this.selectionID = selectionID;

        // 
        // Multiple Employees for Authorisation
        //
        if (sessionObject.getAttribute("selectedEmployeeHashCodes") != null) {
            LinkedList selectedEmployeeLongHashCodes = (java.util.LinkedList<String>) sessionObject.getAttribute("selectedEmployeeHashCodes");
            LinkedList fcEmployees = za.co.ucs.accsys.webmodule.FileContainer.getEmployees();

            // <editor-fold defaultstate="collapsed" desc="MULTI-EMPLOYEE SELECTION">
            // Step through all the employees, and thos who have this LongHashCode should be added to our internal list
            Iterator iter = selectedEmployeeLongHashCodes.iterator();
            while (iter.hasNext()) {
                String hashCode = (String) iter.next();

                for (int i = 0; i < fcEmployees.size(); i++) {
                    Employee fcEmployee = (Employee) fcEmployees.get(i);
                    String longHashCodeForEmployee = new Long(fcEmployee.longHashCode()).toString();
                    if (longHashCodeForEmployee.compareToIgnoreCase(hashCode) == 0) {
                        selectedEmployees.add(fcEmployee);
                    }
                }
            }
            //</editor-fold>

            // <editor-fold defaultstate="collapsed" desc="INITITATION of START-DATE and END-DATE">
            if (this.selectedEmployees.size() == 0) {
                this.canAutoriseHours = false;
            } else {
                String earliestStart = getEarliestStartDate(this.selectedEmployees);
                String latestEnd = getLatestEndDate(this.selectedEmployees);

                if ((earliestStart == null) || (latestEnd == null)) {
                    this.canAutoriseHours = false;
                } else {
                    setFromAndToDate(earliestStart, latestEnd, sFromDate, sToDate);
                }
            }
            //</editor-fold>            
        }

    }

    /**
     * Creates an instance of PageProducer_TnADailySignoff for a single employee
     *
     * @param owner
     * @param employee
     * @param sFromDate
     * @param sToDate
     * @param sessionObject
     */
    public PageProducer_TnADailySignoff(Employee owner, Employee employee, String sFromDate, String sToDate, HttpSession sessionObject) {
        this.employee = employee;
        this.owner = owner;
        this.canAutoriseHours = true;
        this.sessionObject = sessionObject;
        //this.selectionID = selectionID;

        if (this.employee == null) {
            this.canAutoriseHours = false;
        } else {
            // <editor-fold defaultstate="collapsed" desc="INITITATION of START-DATE and END-DATE">
            String earliestStart = getEarliestStartDate(this.employee);
            String latestEnd = getLatestEndDate(this.employee);

            if ((earliestStart == null) || (latestEnd == null)) {
                this.canAutoriseHours = false;
            } else {

                setFromAndToDate(earliestStart, latestEnd, sFromDate, sToDate);
            }
            //</editor-fold>            
        }
    }

    /**
     * Creates an instance of PageProducer_TnADailySignoff for a single employee
     * without the headers
     *
     * @param owner
     * @param employee
     * @param sFromDate
     * @param sToDate
     * @param skipFilters
     * @param sessionObject
     */
    public PageProducer_TnADailySignoff(Employee owner, Employee employee, String sFromDate, String sToDate, boolean skipFilters, HttpSession sessionObject) {
        this.employee = employee;
        this.owner = owner;
        this.canAutoriseHours = true;
        this.sessionObject = sessionObject;
        this.skipFilters = skipFilters;

        if (this.employee == null) {
            this.canAutoriseHours = false;
        } else {
            // <editor-fold defaultstate="collapsed" desc="INITITATION of START-DATE and END-DATE">
            String earliestStart = getEarliestStartDate(this.employee);
            String latestEnd = getLatestEndDate(this.employee);

            if ((earliestStart == null) || (latestEnd == null)) {
                this.canAutoriseHours = false;
            } else {

                setFromAndToDate(earliestStart, latestEnd, sFromDate, sToDate);
            }
            //</editor-fold>            
        }
    }

    /**
     * Given a String earliestStart and String latestEnd, performs null checks,
     * as well as correct chronological order of Start - End
     *
     * @param earliestStart - Earliest allowable Start Date
     * @param latestEnd - Latest allowable End Date
     * @param selectedStart - Start date selected by operator
     * @param selectedEnd - End date selected by operator
     */
    private void setFromAndToDate(String earliestStart, String latestEnd, String selectedStart, String selectedEnd) {
        if (selectedStart == null) {
            selectedStart = earliestStart;
        }

        if (selectedEnd == null) {
            selectedEnd = DatabaseObject.formatDate(DatabaseObject.addNDays(DatabaseObject.formatDate(selectedStart), 7));
        }

        // Correct FROM Date
        if ((DatabaseObject.formatDate(selectedStart)).before(DatabaseObject.formatDate(earliestStart))) {
            this.fromDate = earliestStart;
        } else {
            this.fromDate = selectedStart;
        }
        // Is it later than the latest end-date?
        if ((DatabaseObject.formatDate(this.fromDate)).after(DatabaseObject.formatDate(latestEnd))) {
            this.fromDate = latestEnd;
        }

        // Correct TO Date
        if ((DatabaseObject.formatDate(selectedEnd)).after(DatabaseObject.formatDate(latestEnd))) {
            this.toDate = latestEnd;
        } else {
            this.toDate = selectedEnd;
        }

        if ((DatabaseObject.formatDate(toDate)).before(DatabaseObject.formatDate(fromDate))) {
            toDate = fromDate;
        }

        if ((DatabaseObject.formatDate(fromDate)).after(DatabaseObject.formatDate(toDate))) {
            fromDate = toDate;
        }

    }

    /**
     * What is the earliest date that the person can apply for daily signoffs?
     */
    private String getEarliestStartDate(Employee employee) {
        if (employee.getPayroll() == null) {
            return null;
        } else {
            String rslt = null;
            StringBuilder sqlStatement = new StringBuilder();
            java.sql.Connection con = DatabaseObject.getNewConnectionFromPool();
            try {
                sqlStatement.append("select \"Date\"(timemod_startdate) from p_payrolldetail with (readuncommitted) where payroll_id = ").append(employee.getPayroll().getPayrollID());
                sqlStatement.append("   and payrolldetail_id = fn_P_GetOpenPeriod(").append(employee.getCompany().getCompanyID());
                sqlStatement.append("   ,p_payrolldetail.payroll_id) ");
                ResultSet rs = DatabaseObject.openSQL(sqlStatement.toString(), con);
                if (rs.next()) {
                    rslt = DatabaseObject.formatDate(rs.getDate(1));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DatabaseObject.releaseConnection(con);
            }
            return rslt;
        }
    }

    /**
     * What is the earliest date that any of these persons can apply for a daily
     * signoff?
     *
     * @param employee
     * @return
     */
    private String getEarliestStartDate(LinkedList<Employee> internalEmployeeList) {
        String earliestStart = null;
        for (int i = 0; i < internalEmployeeList.size(); i++) {
            Employee internalEmployee = internalEmployeeList.get(i);
            String earliestStartForThisEmp = getEarliestStartDate(internalEmployee);
            if ((earliestStart == null) && (earliestStartForThisEmp != null)) {
                earliestStart = earliestStartForThisEmp;
            }
            if ((earliestStart != null) && (earliestStartForThisEmp != null)) {
                if (DatabaseObject.formatDate(earliestStartForThisEmp).before(DatabaseObject.formatDate(earliestStart))) {
                    earliestStart = earliestStartForThisEmp;
                }
            }
        }

        return earliestStart;
    }

    private String getLatestEndDate(Employee employee) {
        String rslt = null;
        StringBuilder sqlStatement = new StringBuilder();
        java.sql.Connection con = DatabaseObject.getNewConnectionFromPool();
        // Is this employee linked to a payroll?
        if (employee.getPayroll() == null) {
            return null;
        } else {
            try {
                sqlStatement.append("select \"Date\"(timemod_enddate) from p_payrolldetail with (readuncommitted) where payroll_id = ").append(employee.getPayroll().getPayrollID());
                sqlStatement.append("   and payrolldetail_id = fn_P_GetOpenPeriod(").append(employee.getCompany().getCompanyID());
                sqlStatement.append("   ,p_payrolldetail.payroll_id) ");
                //System.out.println("Debug: " + sqlStatement.toString());
                ResultSet rs = DatabaseObject.openSQL(sqlStatement.toString(), con);
                if (rs.next()) {
                    // Take the payroll's end date and add an extra 16 days to it (in case the months overlap)
                    rslt = DatabaseObject.formatDate(DatabaseObject.addNDays(rs.getDate(1), 16));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                DatabaseObject.releaseConnection(con);
            }

            // If this person tries to authorise into the future, DON'T
            if (DatabaseObject.formatDate(rslt).after(new java.util.Date())) {
                rslt = DatabaseObject.formatDate(new java.util.Date());
            }

            return rslt;
        }
    }

    /**
     * What is the latest end-date that any of these persons can apply for a
     * daily signoff?
     *
     * @param employee
     * @return
     */
    private String getLatestEndDate(LinkedList<Employee> internalEmployeeList) {
        String latestEnd = null;
        for (int i = 0; i < internalEmployeeList.size(); i++) {
            Employee internalEmployee = internalEmployeeList.get(i);
            String latestEndForThisEmp = getLatestEndDate(internalEmployee);
            if ((latestEnd == null) && (latestEndForThisEmp != null)) {
                latestEnd = latestEndForThisEmp;
            }
            if ((latestEnd != null) && (latestEndForThisEmp != null)) {
                if (DatabaseObject.formatDate(latestEndForThisEmp).after(DatabaseObject.formatDate(latestEnd))) {
                    latestEnd = latestEndForThisEmp;
                }
            }

        }
        // If this person tries to authorise into the future, DON'T
        if (DatabaseObject.formatDate(latestEnd).after(new java.util.Date())) {
            latestEnd = DatabaseObject.formatDate(new java.util.Date());
        }
        return latestEnd;
    }

    private String getDataSelectionHTML() {
        //
        // Date Selection
        //
        StringBuilder resultPage = new StringBuilder();

        if (authMultipleEmployees) {
            resultPage.append("<FORM name=\"tnastats_dates\" METHOD=POST action=\"tna_signoff_req.jsp?MultiEmployee=true\"> ");
        } else if (owner != employee) {
            String forEmployeeEncrypted = new Long(employee.longHashCode()).toString();
            resultPage.append("<FORM name=\"tnastats_dates\" METHOD=POST action=\"tna_signoff_req.jsp?forEmployee=").append(forEmployeeEncrypted).append("\"> ");
        } else {
            resultPage.append("<FORM name=\"tnastats_dates\" METHOD=POST action=\"tna_signoff_req.jsp\"> ");
        }

        //resultPage.append("<table  class=\"center\" >");
        //
        // From Date and To Date Picker (START)
        //
        resultPage.append("<table width='800' align='center'><tr>");
        // From Date
        //
        resultPage.append("<td width=\"100\" align=\"right\" ><h3 class=\"shaddow\">From</h3></td>");

        resultPage.append("<td  width=\"180\"><input type=\"text\" name=\"fromDate\" placeholder=\"yyyy/mm/dd\" value=\"").append(fromDate).append("\" size=10 maxlength=10");
        resultPage.append(" onFocus=\"javascript:vDateType='2'\"");
        resultPage.append(" onKeyUp=\"DateFormat(this,this.value,event,false,'2')\" ");
        //resultPage.append(" onBlur=\"DateFormat(this,this.value,event,false,'2')\" ");
        resultPage.append(" >");

        resultPage.append(" <a class=\"hintanchor\" onMouseover=\"showhint('Select the From date', this, event, '130px')\" href=\"javascript:showCal('Calendar3')\"><img style=\"border: 0px solid ; \"  align=\"top\" alt=\"From Date\" src=\"../images/calendar.jpg\"></a></td>");
        // Separator 
        //
        resultPage.append("<td width=\"20\"></td>");
        // To Date
        //
        resultPage.append("<td width=\"60\"  align=\"right\" ><h3 class=\"shaddow\">To</h3></td>");

        resultPage.append("<td width=\"180\" ><input type=\"text\" name=\"toDate\" placeholder=\"yyyy/mm/dd\" value=\"").append(toDate).append("\" size=10 maxlength=10");
        resultPage.append(" onFocus=\"javascript:vDateType='2'\"");
        resultPage.append(" onKeyUp=\"DateFormat(this,this.value,event,false,'2')\" ");
        //resultPage.append(" onBlur=\"DateFormat(this,this.value,event,false,'2')\" ");
        resultPage.append(" >");

        resultPage.append(" <a class=\"hintanchor\" onMouseover=\"showhint('Select the To date', this, event, '130px')\" href=\"javascript:showCal('Calendar4')\"><img style=\"border: 0px solid ; \" align=\"top\"  alt=\"To Date\" src=\"../images/calendar.jpg\"></a></td>");
        resultPage.append("<td><input class=\"gradient-button-grey\" type=\"submit\" value=\"Refresh Selection\" ></td>");

        resultPage.append("</tr>");
        //
        // From Date and To Date Picker (END)
        //

        resultPage.append("</table><hr align=\"center\" width=\"1050px\"><br>");
        resultPage.append("</form>");
        return resultPage.toString();
    }

    /**
     * A LeaveInfo Page Producer
     * @return 
     */
    @Override
    public String generateInformationContent() {
        String imageLink = WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Web, "");

        if (!canAutoriseHours) {
            // <editor-fold defaultstate="collapsed" desc="Unable to authorise hours">
            if (authMultipleEmployees) {
                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                        "Unable to authorise T&A hours.<br>It is possible that these employees are not linked to any payrolls, or that the payrolls are presently not open for calculation.", true, sessionObject);
                producer.setMessageInSessionObject();
                return ("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");
            } else {
                MessageGenerator producer = new MessageGenerator(MessageGenerator.msg_ERROR,
                        "Unable to authorise T&A hours.<br>It is possible that this employee is not linked to a payroll, or that the payroll is presently not open for calculation.", true, sessionObject);
                producer.setMessageInSessionObject();
                return ("<meta http-equiv=\"refresh\" content=\"0;message_prev.jsp\"> ");
            }
            // </editor-fold>
        } else {
            StringBuilder resultPage = new StringBuilder();

            if (WebModulePreferences.getInstance().getPreference(WebModulePreferences.DISPLAY_AllowTnAHoursFor, "").length() == 0) {
                resultPage.append("<CENTER><h3 class=\"shaddow\">Daily Signoff requires the 'WebModulePreferences.DISPLAY_AllowTnAHoursFor' setting to be configured first.</h3></CENTER>");
            } else {
                if (!skipFilters) {
                    resultPage.append(getDataSelectionHTML());
                }
                //
                //
                String baseURL = WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Web, "");
                String link = baseURL + "Controller.jsp";
                resultPage.append("<FORM name=\"req_tna\" METHOD=POST action=\"").append(link).append("\"\">");

                if (authMultipleEmployees) {
                    // <editor-fold defaultstate="collapsed" desc="MULTI-EMPLOYEE SELECTION">

                    // History Table - per Leave_Type
                    String[] headers = new String[5];
                    headers[0] = "Employee";
                    headers[1] = "Day";
                    headers[2] = "First In - Last Out";
                    headers[3] = "Calculated Hours";
                    headers[4] = "Status";

                    resultPage.append("<CENTER>");
                    resultPage.append("<h3 class=\"shaddow\">Daily Signoff for ").append(fromDate).append(" - ").append(toDate).append("</h3>");
                    resultPage.append("<span class=\"standardHeading_NotBold\"> (note that you are limited to the current open payroll period)</span><br><br>");

                    resultPage.append(PageSectionGenerator.getInstance().getHTMLInnerTableTag("displaytable_wide"));
                    resultPage.append("<tr>");
                    resultPage.append("<td align=\"right\" ><input class=\"gradient-button\" type=\"submit\" value=\"Submit All Hours on Screen\" ></td>");
                    resultPage.append("</tr>");
                    resultPage.append("</table>");
                    //resultPage.append(PageSectionGenerator.getInstance().getHTMLInnerTableTag("displaytable_wide"));

                    //
                    // Load all the First-In / Last-Out records for the given period
                    // But ONLY if the system has been set up to allow certain T&A variables
                    for (int i = 0; i < selectedEmployees.size(); i++) {
                        //System.out.print("selectedEmployees.get(" + i + ")-");
                        Employee anEmp = selectedEmployees.get(i);
                        //System.out.println(anEmp);

                        int nbRecords = getClockingRecordCount(anEmp);
                        String[][] contents = new String[5][nbRecords];

                        StringBuilder sqlStatement = new StringBuilder();
                        java.sql.Connection con = DatabaseObject.getNewConnectionFromPool();
                        try {
                            sqlStatement.append("select dayname(day) as DayName, \"date\"(day) as day, ");
                            sqlStatement.append("COALESCE(datediff(minute, \"date\"(FirstIn), FirstIn),0) as  FirstIn_Num, ");
                            sqlStatement.append("right('00'||datepart(hh,FirstIn),2)||':'||right('00'||datepart(mi,FirstIn),2) as FirstIn_Displ, ");
                            sqlStatement.append("COALESCE(datediff(minute, \"date\"(LastOut), LastOut),0) as  LastOut_Num, ");
                            sqlStatement.append("right('00'||datepart(hh,LastOut),2)||':'||right('00'||datepart(mi,LastOut),2) as LastOut_Displ, ");
                            sqlStatement.append("COALESCE(TimeAtWork_ActualMinutes,0) as TimeAtWork_ActualMinutes, TimeAtWork_DisplayValue ");
                            sqlStatement.append("from vw_TA_Report_FirstInLastOut vw with (readuncommitted)  ");
                            sqlStatement.append("where Company_id=").append(anEmp.getCompany().getCompanyID());
                            sqlStatement.append(" and person_id in ");
                            sqlStatement.append("    (select person_id from ta_e_master with (readuncommitted) where ");
                            sqlStatement.append("     company_id=vw.company_id and Employee_id=").append(anEmp.getEmployeeID()).append(") ");
                            sqlStatement.append("and day between '").append(fromDate).append("' and '").append(toDate).append("' ");
                            sqlStatement.append("order by day ");
                            ResultSet rs = DatabaseObject.openSQL(sqlStatement.toString(), con);
                            int row = 0;
                            while (rs.next()) {
                                boolean isSignedOff = isSignedOff(anEmp, rs.getDate("day"));
                                WebProcess_DailySignoff alreadyLoggedProcess = getAlreadyLoggedProcess(anEmp, rs.getDate("day"));

                                contents[0][row] = anEmp.toString();
                                contents[1][row] = rs.getDate("day") + " (" + rs.getString("DayName") + ")";
                                contents[2][row] = "<a href=\"../processes/tna_signoff_req_detail.jsp?forEmployee=" + anEmp.longHashCode() + "&detailDate=" + rs.getString("day").replaceAll("-", "/") + "\">" + rs.getString("FirstIn_Displ") + " - " + rs.getString("LastOut_Displ") + "</a>";
                                if (getEarliestStartDate(anEmp) == null) {
                                    contents[3][row] = "<table border=0 width=100% align=\"left\"><tr><td class=\"tinyFont\">Not linked to a payroll or open period</td></tr></table>";
                                    contents[4][row] = "";
                                } else {
                                    contents[3][row] = getCalculationBreakdown(anEmp, rs.getDate("day"), isSignedOff, alreadyLoggedProcess);
                                    // The hours were signed off
                                    if (isSignedOff) {
                                        contents[4][row] = "<a class=\"hintanchor\" onMouseover=\"showhint('These hours have been signed-off and cannot be changed.', this, event, '250px')\">"
                                                + "<img  style=\"border: 0px\" alt=\"Hours already submitted\" src=\"" + imageLink + "images/note.png\"></a>";
                                    } // The hours are not signed-off yet, but a process already exists for it
                                    else if (alreadyLoggedProcess != null) {
                                        contents[4][row] = "<a class=\"hintanchor\" onMouseover=\"showhint('These hours have already been authorised but still needs someone else to approve it before it will be signed-off.', this, event, '250px')\">"
                                                + "<img  style=\"border: 0px\" alt=\"Hours already submitted\" src=\"" + imageLink + "images/information.gif\"></a>";
                                    } else {
                                        contents[4][row] = "";
                                    }
                                }
                                row++;
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        } finally {
                            DatabaseObject.releaseConnection(con);
                        }
                        resultPage.append(PageSectionGenerator.getInstance().buildHTMLTableOuter("displaytable_wide", headers, contents));
                    }

                    // </editor-fold>
                } else {
                    // <editor-fold defaultstate="collapsed" desc="SINGLE-EMPLOYEE SELECTION">

                    // History Table - per Leave_Type
                    String[] headers = new String[3];
                    headers[0] = "Day";
                    headers[1] = "First In - Last Out";
                    headers[2] = "Calculated Hours";

                    resultPage.append("<CENTER>");
                    if (!skipFilters) {
                        resultPage.append("<h3 class=\"shaddow\">Daily Signoff for ").append(fromDate).append(" - ").append(toDate).append("</h3>");
                        resultPage.append("<span class=\"standardHeading_NotBold\"> (note that you are limited to the current open payroll period)</span><br><br>");
                    }

                    int nbRecords = getClockingRecordCount(this.employee);
                    String[][] contents = new String[3][nbRecords];

                    //
                    // Load all the First-In / Last-Out records for the given period
                    // But ONLY if the system has been set up to allow certain T&A variables
                    StringBuilder sqlStatement = new StringBuilder();
                    java.sql.Connection con = DatabaseObject.getNewConnectionFromPool();
                    try {
                        sqlStatement.append("select dayname(day) as DayName, \"date\"(day) as day, ");
                        sqlStatement.append("COALESCE(datediff(minute, \"date\"(FirstIn), FirstIn),0) as  FirstIn_Num, ");
                        sqlStatement.append("right('00'||datepart(hh,FirstIn),2)||':'||right('00'||datepart(mi,FirstIn),2) as FirstIn_Displ, ");
                        sqlStatement.append("COALESCE(datediff(minute, \"date\"(LastOut), LastOut),0) as  LastOut_Num, ");
                        sqlStatement.append("right('00'||datepart(hh,LastOut),2)||':'||right('00'||datepart(mi,LastOut),2) as LastOut_Displ, ");
                        sqlStatement.append("COALESCE(TimeAtWork_ActualMinutes,0) as TimeAtWork_ActualMinutes, TimeAtWork_DisplayValue ");
                        sqlStatement.append("from vw_TA_Report_FirstInLastOut vw with (readuncommitted)  ");
                        sqlStatement.append("where Company_id=").append(employee.getCompany().getCompanyID());
                        sqlStatement.append(" and person_id in ");
                        sqlStatement.append("    (select person_id from ta_e_master with (readuncommitted) where ");
                        sqlStatement.append("     company_id=vw.company_id and Employee_id=").append(employee.getEmployeeID()).append(") ");
                        sqlStatement.append("and day between '").append(fromDate).append("' and '").append(toDate).append("' ");
                        sqlStatement.append("order by day ");
                        ResultSet rs = DatabaseObject.openSQL(sqlStatement.toString(), con);
                        int row = 0;
                        while (rs.next()) {
                            contents[0][row] = rs.getDate("day") + " (" + rs.getString("DayName") + ")";
                            contents[1][row] = "<a href=\"../processes/tna_signoff_req_detail.jsp?forEmployee=" + this.employee.longHashCode() + "&detailDate=" + rs.getString("day").replaceAll("-", "/") + "\">" + rs.getString("FirstIn_Displ") + " - " + rs.getString("LastOut_Displ") + "</a>";
                            contents[2][row] = getCalculationBreakdown(this.employee, rs.getDate("day"), isSignedOff(this.employee, rs.getDate("day")), getAlreadyLoggedProcess(this.employee, rs.getDate("day")));
                            row++;
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    } finally {
                        DatabaseObject.releaseConnection(con);
                    }

                    resultPage.append(PageSectionGenerator.getInstance().buildHTMLTableOuter("displaytable_wide", headers, contents));
                    resultPage.append("</CENTER>");
//</editor-fold>
                }

                // SUBMIT
                {
                    resultPage.append("<!-- Horizontal Ruler -->");
                    resultPage.append("<hr align=\"center\" width=\"1025px\">");
                    resultPage.append("<center><table class=\"center\" style=\"width: 1050px;\">");
                    resultPage.append("<tr>");
                    if (skipFilters) {
                        resultPage.append("<td align=\"left\"><button  class=\"gradient-button\" style=\"width: 80px; float: none; margin-left: 20px;\" onclick=\"back();\">Back</button></td>");
                    }
                    // Hide the submit button if there is nothing to submit
                    if (skipFilters) {
                        if (!isSignedOff(this.employee, DatabaseObject.formatDate(fromDate))) {
                            if (getAlreadyLoggedProcess(this.employee, DatabaseObject.formatDate(fromDate)) == null) {
                                resultPage.append("<td align=\"right\" ><input class=\"gradient-button\" type=\"submit\" value=\"Submit All Hours on Screen\" ></td>");
                            }
                        }
                    } else if (!skipFilters) {
                        resultPage.append("<td align=\"right\" ><input class=\"gradient-button\" type=\"submit\" value=\"Submit All Hours on Screen\" ></td>");
                    }
                    resultPage.append("<input type=\"hidden\" name=\"action\" value=\"req_tna\">");
                    //resultPage.append("<input type=\"hidden\" name=\"selSelection\" value=\"").append(selectionID).append("\">");
                    resultPage.append("</tr>");
                    resultPage.append("</table></center>");
                }
                resultPage.append("</form>");
            }

            return resultPage.toString();
        }
    }

    private int getClockingRecordCount(Employee anEmployee) {
        int rslt = 0;
        StringBuilder sqlStatement = new StringBuilder();
        java.sql.Connection con = DatabaseObject.getNewConnectionFromPool();
        try {
            sqlStatement.append("select count(*) ");
            sqlStatement.append("from vw_TA_Report_FirstInLastOut vw with (readuncommitted)  ");
            sqlStatement.append("where Company_id=").append(anEmployee.getCompany().getCompanyID());
            sqlStatement.append(" and person_id in ");
            sqlStatement.append("    (select person_id from ta_e_master with (readuncommitted) where ");
            sqlStatement.append("     company_id=vw.company_id and Employee_id=").append(anEmployee.getEmployeeID()).append(") ");
            sqlStatement.append("and day between '").append(fromDate).append("' and '").append(toDate).append("' ");
            ResultSet rs = DatabaseObject.openSQL(sqlStatement.toString(), con);
            if (rs.next()) {
                rslt = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return rslt;
    }

    private boolean isSignedOff(Employee anEmployee, java.util.Date aDay) {
        StringBuilder sqlStatement = new StringBuilder();
        String signedOff = null;
        java.sql.Connection con2 = DatabaseObject.getNewConnectionFromPool();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String aDayString = df.format(aDay);
        try {
            sqlStatement.append("select first SIGNEDOFF_YN from DBA.TA_CALC_EMPLOYEELIST_DAILY where DAY = '").append(aDayString).append("'");
            sqlStatement.append(" and Company_id=").append(anEmployee.getCompany().getCompanyID());
            sqlStatement.append(" and Employee_id=").append(anEmployee.getEmployeeID());
            ResultSet rs = DatabaseObject.openSQL(sqlStatement.toString(), con2);

            // Context sensitive content, to be added into the HTML later
            if (rs.next()) {
                signedOff = rs.getString("SIGNEDOFF_YN");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con2);
        }
        if ((signedOff != null) && (signedOff.compareToIgnoreCase("Y") == 0)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Scans the 'Live' process list to see if hours for this day has already
     * been logged. We cannot allow the person to authorise hours for a day if
     * he/she has already done so.
     *
     * @param anEmployee
     * @param aDay
     * @return
     */
    private WebProcess_DailySignoff getAlreadyLoggedProcess(Employee anEmployee, java.util.Date aDay) {
        boolean rslt = false;
        TreeMap webProcesses = FileContainer.getInstance().getWebProcessesInitiatedBy(owner, true);
        Iterator iter = webProcesses.values().iterator();
        while (iter.hasNext()) {
            WebProcess process = (WebProcess) iter.next();
            if (process.getProcessDefinition().getProcessClassName().compareToIgnoreCase("za.co.ucs.accsys.webmodule.WebProcess_DailySignoff") == 0) {
                Employee processEmployee = process.getEmployee();
                if (processEmployee.equals(anEmployee)) {
                    WebProcess_DailySignoff aProc = (WebProcess_DailySignoff) process;
                    if (DatabaseObject.formatDate(aProc.getAuthDay()).equals(aDay)) {
                        if (aProc.isActive()) {
                            return aProc;
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * This is the MONSTER of this page. Depending on the status of that day,
     * different inner-table content is generated
     *
     * @param anEmployee
     * @param aDay
     * @param isSignedOff
     * @param loggedProcess
     * @return
     */
    private String getCalculationBreakdown(Employee anEmployee, java.util.Date aDay, boolean isSignedOff, WebProcess_DailySignoff loggedProcess) {
        StringBuilder sqlStatement = new StringBuilder();
        StringBuilder rslt = new StringBuilder();
        String imageLink = WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Web, "");

        if ((!isSignedOff) && (loggedProcess != null)) {
            StringBuilder loggedDetail = new StringBuilder();
            // <editor-fold defaultstate="collapsed" desc="LOGGED AND APPROVED">
            //
            // Header 
            //
            loggedDetail.append("<table border=0 width=100% align=\"left\">");
            loggedDetail.append("  <tr><td width=\"100%\" class=\"tinyFont_bold\"></td></tr> ");
            loggedDetail.append("  <tr><td>").append(loggedProcess.toShortHTMLString()).append("</td>");
            loggedDetail.append("  </tr></table>");
            return loggedDetail.toString();
            // </editor-fold>
        }

        // <editor-fold defaultstate="collapsed" desc="OPEN / NOT YET LOGGED">
        java.sql.Connection con = DatabaseObject.getNewConnectionFromPool();
        try {
            String variableList = WebModulePreferences.getInstance().getPreference(WebModulePreferences.DISPLAY_AllowTnAHoursFor, "");
            // <editor-fold defaultstate="collapsed" desc="SELECT VARIABLES TO DISPLAY">
            if (variableList.length() > 0) {

                // We need to build a list of variable names that can be called from the SQL's ... in (  ) clause
                StringBuilder tokenisedList = new StringBuilder();
                StringTokenizer token = new StringTokenizer(variableList, ",");
                while (token.hasMoreElements()) {
                    if (tokenisedList.length() == 0) {
                        tokenisedList.append("'").append(token.nextToken().trim()).append("'");
                    } else {
                        tokenisedList.append(",'").append(token.nextToken().trim()).append("'");
                    }
                }
                sqlStatement.append("select v.variable_id, v.name as theVariable, e.company_id, e.employee_id, s.day, ");
                sqlStatement.append("round(COALESCE((select sum(ta.theValue) from DBA.tal_emaster_pvariable_daily as ta where ta.company_id = e.company_id and ta.employee_id = e.employee_id ");
                sqlStatement.append("and ta.Day = s.Day and ta.variable_id = v.variable_id)*60,0),0) as MinutesAfterSCL, ");
                sqlStatement.append("right('00'||convert(integer,MinutesAfterSCL/60),2)||':'||right('00'||convert(integer,remainder(MinutesAfterSCL,60)),2) as HHMM_CALC, ");
                sqlStatement.append("round(COALESCE((select sum(ta.FINAL_VALUE) from DBA.tal_emaster_pvariable_daily as ta where ta.company_id = e.company_id and ta.employee_id = e.employee_id ");
                sqlStatement.append("and ta.Day = s.Day and ta.variable_id = v.variable_id)*60,0),0) as MinutesAfterSignoff, ");
                sqlStatement.append("right('00'||convert(integer,MinutesAfterSignoff/60),2)||':'||right('00'||convert(integer,remainder(MinutesAfterSignoff,60)),2) as HHMM_SO ");

                sqlStatement.append(" from p_variable v, e_master e, S_CALENDAR_DAYs s with (readuncommitted) ");
                sqlStatement.append(" where s.day='").append(aDay).append("'");
                sqlStatement.append(" and v.name in  (").append(tokenisedList.toString()).append(") ");
                sqlStatement.append(" and e.Company_id=").append(anEmployee.getCompany().getCompanyID());
                sqlStatement.append(" and e.Employee_id=").append(anEmployee.getEmployeeID());
                sqlStatement.append(" order by v.name");

                ResultSet rs = DatabaseObject.openSQL(sqlStatement.toString(), con);
                StringBuilder pageContent = new StringBuilder();

                if (isSignedOff) {
                    // <editor-fold defaultstate="collapsed" desc="SIGNED-OFF">
                    pageContent.append("<table id=\"displaytable_inner_italics\" >");
                    //pageContent.append("  <tr><td width=\"100%\" class=\"tinyFont_bold\"></td><td width=\"30\" align=\"right\"></td></tr> ");
                    pageContent.append("  <tr><td>");
                    pageContent.append("          <table  id=\"displaytable_inner_italics\"><font size='-3'>");
                    pageContent.append("             <tr><th>Variable</th><th>Signed Off</th></tr> ");
                    while (rs.next()) {
                        if (rs.getString("HHMM_SO").compareTo("00:00") != 0) {
                            pageContent.append("         <tr><td align=\"left\">").append(rs.getString("theVariable")).append("</td>");
                            pageContent.append("             <td align=\"left\">").append(rs.getString("HHMM_SO")).append("</td>");
                            pageContent.append("             </tr>");
                        }
                    }
                    pageContent.append("          </table></td></tr>");

                    // </editor-fold>
                } else {
                    // <editor-fold defaultstate="collapsed" desc="NOT SIGNED-OFF">

                    pageContent.append("<div class=\"grouping\" style=\"display: table; border: none;\"><table id=\"displaytable_inner\" width=100% align=\"left\">");
                    //pageContent.append("  <tr><td width=\"100%\" class=\"tinyFont_bold\"></td><td width=\"30\" align=\"right\"></td></tr> ");
                    pageContent.append("  <tr><td>");
                    pageContent.append("          <table id=\"displaytable_inner\"><font size='-3'>");
                    pageContent.append("             <tr><th>Variable</th><th>Calculated</th><th>Modified</th><th>Reason</th></tr> ");

                    //
                    // Does the client use a User Defined Field to define the drop-down list of reasons, or is it text?
                    //
                    String userDefinedFieldName = WebModulePreferences.getInstance().getPreference(WebModulePreferences.DISPLAY_UserDefinedFieldForTnAReasons, "");
                    LinkedList userDefReasons = new LinkedList();
                    StringBuilder userDefinedHTMLOptions = new StringBuilder();
                    if (userDefinedFieldName.trim().length() > 0) {
                        // Add Reasons from the User Defined Fields
                        //
                        userDefReasons = FileContainer.getInstance().getUserDefinedFieldItems(userDefinedFieldName.trim());
                        Iterator iter = userDefReasons.iterator();
                        while (iter.hasNext()) {
                            String aReason = (String) iter.next();
                            userDefinedHTMLOptions.append("<option VALUE=\"").append(aReason).append("\" >").append(aReason).append("</option>");
                        }
                    }

                    while (rs.next()) {
                        pageContent.append("  <tr>");
                        pageContent.append("    <td class=\"smallFont\">").append(rs.getString("theVariable")).append("</td>");
                        pageContent.append("    <td class=\"smallFont\">").append(rs.getString("HHMM_CALC")).append("</td>");

                        // Construct a field that we can decipher again later
                        // consisting of company_id, employee_id, day, variable_id, old_value 
                        String subsetHrs = rs.getString("HHMM_CALC").substring(0, 2);
                        String subsetMins = rs.getString("HHMM_CALC").substring(3, 5);

                        String fieldValueHrsName = "HRS:" + rs.getString("company_id") + "," + rs.getString("employee_id") + "," + aDay + "," + rs.getString("variable_id") + "," + rs.getString("HHMM_CALC").substring(0, 5);
                        String fieldValueMinsName = "MNS:" + rs.getString("company_id") + "," + rs.getString("employee_id") + "," + aDay + "," + rs.getString("variable_id") + "," + rs.getString("HHMM_CALC").substring(0, 5);
                        String fieldReasonName = "RSN:" + rs.getString("company_id") + "," + rs.getString("employee_id") + "," + aDay + "," + rs.getString("variable_id") + "," + rs.getString("HHMM_CALC");

                        pageContent.append("<td>");
                        pageContent.append("<input maxlength=\"2\" size=\"2\"  name=\"").append(fieldValueHrsName).append("\" value=\"").append(subsetHrs).append("\">");
                        pageContent.append(":");
                        pageContent.append("<input maxlength=\"2\" size=\"2\"  name=\"").append(fieldValueMinsName).append("\" value=\"").append(subsetMins).append("\">");
                        pageContent.append("</td>");

                        // TEST reason, or a drop-down list based on User Defined Fields?
                        if ((userDefinedFieldName.trim().length() > 0) && (userDefReasons.size() > 0)) {
                            pageContent.append("   <td> <select name=\"").append(fieldReasonName).append("\" >").append(userDefinedHTMLOptions).append(" </select></td>");
                        } else {
                            pageContent.append("   <td> <input maxlength=\"100\" size=\"40\"  name=\"").append(fieldReasonName).append("\" > </td>");
                        }
                        pageContent.append("  </tr>");
                        // </editor-fold>

                    }
                    pageContent.append("          </table></td></tr>");

                }
                rslt.append(pageContent.toString()).append("</font></table></div>");

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseObject.releaseConnection(con);
        }
        return rslt.toString();
        // </editor-fold>

    }

    /**
     * @return the canAutoriseHours
     */
    public boolean canAutoriseHours() {
        return canAutoriseHours;
    }
    // <editor-fold defaultstate="collapsed" desc="PRIVATE DECLARATIONS">
    private Employee employee = null;
    private Employee owner = null;
    private String fromDate;
    private String toDate;
    private boolean canAutoriseHours = true;
    private final HttpSession sessionObject;
    private boolean skipFilters = false;
    private LinkedList<Employee> selectedEmployees = null;
    private boolean authMultipleEmployees = false;
    //private String selectionID;
// </editor-fold>
} // end PageProducer_TnADailySignoff

