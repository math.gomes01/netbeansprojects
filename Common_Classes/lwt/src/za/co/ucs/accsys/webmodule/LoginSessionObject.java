/*
 * SessionObject.java
 *
 * Created on May 18, 2004, 8:57 AM
 */

package za.co.ucs.accsys.webmodule;
import za.co.ucs.accsys.peopleware.*;

/**
 * A SessionObject is used in the Web Application to identify who the person is that
 * is currently logged in, and on which person he/she is trying to work.
 * In a supervisor-employee environment, one might find that the supervisor
 * should have access to employee-information of those people that reports to him/her.
 * <br>
 * During a Web session, this combination will be stored as a session cookie for the 
 * duration of the session.
 * @author  liam
 */
public class LoginSessionObject implements java.io.Serializable {
    
    /** Creates a new instance of LoginSessionObject,
     * @param user the person logged into the WebModule
     * @param employee the person whos information will be viewed/modified
     */
    public LoginSessionObject(Employee user, Employee employee) {
        this.user = user;
        this.employee = employee;
    }
    
    public Employee getUser(){
        return user;
    }
    
    public Employee getEmployee(){
        return employee;
    }
    
    public void setEmployee(Employee employee){
        this.employee = employee;
    }
    
    private final Employee user;
    private Employee employee;
}
