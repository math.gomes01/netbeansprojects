package za.co.ucs.ess;

import za.co.ucs.accsys.webmodule.LoginSessionObject;
import za.co.ucs.accsys.webmodule.WebModulePreferences;
import za.co.ucs.accsys.webmodule.WebProcess;
import za.co.ucs.accsys.webmodule.WebProcessStage;
import za.co.ucs.accsys.webmodule.WebProcess_LeaveRequisition;

/**
 * <p>
 * PageProducer_ProcessPreview inherits from PageProducer. It is used to display
 * the detail - including history - of a given WebProcess
 * </p>
 */
public class PageProducer_ProcessPreview extends PageProducer {

    /**
     * Constructor
     *
     * @param webProcess The Process that needs previewing
     * @param loginSessionObject The object created by loggin in to the Web
     * Module
     */
    public PageProducer_ProcessPreview(WebProcess webProcess, LoginSessionObject loginSessionObject) {
        //System.out.println("PageProducer_ProcessPreview - constructor");
        this.webProcess = webProcess;
        this.loginSessionObject = loginSessionObject;
    }

    /**
     * A ProcessPreview Page Producer
     *
     * @return
     */
    @Override
    public String generateInformationContent() {
        //System.out.println("PageProducer_ProcessPreview - generateInformationContent");

        if (webProcess == null) {
            return "";
        }

        StringBuilder resultPage = new StringBuilder();

        // Overview Table
        String[] headers = {"Type", "Logged By", "On Behalf Of", "Logged On", "Detail"};
        String[][] contents = new String[5][1];
        // construct the contents matrix
        contents[0][0] = webProcess.getProcessName();
        contents[1][0] = webProcess.getLogger().toString();
        contents[2][0] = webProcess.getEmployee().toString();
        contents[3][0] = webProcess.formatDate(webProcess.getCreationDate());
        contents[4][0] = webProcess.toHTMLString();
        resultPage.append(PageSectionGenerator.getInstance().buildHTMLTable("displaytable_wide", headers, contents));

        // Detail Table - per WebProcessStage
        String[] detailHeader = {"Created", "Responsibility", "Action", "Reason", "Action Date"};
        String[][] processHistoryDetail = new String[5][webProcess.getProcessHistory().getProcessStages().size() + 1];
        resultPage.append("<br>");
        resultPage.append(PageSectionGenerator.getInstance().getHTMLOuterTableTag());
        resultPage.append("<tr><td><center><h3 class=\"shaddow\">Process History</h3></font></center></td></tr>");

        // Put the detail inside it's own table
        for (int i = 0; i < webProcess.getProcessHistory().getProcessStages().size(); i++) {
            WebProcessStage stage = (WebProcessStage) webProcess.getProcessHistory().getProcessStages().get(i);
            processHistoryDetail[0][i] = webProcess.formatDate(stage.getCreationDate());
            processHistoryDetail[1][i] = stage.getOwner().toString();
            processHistoryDetail[2][i] = stage.getStageAction();
            if (webProcess.getProcessName().compareToIgnoreCase("Leave Request") == 0 && i == 0) {
                WebProcess_LeaveRequisition currentWebProcess_LeaveRequisition = (WebProcess_LeaveRequisition) webProcess;
                if (currentWebProcess_LeaveRequisition.additionalDetail.compareToIgnoreCase("") != 0) {
                    processHistoryDetail[3][i] = stage.getStageReason() + ": " + currentWebProcess_LeaveRequisition.additionalDetail;
                }
                else {
                    processHistoryDetail[3][i] = stage.getStageReason();
                }
            } else {
                processHistoryDetail[3][i] = stage.getStageReason();
            }
            processHistoryDetail[4][i] = webProcess.formatDate(stage.getActionDate());
        }

        // Add the current stage to the list
        WebProcessStage currentStage = (WebProcessStage) webProcess.getCurrentStage();
        processHistoryDetail[0][webProcess.getProcessHistory().getProcessStages().size()] = webProcess.formatDate(currentStage.getCreationDate());
        processHistoryDetail[1][webProcess.getProcessHistory().getProcessStages().size()] = currentStage.getOwner().toString();
        processHistoryDetail[2][webProcess.getProcessHistory().getProcessStages().size()] = currentStage.getStageAction();
        processHistoryDetail[3][webProcess.getProcessHistory().getProcessStages().size()] = currentStage.getStageReason();
        processHistoryDetail[4][webProcess.getProcessHistory().getProcessStages().size()] = webProcess.formatDate(currentStage.getActionDate());

        resultPage.append("<tr><td>").append(PageSectionGenerator.getInstance().buildHTMLTable("displaytable_wide", detailHeader, processHistoryDetail)).append("</td></tr>");
        resultPage.append("</table>");

        // Finally, we add the option to cancel the process IF the user that is currently logged in (loginSessionObject.getUser())
        // is the person who logged the Process in the first place (process.getLogger())
        // Cancel Process
        if ((webProcess.isActive()) && (loginSessionObject.getUser().equals(webProcess.getLogger()))) {
            String baseURL = WebModulePreferences.getInstance().getPreference(WebModulePreferences.URL_Web, "");
            String link = "\"" + baseURL + "processes/proc_cancel.jsp? ";

            // Java Script Function
            resultPage.append("<script language=\"javascript\">");
            resultPage.append("function verify(){");
            resultPage.append("    msg = 'Are you sure you want to cancel this Web Process?';");
            resultPage.append("    return confirm(msg);");
            resultPage.append("    }");
            resultPage.append("</SCRIPT>");
            resultPage.append("<CENTER>");
            resultPage.append("<FORM ACTION=").append(link).append("method=\"post\" onSubmit=\"return confirm('Are you sure?');\">");
            resultPage.append("   <INPUT class=\"gradient-button\" style=\"float: none;\" type=\"submit\" value=\"Cancel Web Process\">");
            resultPage.append("   <input type=\"hidden\" name=\"hashValue\" value=\"").append(webProcess.hashCode()).append("\">");
            resultPage.append("</FORM>");
            resultPage.append("</CENTER>");
        }
        return resultPage.toString();
    }
    private final WebProcess webProcess;
    private final LoginSessionObject loginSessionObject;
} // end LeavePageProducer

