/*
 * Loan.java
 *
 * Created on July 15, 2004, 12:34 PM
 */

package za.co.ucs.accsys.peopleware;
import java.sql.*;
import za.co.ucs.lwt.db.*;

/**
 * Variable contains the detail of an entry in P_VARIABLE
 * @author  liam
 */
public final class Variable implements AccsysReadOnlyObjectInterface, java.io.Serializable {
    
    /** Creates a new instance of Variable
     * @param variableID
     */
    public Variable(String variableID) {
        this.variableID = variableID;
        if (isValid()) {
            loadFromDB();
        }
    }
    
    @Override
    public void loadFromDB() {
        Connection con = null;
        try{
            con = DatabaseObject.getNewConnectionFromPool();
            //
            // P_VARIABLE
            //
            ResultSet rs = DatabaseObject.openSQL("select * from p_variable with (nolock) where "+
            "variable_id="+variableID, con);
            if (rs.next()){
                name = rs.getString("NAME");
                description = rs.getString("DESCRIPTION");
                activeForBasicRun = (rs.getString("ACTIVE_FOR").toUpperCase().indexOf("BASIC RUN")>=0);
            }
            rs.close();
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        finally{
            DatabaseObject.releaseConnection(con);
        }
    }
    
    @Override
    public boolean isValid() {
        return (true);
    }
    
    /** Getter for property name.
     * @return Value of property name.
     *
     */
    public java.lang.String getName() {
        return name;
    }    
    
    
    /** Getter for property description.
     * @return Value of property description.
     *
     */
    public java.lang.String getDescription() {
        return description;
    }

    
    /** Getter for property activeForBasicRun.
     * @return Value of property activeForBasicRun.
     *
     */
    public boolean isActiveForBasicRun() {
        return activeForBasicRun;
    }
    
    /** Getter for property variableID.
     * @return Value of property variableID.
     *
     */
    public String getVariableID() {
        return variableID;
    }    
    
    /** Returns the value of this variable, should it be calculated by
     * the payroll, right now.
     * @param employee Employee in question
     * @param payrollDetailID period in which calculation should take place
     * @param runtypeID runtype
     * @return 
     */
    public float getValue(Employee employee, String payrollDetailID, String runtypeID) {
        Connection con = null;
        float result = 0;
        try{
            con = DatabaseObject.getNewConnectionFromPool();
            //
            // P_VARIABLE
            //
            ResultSet rs = DatabaseObject.openSQL("select sp_P_CALCExtractVariableValue("+
                employee.getCompany().getCompanyID()+", "+
                employee.getEmployeeID()+", "+
                this.getVariableID()+", "+
                employee.getPayroll().getPayrollID()+", "+
                payrollDetailID+", "+runtypeID+")", con);
            if (rs.next()){
                result = rs.getFloat(1);
            }
            rs.close();
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        finally{
            DatabaseObject.releaseConnection(con);
        }
        return result;
    }
    
    private final String variableID;
    private String name;
    private String description;
    private boolean activeForBasicRun;
    
}

