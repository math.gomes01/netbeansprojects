package za.co.ucs.ess;

import java.util.*;
import za.co.ucs.accsys.peopleware.*;

/**
 * <p>
 * PageProducer_FamilyInfo inherits from PageProducer and will display all the
 * family members of a given employee in HTML
 * </p>
 */
public class PageProducer_FamilyInfo extends PageProducer{
    
    /** Constructor
     */
    public PageProducer_FamilyInfo(Employee employee){
        this.employee = employee;
    }
    
    /** A LeaveInfo Page Producer
     */
    @Override
    public String generateInformationContent() {
        
        StringBuilder resultPage = new StringBuilder();
        LinkedList familyMembers = this.employee.getFamilyMembers();
        
        // Overview Table
        String[] headers = {"Relation","Name","Surname","Birth Date", "Tel Home", "Tel Work", "Cell", "E-mail", "Medical Aid Dependent"};
        String[][] contents = new String[9][familyMembers.size()];
        // construct the contents matrix
        for (int row=0;row<familyMembers.size();row++){
            Family family = (Family)familyMembers.get(row);
            // Detail
            contents[0][row] = family.getRelation();
            contents[1][row] = family.getFirstName();
            contents[2][row] = family.getSurname();
            contents[3][row] = family.getBirthDate();
            contents[4][row] = family.getTelHome();
            contents[5][row] = family.getTelWork();
            contents[6][row] = family.getTelCell();
            contents[7][row] = family.getEMail();
            if (family.isMedDependent()){
            contents[8][row] = "Y";}
            else{
            contents[8][row] = "N";}
                
        }
        resultPage.append(PageSectionGenerator.getInstance().buildHTMLTable(headers, contents));
        
        return resultPage.toString();
    }
    
    private Employee employee;
} // end LeavePageProducer



