

package za.co.ucs.accsys.webmodule;
import za.co.ucs.accsys.peopleware.*;


/**
 * The EmployeeLocator is a Singleton used to extract an Employee
 * from the database, given a user_id and password.
 * It is important to state that both the user_id and
 * password will be encrypted in the database.
 * @author  liam
 */
public class EmployeeLocator {
    
    /** Creates a new instance of EmployeeLocator */
    public EmployeeLocator() {
    }
    
    public static EmployeeLocator getInstance(){
        if (employeeLocator == null){
            employeeLocator = new EmployeeLocator();
        }
        return employeeLocator;
    }
    
    public Employee getEmployee(String loginName, String password){
        FileContainer fc = FileContainer.getInstance();
        Employee employee = FileContainer.getEmployeeFromContainer(loginName, password);
            return employee;
    }
    
    private static EmployeeLocator employeeLocator = null;
}
